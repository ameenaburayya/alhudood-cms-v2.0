import Custom404 from "../components/layouts/custom404";
import Footer from "../includes/footer/footer";
import Header from "../includes/header/header";
import React from 'react'
import PageLayout from "../components/layouts/pageLayout";


const fourOhFours = [
    {
        image: '/Assets/404_Images/404 laughing.png',
        text: ' لقد تم حذف المقال لأنه كان مضحكاً بشكل هستيري و أودى بحياة الكثيرين'
    },
    {
        image: '/Assets/404_Images/404-breaking.png',
        text: 'هذه الصفحة غير موجودة، جرّب أن تخبط الشاشة قليلاً عليها كي تظهر'
    },
    {
        image: '/Assets/404_Images/404-hanging.png',
        text: 'المقال غير موجود، جاري إعدام مسؤول الدعم الفني للموقع'
    },
    {
        image: '/Assets/404_Images/404-peeing.png',
        text: 'كيف استطعت الوصول إلى هذه الصفحة؟ الرجاء الخروج لو سمحت، عيب عليك'
    },
]

export default class FourOhFour extends React.Component {

    state = {
        error: null
    }


    componentDidMount(){
        const fourOhFourError = fourOhFours[Math.floor(Math.random() * fourOhFours.length)]
        this.setState({error: fourOhFourError })
    }

    render(){
        return (
            <>
                <Header />
                <PageLayout pageTitle="خطأ ٤٠٤">
                    <Custom404 error={this.state.error} />
                </PageLayout>
                <Footer />
            </>
        )
    }
}