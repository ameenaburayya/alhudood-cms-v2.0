import React from "react"
import PageTitle from "../components/headings/pageTitle"
import PageLayout from "../components/layouts/pageLayout"
import Footer from "../includes/footer/footer"
import Header from "../includes/header/header"
import styles from "../scss/pages/supporters.module.scss"
import Link from "next/link"

export default function Supporters() {
    return (
        <>
            <Header />
            <PageLayout smallWidth className={styles["page-layout"]} pageTitle="الداعمون">
                <div className={`${styles["supporters-page"]}`}>
                    <PageTitle>الداعمون</PageTitle>
                    <div className={styles["content"]}>
                        <div className={styles["block"]}>
                            <p>
                                الحدود مؤسسة إعلامية مستقلة غير مرتبطة بأي حزب أو جهة سياسية، وتستقبل الدعم من مؤسسات
                                وأشخاص وأعضاء يحترمون جميعاً الاستقلالية التحريرية للمؤسسة ولا يشترطون التدخل بها
                                (يمكنكم قراءة السياسة التحريرية للحدود <Link href="/editorial-policy">هنا</Link>).
                            </p>
                        </div>
                        <div className={styles["block"]}>
                            <p>
                                ولضمان استمرارية المؤسسة واستقلاليتها؛ تعمل الحدود على أن تكون مستقلة مادياً من خلال عدة
                                مصادر لاستدامة الدخل، من بينها إطلاق برنامج العضوية الذي يضم آلاف الأشخاص ممن يتشاركون
                                مع شبكة الحدود أهدافها، ينخرطون فيما نعمل عليه من خلال “مجتمع الحدود” وعبر فعالياتها
                                التي تنظّم على أرض الواقع أو المنصات الرقمية.{" "}
                            </p>
                        </div>
                        <div className={styles["block"]}>
                            <p>
                                تقدّم الحدود، كمؤسسة، عدداً من الخدمات الأخرى التي تساعدها على تغطية تكاليفها: مثل إنتاج
                                محتوى لمؤسسات تتشارك معها في الدفاع عن القضايا ذاتها، إضافة لقسم التطوير والبرمجة الذي
                                يقوم بإدارة وتطوير مواقع الحدود الإلكترونية إضافة لتقديم الخدمات البرمجية للمؤسسات
                                الإعلامية ومنظمات المجتمع المدني.{" "}
                            </p>
                        </div>
                        <div className={styles["block"]}>
                            <p>
                                إلى حين بلوغ هذه الاستقلالية المالية، تتلقى الحدود دعماً من منظمة المجتمع المفتوح ومؤسسة
                                دعم الصحافة الدولية إضافة لعملها على مشاريع تتعلق بالتدريب على الكتابة الإبداعية مع
                                مؤسسة DOEN، إضافة للعمل مع مؤسسة روزا لوكسمبورغ الألمانية لتطوير جائزة الحدود للصحافة
                                العربية.
                            </p>
                        </div>
                        <div className={styles["block"]}>
                            <p>
                                سبق للحدود وأن نفذت برامج تدريبية مع الاتحاد الأوروبي من خلال الوكالة الفرنسية لتطوير
                                الإعلام، وحصلت على دعم من المؤسسة الأوروبية للديمقراطية، وعملت على مشاريع تُعنى
                                بالديمقراطية وحقوق الإنسان مع مؤسسة هاينرتش بول الألمانية.{" "}
                            </p>
                        </div>
                        <div className={styles["block"]}>
                            <p>
                                تغطي هذه المؤسسات جزءاً من مصاريف الحدود – التكلفة الأساسية لإدارة المكان – ولكننا بحاجة
                                إلى الدعم منكم كي نغطي قضايا أكثر ومناطق أكثر ومؤسسات إعلامية أكبر، من خلال العمل مع عدد
                                أكبر من الكتاب.
                            </p>
                        </div>
                        <div className={styles["block"]}>
                            <p>
                                <b>الآراء المنشورة على الموقع قد لا تعكس رؤى المؤسسات الداعمة بالضرورة: </b>
                            </p>
                        </div>
                        <div className={styles["supporters"]}>
                            <div className={styles["grid"]}>
                                <div className={styles["cols"]}>
                                    <div className={styles["col"]} />
                                    <div className={styles["col"]} />
                                    <div className={styles["col"]} />
                                    <div className={styles["col"]} />
                                    <div className={`${styles["col"]} ${styles["last"]}`} />
                                </div>
                                <div className={styles["rows"]}>
                                    <div className={styles["row"]} />
                                    <div className={styles["row"]} />
                                    <div className={styles["row"]} />
                                    <div className={styles["row"]} />
                                    <div className={styles["row"]} />
                                    <div className={styles["row"]} />
                                    <div className={`${styles["row"]} ${styles["sec"]}`} />
                                    <div className={`${styles["row"]} ${styles["third"]}`} />
                                </div>
                            </div>
                            <div className={styles["logos"]}>
                                <div className={styles["logo"]}>
                                    <a rel="noreferrer" target="_blank" href="https://www.doen.nl">
                                        <img
                                            className={styles["noContrast"]}
                                            src="/Assets/logos/DOEN.png"
                                            alt="DOEN logo"
                                        />
                                    </a>
                                </div>
                                <div className={styles["logo"]}>
                                    <a rel="noreferrer" target="_blank" href="https://www.opensocietyfoundations.org">
                                        <img
                                            src="/Assets/logos/open-society-foundation.png"
                                            alt="شعار منظمة المجتمع المفتوح"
                                        />
                                    </a>
                                </div>
                                <div className={styles["logo"]}>
                                    <a rel="noreferrer" target="_blank" href="https://www.rosalux.de">
                                        <img
                                            src="/Assets/logos/Rosa-Luxemburg-Stiftung.png"
                                            alt="شعار روزا لوكسمبورغ"
                                        />
                                    </a>
                                </div>
                                <div className={styles["logo"]}>
                                    <a rel="noreferrer" target="_blank" href="https://www.mediasupport.org">
                                        <img
                                            src="/Assets/logos/international-media-support.png"
                                            alt="شعار مؤسسة دعم الصحافة الدولي"
                                        />
                                    </a>
                                </div>
                                <div className={styles["logo"]}>
                                    <a rel="noreferrer" target="_blank" href="https://www.boell.de/de">
                                        <img src="/public/Assets/logos/Stiftung.png" alt="شعار هاينرتش بول الألمانية" />
                                    </a>
                                </div>
                                <div className={styles["logo"]}>
                                    <a rel="noreferrer" target="_blank" href="https://cfi.fr/en">
                                        <img
                                            className={styles["noContrast"]}
                                            src="/Assets/logos/cfi.png"
                                            alt="شعار الوكالة الفرنسية لتطوير الإعلام"
                                        />
                                    </a>
                                </div>
                                <div className={styles["logo"]}>
                                    <a rel="noreferrer" target="_blank" href="https://www.democracyendowment.eu/en">
                                        <img
                                            src="/Assets/logos/European-Endowment-for-Democracy.png"
                                            alt="شعار المؤسسة الأوروبية للديمقراطية"
                                        />
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </PageLayout>
            <Footer />
        </>
    )
}
