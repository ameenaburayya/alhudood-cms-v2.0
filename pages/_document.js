import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {
    static async getInitialProps(ctx) {
        const initialProps = await Document.getInitialProps(ctx)
        return { ...initialProps }
    }

    // componentDidMount(){
    //     window.addEventListener('mousemove', this.handleMouseCursor)
    // }

    // componentWillUnmount(){
    //     window.removeEventListener('mousemove', this.handleMouseCursor)
    // }

    // handleMouseCursor(e){
    //     console.log(e.clientX)
    // }

    render() {
        return (
        <Html>
            <Head>

                <link rel='icon" href="/favicon.ico' />
                <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png" />
                <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png" />
                <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png" />
                <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png" />
                <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png" />
                <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png" />
                <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png" />
                <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png" />
                <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png" />
                <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png" />
                <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png" />
                <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png" />
                <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png" />
                <link rel="manifest" href="/manifest.json" />


                <meta name="application-name" content="شبكة الحدود" />
                <meta property="og:site_name" content="شبكة الحدود" />
                <meta property="og:url" content={`https://alhudood.net/`} />
                <meta property="og:locale" content="ar"></meta>
                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:site" content="@AlHudoodNet" />
                <meta name="twitter:domain" content="alhudood.net" />
                <meta name="twitter:creator" content="@AlHudoodNet" />
            </Head>
            <body>
                
                <Main />
                <NextScript />
            </body>
        </Html>
        )
    }
}

export default MyDocument