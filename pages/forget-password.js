import { useRouter } from "next/router"
import React from "react"
import ForgetPasswordForm from "../components/auth/forgetPasswordForm"
import PageLayout from "../components/layouts/pageLayout"

export default function ForgetPassword() {
    const router = useRouter()

    const handleFormChange = (e) => {
        e.preventDefault()
        router.push("/signup")
    }

    const handleBack = () => {
        router.push("/login")
    }

    return (
        <PageLayout noCSS pageTitle="نسيت كلمة السر">
            <ForgetPasswordForm
                handleBack={router.query.login ? handleBack : null}
                formChange={handleFormChange}
            />
        </PageLayout>
    )
}
