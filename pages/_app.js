import "../scss/main.scss"
import "../scss/singlePost.scss"
//import '../scss/abstracts/_variables.scss'
import React, { useState, useEffect } from "react"
import dictionaryContext from "../contexts/dictionaryContext"
import { getDictionaryPosts } from "../requests/contentApi"
import WebsiteContext from "../contexts/websiteContext"
import useUser from "../data/useUser"
import CookieBanner from "react-cookie-banner"
import CookieConsentContent from "../components/cookieConsentContent"
import cookieStyles from "../scss/components/cookieBar.module.scss"
import { Cookies, CookiesProvider, CookieBannerUniversal } from "react-cookie-banner"
import PageCursor from "../components/pageCursor"
import { LOCAL_CMS_DOMAIN } from "../lib/constants"

function Alhudood({ Component, pageProps }) {
    //const [isLoggedIn, setLoggedIn] = useState(false)
    // console.log("testing rerender")
    const { mutate, loggedIn, user, loading } = useUser()

    const [cookieBarClosed, setCookieBarClose] = useState(false)
    const onCookieClose = () => {
        setCookieBarClose(true)
    }

    const [dictionaryPosts, setDictionaryPosts] = useState(null)
    const [filteredDictionaryPosts, setFilteredDictionaryPosts] = useState(null)
    const [filtered, setFiltered] = useState(false)
    const [filteredLetterSlug, setFilteredLetterSlug] = useState(null)
    // const [dictionaryPosts, setDictionaryPosts] = useState(null)

    useEffect(() => {
        console.log("see", Component)
        mutate()
        //setLoggedIn({loggedIn, user, loading})
        // console.log('testing one more time ', loggedIn, user, loading)
    }, [Component]) // checking if the user is loggedIn in, when the page change

    useEffect(() => {
        getPosts() // getting the dictionary posts on website load
    }, [])

    const getPosts = async () => {
        // this the function the gets the dictionary posts and store them in the state
        const dictionaryPosts = await getDictionaryPosts()
        setDictionaryPosts(dictionaryPosts)
        setFilteredDictionaryPosts(dictionaryPosts)
        // console.log('page props', dictionaryPosts)
    }

    const handleUnFilterDictionary = () => {
        // this function handle unFiltering
        setFilteredDictionaryPosts(dictionaryPosts)
        setFiltered(false)
        setFilteredLetterSlug(null)
    }

    const handleFilterDictionary = (targetLetterSlug) => {
        // this function handle unFiltering
        function filterByLetter(post) {
            // console.log('iam in', post)
            if (post.dictionary.letter.slug == targetLetterSlug) {
                return true
            }
            return false
        }
        const filteredPosts = dictionaryPosts.filter(filterByLetter)
        // console.log('filtering', filteredPosts)
        setFilteredDictionaryPosts(filteredPosts)
        setFiltered(true)
        setFilteredLetterSlug(targetLetterSlug)
    }

    return (
        <WebsiteContext.Provider value={{ auth: { loggedIn, user, loading } }}>
            <dictionaryContext.Provider
                value={{
                    handleFilterDictionary,
                    handleUnFilterDictionary,
                    filteredDictionaryPosts,
                    filtered,
                    filteredLetterSlug,
                    // initialized: this.state.initialized
                }}
            >
                {/* <PageCursor /> */}
                <Component {...pageProps} />
                {!cookieBarClosed && (
                    <CookiesProvider>
                        <CookieBannerUniversal
                            className={cookieStyles["cookie-bar"]}
                            dismissOnScroll={false}
                            disableStyle={true}
                        >
                            {(onAccept) => <CookieConsentContent onClose={onCookieClose} onAccept={onAccept} />}
                        </CookieBannerUniversal>
                    </CookiesProvider>
                )}
            </dictionaryContext.Provider>
        </WebsiteContext.Provider>
    )
}

export default Alhudood

// export async function getServerSideProps({ res, req }) {
//     const parsedCookies = cookie.parse(req.headers.cookie);
//     if (!parsedCookies['XSRF-TOKEN']){
//         const csrf = await fetch(`${LOCAL_CMS_DOMAIN}/sanctum/csrf-cookie`)
//         res.setHeader('set-cookie', csrf.headers.raw()['set-cookie']);
//     }
//     return { props: { } };
// }
