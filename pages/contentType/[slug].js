import { gettingContentTypeBySlug } from '../../requests/contentApi'

export default function ContentType() {
	return null
}

export async function getServerSideProps({ query }) {
	const contentTypeInfo = await gettingContentTypeBySlug()

	if (Object.entries(contentTypeInfo).length === 0) {
		return {
			notFound: true,
		}
	}

	return {
		redirect: {
			destination: `/category/${contentTypeInfo.category}/?contentType=${contentTypeInfo.slug}`,
			permanent: false,
		},
	}
}
