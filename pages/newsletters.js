import React from 'react'
import NewslettersList from '../components/newsletters/newslettersList'
import Footer from '../includes/footer/footer'
import Header from '../includes/header/header'
import NewslettersPageBackground from '../components/svg/newslettersPageBackground'
import styles from '../scss/pages/newsletters.module.scss'
import PageLayout from '../components/layouts/pageLayout'
import PageTitle from '../components/headings/pageTitle'

export default function NewslettersPage() {
    return (
        <>
            <Header />
            <PageLayout pageTitle="نشرات الحدود">
                <div className={styles['newsletters-page']}>
                    <div className={styles['heading']}>
                        <PageTitle>نشرات الحدود</PageTitle>
                        <p>لغزو احتار كل أسر, بـ هُزم النمسا الخاسر بعد, من مسرح ألمانيا البشريةً فعل. والجنوب ارتكبها وبالتحديد، فعل. الا مع قِبل أمدها جديداً.</p>
                    </div>
                    <NewslettersList />
                    <div className={styles['background']}>
                        <NewslettersPageBackground />
                    </div>
                </div>
            </PageLayout>
            <Footer />
        </>
    )
}
