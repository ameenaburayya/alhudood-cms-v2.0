import { useRouter } from "next/router"
import axios from "axios"
import ErrorPage from "next/error"
import PostLayout from "../components/layouts/postLayout"
// import PostTemplate from '../../../templates/post/postTemplate'
import BoxTemplate from "../templates/post/boxTemplate"
import DefaultPostTemplate from "../templates/post/defaultPostTemplate"
import RelatedPosts from "../components/singlePost/relatedPosts"
import { getAllPosts, getAllPostsWithId, getPostByID, getPreviewPostById } from "../requests/contentApi"
import AlhudoodpediaTemplate from "../templates/post/alhudoodpediaTemplate"
import { categories, contentTypes, LOCAL_CMS_DOMAIN } from "../lib/constants"
import DefaultPostSkeleton from "../components/skeletons/defaultPostSkeleton"
import CountriesSkeleton from "../components/skeletons/countriesSkeleton"
import FourOhFour from "./404"
import { useEffect } from "react"

export default function Post({ post, history, test }) {
    const router = useRouter()

    const getTestArticlce = async () => {
        const res = await axios.get(`${LOCAL_CMS_DOMAIN}/api/v1/article/3`)
        console.log(res)
    }

    useEffect(() => {
        getTestArticlce()
    })

    console.log("test article", test)

    if (!post || router.isFallback) {
        const pageTitle = "شبكة الحدود"
        return (
            <PostLayout pageTitle={pageTitle} empty post={null}>
                <article>
                    {(() => {
                        switch (router.query.contentType) {
                            case `${contentTypes.countries.slug}`:
                                return <CountriesSkeleton screen="mobile" />
                            default:
                                return <DefaultPostSkeleton screen="mobile" />
                        }
                    })()}
                </article>
            </PostLayout>
        )
    }

    if (!router.isFallback && !post?._id) {
        return <FourOhFour statusCode={404} />
    }

    const postRelated = post.relatedArticles
    const pageTitle =
        post.contentType.slug == contentTypes.middleEastHeroes.slug
            ? post.contentType.title + ": " + post.title
            : post.contentType.slug == contentTypes.dictionary.slug
            ? "معجم الحدود: " + post.title
            : post.category.slug == categories.alhudoodPedia.slug
            ? post.category.title + ": " + post.title
            : undefined
    return (
        <PostLayout pageTitle={pageTitle} post={post}>
            <article>
                {/* <div dangerouslySetInnerHTML={{__html: test.content.blocks}}></div> */}
                {(() => {
                    switch (post.category.slug) {
                        case `${categories.mediaMonitoring.slug}`:
                            switch (post.contentType.slug) {
                                case `${contentTypes.nominations.slug}`:
                                    return <DefaultPostTemplate post={post} />
                                default:
                                    return <BoxTemplate post={post} />
                            }
                        case `${categories.mediaMonitoring.slug}`:
                            return <BoxTemplate post={post} />
                        case `${categories.alhudoodPedia.slug}`:
                            return <AlhudoodpediaTemplate router={router} post={post} />
                        case `${categories.middleEastShit.slug}`:
                            switch (post.contentType.slug) {
                                case `${contentTypes.middleEastHeroes.slug}`:
                                    return <BoxTemplate post={post} />
                                case "laytaha":
                                    return <DefaultPostTemplate post={post} />
                                case `${contentTypes.antiFreedom.slug}`:
                                    return <BoxTemplate post={post} />
                                default:
                                    return <DefaultPostTemplate post={post} />
                            }
                        default:
                            return <DefaultPostTemplate post={post} />
                    }
                })()}
                {postRelated && post.contentType.slug != contentTypes.dictionary.slug ? (
                    <RelatedPosts posts={postRelated} />
                ) : null}
            </article>
        </PostLayout>
    )
}

export async function getStaticPaths() {
    const allPosts = await getAllPostsWithId()
    // const paths = allPosts.map((post) => ({
    //     params: {
    //         id: post._id ,
    //         category: post.category.slug,
    //         contentType: post.contentType.slug
    //     },
    // }))
    // return {
    //     paths,
    //     fallback: true,
    // }

    return {
        paths: [
            {
                params: {
                    id: "c8bcfd4b-3774-47a7-97ef-3893b4d22c2d",
                },
            },
            {
                params: {
                    id: "3ef089dc-d8fd-4f06-9794-e93887f2612b",
                },
            },
        ],
        fallback: true,
    }
}

// this is for the CMS

// export async function getStaticPaths() {
//     const allPosts = await getAllPosts()
//     console.log(allPosts)
//     const paths = allPosts.map((post) => ({
//         params: {
//             id: '11' ,
//             category: post.category.slug,
//             contentType: post.contentType.slug
//         },
//     }))
//     return {
//         paths,
//         fallback: false,
//     }
// }

export async function getStaticProps({ params, preview = false }) {
    const data = await getPreviewPostById(params.id)

    return {
        props: {
            preview,
            // test: res.data,
            post: {
                ...(data?.post || []),
            },
        },
    }
}

// this is for the CMS

// export async function getStaticProps({ params, preview = false }) {

//     const data = await getPostByID(params.id)
//     console.log(data)
//     // const content = await markdownToHtml(
//     //     (data?.post?.items || [])[0]?.content || ''
//     // )
//     return {
//         props: {
//             preview,
//             // post: {
//             //     ...(data?.post || [])
//             // }
//             post: data
//         },
//     }
// }
