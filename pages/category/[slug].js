import {
    getAllTags,
    getAllCategories,
    getArticlesByCategory,
    getEditorChoices,
    getCategoryBySlug,
    gettingCategories,
    gettingArticlesByCategorySlug,
    gettingContentTypes,
    gettingCategoryBySlug,
    gettingContentTypesByCategorySlug,
} from "../../requests/contentApi"
import CategoryLayout from "../../components/Layouts/categoryLayout"
import DefaultCategoryTemplate from "../../templates/category/defaultCategoryTemplate"
import AlhudoodpediaTemplate from "../../templates/category/alhudoodpediaTemplate"
import ErrorPage from "next/error"
import { categories } from "../../lib/constants"
import AAAJTemplate from "../../templates/category/aaajTemplate"
import Header from "../../includes/header/header"
import Footer from "../../includes/footer/footer"
import { useRouter } from "next/router"
import alhudoodCategories from "../../lib/categories"

export default function Category({ categoryData, query, contentType }) {
    const router = useRouter()

    if (!categoryData) return <div>Loading...</div>

    const category = categoryData.categoryInfo
    const pageTitle = contentType ? category.title : undefined
    const allCategoryArticles = categoryData.articles

    return (
        <>
            <Header />
            <CategoryLayout pageTitle={pageTitle} category={category}>
                <>
                    {(() => {
                        switch (category.id) {
                            case categories.mediaMonitoring.id:
                                return <AAAJTemplate posts={categoryData.articles} />
                            case categories.alhudoodPedia.id:
                                return <AlhudoodpediaTemplate articles={allCategoryArticles} />
                            default:
                                return <DefaultCategoryTemplate query={query} categoryData={categoryData} />
                        }
                    })()}
                </>
            </CategoryLayout>
            <Footer />
        </>
    )
}

export async function getServerSideProps({ query }) {
    const allTags = await getAllTags()
    const data = await getArticlesByCategory(query.slug)

    const categoryInfo = await gettingCategoryBySlug(query.slug)
    const categoryArticles = await gettingArticlesByCategorySlug(query.slug)

    if (Object.entries(categoryInfo).length === 0) {
        return {
            notFound: true,
        }
    }

    return {
        props: {
            query,
            categoryData: {
                categoryInfo,
                articles: data || [],
                allTags,
                categoryArticles,
            },
        },
    }
}
