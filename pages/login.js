import React, { useState, useEffect } from "react"
import LoginForm from "../components/auth/loginForm"
// import { withRouter } from 'next/router'
import PageLayout from "../components/layouts/pageLayout"
import * as cookie from "cookie"
import Router from "next/router"
import useUser from "../data/useUser"
import Header from "../includes/header/header"

const Login = () => {
    const { mutate, loggedIn } = useUser()
    const [windowWidth, setWindowWidth] = useState(0)

    useEffect(() => {
        // if (loggedIn) Router.replace("/");
    })

    useEffect(() => {
        handleResize()
        window.addEventListener("resize", handleResize)

        return (_) => {
            window.removeEventListener("resize", handleResize)
        }
    })

    const handleResize = () => {
        setWindowWidth(window.innerWidth)
    }

    // if (loggedIn) return <> Redirecting.... </>

    const loginSuccess = () => {
        // Router.replace("/")
    }

    const handleFormChange = (e) => {
        e.preventDefault()
        Router.push("/signup", undefined, { shallow: true })
    }

    const handleForgetPassword = () => {
        Router.push("/forget-password?login=true")
    }

    return (
        <PageLayout noCSS pageTitle="تسجيل الدخول">
            {windowWidth < 1024 ? <Header compact /> : null}
            <LoginForm
                handleForgetPassword={handleForgetPassword}
                formChange={handleFormChange}
                loginSuccess={loginSuccess}
            />
        </PageLayout>
    )
}

export default Login

// export async function getServerSideProps({ res, req }) {
//     console.log(req.session)
//     const parsedCookies = cookie.parse(req.headers.cookie);
//     if (!parsedCookies['XSRF-TOKEN']){
//         const csrf = await fetch(`http://localhost:8082/sanctum/csrf-cookie`)
//         res.setHeader('set-cookie', csrf.headers.raw()['set-cookie']);
//     }
//     return { props: { } };
// }
