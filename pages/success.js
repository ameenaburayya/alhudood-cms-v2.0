import React from 'react'
import EmailActivation from '../components/auth/emailActivation'
import RegistrationSuccess from '../components/auth/registrationSuccess'

export default function Success() {
    return (
        <RegistrationSuccess emailService="gmail" email="a.aburayya95@gmail.com" />
    )
}
