import React, { useState, useEffect } from 'react'
import SignupForm from '../components/auth/signupForm'
import {useRouter} from 'next/router'
import PageLayout from '../components/layouts/pageLayout'
import Header from '../includes/header/header'
import useUser from "../data/useUser";



export default function SignupPage() {

    const router = useRouter()
    const [windowWidth, setWindowWidth] = useState(0)
    const { mutate, loggedIn } = useUser();

    const handleResize = () => {
        setWindowWidth(window.innerWidth)
    }

    useEffect(() => {
        handleResize()
        window.addEventListener('resize', handleResize)
    
        return _ => {
        window.removeEventListener('resize', handleResize)
        }
    })


    const handleFormChange = e => {
        e.preventDefault();
        router.push("/login", undefined, {shallow: true})
    }

    return (
        <PageLayout noCSS pageTitle="إنشاء حساب">
            {windowWidth < 1024 ? <Header compact /> : null }
            <SignupForm formChange={handleFormChange} />
        </PageLayout>
    )
}




// export async function getServerSideProps({ res, req }) {

//     const parsedCookies = cookie.parse(req.headers.cookie);
//     if (!parsedCookies['XSRF-TOKEN']){
//         const csrf = await fetch(`http://localhost:8082/sanctum/csrf-cookie`)
//         res.setHeader('set-cookie', csrf.headers.raw()['set-cookie']);
//     }
//     return { props: { } };
// }