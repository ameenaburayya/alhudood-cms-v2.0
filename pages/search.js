import React, { useState, useEffect, useRef } from "react"
import HighlightedProfile from "../components/blocks/highlightedProfile"
import Header from "../includes/header/header"
import Footer from "../includes/footer/footer"
import SearchBar from "../components/search/searchPageSearchBar"
import { search, searchArticles } from "../requests/contentApi"
import styles from "../scss/pages/search.module.scss"
import PageLayout from "../components/layouts/pageLayout"
import PostsBlock from "../components/blocks/postsBlock"
import SearchAndTagPageSkeleton from "../components/skeletons/searchAndTagPageSkeleton"
import Pagination from "../components/pagination"
import { useRouter } from "next/router"
import { postsPerPage } from "../lib/constants"
import PageTitle from "../components/headings/pageTitle"
import zenscroll from "zenscroll"
import NoResults from "../components/noResults"
import Loading from "../components/loading"

export default function Search({ query }) {
    const size = postsPerPage

    const [windowWidth, setWindowWidth] = useState()
    const [loading, setLoading] = useState(query.s)
    const [posts, setPosts] = useState(null)
    const [total, setTotal] = useState(null)
    const [from, setFrom] = useState(0)
    const [term, setTerm] = useState("")
    const [tempTerm, setTempTerm] = useState("")
    const postsDiv = useRef()
    const router = useRouter()

    const updateWindowWidth = () => {
        setWindowWidth(window.innerWidth)
    }

    useEffect(() => {
        testSearch()
        updateWindowWidth()
        window.addEventListener("resize", updateWindowWidth)

        return () => window.removeEventListener("resize", updateWindowWidth)
    }, [])

    useEffect(() => {
        if (query.s) {
            setTerm(query.s)
            handleSearch(query.s)
        } else {
            setLoading(false)
        }
    }, [query])

    const handleFieldChange = (temp) => {
        setTerm(temp)
    }

    const testSearch = () => {
        search("مقال").then((res) => {
            console.log("test search", res)
        })
    }

    const handleSearch = async (temp) => {
        setLoading(true)
        setFrom(0)
        const searchTerm = temp && temp.length ? temp : term
        setTempTerm(searchTerm)
        searchArticles(searchTerm, size, 0).then((res) => {
            setPosts(res.items)
            setTotal(res.total)
            setLoading(false)
            router.push(
                {
                    query: { s: `${searchTerm}` },
                },
                undefined,
                { shallow: true }
            )
        })
    }

    const handleNext = async (e) => {
        e.preventDefault()
        setLoading(true)
        const temp = from + size
        await searchArticles(term, size, temp).then((res) => {
            setFrom(from + size)
            setPosts(res.items)
            setLoading(false)
            zenscroll.to(postsDiv.current)
        })
    }

    const handlePrev = async (e) => {
        e.preventDefault()
        if (loading) return
        setLoading(true)
        const temp = from - size
        searchArticles(term, size, temp).then((res) => {
            setFrom(from - size)
            setPosts(res.items)
            setLoading(false)
            zenscroll.to(postsDiv.current)
        })
    }

    return (
        <>
            <Header />
            <PageLayout pageTitle="البحث">
                <PageTitle className={styles["page-title"]}>البحث</PageTitle>
                <SearchBar
                    term={term}
                    setTerm={handleFieldChange}
                    handleSearch={handleSearch}
                    desktop={windowWidth >= 1024 ? true : false}
                />
                {/* <HighlightedProfile /> */}
                <div ref={postsDiv} className={styles["results-posts"]}>
                    {!loading ? (
                        posts ? (
                            posts.length > 0 ? (
                                <>
                                    <PostsBlock
                                        withDate
                                        withTags
                                        layout="archive"
                                        className={styles["posts-block"]}
                                        posts={posts}
                                    />
                                </>
                            ) : (
                                <NoResults text={`لا يوجد نتائج بحث لـ (${tempTerm})، جرب البحث عن شيء اخر`} />
                            )
                        ) : null
                    ) : null}
                    {loading && <Loading />}
                    {total > size && (
                        <Pagination
                            className={styles["pagination"]}
                            from={from}
                            total={total}
                            size={size}
                            handleNext={handleNext}
                            handlePrev={handlePrev}
                        />
                    )}
                </div>
            </PageLayout>
            <Footer />
        </>
    )
}

export async function getServerSideProps({ query }) {
    return {
        props: {
            query,
        },
    }
}
