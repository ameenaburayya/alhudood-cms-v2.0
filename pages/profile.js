import React, { useState, useRef, useEffect } from "react"
import { Swiper, SwiperSlide } from "swiper/react"
import SwiperCore, { EffectFade } from "swiper"
import "swiper/swiper-bundle.css"
import styles from "../scss/pages/profile.module.scss"
import Header from "../includes/header/header"
import ProfileAccountInfo from "../components/profile/profileAccountInfo"
import ProfileNavigation from "../components/profile/profileNavigation"
import ProfileBookmarks from "../components/profile/profileBookmarks"
import ProfileCommunity from "../components/profile/profileCommunity"
import { getArticlesByCategory } from "../requests/contentApi"
import { categories } from "../lib/constants"
import Snackbar from "@material-ui/core/Snackbar"
import Button from "@material-ui/core/Button"
import snackbarStyles from "../scss/components/snackbar.module.scss"
import ProfileUser from "../components/profile/profileUser"
import { newsletters, profile, comments } from "../lib/fakeData"
import ProfileNewsletters from "../components/profile/profileNewsletters"
import ProfileEmptySection from "../components/profile/profileEmptySection"
import CommentsIcon from "../components/icons/commentsIcon"
import NewsletterIcon from "../components/icons/newsletterIcon"
import BookmarkIcon from "../components/icons/bookmarkIcon"
import ProfileImage from "../components/profile/profileImage"
import ProfileDelete from "../components/profile/profileDelete"
import PageLayout from "../components/layouts/pageLayout"
import { gettingProfile } from "../requests/profileApi"

SwiperCore.use([EffectFade])

export default function Profile(props) {
    const { bookmarkPosts } = props

    const profileNavigation = useRef()
    const profileBookmarks = useRef()
    const slides = useRef(new Array())

    const [swiper, setSwiper] = useState(null)
    const [windowWidth, setWindowWidth] = useState(0)
    const [infoEdit, setInfoEdit] = useState(false)
    const [passwordEdit, setPasswordEdit] = useState(false)
    const [activeTab, setActiveTab] = useState(0)
    const [crop, setCrop] = useState({ x: 0, y: 0 })
    const [snackbarId, setSnackbarId] = useState(null)
    const [profileImage, setProfileImage] = useState(null)
    const [imageCrop, setImageCrop] = useState(false)

    const [snackPack, setSnackPack] = useState([])
    const [snackbarOpen, setSnackbarOpen] = useState(false)
    const [messageInfo, setMessageInfo] = useState(undefined)
    const [snackbarState, setSnackbarState] = useState(undefined)
    const [snackbarType, setSnackbarType] = useState(undefined)

    useEffect(() => {
        handleTabResize(0)
        updateWindowWidth()
        handleApiTest()
        window.addEventListener("resize", updateWindowWidth)

        return (_) => {
            window.removeEventListener("resize", updateWindowWidth)
        }
    })

    const updateWindowWidth = () => {
        setWindowWidth(window.innerWidth)
        fixSliderTouch()
    }

    const handleTabResize = (index) => {
        setTimeout(() => {
            swiper?.updateAutoHeight(10)
        }, 100)
    }

    const handleApiTest = async () => {
        gettingProfile("user").then((res) => {
            console.log("profile", res)
        })
    }

    const onCropChange = (crop) => {
        setCrop({ crop })
    }

    const changeTab = (index) => {
        if (!infoEdit && !passwordEdit) {
            setActiveTab(index)
            swiper?.slideToLoop(index)
        }
    }

    const changeSlide = (slider) => {
        console.log(slider)
        profileNavigation.current.changeNavTab(slider.realIndex)
        setActiveTab(slider.realIndex)
        handleTabResize(slider.realIndex)
    }

    const handleInfoEdit = (type, state) => {
        if (type == "info") {
            setInfoEdit(state)
            swiper.allowTouchMove = !state && !passwordEdit
        } else if (type == "password") {
            setPasswordEdit(state)
            swiper.allowTouchMove = !infoEdit && !state
        }
    }

    const fixSliderTouch = () => {
        if (swiper) {
            if (windowWidth >= 1024) {
                swiper.allowTouchMove = false
            } else {
                swiper.allowTouchMove = true
            }
        }
    }

    const handleUndo = () => {
        profileBookmarks.current.handleUndo()
        setSnackbarOpen(false)
    }

    const handleImageSelect = (e) => {
        setProfileImage(e.target.files[0])
        setImageCrop(true)
    }

    const handleImageUpload = () => {
        const fd = new FormData()
        fd.append("image", profileImage, profileImage.name)
    }

    useEffect(() => {
        if (snackPack.length && !messageInfo) {
            // Set a new snack when we don't have an active one
            setMessageInfo({ ...snackPack[0] })
            setSnackPack((prev) => prev.slice(1))
            setSnackbarOpen(true)
        } else if (snackPack.length && messageInfo && snackbarOpen) {
            // Close an active snack when a new one is added
            setSnackbarOpen(false)
        }
    }, [snackPack, messageInfo, snackbarOpen])

    const handleSnackbarOpen = (message, state, id, type) => {
        console.log("iam in open")
        setSnackPack((prev) => [...prev, { message, key: id, state, type }])
        setSnackbarState(state)
        setSnackbarType(type)
    }

    const handleSnackbarClose = (event, reason) => {
        if (reason === "clickaway") {
            return
        }
        setSnackbarOpen(false)
    }

    const handleSnackbarExited = () => {
        setMessageInfo(undefined)
    }

    return (
        <>
            <Header />
            <PageLayout pageTitle="الصفحة الشخصية">
                <div className={styles["profile"]}>
                    <div className={styles["profile-header"]}>
                        <ProfileImage
                            windowWidth={windowWidth >= 1024 ? 750 : windowWidth}
                            width={windowWidth > 500 ? 500 : windowWidth}
                            image={profile.userInfo.image}
                            edit={infoEdit}
                        />
                        {windowWidth < 1024 && <ProfileUser className={styles["profile-user"]} />}
                        <ProfileNavigation
                            snackbarOpen={handleSnackbarOpen}
                            className={styles["profile-tabs"]}
                            ref={profileNavigation}
                            disabled={infoEdit || passwordEdit}
                            activeTab={activeTab}
                            changeTab={changeTab}
                        />
                    </div>
                    <div className={styles["profile-sections"]}>
                        <Snackbar
                            key={snackbarId}
                            anchorOrigin={{
                                vertical: "bottom",
                                horizontal: "center",
                            }}
                            open={snackbarOpen}
                            autoHideDuration={4000}
                            onClose={handleSnackbarClose}
                            onExited={handleSnackbarExited}
                            message={messageInfo ? messageInfo.message : undefined}
                            className={`${snackbarStyles["snackbar"]} ${snackbarStyles[`${snackbarState}`]} ${
                                snackbarType == "bookmark" ? snackbarStyles["with-button"] : ""
                            }`}
                            action={
                                <React.Fragment>
                                    {snackbarType == "bookmark" && (
                                        <Button color="secondary" size="small" onClick={handleUndo}>
                                            إعادة
                                        </Button>
                                    )}
                                </React.Fragment>
                            }
                        />
                        {windowWidth >= 1024 && <ProfileUser className={styles["profile-user"]} />}
                        {windowWidth && (
                            <Swiper
                                spaceBetween={0}
                                effect={windowWidth >= 1024 ? "fade" : "Overflow"}
                                slidesPerView={1}
                                onSlideChange={(swiper) => changeSlide(swiper)}
                                // onSlideChange={(swiper) => console.log(swiper)}
                                onSwiper={(swiper) => setSwiper(swiper)}
                                // autoHeight={true}
                                className={styles["profile-slider"]}
                                // allowTouchMove={windowWidth >= 1024 ? false : true}
                                autoHeight={true}
                                loop
                            >
                                <SwiperSlide
                                    ref={(element) => slides.current.push(element)}
                                    className={styles["profile-slider-content"]}
                                >
                                    <ProfileAccountInfo
                                        snackbarOpen={handleSnackbarOpen}
                                        handleInfoEdit={handleInfoEdit}
                                        checkSize={() => handleTabResize(0)}
                                    />
                                </SwiperSlide>
                                <SwiperSlide
                                    ref={(element) => slides.current.push(element)}
                                    className={styles["profile-slider-content"]}
                                >
                                    العضوية
                                </SwiperSlide>
                                <SwiperSlide
                                    ref={(element) => slides.current.push(element)}
                                    className={styles["profile-slider-content"]}
                                >
                                    {comments && comments.length > 0 ? (
                                        <ProfileCommunity comments={comments} />
                                    ) : (
                                        <ProfileEmptySection
                                            section="community"
                                            type="community"
                                            icon={<CommentsIcon />}
                                            href="https://community.alhudood.net"
                                            text="<span>نشاطك على</span> <span>مجتمع الحدود</span>"
                                            arrowLabel="استكشف المجتمع"
                                        />
                                    )}
                                </SwiperSlide>
                                <SwiperSlide
                                    ref={(element) => slides.current.push(element)}
                                    className={styles["profile-slider-content"]}
                                >
                                    {newsletters && newsletters.length > 0 ? (
                                        <ProfileNewsletters newsletters={newsletters} />
                                    ) : (
                                        <ProfileEmptySection
                                            section="newsletters"
                                            type="newsletters"
                                            icon={<NewsletterIcon />}
                                            href="/newsletters"
                                            text="<span>لم تشترك بعد بأيٍّ من </span><span>النشرات البريدية</span>"
                                            arrowLabel="حسّن الإنبوكس"
                                        />
                                    )}
                                </SwiperSlide>
                                <SwiperSlide
                                    ref={(element) => slides.current.push(element)}
                                    className={`${styles["profile-slider-content"]} ${styles["bookmarks"]}`}
                                >
                                    {bookmarkPosts && bookmarkPosts.length > 0 ? (
                                        <ProfileBookmarks
                                            ref={profileBookmarks}
                                            snackbarOpen={handleSnackbarOpen}
                                            posts={bookmarkPosts}
                                        />
                                    ) : (
                                        <ProfileEmptySection
                                            section="bookmarks"
                                            type="bookmark"
                                            icon={<BookmarkIcon />}
                                            href="/"
                                            text="ستجد مقالاتك المفضلة هنا"
                                            arrowLabel="الصفحة الرئيسية"
                                        />
                                    )}
                                </SwiperSlide>
                                <SwiperSlide
                                    ref={(element) => slides.current.push(element)}
                                    className={`${styles["profile-slider-content"]} ${styles["delete-account"]}`}
                                >
                                    <ProfileDelete />
                                </SwiperSlide>
                            </Swiper>
                        )}
                    </div>
                </div>
            </PageLayout>
        </>
    )
}

export async function getStaticProps({ preview = true }) {
    const currentAffairsArticles = await getArticlesByCategory(categories.currentAffairs.slug)
    return {
        props: {
            preview,
            bookmarkPosts: currentAffairsArticles,
        },
    }
}
