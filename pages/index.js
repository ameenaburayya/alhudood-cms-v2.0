/* eslint-disable import/no-named-as-default-member */
/* eslint-disable import/no-named-as-default */
import TrackVisibility from "react-on-screen"
import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import Header from "../includes/header/header"
import {
    //   getAAAJPost,
    getAllPostsForHome,
    getArticlesByCategory,
    //   getPostsByCategory,
} from "../requests/contentApi"
import PollCard from "../components/blocks/pollCard"
import DidYouKnow from "../components/blocks/didYouKnow"
import Dorar from "../components/blocks/dorar"
import NewsletterBox from "../components/newsletters/newsletterBox"
import Footer from "../includes/footer/footer"
import ContentSection from "../components/sections/contentSection"
import { categories } from "../lib/constants"
import RandomBar from "../components/randomBar"
import PostBlock from "../components/blocks/postBlock"
import styles from "../scss/pages/home.module.scss"
import SupportUs from "../components/blocks/supportUs"
import Grid from "../components/svg/grid"
import LuckyToday from "../components/blocks/luckToday"
import PageLayout from "../components/layouts/pageLayout"
import AAAJSection from "../components/homePage/aaajSection"
import Deck from "../components/aaaj/votingDeck"

export default function Home({ allData }) {
    const {
        currentAffairsArticles,
        mediaMonitoringArticles,
        middleEastShitArticles,
        nonNewsArticles,
        allArticles,
    } = allData

    //   console.log('test', allData.test)

    // allArticles.forEach(function(post){
    //     post['id'] = post._id
    // })

    const [windowWidth, setWindowWidth] = useState()

    const updateWindowWidth = () => {
        setWindowWidth(window.innerWidth)
    }

    useEffect(() => {
        updateWindowWidth()
        window.addEventListener("resize", updateWindowWidth)

        return () => window.removeEventListener("resize", updateWindowWidth)
    }, [])

    //   console.log(visuals[0])

    return (
        <>
            <Header />
            <PageLayout
                withPadding
                customTitle
                pageTitle="شبكة الحدود: اخبار ساخرة أكثر من الأخبار الحقيقية"
                pageDescription="فالأخبار في نهاية المطاف، نميمة منظمة. موقع الحدود هو موقع مخصص للسخرية والكوميديا التي يصيبها السواد أحياناً."
            >
                <div className={styles["home-page"]}>
                    <RandomBar className={styles["section"]} posts={allArticles} />
                    <section className={`${styles["main-section"]} ${styles.section}`}>
                        {windowWidth >= 1024 ? (
                            <ContentSection
                                className={styles["content-section"]}
                                layout="square-list"
                                category={categories.nonNews}
                                posts={nonNewsArticles.slice(0, 3)}
                            />
                        ) : null}
                        <div className={styles["main-story"]}>
                            <PostBlock withTags mainPost withExcerpt post={currentAffairsArticles[0]} />
                        </div>
                        {windowWidth >= 1024 ? (
                            <div className={styles.mix}>
                                <LuckyToday />
                                <PollCard className={styles["poll"]} />
                            </div>
                        ) : null}
                    </section>

                    <section className={`${styles["currentAffairs-section"]} ${styles.section}`}>
                        <ContentSection
                            withTags
                            layout={windowWidth >= 1024 ? "default" : "square-list"}
                            category={categories.currentAffairs}
                            posts={currentAffairsArticles.slice(1, 4)}
                        />
                    </section>

                    <section className={`${styles.section} ${styles.mix}`}>
                        <div className={styles.inner}>
                            <div className={styles.grid}>
                                <Grid />
                            </div>
                            {/* <VisualsSection className={styles.visuals} post={visuals[1]} /> */}
                            {windowWidth >= 1024 ? <Dorar className={styles.dorar} /> : null}
                        </div>
                    </section>

                    {windowWidth >= 1024 ? null : (
                        <section className={styles["section"]}>
                            <ContentSection
                                className={styles["section"]}
                                layout="square-list"
                                category={categories.nonNews}
                                posts={nonNewsArticles.slice(0, 3)}
                            />
                        </section>
                    )}

                    {windowWidth >= 1024 ? null : (
                        <section className={`${styles["section"]} ${styles["stretch"]}`}>
                            <Dorar />
                        </section>
                    )}

                    {/* <section className={`${styles['mediaMonitoring-section']} ${styles['section']}`}>
                        <ContentSection layout={windowWidth >= 1024 ? 'grid' : 'pyramid'} category={categories.mediaMonitoring} posts={mediaMonitoringArticles.slice(0,3)} />
                    </section> */}

                    <AAAJSection
                        className={styles["mediaMonitoring-section"]}
                        windowWidth={windowWidth}
                        category={categories.mediaMonitoring}
                        posts={mediaMonitoringArticles.slice(0, 3)}
                    />

                    {windowWidth >= 1024 ? null : (
                        <section className={`${styles["section"]} ${styles["poll"]} ${styles["stretch"]}`}>
                            <PollCard />
                        </section>
                    )}

                    <section className={`${styles["middleEastShit-section"]} ${styles.section}`}>
                        <ContentSection
                            className={styles.content}
                            layout={windowWidth >= 1024 ? "list-wideImage" : "horizontal-scroll"}
                            category={categories.middleEastShit}
                            posts={middleEastShitArticles.slice(0, 4)}
                        />
                        {windowWidth >= 1024 ? <Deck homepage /> : null}
                    </section>

                    {windowWidth >= 1024 ? null : (
                        <section className={`${styles.section} ${styles.stretch}`}>
                            <TrackVisibility once offset={10}>
                                {({ isVisible }) => <DidYouKnow isVisible={!!isVisible} />}
                            </TrackVisibility>
                        </section>
                    )}

                    {windowWidth >= 1024 ? null : (
                        <section className={styles["section"]}>
                            <Deck homepage />
                        </section>
                    )}

                    <section className={`${styles.section} ${styles.stretch} ${styles.newsletter}`}>
                        <NewsletterBox desktop={windowWidth >= 1024} />
                    </section>

                    <section className={`${styles.section} ${styles["support-us-section"]}`}>
                        <SupportUs />
                    </section>

                    {/* <LatestPosts posts={allArticles} /> */}
                </div>
            </PageLayout>
            <Footer />
        </>
    )
}

// export async function getStaticProps() {
//     const res = await fetch('https://staging.alhudoodcms.alhudoodnet.com/api/tv1/articles')
//     const data = await res.json()

//     return {
//         props: {
//         res,
//         }
//     }
// }

export async function getStaticProps({ preview = true }) {
    const allArticles = await getAllPostsForHome(preview)
    const currentAffairsArticles = await getArticlesByCategory(categories.currentAffairs.slug)
    const mediaMonitoringArticles = await getArticlesByCategory(categories.mediaMonitoring.slug)
    const middleEastShitArticles = await getArticlesByCategory(categories.middleEastShit.slug)
    const nonNewsArticles = await getArticlesByCategory(categories.nonNews.slug)
    const visuals = await getArticlesByCategory(categories.visuals.slug)
    // const aaaj = await getAAAJPost()
    // const test = await getArticlesByCategory(categories.currentAffairs.slug)
    return {
        props: {
            preview,
            allData: {
                allArticles,
                currentAffairsArticles,
                mediaMonitoringArticles,
                middleEastShitArticles,
                nonNewsArticles,
                visuals,
                // aaaj,
                // test,
            },
        },
    }
}

Home.propTypes = {
    allData: PropTypes.object.isRequired,
}
