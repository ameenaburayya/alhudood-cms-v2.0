import React, {useState, useEffect, useRef} from 'react'
import Footer from '../../includes/footer/footer'
import ErrorPage from 'next/error'
import Header from '../../includes/header/header'
import { getAllTags, getArticlesByTag, getTagData } from '../../requests/contentApi'
import TagPageIcon from '../../components/icons/tagPageIcon'
import styles from '../../scss/templates/tags/tag.module.scss'
import PostBlock from '../../components/blocks/postBlock'
import HighlightedProfile from '../../components/blocks/highlightedProfile'
import TagsTicker from '../../components/tagsTicker'
import SearchAndTagPageSkeleton from '../../components/skeletons/searchAndTagPageSkeleton'
import PageLayout from '../../components/layouts/pageLayout'
import PostsBlock from '../../components/blocks/postsBlock'
import { postsPerPage } from '../../lib/constants'
import Pagination from '../../components/pagination'
import { useRouter } from 'next/router'
import FourOhFour from '../404'



var pageTitle = null

export default function Tag({tagsPosts, tag, total}) {

    // console.log(tagsPosts, tag)

    const size = postsPerPage

    const [windowWidth, setWindowWidth] = useState()
    const [loading , setLoading] = useState(false)
    const [allTags, setAllTags] = useState(null)
    const [posts, setPosts] = useState(null)
    const [totalPosts, setTotalPosts] = useState(total)
    const [from, setFrom] = useState(0)
    const postsDiv = useRef()
    const router = useRouter()

    console.log(tagsPosts)


    const updateWindowWidth = () => {
        setWindowWidth(window.innerWidth)
    }

    const getTags = async () => {
        const allTags = await getAllTags()
        setAllTags(allTags)
    } 

    useEffect(() => {
        updateWindowWidth()
        getTags()
        window.addEventListener('resize', updateWindowWidth);

        return () =>  window.removeEventListener('resize', updateWindowWidth);
    }, []);



    useEffect(() => {
        console.log('innn')
        if (tagsPosts && tagsPosts.length > 0) {
            setLoading(false)
            setPosts(tagsPosts)
        }
        else{
            setLoading(true)
            setPosts(null)
        }
    }, [tagsPosts])



    const handleNext = async (e) => {
        e.preventDefault()
        setLoading(true)
        const temp = from + size
        getArticlesByTag(tag.slug, size,  temp).then(
            res => {
                setFrom(from + size)
                setPosts(res.items)
                setLoading(false)
                postsDiv.current.scrollIntoView()  
            }
        )
    }

    if (!tag || router.isFallback){
        pageTitle = "شبكة الحدود"
        return (
            'loading'
        )

    }

    if (!router.isFallback && !tag?.slug) {
        return <FourOhFour statusCode={404} />
    }

    const handlePrev = async (e) => {
        e.preventDefault()
        if (loading) return
        setLoading(true)
        const temp = from - size
        getArticlesByTag(tag.slug, size, temp).then(
            res => {
                setFrom(from - size)
                setPosts(res.items)
                setLoading(false)
                postsDiv.current.scrollIntoView()  
            }
        )
    }

    

    return (
        <>
            <Header />
            <PageLayout pageTitle={pageTitle || `#${tag.title}`}>
                <div className={styles['tag-page']}>
                    {allTags && <TagsTicker tags={allTags} /> }
                    <div className={styles['tag-info']}>
                        <TagPageIcon />
                        <h1 className={styles['title']}>{tag.title}</h1>
                    </div>
                    {/* <HighlightedProfile /> */}
                    <div ref={postsDiv} className={styles['tag-posts']}>
                        {!loading ? 
                            posts ? 
                                posts.length > 0 
                            ? 
                                <>
                                    <PostsBlock withDate withTags layout="archive" posts={posts} />
                                </>
                            : "لا يوجد نتائج"
                            : null
                            : null
                        }
                        {loading && <SearchAndTagPageSkeleton screen={windowWidth < 1024 ? 'mobile' : 'desktop'} />}
                        {totalPosts > size && <Pagination className={styles['pagination']} from={from} total={totalPosts} size={size} handleNext={handleNext} handlePrev={handlePrev} />}
                    </div>
                    {/* <div className={styles['tag-posts']}>
                        {!loading ? <PostsBlock withDate withTags layout="archive" posts={posts} />
                        : <SearchAndTagPageSkeleton screen={windowWidth < 1024 ? 'mobile' : 'desktop'} />}
                    </div> */}
                </div>
            </PageLayout>
            <Footer />
        </>
    )
}



export async function getStaticPaths() {
    return {
        paths: [{
            params: { 
                slug: 'sisi'
            }},
            {
                params: { 
                slug: 'uae'
            }}
        ],
        fallback: true,
    }
}



export async function getStaticProps({ params, preview = false }) {
    const data = await getTagData(params.slug)
    const tagsPosts = await getArticlesByTag(params.slug, postsPerPage, 0)
    return {
        props: {
            
            preview,
            tag: {
                ...(data?.tag.items[0] || {})
            },
            tagsPosts: tagsPosts.items,
            total: tagsPosts.total,
        },
    }
}
