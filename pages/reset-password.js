import React, { Component } from 'react'
import PageLayout from '../components/layouts/pageLayout'
import ResetPasswordForm from '../components/auth/resetPasswordForm'
import Header from '../includes/header/header'

export default class resetPassword extends React.Component {

    // const router = useRouter()
    componentDidMount(){
        console.log('query', this.props.query)
    }

    render(){
        return (
            <>
                <Header />
                <PageLayout pageTitle="إعادة تعيين كلمة السر">
                    <ResetPasswordForm token={this.props.query.token} email={this.props.query.email} />
                </PageLayout>
            </>
        )
    }
}

export async function getServerSideProps({ query }) {

    // we want to check if the url has token in it or not, then we want to check if the token is active or not
    return {
        props: {
            query,
        },
    }
}