import React, { useState, useRef, useEffect } from "react"
import Image from "next/image"
import { Player } from "@lottiefiles/react-lottie-player"
import { Swiper, SwiperSlide } from "swiper/react"
import SwiperCore, { Navigation, Pagination, Controller, Autoplay, EffectFade } from "swiper"
import TrackVisibility from "react-on-screen"
import MainHeading from "../components/headings/mainHeading"
import { principles } from "../lib/constants"
import "swiper/swiper-bundle.css"
import Dal from "../components/icons/dal"
import styles from "../scss/pages/about.module.scss"
// import PrinciplesArrowMobile from '../components/icons/PrinciplesArrowMobile';
import PrinciplesArrowDesktop from "../components/icons/principlesArrowDesktop"
import FansMailIcon from "../components/icons/fansMailIcon"
import Header from "../includes/header/header"
import Footer from "../includes/footer/footer"
import AlhudoodLogo from "../components/logos/alhudoodLogo"
import "swiper/components/effect-fade/effect-fade.min.css"
import PageLayout from "../components/layouts/pageLayout"
import PageTitle from "../components/headings/pageTitle"
import { gettingFansMail } from "../requests/contentApi"

SwiperCore.use([Navigation, Pagination, Controller, Autoplay, EffectFade])

export default function About() {
    const [controlledSwiper, setControlledSwiper] = useState(null)
    const [autoplay, setAutoplay] = useState(true)
    const [windowWidth, setWindowWidth] = useState()
    const [fansMail, setFansMail] = useState(null)
    const countrySwiper = useRef()
    const animation = useRef()

    const countries = []
    const widths = []

    const updateWindowWidth = () => {
        setWindowWidth(window.innerWidth)
    }

    countries[0] = useRef()
    countries[1] = useRef()
    countries[2] = useRef()

    const getFansMail = async () => {
        gettingFansMail().then((res) => {
            setFansMail(res)
        })
    }

    useEffect(() => {
        updateWindowWidth()
        getFansMail()
        window.addEventListener("resize", updateWindowWidth)

        return () => window.removeEventListener("resize", updateWindowWidth)
    }, [])

    const handleCountrySwiper = (swiper) => {
        if (countries.length > 0) {
            fansMail?.map((mail, index) => {
                widths[index] = countries[index]?.current.getBoundingClientRect().width
                countries[index].current.style.position = "unset"
            })
            document.documentElement.style.setProperty("--country-width", widths[0] + "px")
        }
    }

    const handleCountryChange = (index) => {
        if (index <= fansMail.length && index >= 1) {
            widths[index - 1]
                ? document.documentElement.style.setProperty("--country-width", widths[index - 1] + "px")
                : null
        } else if (index == 4) {
            widths[index - 4]
                ? document.documentElement.style.setProperty("--country-width", widths[index - 4] + "px")
                : null
        } else if (index == 0) {
            widths[index + 2]
                ? document.documentElement.style.setProperty("--country-width", widths[index + 2] + "px")
                : null
        }
    }

    const handleAutoplay = (swiper) => {
        setAutoplay(false)
    }

    const animationControl = () => {
        animation.current.play()
        setTimeout(() => {
            animation.current.pause()
        }, 1900)
    }

    return (
        <>
            <Header />
            <PageLayout smallWidth pageTitle="من نحن" pageDescription="قصة موقع الحدود للاخبار السياسية الساخرة">
                <div className={`${styles["about-page"]} about-page`}>
                    <div className={styles["background-logo"]}>
                        <AlhudoodLogo />
                    </div>
                    <section className={styles["about-us"]}>
                        <PageTitle className={styles["section-header"]} withDash>
                            من نحن
                        </PageTitle>
                        <div className={styles["content"]}>
                            نحن، ونعوذ بالله من كلمة نحن، مجموعة من الشباب بهدف ما ورؤية فضفاضة، نسعى بشكل حثيث لتعميم
                            مفاهيم السلام العالمي والعدالة الاجتماعية والرقص بالعتمة. شعارنا “زي الهوى يا حبيبي، زي
                            الهوى، آه من الهوى، آه يا حبيبي”. وعلى الرغم من أننا أكثر الناس تواضعاً على وجه المعمورة،
                            إلا أننا نحتفي بأنفسنا أحياناً ولا ننكر أننا لا نجهل الحقيقة كاملة.
                        </div>
                        <div className={styles["content"]}>
                            بصراحة، لا يمثل موقعنا موقعاً إخبارياً، فالأخبار في نهاية المطاف، نميمة منظمة. موقع الحدود
                            هو موقع مخصص للسخرية والكوميديا التي يصيبها السواد أحياناً. ولذلك، فإننا نرحب دوماً
                            بمساهماتكم بالأفكار والمقالات، أو رسائل الإعجاب بعبقرية النصوص الموجودة على موقعنا والتي
                            تعكس عبقرية كتابها أو القائمين على الموقع.
                        </div>
                        <div className={styles["content"]}>
                            أيضاً، يمثل موقعنا مشروعاً غير ربحي، على الرغم من أننا رابحون معنوياً على الدوام. ولذلك،
                            نرحب دوماً بمساهماتكم المادية (يحبذ أن تكون المساهمات بالدولار أو بطاقات الإتصالات المدفوعة
                            مسبقاً) أو العينية (سكر، شاي، عدس، رز، طحين..) إذ أننا، نحب المال إذا ما استطعنا إليه سبيلا.
                        </div>
                    </section>
                    <section className={styles["press"]}>
                        <MainHeading className={styles["section-header"]}>الحدود في الصحافة</MainHeading>
                        <div className={styles["logos"]}>
                            <div className={styles.logo}>
                                <Image layout="fill" src="/Assets/logos/theguardian.svg" alt="The Guardian" />
                            </div>
                            <div className={styles.logo}>
                                <Image layout="fill" src="/Assets/logos/Arageek-logo.png" alt="The Guardian" />
                            </div>
                            <div className={styles.logo}>
                                <Image layout="fill" src="/Assets/logos/middle-east-eye.svg" alt="The Guardian" />
                            </div>
                            <div className={styles.logo}>
                                <Image layout="fill" src="/Assets/logos/Deutsche-Welle.svg" alt="The Guardian" />
                            </div>
                        </div>
                    </section>
                    <section id="principles" className={styles["principles"]}>
                        <MainHeading className={styles["section-header"]}>مبادئ الحدود الإحدى عشر</MainHeading>

                        <div className={styles["container-parent"]}>
                            <div className={`${styles["arrow"]} ${styles["prev"]} prev`}>
                                {/* <PrinciplesArrowMobile className={styles['mobile']} /> */}
                                <PrinciplesArrowDesktop className={styles["desktop"]} />
                            </div>
                            <Swiper
                                className={styles["container"]}
                                slidesPerView={windowWidth < 1024 ? 1 : 2}
                                slidesPerGroup={windowWidth < 1024 ? 1 : 2}
                                loop
                                navigation={{
                                    nextEl: ".next",
                                    prevEl: ".prev",
                                }}
                            >
                                {principles.map((principle) => (
                                    <SwiperSlide className={styles["swiper-slide"]}>
                                        <div className={styles["principle"]}>
                                            <div className={styles["number"]}>
                                                <Dal className={styles["dal"]} />
                                                <span>{principle.number}</span>
                                            </div>
                                            <div className={styles["content"]}>
                                                <h3>{principle.title}</h3>
                                                <p>{principle.content}</p>
                                            </div>
                                        </div>
                                    </SwiperSlide>
                                ))}
                            </Swiper>
                            <div className={`${styles["arrow"]} ${styles["next"]} next`}>
                                {/* <PrinciplesArrowMobile className={styles['mobile']} /> */}
                                <PrinciplesArrowDesktop className={styles["desktop"]} />
                            </div>
                        </div>
                    </section>
                    <div className={styles["divider"]} />

                    <section className={styles["fans-mail"]}>
                        <TrackVisibility once offset={-100}>
                            {({ isVisible }) => (
                                <div className={styles["header"]}>
                                    {isVisible && (
                                        <Player
                                            onEvent={(event) => {
                                                if (event === "load") animationControl() // check event type and do something
                                            }}
                                            speed={2000}
                                            loop
                                            ref={animation}
                                            src="/Assets/animated_icons/heart.json"
                                            style={{ height: "100%", width: "100%" }}
                                            className={styles["heart-animation"]}
                                        />
                                    )}
                                    <MainHeading className={styles["section-header"]}>من بريد المعجبين</MainHeading>
                                </div>
                            )}
                        </TrackVisibility>
                        {fansMail && (
                            <Swiper
                                //spaceBetween={50}
                                className={styles["swiper"]}
                                slidesPerView={1}
                                loop
                                effect="fade"
                                autoplay={
                                    autoplay
                                        ? {
                                              delay: 5000,
                                          }
                                        : false
                                }
                                onTouchMove={(swiper) => handleAutoplay(swiper)}
                                pagination={{ type: "bullets", el: ".pagination" }}
                                controller={{ control: controlledSwiper }}
                                onSlideChange={(swiper) => handleCountryChange(swiper.activeIndex)}
                            >
                                {fansMail?.map((mail) => (
                                    <SwiperSlide className={styles["mail-slide"]}>
                                        <div className={styles["mail"]}>
                                            <div
                                                className={styles["content"]}
                                                dangerouslySetInnerHTML={{ __html: mail.content }}
                                            />
                                            {/* /<div className={styles['country']}>{mail.country}</div> */}
                                        </div>
                                    </SwiperSlide>
                                ))}
                            </Swiper>
                        )}
                        {fansMail && fansMail.length > 0 && (
                            <div className={styles["details"]}>
                                <div className={styles["content"]}>
                                    <div className={`${styles["line"]} ${styles["desktop"]}`} />
                                    <div className={styles["inner"]}>
                                        <Swiper
                                            ref={countrySwiper}
                                            slidesPerView={1}
                                            loop
                                            effect="fade"
                                            initialSlide={1}
                                            onSwiper={setControlledSwiper}
                                            className={styles["country-swiper"]}
                                            onInit={(swiper) => handleCountrySwiper(swiper)}
                                        >
                                            {fansMail?.map((mail, index) => (
                                                <SwiperSlide>
                                                    <div ref={countries[index]} className={styles["country"]}>
                                                        {mail.name}
                                                    </div>
                                                </SwiperSlide>
                                            ))}
                                        </Swiper>
                                        <FansMailIcon />
                                    </div>
                                    <div className={styles["line"]} />
                                </div>
                                <div className={`${styles["pagination"]} pagination`} />
                            </div>
                        )}
                    </section>
                    <div className={`${styles["divider"]} remove-desktop`} />
                    <section id="contactUs" className={styles["contact-us"]}>
                        <MainHeading className={styles["section-header"]}>اتصل بنا</MainHeading>
                        <p>
                            تغطي هذه المؤسسات جزءاً من مصاريف الحدود – التكلفة الأساسية لإدارة المكان – ولكننا بحاجة إلى
                            الدعم منكم كي نغطي قضايا أكثر ومناطق أكثر ومؤسسات إعلامية أكبر، من خلال العمل مع عدد أكبر من
                            الكتاب.
                        </p>
                        <a href="mailto: support@alhudood.net">
                            <div className={styles["email"]}>
                                <span>support@alhudood.net</span>
                            </div>
                        </a>
                    </section>
                </div>
            </PageLayout>
            <Footer />
        </>
    )
}
