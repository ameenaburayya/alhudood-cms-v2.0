import { gettingCategories } from "../requests/contentApi"

export const EXAMPLE_PATH = "cms-buttercms"
export const CMS_NAME = "شبكة الحدود"
export const CMS_URL = "https://staging.alhudoodcms.alhudoodnet.com"
export const LOCAL_CMS_DOMAIN = "http://localhost:8082"
export const DOMAIN = "https://alhudood.net"
export const URL_SLUG = "alhudoodnextjs.vercel.app"
export const posts_to_show = 5
export const postsPerPage = 10

export const contentTypes = {
    breaking: {
        title: "عاجل",
        slug: "breakingNews",
    },
    middleEastHeroes: {
        title: "أبطال من الشرق الأوسط",
        slug: "middleEastHeroes",
    },
    antiFreedom: {
        title: "دائرة مكافحة الحريات",
        slug: "antiFreedom",
    },
    schizophrenia: {
        title: "الفصام الإعلامي",
        slug: "schizophrenia",
    },
    files: {
        title: "ملفات",
        slug: "files",
    },
    dictionary: {
        title: "المعجم",
        slug: "dictionary",
    },
    countries: {
        title: "دول",
        slug: "countries",
    },
    profiles: {
        title: "شخصيات",
        slug: "profiles",
    },
    comic: {
        title: "كوميك",
        slug: "comic",
    },
    caricature: {
        title: "كاريكاتير",
        slug: "caricature",
    },
    nominations: {
        title: "ترشيحات",
        slug: "nominations",
    },
    laytaha: {
        title: "ليتها",
        slug: "laytaha",
    },
}

export const categories = {
    currentAffairs: {
        title: "في الأخبار",
        slug: "currentAffairs",
        description: "كما أن وقام وبدأت، لم أدوات للمجهود بلا",
        id: 1,
    },
    mediaMonitoring: {
        title: "حصاد الإعلام",
        slug: "aaaj",
        description: "كما أن وقام وبدأت، لم أدوات للمجهود بلا",
        id: 3,
    },
    nonNews: {
        title: "ليس في الأخبار",
        slug: "nonNews",
        description: "كما أن وقام وبدأت، لم أدوات للمجهود بلا",
        id: 2,
    },
    alhudoodPedia: {
        title: "الحدودبيديا",
        slug: "alhudoodPedia",
        description: "كما أن وقام وبدأت، لم أدوات للمجهود بلا",
        id: 6,
    },
    middleEastShit: {
        title: "خرا من الشرق الأوسط",
        slug: "middleEastShit",
        description: "كما أن وقام وبدأت، لم أدوات للمجهود بلا",
        id: 4,
    },
    visuals: {
        title: "بصري",
        slug: "visuals",
        description: "كما أن وقام وبدأت، لم أدوات للمجهود بلا",
        id: 7,
    },
}

export const profileTabs = [
    {
        title: "معلومات الحساب",
        slug: "account",
    },
    {
        title: "العضوية",
        slug: "membership",
    },
    {
        title: "مجتمع الحدود",
        slug: "community",
    },
    {
        title: "النشرات الإخبارية",
        slug: "newsletters",
    },
    {
        title: "المقالات المحفوظة",
        slug: "bookmarks",
    },
    {
        title: "مسح الحساب",
        slug: "delete-account",
    },
]

export const alhudoodPediaContentTypes = [contentTypes.dictionary, contentTypes.countries, contentTypes.profiles]

export const fansMail = [
    {
        content:
            "نكرهكم ملئ السموات و ملئ الاراضين وملئ ما بينهما ازكمتم انوفنا بروائح افكاركم النجسة و الخبيثة تدسون السم في العسل ، أنتم الشر كله والفساد كله والعمالة و الخيانة كلها في ثوب من الضحك و السخرية و الاستهزاء برموز القومية و الوحدة العربية بُعداً وسحقاً لكم.",
        country: "حسين، السودان",
    },
    {
        content:
            ".wooow how fake is this picture <br> الصورة مفبركة انضرو الى درجة وضوح الحكام العرب ودرجة وضوح رئيس اسرائيل",
        country: "زكريا طاهر، المغرب",
    },
    {
        content:
            "أنتم صفحة مدعومه من الصهاينة و هدفكم تدمير الشباب المسلم و جعله يفقد الثقة بنفسه و بمجتمعه و تشجعون المجتمعات الغربية الكافرة الداعيه للشذوذ الجنسي و تقدسون الكلاب تفو",
        country: "ولاء عدنان، لبنان",
    },
]

export const principles = [
    {
        number: "١",
        title: "نسخر من الطرف الأقوى",
        content:
            "نتفادى دائماً استغلال الجهة الأضعف، ولا نسخر منها أبداً؛ فهذا النوع من السخرية ليس سهلاً وغير مضحكٍ فقط، بل يعزز ثقافة التنمّر.",
    },
    {
        number: "٢",
        title: "نتعامل مع السخرية بحذر شديد",
        content:
            "السخرية أداة خطرة قد يُساء فهمها بسهولة وقد توصل للقارئ رسالةً أو وجهة نظرٍ تختلف عما يحاول الكاتب طرحه.",
    },
    {
        number: "٣",
        title: "لا مجال في عملنا للمديح والإطراء",
        content: "ندرك وجود كثير من الإيجابيات من حولنا، إلا أن مهمة السخرية هي الإضاءة على مواقع الخلل.",
    },
    {
        number: "٤",
        title: "السخرية بطبيعتها تحتفي بالنقد",
        content:
            "ونحن أيضاً نحتفي بالنقد ونرحّب به من قرائنا ونمارسه ذاتياً بشكل دوري؛ فالجميع يخطئ ويجب عليه إدراك ذلك كي يصوبه، ونحن سنخطئ، وعندما يحدث ذلك سنعتذر.",
    },
    {
        number: "٥",
        title: "السخرية ليست وظيفتها تقديم الحلول",
        content:
            "لسخرية عمل نقدي، وهذا الجزء هو الأهم في تشخيص المشكلة قبل علاجها، ولكن، ليس من مهامنا تقديم خطة خمسية أو خارطة طريق للخروج من أزمة سياسية أو اقتصادية.",
    },
    {
        number: "٦",
        title: "لمحتوى الذي ننشره من إنتاجنا دائماً",
        content:
            "نود الإسهام في حل مشكلة المحتوى العربي بإنتاج مواد فكرية كوميدية جديدة تشكل إضافة نوعية، ولنتمكن من ذلك، نولي اهتماماً لتدريب كتاب جدد يحرصون على جودة الكتابة.",
    },
    {
        number: "٧",
        title: "السخرية والكوميديا ليست الهدف",
        content:
            "إنها وسيلتنا لإيصال أفكارنا. إلّا أننا نقدِّر النكات الخفيفة في بعض الأحيان، شرط أن تكون مضحكة وغير مؤذية، أي لا تتعارض مع أيٍّ من مبادئنا أو سياستنا التحريرية.",
    },
    {
        number: "٨",
        title: "إثارة الجدل هي نتيجة ثانوية للسخرية",
        content: "السخرية قد تثير الجدل، لكن لن يكون هدفنا أبداً إثارة الجدل لمجرد إثارته فحسب.",
    },
    {
        number: "٩",
        title: "لا نسخر بهدف الإهانة",
        content:
            "إلّا أنه لا يمكننا منع هذا الشعور عن البعض. العديد من الأشخاص يشعرون بالإهانة بمجرد ذكر موضوع ما في سياق ساخر، ولكننا ندرك أن التقييد الذي يبدأ بقدر بسيط قد يتمدد ويتوسع ليصبح تضييقاً على الحريات.",
    },
    {
        number: "١٠",
        title: "نفتح نقاشات ولا نملي على القراء",
        content:
            "نفتح نقاشات مع القراء والمتابعين، وحتى من يشعرون بالكره صوبنا أيضاً، وسندفع الناس للتعبير عن أنفسهم ونوفر لهم سبل ذلك.",
    },
    {
        number: "١١",
        title: "لن يكون لدينا عشرة مبادئ لمجرد أن الرقم عشرة رقم متكامل ومقنع",
        content:
            "فقط لأنه يوجد عشرة وصايا أو لأن النظام الرقمي العشري هو الأكثر انتشاراً لا يعني أن ذلك يجبرنا على حشر أنفسنا بعشرة مبادئ فقط.",
    },
]

export const emailServices = {
    gmail: {
        name: "Gmail",
        link: "http://mail.google.com",
        emailExtension: "gmail.com",
    },
    yahoo: {
        name: "Yahoo",
        link: "https://mail.yahoo.com",
        emailExtension: "yahoo.com",
    },
    outlook: {
        name: "Outlook",
        link: "https://outlook.live.com/mail/0/inbox",
        emailExtension: "outlook.com",
    },
    hotmail: {
        name: "Hotmail",
        link: "https://outlook.live.com/mail/0/inbox",
        emailExtension: "hotmail.com",
    },
    live: {
        name: "Live",
        link: "https://outlook.live.com/mail/0/inbox",
        emailExtension: "live.com",
    },
    icloud: {
        name: "iCloud",
        link: "https://www.icloud.com/mail",
        emailExtension: "icloud.com",
    },
}
