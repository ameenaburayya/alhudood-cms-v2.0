import { categories } from "./constants"
import { gettingCategories } from "../requests/contentApi"

export const allCategories = gettingCategories()

export const FooterMenu = [
    {
        title: "من نحن",
        slug: "/about",
    },
    {
        title: "السياسة التحريرية",
        slug: "/editorial-policy",
    },
    {
        title: "مبادئ الحدود الإحدى عشر",
        slug: "/about/#principles",
    },
    {
        title: "سياسة الخصوصية",
        slug: "/privacy-policy",
    },
    {
        title: "اتصل بنا",
        slug: "/about/#contactUs",
    },
    {
        title: "الداعمون",
        slug: "/supporters",
    },
]

export const HeaderMenuList = [
    {
        label: "محتمع الحدود",
        link: "/about-us",
    },
    {
        label: "جَحُصَعْ",
        link: "/aaaj",
    },
    {
        label: "الحدودبيديا",
        link: "/alhudoodPedia",
    },
]

export const MainMenuList = [
    {
        title: "Al-Hudood English",
        slug: "english.alhudood.net",
        description: "Lorem ipsum dolor sit amet",
        newTab: true,
    },
    {
        title: "السوق السوداء",
        slug: "/",
        description: "كما أن وقام وبدأت، لم أدوات للمجهود بلا",
        comingSoon: true,
    },
]

export const SideMenuSecList = [
    {
        label: "من نحن",
        link: "/",
    },
    {
        label: "اتصل بنا",
        link: "/",
    },
    {
        label: "السياسة التحريرية",
        link: "/",
    },
]
