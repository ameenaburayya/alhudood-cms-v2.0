import useSWR from 'swr';
import fetcher from '../lib/fetch';

export function useCurrentUser() {
    const { data, mutate } = useSWR('https://api.jsonapi.co/rest/v1/user/list/', fetcher);
    console.log('data', data)
    const user = data?.user;
    return [user, { mutate }];
}

export function useUser(id) {
    const { data } = useSWR(`https://api.jsonapi.co/rest/v1/user/${id}`, fetcher, { revalidateOnFocus: false });
    return data?.user;
}