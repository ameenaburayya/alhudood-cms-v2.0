import React, { useState } from 'react'
import {nominations} from "../../lib/fakeData";
import VotingSlide from './votingSlide';
import { Swiper, SwiperSlide } from 'swiper/react';
import SwiperCore, {EffectFade, Controller, Navigation} from 'swiper';
import 'swiper/swiper-bundle.css';
import styles from '../../scss/components/aaaj/votingSlider.module.scss'
import VotingInfoSlide from './votingInfoSlide';
import PrinciplesArrowDesktop from '../icons/PrinciplesArrowDesktop';



SwiperCore.use([EffectFade, Controller, Navigation]);

export default function VotingSlider() {

    const [controlledSwiper, setControlledSwiper] = useState(null);

    return (
        <div className={styles['voting-slider']}>
            <div className={styles['main']}>
                <div className={`${styles['arrow']} ${styles['prev']} prev`}>
                    <PrinciplesArrowDesktop />
                </div>
                <Swiper
                    controller={{ control: controlledSwiper }}
                    spaceBetween={50}
                    effect={"Overflow"}
                    className={styles['slider']}
                    // ref={this.swiper}
                    // onInit={() => this.handleTimerStart()}
                    // onSlideChange={(swiper) => this.handleChange(swiper.activeIndex)}
                    slidesPerView={1}
                    loop={true}
                    allowTouchMove={false}
                    navigation={{
                        nextEl: '.next',
                        prevEl: '.prev'
                    }}
                >
                    {nominations.map(nomination => 
                        <SwiperSlide className={styles['slide']}>
                            <VotingSlide nomination={nomination} />
                        </SwiperSlide >
                    )}
                    
                </Swiper>
                <div className={`${styles['arrow']} ${styles['next']} next`}>
                    <PrinciplesArrowDesktop />
                </div>
            </div>
            <div className={styles['info']}>
                <Swiper
                    onSwiper={setControlledSwiper}
                    spaceBetween={0}
                    effect={"fade"}
                    className={styles['slider']}
                    allowTouchMove={false}
                    // ref={this.swiper}
                    // onInit={() => this.handleTimerStart()}
                    // onSlideChange={(swiper) => this.handleChange(swiper.activeIndex)}
                    slidesPerView={1}
                >
                    {nominations.map(nomination => 
                        <SwiperSlide>
                            <VotingInfoSlide nomination={nomination} />
                        </SwiperSlide >
                    )}
                    
                </Swiper>
            </div>
        </div>
    )
}
