import React from 'react'
import styles from '../../scss/components/aaaj/votingSlide.module.scss'
import LinesEllipsis from "react-lines-ellipsis";
import responsiveHOC from 'react-lines-ellipsis/lib/responsiveHOC'
import VotingButtons from './votingButtons';

const ResponsiveEllipsis = responsiveHOC()(LinesEllipsis)



export default function VotingSlide({nomination}) {

    const {featuredImage, title, excerpt} = nomination

    return (
        <div className={styles['main-slide']}>
            <div className={styles['content']}>
                <div className={styles['image']}>
                    <figure>
                        <picture>
                            <img src={featuredImage} />
                        </picture>
                    </figure>
                </div>
                <div className={styles['details']}>
                    <h2>
                        <ResponsiveEllipsis
                            text={title}
                            maxLine='2'
                            ellipsis='...'
                            trimRight={false}
                        />
                    </h2>
                    <div className={styles['excerpt']}>
                        <ResponsiveEllipsis
                            text={excerpt}
                            maxLine='3'
                            ellipsis='...'
                            trimRight={false}
                        />
                    </div>
                </div>
            </div>
            <VotingButtons type="slider" className={styles['buttons']} />
        </div>
    )
}
