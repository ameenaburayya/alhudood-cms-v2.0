import React, { useState, useEffect } from "react"
import { useSprings } from "react-spring/hooks.cjs"
import { useGesture, useDrag } from "react-use-gesture"
import Card from "./votingCard"
import { nominations } from "../../lib/fakeData"
import styles from "../../scss/components/aaaj/votingDeck.module.scss"

const to = (i) => ({
    x: 0,
    y: i * -20,
    // y: 0,
    scale: 1 - i * 0.05,
    // rot: -10 + Math.random() * 20,
    rot: 0,
    delay: i * 100,
})

const from = (i) => ({ rot: 0, scale: 1.5, y: -1000, opacity: 0 })

const trans = (r, s) => `perspective(1500px) rotateX(0deg) rotateY(${r / 10}deg) rotateZ(${r}deg)`

export default function Deck({ homepage }) {
    const [gone] = useState(() => new Set())
    const [activeSlide, setActiveSlide] = useState(0)
    const [secondSlide, setSecondSlide] = useState(1)
    const [thirdSlide, setThirdSlide] = useState(2)

    const [props, set] = useSprings(nominations.length, (i) => ({
        ...to(i),
        from: from(i),
    }))

    const handleBodyOverflow = (state) => {
        if (state) {
            document.getElementsByTagName("body")[0].ontouchmove = (e) => {
                e.preventDefault()
                return false
            }
            document.getElementsByTagName("body")[0].style.overflow = "hidden"
        } else {
            document.getElementsByTagName("body")[0].ontouchmove = (e) => {
                return true
            }
            document.getElementsByTagName("body")[0].style.overflow = "auto"
        }
    }

    const bind = useDrag(({ args: [index], down, movement: [mx], direction: [xDir], velocity, active }) => {
        if (activeSlide !== index) return
        handleBodyOverflow(active)
        const trigger = velocity > 1 // If you flick hard enough it should trigger the card to fly out
        const dir = xDir < 0 ? -1 : 1 // Direction should either point left or right
        if (!down && trigger) gone.add(index) // If button/finger's up and trigger velocity is reached, we flag the card ready to fly out
        set((i) => {
            const isGone = gone.has(index)
            if (index !== i) {
                let scale, y
                if (isGone) {
                    scale = props[i].scale.value + 0.05
                    y = props[i].y.value + 20
                    // opacity = props[i].opacity.value + 0.3
                    return { scale, y }
                }
                return null
            }
            if (isGone) {
                setActiveSlide(index + 1)
                setSecondSlide(index + 2)
                setThirdSlide(index + 3)
            }
            const x = isGone ? (200 + window.innerWidth) * dir : down ? mx : 0 // When a card is gone it flys out left or right, otherwise goes back to zero
            // const rot = mx / 100 + (isGone ? dir * 10 * velocity : 0) // How much the card tilts, flicking it harder makes it rotate faster
            // const scale = down ? 1.1 : 1 // Active cards lift up a bit
            return {
                x,
                // rot,
                delay: undefined,
                config: { friction: 40, tension: down ? 800 : isGone ? 200 : 500 },
            }
        })
        handleBodyOverflow(active)
        if (!down && gone.size === nominations.length) setTimeout(() => gone.clear() || set((i) => to(i)), 600)
    })

    return (
        <div className={styles["deck"]}>
            {props.map(({ x, y, rot, scale, opacity }, i) => {
                return (
                    <Card
                        key={i}
                        i={i}
                        x={x}
                        y={y}
                        rot={rot}
                        scale={scale}
                        trans={trans}
                        data={nominations}
                        bind={bind}
                        length={nominations.length}
                        active={activeSlide}
                        second={secondSlide}
                        third={thirdSlide}
                    />
                )
            })}
        </div>
    )
}
