import React from 'react'
import Sonboleh from '../icons/sonboleh'
import styles from '../../scss/components/aaaj/votingSlide.module.scss'
import SocialShare from '../social/socialShare'

export default function VotingInfoSlide({nomination}) {

    const {source, type, id} = nomination

    return (
        <div className={styles['info-slide']}>
            <div className={styles['col']}>
                <div className={styles['source']}>
                    <div className={styles['logo']}>
                        <img src={source.logo}/>
                    </div>
                    <div className={styles['name']}>
                        {source.title}
                    </div>
                    <div className={styles['country']}>
                        {source.country}
                    </div>
                </div>
                <div className={styles['share']}>
                    <SocialShare id={id} />
                </div>
            </div>
            <div className={styles['type-col']}>
                <div className={styles['type']}>
                    <div className={styles['icon']}>
                        <Sonboleh />
                    </div>
                    <span>{type.title}</span>
                    <div className={`${styles['icon']} ${styles['reverse']}`}>
                        <Sonboleh />
                    </div>
                </div>
            </div>
        </div>
    )
}
