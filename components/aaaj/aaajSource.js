import React from 'react'
import styles from '../../scss/components/aaaj/aaajSource.module.scss'
import {getImageUrl} from '@takeshape/routing'


export default function AAAJSource({source, withCountry}) {
    return (
        <div className={styles['aaaj-source']}>
            <div className={styles['logo']}>
                <img src={getImageUrl(source.logo.path)}  alt="aaaj source logo"/>
            </div>
            <div className={styles['source']}>
                <div className={styles['title']}>{source.title}</div>
                {withCountry && source.country && <div className={styles['country']}>، {source.country}</div>}
            </div>
        </div>
    )
}
