import React, {useRef, useEffect} from "react";
import { string, number, array } from "prop-types";
import { animated, interpolate } from "react-spring/hooks.cjs";
import Sonboleh from '../icons/sonboleh'

import styles from '../../scss/components/aaaj/votingCard.module.scss'
import LinesEllipsis from "react-lines-ellipsis";
import responsiveHOC from 'react-lines-ellipsis/lib/responsiveHOC'
import VotingButtons from "./votingButtons";


const ResponsiveEllipsis = responsiveHOC()(LinesEllipsis)

const Card = ({ i, x, y, rot, scale, trans, bind, data, length, opacity, active, second, third }) => {
    const { title, excerpt, featuredImage, type, source, id } = data[i];
    const titleParentRef = useRef()
    const titleRef = useRef()

    useEffect(() => {
        // const height = titleRef.current.getBoundingClientRect().height
        // if (height > 52){
        //     titleParentRef.current.classList.add(styles['tall'])
        // }
    });

    return (
        <>
            <animated.div
                key={i}
                style={{
                    transform: interpolate([x, y, scale], (x, y, scale) => `translate3d(${x}px,${y}px,0) scale(${scale})`),
                    zIndex: `${length - i}`,
                    // opacity: interpolate([opacity], (opacity) => `${opacity}`),
                }}
                className={`${styles['card']} ${active === i ? styles['active'] : ''} ${second === i ? styles['second'] : ''} ${third === i ? styles['third'] : ''}`}
            >
                <animated.div
                    {...bind(i)}
                    // style={{
                    //     transform: interpolate([rot, scale], trans)
                    // }}
                    className={styles['inner']}
                >
                    <div className={styles['content']}>
                        <div className={styles['tax']}>
                            <div className={styles['type']}>
                                <div className={styles['icon']}>
                                    <Sonboleh />
                                </div>
                                <span>{type.title}</span>
                                <div className={`${styles['icon']} ${styles['reverse']}`}>
                                    <Sonboleh />
                                </div>
                            </div>
                            <div className={styles['source']}>
                                <div className={styles['logo']}>
                                    <img src={source.logo} />
                                </div>
                                <div className={styles['name']}>
                                    {source.title}
                                </div>
                            </div>
                        </div>
                        <div className={styles['image']}>
                            <figure>
                                <picture>
                                    <img src={featuredImage} />
                                </picture>
                            </figure>
                        </div>
                        <div className={styles['details']}>
                            
                            <h2 ref={titleParentRef}>
                                {/* <ResponsiveEllipsis
                                    text={title}
                                    maxLine='2'
                                    ellipsis='...'
                                    trimRight={false}
                                /> */}
                                <span ref={titleRef}>{title}</span>
                            </h2>
                            <div className={styles['excerpt']}>
                                {/* <ResponsiveEllipsis
                                    text={excerpt}
                                    maxLine='3'
                                    ellipsis='...'
                                    trimRight={false}
                                /> */}
                                <span>{excerpt}</span>
                            </div>
                        </div>
                        
                    </div>
                    <VotingButtons id={id} type="card" className={styles['buttons']} />
                </animated.div>
            </animated.div>
        </>
    );
};

export default Card;
