import React from 'react'
import Sonboleh from '../icons/sonboleh'
import styles from '../../scss/components/aaaj/aaajType.module.scss'

export default function AAAJType({type}) {
    return (
        <div className={styles['aaaj-type']}>
            <div className={styles['icon']}><Sonboleh /></div>
            <div className={styles['type']}>{type}</div>
            <div className={`${styles['icon']} ${styles['reverse']}`}><Sonboleh /></div>
        </div>
    )
}
