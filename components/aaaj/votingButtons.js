import React, { useState } from 'react'
import styles from '../../scss/components/aaaj/votingButtons.module.scss'
import CircularProgressWithLabel from '../circularProgressWithLabel'

export default function VotingButtons({id, type, className}) {

    const [loadingYes, setLoadingYes] = useState(false)
    const [loadingNo, setLoadingNo] = useState(false)
    const [results, setResults] = useState({yes: 0, no: 0})
    const [resultsOut, setResultsOut] = useState(false)

    const handleClick = (answer) => {
        if (!loadingNo && !loadingYes){
            if (answer == 'yes'){
                setLoadingYes(true)
            }
            else {
                setLoadingNo(true)
            }
    
            setTimeout(() => { 
                const results = {
                    yes: 30,
                    no: 70
                }
                setLoadingYes(false)
                setLoadingNo(false)
                setResults(results)
                setResultsOut(true)
            }, 3000)
        }
    }

    return (
        <div className={`${styles['buttons']} ${styles[`${type}`]} ${className ? className : ''}`}>
            <div onClick={() => handleClick('yes')} className={`${styles['button']} ${styles['yes']} ${loadingYes ? styles['loading']: ''} ${loadingNo ? styles['disabled']: ''} ${resultsOut ? styles['results'] : ''}`}>
                {loadingYes ? 
                <div class={styles['spring-spinner']}>
                    <div class={`${styles['spring-spinner-part']} ${styles['top']}`}>
                        <div class={styles['spring-spinner-rotator']}></div>
                    </div>
                    <div class={`${styles['spring-spinner-part']} ${styles['bottom']}`}>
                        <div class={styles['spring-spinner-rotator']}></div>
                    </div>
                </div>
                : <button>نعم</button>}
                <CircularProgressWithLabel textBox={styles['text-box']} text={styles['text']} className={styles['progress']} value={results.yes} />
            </div>
            <div className={styles['question']}>
            هل يستحق هذا الخبر الفوز بجحصع؟ 
            </div>
            <div onClick={() => handleClick('no')} className={`${styles['button']} ${styles['no']} ${loadingNo ? styles['loading']: ''} ${loadingYes ? styles['disabled']: ''} ${resultsOut ? styles['results'] : ''}`}>
                {loadingNo ? 
                <div class={styles['spring-spinner']}>
                    <div class={`${styles['spring-spinner-part']} ${styles['top']}`}>
                        <div class={styles['spring-spinner-rotator']}></div>
                    </div>
                    <div class={`${styles['spring-spinner-part']} ${styles['bottom']}`}>
                        <div class={styles['spring-spinner-rotator']}></div>
                    </div>
                </div>
                : <button>لا</button>}
                <CircularProgressWithLabel textBox={styles['text-box']} text={styles['text']} className={styles['progress']} value={results.no} />
            </div>
        </div>
    )
}
