import React, { Component } from 'react'
import Comment from './comment'



export default class Comments extends Component {
    render() {
        return (
            <div className="comments">
                {this.props.comments.map((comment, i) => 
                    <Comment key={i} comment={comment} />
                )}
            </div>
            
        )
    }
}
