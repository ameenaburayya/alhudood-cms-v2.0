import React, { useEffect, useRef, useState } from "react"
import HamburgerIcon from "../icons/hamburgerIcon"
import styles from "../../scss/components/headerComponents/navMenu.module.scss"
import { clearAllBodyScrollLocks } from "body-scroll-lock"
import MenuDrawer from "./menuDrawer"
import ModalMenu from "./modalMenu"

export default function NavMenu({ sticky }) {
    const [open, setOpen] = React.useState(false)
    const [windowWidth, setWindowWidth] = useState(0)
    const modalRef = useRef()

    const handleDrawerOpen = () => {
        if (windowWidth < 1024) {
            setOpen(!open)
        } else {
            setOpen(!open)
            modalRef.current.handleClick()
        }
    }

    const handleDrawerClose = () => {
        setOpen(false)
        clearAllBodyScrollLocks()
    }

    const updateWindowWidth = () => {
        setWindowWidth(window.innerWidth)
    }

    useEffect(() => {
        clearAllBodyScrollLocks()
    }, [])

    useEffect(() => {
        updateWindowWidth()
        window.addEventListener("resize", updateWindowWidth)

        return () => {
            window.removeEventListener("resize", updateWindowWidth)
        }
    })

    return (
        <div className={styles["nav-menu"]}>
            <div onClick={handleDrawerOpen} className={`${styles["icon"]} ${open ? styles["active"] : ""}`}>
                <HamburgerIcon />
                <div className={styles["close-icon"]} />
            </div>
            {open && windowWidth < 1024 && <MenuDrawer close={handleDrawerClose} />}
            {windowWidth >= 1024 && <ModalMenu sticky={sticky} ref={modalRef} />}
        </div>
    )
}
