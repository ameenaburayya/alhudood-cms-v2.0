import React from 'react';
import Link  from 'next/link';


export default function MenuList(menu) {
    const temp = Object.values(menu)
    const list = temp[0]
    return (
        <ul>
            {list.map((item, i) =>
                <li key={i}>
                    <Link href={`${item.slug}`}>
                        <a>{item.title}</a>
                    </Link>
                    <Link href={`${item.slug}`}>
                        <span>{item.description}</span>
                    </Link>
                </li>   
            )}
        </ul>
    )
}