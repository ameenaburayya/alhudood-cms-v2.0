import React, { forwardRef, useEffect, useState, useImperativeHandle } from "react"
import Modal from "@material-ui/core/Modal"
import styles from "../../scss/components/headerComponents/modalMenu.module.scss"
import MenuList from "./menuList"
import { MainMenuList } from "../../lib/menu"
import SocialMedia from "../social/socialMedia"

const ModalMenu = forwardRef((props, ref) => {
    const [modalOpen, setModalOpen] = useState(false)
    const [animationActive, setAnimationActive] = useState(false)
    const [loading, setLoading] = useState(false)

    useImperativeHandle(ref, () => ({
        handleClick() {
            if (loading) return
            setLoading(true)
            if (!modalOpen) {
                setAnimationActive(true)
                setModalOpen(true)
                setLoading(false)
            } else {
                setAnimationActive(false)
                setTimeout(() => {
                    setModalOpen(false)
                    setLoading(false)
                }, 900)
            }
        },
    }))

    const handleClick = () => {
        if (loading) return
        setLoading(true)
        if (!modalOpen) {
            setAnimationActive(true)
            setModalOpen(true)
            setLoading(false)
        } else {
            setAnimationActive(false)
            setTimeout(() => {
                setModalOpen(false)
                setLoading(false)
            }, 900)
        }
    }

    return (
        <Modal
            BackdropProps={{
                className: styles["backdrop"],
            }}
            // ref={this.modalRef}
            // disableEnforceFocus
            // disableAutoFocus
            open={modalOpen}
            onClose={() => handleClick()}
            className={`${styles["menu-modal"]} ${props.sticky ? styles["sticky"] : ""}  ${
                animationActive ? styles["active"] : ""
            }`}
        >
            <div className={styles["modal-content"]}>
                <div className={styles["inner"]}>
                    <div className={styles["main-list"]}>
                        <MenuList menu={MainMenuList} />
                    </div>
                </div>
                <div className={styles["social"]}>
                    <SocialMedia className={styles["social-icons"]} />
                </div>
            </div>
        </Modal>
    )
})

export default ModalMenu
