import React, { useEffect, useRef, useState } from "react"
import SwipeableDrawer from "@material-ui/core/SwipeableDrawer"
import { disableBodyScroll, enableBodyScroll, clearAllBodyScrollLocks } from "body-scroll-lock"
import MenuList from "./menuList"
import { FooterMenu, MainMenuList, SideMenuSecList } from "../../lib/menu"
import SocialMedia from "../social/socialMedia"
import CloseIcon from "../icons/closeIcon"
import Dal from "../icons/dal"
import styles from "../../scss/components/headerComponents/menuDrawer.module.scss"
import SearchIcon from "../icons/searchIcon"
import UserStatus from "../user/userStatus"
import SearchForm from "../search/searchForm"

let targetElement
export default function MenuDrawer(props) {
    const targetRef = useRef()
    const testRef = useRef()
    const [modalOpen, setOpen] = useState(false)

    useEffect(() => {
        setTimeout(() => {
            setOpen(true)
            setTimeout(() => {
                targetElement = targetRef.current
                console.log(targetElement)
                disableBodyScroll(targetElement)
            }, 250)
        }, 10)
    }, [])

    const handleModalClose = () => {
        setOpen(false)
        enableBodyScroll(targetElement)
        setTimeout(() => {
            props.close()
        }, 250)
    }

    return (
        <SwipeableDrawer
            ref={testRef}
            classes={{ paper: styles["drawer"] }}
            transitionDuration={200}
            anchor="right"
            open={modalOpen}
            onClose={handleModalClose}
        >
            <div className={styles["side-menu-inner"]}>
                <div id="target" ref={targetRef} className={styles["content"]}>
                    <div className={styles["close-button"]} onClick={handleModalClose}>
                        <CloseIcon />
                    </div>
                    <UserStatus white />
                    <div className={styles["search-form"]}>
                        <SearchIcon />
                        <SearchForm sideMenu modalClose={handleModalClose} />
                    </div>
                    <div className={styles["main-list"]}>
                        <MenuList menu={MainMenuList} />
                    </div>
                    <div className={styles["divider"]}>
                        <Dal className={styles["dal"]} />
                    </div>
                    <div className={styles["sec-list"]}>
                        <MenuList menu={FooterMenu} />
                    </div>
                </div>
                <div className={styles["social"]}>
                    <SocialMedia className={styles["social-icons"]} />
                </div>
            </div>
        </SwipeableDrawer>
    )
}
