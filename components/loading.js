import React from "react"
import styles from "../scss/components/buttons/buttonLoading.module.scss"

export default class Loading extends React.Component {
    render() {
        return (
            <div className={styles["button-loading"]}>
                <ul role="progressbar" aria-busy="true" aria-label="Loading">
                    <li role="presentation" />
                    <li role="presentation" />
                    <li role="presentation" />
                    <li role="presentation" />
                    <li role="presentation" />
                    <li role="presentation" />
                    <li role="presentation" />
                </ul>
            </div>
        )
    }
}
