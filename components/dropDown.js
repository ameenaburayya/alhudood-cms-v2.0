import React, { useEffect, useState } from "react";
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import styles from '../scss/components/dropdown.module.scss'

export default function DropDown({options, onClick, search, activeOption}) {
    const [isOpen, setIsOpen] = useState(false);
    const [selectedOption, setSelectedOption] = useState(null);


    useEffect(() => {
        console.log('active', activeOption)
        if (activeOption){
            const currentOption = options?.filter(option => option.id == activeOption)[0]
            setSelectedOption(currentOption)
        }
    })

    const toggling = () => setIsOpen(!isOpen);

    const onOptionClicked = (option) => {
        setSelectedOption(option);
        setIsOpen(false);
    };
    const handleClickAway = () => {
        setIsOpen(false);
    }

    const handleOptionSelect = (option) =>{
        onClick(option.id)
        onOptionClicked(option)
    }


    return (
        <ClickAwayListener onClickAway={handleClickAway}>
            <div className={`${styles['dropdown']} ${search ? styles['search']: ''}`}>
                <div className={`${styles['header']} ${isOpen ? styles['open'] : ''}`} onClick={toggling}>
                        <div className={styles['arrow']}>

                        </div>
                        <div className={styles['selected']}>
                            <span>{selectedOption?.title || "اختر"}</span>
                        </div>
                </div>
                <div className={`${styles['list']} ${isOpen ? styles['open'] : ''}`}>
                    <ul>
                    {options && options.map(option => (
                        <li className={selectedOption?.id == option.id ? styles['active'] : ''} 
                        onClick={() => handleOptionSelect(option)} 
                        key={option.id}>
                            <span>{option.title}</span> 
                        </li>
                    ))}
                    </ul>
                </div>
            </div>
        </ClickAwayListener>
    );
  }