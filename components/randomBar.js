import React from 'react'
import Ticker from 'react-ticker'
import styles from '../scss/components/randomBar.module.scss'
import Dal from './icons/dal'
import Link from 'next/link'

export default function RandomBar({posts}) {
    return (
        <div className={styles['random-bar']}>
            <span className={styles['title']}>الشريط العشوائي</span>
            <Ticker  move={true} direction="toRight" speed={3}>
                {() => (
                    <>
                        <div className={styles['posts']}>
                            {posts.map((post, index) => 
                                <span key={index} className={styles['post']}>
                                    <Link as={`/${post.category.slug}/${post.contentType.slug}/${post._id}`} href="/[category]/[contentType]/[id]">
                                        <span key={index}>{post.title}</span>
                                    </Link>
                                    <Dal className={styles['dal']} />
                                </span>
                            )}
                        </div>
                    </>
                )}
            </Ticker>
        </div>
    )
}
