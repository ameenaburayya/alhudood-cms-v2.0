//import arrowCreate, { DIRECTION } from 'arrows-svg'
import Arrow, { DIRECTION } from 'react-arrows'
import React, { Component } from 'react'

export default function ArrowLine(props) {
    const comment_box_position = props.correctionSummary.comment_position
    const comment_box_position_arrow = comment_box_position == "top" ? "BOTTOM" : "TOP"
    var fromDirection = props.correctionSummary.arrow_start.right ? "RIGHT" : props.correctionSummary.arrow_start.left ? "LEFT" : null
    var toDirection = comment_box_position_arrow + "_" + fromDirection
    var fromNode = props.correctionSummary.arrow_start_word
    const lines_corrections = props.lines_corrections
    var toNode = props.toNode
    var fromX = 0
    var fromY = 0
    var toX = 0
    var toY = 0
    const specialLine = props.correctionSummary.first_line_or_last_line
    const lineStatus = props.correctionSummary.line_status
    const onLinePosition =  props.correctionSummary.on_line_position

    const line_number = props.correctionSummary.line_number

    
    

    if (fromDirection == "RIGHT" && comment_box_position =="top"){
        toX = 1.5
        toY = 0.5
        props.toNode[0].style.marginLeft = "auto"
        if (onLinePosition == "line_middle"){
            fromX = 1
            fromY = 0.3
            toX = 0.4
            toY = 0.5
            fromDirection = 'BOTTOM_RIGHT'
        }
    }
    else if(fromDirection == "LEFT" && comment_box_position =="top"){
        toX = -1.5
        toY = -0.5
        
        if (onLinePosition == "line_middle"){
            fromX = -1
            fromY = 0.3
            toX = -0.4
            toY = 0.5
            fromDirection = 'BOTTOM_LEFT'
        }
        else if(onLinePosition == "line_end"){
            toDirection = "LEFT"
        }

        
    }
    else if(fromDirection == "RIGHT" && comment_box_position =="bottom"){
        toX = 1.5
        toY = -0.5
        props.toNode[0].style.marginLeft = "auto"
    }
    else if(fromDirection == "LEFT" && comment_box_position =="bottom"){
        toX = -2.5
        toY = -0.5
    }


    if (lines_corrections[line_number - 1]){
        var array_length = lines_corrections[line_number - 1].length
        var testing_line_comment = lines_corrections[line_number - 1][array_length - 1]
        var testing_line_comment_offset_left = testing_line_comment.coordinates.left

        var fromNode_offset_right = fromNode.getBoundingClientRect().right

        if (fromNode_offset_right > testing_line_comment_offset_left && comment_box_position == "bottom" && onLinePosition == "line_middle"){
            props.remove_circle(fromNode)
            props.correctionSummary.correction.nextElementSibling.nextElementSibling.style.display = "none"

            if(fromDirection == "LEFT" && comment_box_position =="bottom"){
                fromX = -1
                fromY = 0
                toX = -0.2
                toY = -0.5
                fromDirection = 'BOTTOM_LEFT'
            } else if(fromDirection == "RIGHT" && comment_box_position =="bottom"){
                fromX = 1
                fromY = 0.1
                toX = 0.4
                toY = -0.5
                fromDirection = 'BOTTOM_RIGHT'
            }
        }
        else if (fromNode_offset_right > testing_line_comment_offset_left && comment_box_position == "top" && onLinePosition == "line_middle"){
            props.remove_circle(fromNode)
            props.correctionSummary.correction.nextElementSibling.nextElementSibling.style.display = "none"
        }
    }

    if (specialLine){
        if (specialLine == "top"){
            
        }
    }

    return (
        <Arrow
            className='arrow'
            from={{
                direction: DIRECTION[fromDirection],
                node: () => fromNode,
                translation: [fromX, fromY],
            }}
            to={{
                direction: DIRECTION[toDirection],
                node: () => toNode[0],
                translation: [toX, toY],
            }}    
        />
    );
}

