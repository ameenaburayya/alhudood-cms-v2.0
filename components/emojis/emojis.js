import React from 'react';
import Component from 'react';
import CryingEmoji from './cryingEmoji'
import LaughingEmoji from './laughingEmoji'
import FireEmoji from './fireEmoji'
import PoopEmoji from './poopEmoji'
import styles from '../../scss/components/emojis.module.scss'

const e2a = s => s.replace(/\d/g, d => '٠١٢٣٤٥٦٧٨٩'[d])

class Reacting extends React.Component {

    state = {
        alreadyReacted: this.props.emojis?.alreadyReacted,
        cry: this.props.emojis?.cry,
        fire: this.props.emojis?.fire,
        laugh: this.props.emojis?.laugh,
        poop: this.props.emojis?.poop,
        currentReaction: this.props.emojis?.alreadyReacted
    }


    handleReaction = (reaction) => {

        if (this.state.alreadyReacted === false){
            this.setState({
                alreadyReacted: true,
                [reaction]: this.state[reaction] + 1,
                currentReaction: [reaction]
            })
        } else if (this.state.currentReaction == reaction) {
            this.setState({
            alreadyReacted: false,
            [reaction]: this.state[reaction] - 1,
            currentReaction: null
            })
        } else{
            this.setState({
            alreadyReacted: true,
            [reaction]: this.state[reaction] + 1,
            [this.state.currentReaction]: this.state[this.state.currentReaction] - 1,
            currentReaction: [reaction]
            })
        }
    }

  
    render() {
        return (
            <div className={styles['emojis']}>
                <ul className={this.state.alreadyReacted ? styles['reacted'] : styles['unreacted']}>
                    <li className={`${styles['fire']} ${this.state.currentReaction == 'fire' ? styles['active'] : null}`}>
                        <button onClick={ () => this.handleReaction("fire") }>
                            <FireEmoji className={styles['emoji']} colored={this.state.currentReaction == "fire" ? true : false}  />
                            {this.state.fire ? <div className={styles['emoji-counter']}>{e2a(`${this.state.fire}`)}</div> : null}
                        </button>
                    </li>
                    <li className={this.state.currentReaction == "cry" ? styles['active'] : null}>
                        <button onClick={ () => this.handleReaction("cry") }>
                            <CryingEmoji className={styles['emoji']} colored={this.state.currentReaction == "cry" ? true : false}  />
                            {this.state.cry ? <div className={styles['emoji-counter']}>{e2a(`${this.state.cry}`)}</div> : null}
                        </button>
                    </li>
                    <li className={this.state.currentReaction == "laugh" ? styles['active'] : null}>
                        <button  onClick={ () => this.handleReaction("laugh") }>
                            <LaughingEmoji className={styles['emoji']} colored={this.state.currentReaction == "laugh" ? true : false}  />
                            {this.state.laugh ? <div className={styles['emoji-counter']}>{e2a(`${this.state.laugh}`)}</div> : null}
                        </button>
                    </li>
                    <li className={this.state.currentReaction == "poop" ? styles['active'] : null}>
                        <button onClick={ () => this.handleReaction("poop") }>
                            <PoopEmoji className={styles['emoji']} colored={this.state.currentReaction == "poop" ? true : false}  />
                            {this.state.poop ? <div className={styles['emoji-counter']}>{e2a(`${this.state.poop}`)}</div> : null}
                        </button>
                    </li>
                </ul>
            </div> 
        );
    }
}

export default Reacting;