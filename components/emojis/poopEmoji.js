import React, { Component } from 'react'


export default class PoopEmoji extends Component {
    render() {
        return (
            <div className={`${this.props.className}`}>
                <img src="/Assets/UI_Elements/emojis/poop-coloured-icon.svg" alt="Poop Emoji"></img>
                <svg
                xmlns="http://www.w3.org/2000/svg"
                width="36"
                height="30"
                viewBox="0 0 36 30"
                >
                <g data-name="Group 3682" transform="translate(-410.791 -324.335)">
                    <path
                    fillRule="evenodd"
                    d="M421.7 339.955a1.873 1.873 0 111.873 1.876 1.876 1.876 0 01-1.873-1.876m11.962 1.876a1.876 1.876 0 111.872-1.876 1.876 1.876 0 01-1.872 1.876m-2.476 3.735a2.947 2.947 0 01-5.58 0 1.156 1.156 0 10-2.187.749 5.257 5.257 0 009.953 0 1.156 1.156 0 10-2.187-.749zm8.519 6.455h-21.831a4.649 4.649 0 01-4.773-4.5 4.52 4.52 0 013.345-4.3 1.158 1.158 0 00.683-1.672 4.246 4.246 0 01-.541-2.082 4.648 4.648 0 014.763-4.5h.013a1.156 1.156 0 001.155-1.135 4.67 4.67 0 014.759-4.419 9.136 9.136 0 005.739-1.943 8.56 8.56 0 01.342.869 6.331 6.331 0 01-.11 4.609 1.157 1.157 0 001.182 1.609 5.174 5.174 0 01.54-.029 4.649 4.649 0 014.773 4.5 4.3 4.3 0 01-.635 2.241 1.158 1.158 0 00.914 1.759 4.5 4.5 0 11-.32 8.993m2.352-12.993a6.923 6.923 0 00-6.177-6.761 9.96 9.96 0 00-1.747-7.477 1.154 1.154 0 00-1.889.077c-1.462 2.275-4.776 2.233-4.917 2.23h-.04a7.189 7.189 0 00-4.944 1.936 6.7 6.7 0 00-2.03 3.692 6.917 6.917 0 00-6.037 6.743 6.6 6.6 0 00.314 2.008 7 7 0 00-2.374 1.939 6.592 6.592 0 00-1.427 4.1 6.962 6.962 0 007.084 6.817h21.833a6.962 6.962 0 007.084-6.817 6.874 6.874 0 00-5.033-6.524 6.576 6.576 0 00.305-1.964z"
                    data-name="Path 5305"
                    ></path>
                </g>
                </svg>
            </div>
        )
    }
}
