import React from "react"
import ContentLoader from "react-content-loader"
import styles from '../../scss/components/skeletons/skeleton.module.scss'

const DefaultPostSkeleton = (props) => {
    if (props.screen == "mobile"){
        return (
            <div className={`${styles['skeleton']}`}>
                <ContentLoader 
                    speed={2}
                    width={283}
                    height={493}
                    viewBox="0 0 283 493"
                    backgroundColor="#f3f3f3"
                    foregroundColor="#ecebeb"
                    {...props}
                    id={1}
                >
                    <path d="M 213.9 0 h 55.5 v 9.9 h -55.5 z M 22.4 33 h 247.1 v 20.4 H 22.4 z M 22.4 61.4 h 247.1 v 20.4 H 22.4 z M 0 180.6 h 281.5 v 157.8 H 0 z M 22.4 375.6 h 247.1 v 7.2 H 22.4 z M 22.4 396.9 h 247.1 v 7.2 H 22.4 z M 22.4 418.3 h 247.1 v 7.2 H 22.4 z M 22.4 440.8 h 247.1 v 7.2 H 22.4 z M 22.4 463.3 h 247.1 v 7.2 H 22.4 z M 22.4 485.8 h 247.1 v 7.2 H 22.4 z M 10.5 145 H 30 v 19.5 H 10.5 z M 39.5 145 H 59 v 19.5 H 39.5 z" />
                </ContentLoader>
            </div>
            )
    }
    else if (props.screen == 'desktop'){
        return (
            <div className={`${styles['skeleton']} ${styles['desktop']}`}>
                <ContentLoader 
                    speed={2}
                    width={417}
                    height={407}
                    viewBox="0 0 417 407"
                    backgroundColor="#f3f3f3"
                    foregroundColor="#ecebeb"
                    {...props}
                    id={3}
                >
                    <path d="M 0 102.4 h 417 V 317 H 0 z M 336.1 0 h 46.6 v 6.9 h -46.6 z M 136.5 21.2 h 246.2 v 12.5 H 136.5 z M 57.9 338 h 324.9 v 5.7 H 57.9 z M 57.9 350.8 h 324.9 v 5.7 H 57.9 z M 57.9 363.7 h 324.9 v 5.7 H 57.9 z M 57.9 376.6 h 324.9 v 5.7 H 57.9 z M 57.9 389.4 h 324.9 v 5.7 H 57.9 z M 57.9 402.3 h 324.9 v 5.7 H 57.9 z M 5.1 71.7 h 17.3 V 89 H 5.1 z M 32.1 71.7 h 17.3 V 89 H 32.1 z" />
                </ContentLoader>
            </div>
        )
    }
    else {
        return null
    }
}

export default DefaultPostSkeleton