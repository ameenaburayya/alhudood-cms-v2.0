import React from "react"
import ContentLoader from "react-content-loader"

const PollSkeleton = (props) => (
    <div className={`skeleton ${props.className ? props.className : ""}`}>
        <ContentLoader 
        speed={2}
        width={351.3}
        height={237.6}
        viewBox="0 0 351.3 237.6"
        backgroundColor="#f3f3f3"
        foregroundColor="#ecebeb"
        {...props}
        >
            <path d="M320.5,9.9H31.9c-2.7,0-5-2.2-5-5v0c0-2.7,2.2-5,5-5h288.6c2.7,0,5,2.2,5,5v0
            C325.5,7.7,323.3,9.9,320.5,9.9z"/>
            <rect y="55.3" width="351.3" height="44.5"/>
            <rect y="124.2" width="351.3" height="44.5"/>
            <rect y="193.1" width="351.3" height="44.5"/>
        </ContentLoader>
    </div>
)

export default PollSkeleton
