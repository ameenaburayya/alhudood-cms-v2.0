import React from 'react'
import MoreArrowIcon from '../icons/moreArrowIcon'
import Link from 'next/link'
import styles from '../../scss/components/buttons/arrowButton.module.scss'

export default function ArrowButton(props) {
    return (
        props.category 
        ? <Link as={`${props.category.slug}`} href="/[category]">
            <div className={`${styles['more-button']} ${props.className ? props.className : ''} ${props.small ? styles['small'] : ''} ${props.white ? styles['white'] : ''}`}>
                <span className={styles['label']}>{props.label}</span>
                <div className={styles['icon']}>
                    <span></span>
                    <MoreArrowIcon />
                </div>
            </div>
        </Link>
        : <Link href={props.href} >
            <div className={`${styles['more-button']} ${props.className ? props.className : ''} ${props.small ? styles['small'] : ''} ${props.white ? styles['white'] : ''}`}>
                <span className={styles['label']}>{props.label}</span>
                <div className={styles['icon']}>
                    <span></span>
                    <MoreArrowIcon />
                </div>
            </div>
        </Link>
    )
}
