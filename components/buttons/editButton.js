import React from 'react'
import styles from '../../scss/components/buttons/editButton.module.scss'
import EditIcon from '../icons/editIcon'

export default function EditButton(props) {
    return (
        <div 
            className={`${styles['edit-button']} ${props.className}`} 
            onClick={props.handleClick}
        >
            {props.icon && <div className={styles['icon']}>
                <EditIcon />
            </div>}
            <span>{props.label}</span>
        </div>
    )
}
