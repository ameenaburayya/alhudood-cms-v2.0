import React, { Component } from "react"
import Link from "next/link"
import PropTypes from "prop-types"
import primaryButton from "../../scss/components/buttons/primaryButton.module.scss"
import Loading from "../loading"

export default class PrimaryButton extends Component {
    render() {
        return (
            <div
                className={`${primaryButton["button"]} ${primaryButton["primary-button"]} ${
                    this.props.loading ? primaryButton["loading"] : ""
                } ${this.props.fullWidth ? primaryButton["full-width"] : ""} ${this.props.className || ""}`}
            >
                {this.props.link ? (
                    <>
                        <Link href={this.props.link}>
                            <a
                                target={this.props.newTab ? "_blank" : null}
                                rel={this.props.newTab ? "noreferrer" : null}
                                className={`${this.props.icon ? primaryButton["has-icon"] : ""}`}
                            >
                                <span>{this.props.label}</span>
                                {this.props.icon}
                            </a>
                        </Link>
                    </>
                ) : null}
                {this.props.onClick ? (
                    <>
                        <button
                            type={this.props.type || null}
                            onClick={this.props.onClick}
                            className={`${this.props.icon ? primaryButton["has-icon"] : ""}`}
                        >
                            {this.props.loading ? (
                                <Loading />
                            ) : (
                                <>
                                    <span>{this.props.label}</span>
                                    {this.props.icon}
                                </>
                            )}
                        </button>
                    </>
                ) : (
                    <button
                        type={this.props.type || null}
                        className={`${this.props.icon ? primaryButton["has-icon"] : ""}`}
                    >
                        {this.props.loading ? (
                            <Loading />
                        ) : (
                            <>
                                <span>{this.props.label}</span>
                                {this.props.icon}
                            </>
                        )}
                    </button>
                )}
            </div>
        )
    }
}

PrimaryButton.propTypes = {
    label: PropTypes.string.isRequired,
}
