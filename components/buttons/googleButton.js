import React from 'react'
import GoogleIcon from '../icons/googleIcon'
import styles from '../../scss/components/buttons/googleButton.module.scss'

export default function GoogleButton(props) {
    return (
        <div className={`${styles['google-button']} ${props.modal ? styles['modal'] : ''}`}>
            <button onClick={props.onClick}>
                <span>{props.label}</span>
                <GoogleIcon />
            </button>
        </div>
    )
}
