import React, { Component } from "react"
import Link from "next/link"
import PropTypes from "prop-types"
import styles from "../../scss/components/buttons/lineButton.module.scss"

export default class LineButton extends Component {
    render() {
        return (
            <div className={`${styles["line-button"]} ${this.props.className || ""}`}>
                {this.props.link ? (
                    <>
                        <Link href={this.props.link}>
                            <a target={this.props.newTab ? "_blank" : null}>
                                <span>{this.props.label}</span>
                            </a>
                        </Link>
                    </>
                ) : this.props.onClick ? (
                    <>
                        <button type={this.props.type || null} onClick={this.props.onClick}>
                            <span>{this.props.label}</span>
                        </button>
                    </>
                ) : (
                    <button type={this.props.type || null}>
                        <span>{this.props.label}</span>
                    </button>
                )}
            </div>
        )
    }
}

LineButton.propTypes = {
    label: PropTypes.string.isRequired,
}
