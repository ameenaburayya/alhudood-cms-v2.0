import BookmarkIcon from "../icons/bookmarkIcon"
import React from "react"
import Component from "react"
import axios from "axios"

import Alert from "@material-ui/lab/Alert"
import Snackbar from "@material-ui/core/Snackbar"
import AlhudoodModal from "../alhudoodModal"
import UserModal from "../auth/userModal"
import { bookmarkArticle } from "../../requests/interactionApi"
import { LOCAL_CMS_DOMAIN } from "../../lib/constants"

class BookmarkButton extends React.Component {
    constructor(props) {
        super(props)
        this.handleTestBookmark = this.handleTestBookmark.bind(this)
        this.userModal = React.createRef()
        this.state = {
            bookmark: this.props.bookmark,
            open: false,
            modalOpen: false,
        }
    }

    handleBookmark = (id) => {
        axios
            .post(`${LOCAL_CMS_DOMAIN}/api/v1/bookmark?article_id=3`, {
                bookmark: !this.state.bookmark,
            })
            .then(
                (response) => {
                    if (response.status == 200) {
                        this.setState({
                            bookmark: !this.state.bookmark,
                            open: true,
                        })
                    }
                },
                (error) => {
                    console.log(error)
                }
            )
    }

    handleClick = (id) => {
        bookmarkArticle(id).then((res) => {
            console.log(res)
        })
    }

    handleTestBookmark() {
        this.userModal.current.handleOpen()
    }

    render() {
        return (
            <>
                <button className={this.state.bookmark ? "active" : null} onClick={() => this.handleClick(18)}>
                    <BookmarkIcon filled={this.state.bookmark ? true : false} />
                </button>
                <Snackbar
                    anchorOrigin={{
                        vertical: "bottom",
                        horizontal: "right",
                    }}
                    open={this.state.open}
                    onClose={this.handleClose}
                    autoHideDuration={3000}
                >
                    <Alert
                        icon={false}
                        variant="filled"
                        onClose={this.handleClose}
                        severity={this.state.bookmark ? "success" : "error"}
                    >
                        {this.state.bookmark ? "تم حفظ المقال" : "تم ازالة المقال"}
                    </Alert>
                </Snackbar>
                <UserModal ref={this.userModal} />
            </>
        )
    }
}

export default BookmarkButton
