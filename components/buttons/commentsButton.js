import React from 'react'
import CommentsIcon from '../icons/commentsIcon'

export default function CommentButton() {
    return (
        <>
            <button>
                <CommentsIcon />
            </button>
        </>
    )
}
