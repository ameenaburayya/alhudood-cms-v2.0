import React, { Component } from "react"
import Link from "next/link"
import styles from "../../scss/components/buttons/secondaryButton.module.scss"
import Loading from "../loading"

export default class SecondaryButton extends Component {
    render() {
        return (
            <div className={`${styles["secondary-button"]} ${this.props.loading ? styles["loading"] : ""}`}>
                {this.props.link ? (
                    <>
                        <Link href={this.props.link}>
                            <a
                                className={`${this.props.rounded ? "rounded" : ""} ${
                                    this.props.icon ? "has-icon" : ""
                                } ${this.props.color}`}
                            >
                                <span>{this.props.label}</span>
                                {this.props.icon}
                            </a>
                        </Link>
                    </>
                ) : null}
                {this.props.onClick ? (
                    <>
                        <button
                            type={this.props.type || null}
                            onClick={this.props.onClick}
                            className={`${this.props.icon ? primaryButton["has-icon"] : ""}`}
                        >
                            {this.props.loading ? (
                                <Loading />
                            ) : (
                                <>
                                    <span>{this.props.label}</span>
                                    {this.props.icon}
                                </>
                            )}
                        </button>
                    </>
                ) : (
                    <button
                        type={this.props.type || null}
                        className={`${this.props.icon ? primaryButton["has-icon"] : ""}`}
                    >
                        {this.props.loading ? (
                            <Loading />
                        ) : (
                            <>
                                <span>{this.props.label}</span>
                                {this.props.icon}
                            </>
                        )}
                    </button>
                )}
            </div>
        )
    }
}
