import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

const e2a = s => s.replace(/\d/g, d => '٠١٢٣٤٥٦٧٨٩'[d])

export default function CircularProgressWithLabel(props) {
    const resultValue = e2a(`${props.value}`)
    return (
        <Box className={props.className} position="relative" display="inline-flex">
            <CircularProgress variant="determinate" value={props.value} />
            <Box
                top={0}
                left={0}
                bottom={0}
                right={0}
                position="absolute"
                display="flex"
                alignItems="center"
                justifyContent="center"
                className={props.textBox}
            >
                <Typography className={props.text} variant="caption" component="div" color="textSecondary">
                    {`${resultValue}%`}
                </Typography>
            </Box>
        </Box>
    );
}