import React, { Component } from 'react'

import ProfileCommunityComment from './profileCommunityComment'

export default class ProfileCommunity extends Component {
    render() {
        const {comments} = this.props
        return (
            <div>
                {comments.map(comment => 
                    <ProfileCommunityComment comment={comment} key={comment.id} />
                )}
            </div>
        )
    }
}
