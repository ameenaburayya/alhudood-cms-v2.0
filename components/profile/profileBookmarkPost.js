import React from 'react'
import styles from '../../scss/components/profile/profileBookmarkPost.module.scss'
import Link from 'next/link'
import { getImageUrl } from '@takeshape/routing'

export default function ProfileBookmarkPost(props) {

    const {post} = props
    
    return (
        <article className={`${styles['post-block']}`}>
            <div className={styles['post-image']}>
                <figure>
                    <Link as={`/${post.category.slug}/${post.contentType.slug}/${post._id ? post._id : post.id}`} href="/[category]/[contentType]/[id]">
                        <picture>
                            <img src={getImageUrl(post.featuredImage.path)} />
                        </picture>
                    </Link>
                </figure>
            </div>
            <div className={styles['post-content']}>
                <div className={styles['details']}>
                    <h2 className={styles['title']}>
                        <Link as={`/${post.category.slug}/${post.contentType.slug}/${post._id}`} href="/[category]/[contentType]/[id]">
                            {post.title}
                        </Link>
                    </h2>
                    <p className={styles['excerpt']}>
                            <Link as={`/${post.category.slug}/${post.contentType.slug}/${post._id}`} href="/[category]/[contentType]/[id]">{post.excerpt}</Link>
                    </p> 
                </div>
                <div className={styles['post-extra']}>
                    <div className={styles['contentType']}>
                        <Link as={`/${post.category.slug}/${post.contentType.slug}`} href="/[category]/[contentType]">
                            <span>{post.contentType.title}</span>
                        </Link>
                    </div>
                    <div className={styles['date']}>٩ أكتوبر، ٢٠٢٠</div>
                </div>
            </div>
        </article>
    )
}
