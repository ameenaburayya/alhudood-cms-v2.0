import React, { Component } from 'react'
import CommentIcon from '../icons/commentsIcon'
import styles from '../../scss/components/profile/profileCommunityComment.module.scss'

export default class ProfileCommunityComment extends Component {

    constructor(props){
        super(props)
        this.reply = React.createRef()
        this.username = React.createRef()
        this.checkOverflow = this.checkOverflow.bind(this)
        this.state = {
            overflow: false,
        }
    }

    checkOverflow(){
        const parent = this.reply.current?.getBoundingClientRect().x
        const username = this.username.current?.getBoundingClientRect().x
        if (parent > username){
            this.setState({overflow: true})
        }
        else {
            this.setState({
                overflow: false
            })
        }
    }

    componentDidMount(){
        this.checkOverflow()
        window.addEventListener('resize', this.checkOverflow);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.checkOverflow);
    }


    render() {
        const comment = this.props.comment
        return (
            <div className={styles['comment']}>
                <div className={styles['thread-details']}>
                    <div className={styles['icon']}>
                        <CommentIcon />
                    </div>
                    <div className={styles['thread-details-inner']}>
                        <div className={styles['thread-title']}>
                            {comment.title}
                        </div>
                        <div className={styles['thread-extra']}>
                            <div className={styles['date']}>
                                {comment.date}
                            </div>
                            <div className={styles['section']}>
                                {comment.section}
                            </div>
                        </div>
                    </div>
                </div>
                {comment.lastReply ?
                    <div className={`${styles['last-reply']} ${this.state.overflow && styles['overflow']}`} ref={this.reply}>
                        <div className={styles['title']}>اخر رد</div>
                        <div className={styles['user']}>
                            <div className={styles['image']}>
                                <img src={comment.lastReply.image} />
                            </div>
                            <div className={styles['user-details']}>
                                <div ref={this.username} className={styles['username']}>{comment.lastReply.username}</div>
                                <div className={styles['reply-date']}>قبل يومين</div>
                            </div>
                        </div>
                    </div>
                    : null
                }
            </div>
        )
    }
}
