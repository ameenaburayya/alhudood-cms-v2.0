import React, { useState, useCallback } from "react"
import styles from "../../scss/components/profile/profileImage.module.scss"
import UploadImageIcon from "../icons/uploadImageIcon"
import RemoveImageIcon from "../icons/removeImageIcon"
import CommentAvatar from "../icons/commentAvatar"
import { getOrientation } from "get-orientation/browser"
import { getCroppedImg, getRotatedImage } from "../../lib/canvasUtils"
import Slider from "@material-ui/core/Slider"
import Cropper from "react-easy-crop"
import CloseIcon from "../icons/closeIcon"
import ZoomOutIcon from "../icons/zoomOutIcon"
import ZoomInIcon from "../icons/zoomInIcon"

const ORIENTATION_TO_ANGLE = {
    3: 180,
    6: 90,
    8: -90,
}

export default function ProfileImage(props) {
    const [imageSrc, setImageSrc] = React.useState(null)
    const [crop, setCropPosition] = useState({ x: 0, y: 0 })
    const [zoom, setZoom] = useState(1)
    const [imageProps, setImageProps] = useState({ height: null, width: null })
    const [rotation, setRotation] = useState(0)
    const [imageClass, setImageClass] = useState("")
    const [croppedAreaPixels, setCroppedAreaPixels] = useState(null)
    const [croppedImage, setCroppedImage] = useState(null) // use this to give back an image to profile file

    const onFileChange = async (e) => {
        if (e.target.files && e.target.files.length > 0) {
            const file = e.target.files[0]
            let imageDataUrl = await readFile(file)

            // apply rotation if needed
            const orientation = await getOrientation(file)
            const rotation = ORIENTATION_TO_ANGLE[orientation]
            if (rotation) {
                imageDataUrl = await getRotatedImage(imageDataUrl, rotation)
            }

            setImageSrc(imageDataUrl)
        }
    }

    const onCropComplete = useCallback((croppedArea, croppedAreaPixels) => {
        setCroppedAreaPixels(croppedAreaPixels)
    }, [])

    const setCrop = (crop) => {
        let temp = crop
        if (imageClass == "tall") {
            if (temp.y > (imageProps.height * zoom - props.width) / 2) {
                temp.y = (imageProps.height * zoom - props.width) / 2
            } else if (temp.y < -(imageProps.height * zoom - props.width) / 2) {
                temp.y = -(imageProps.height * zoom - props.width) / 2
            }

            if (temp.x > (imageProps.width * zoom - props.width) / 2) {
                temp.x = (imageProps.width * zoom - props.width) / 2
            } else if (temp.x < -(imageProps.width * zoom - props.width) / 2) {
                temp.x = -(imageProps.width * zoom - props.width) / 2
            }
            setCropPosition(temp)
        } else if (imageClass == "wide") {
            if (temp.x > -(props.width / 2) + (imageProps.width * zoom - imageProps.width) / 2) {
                temp.x = -(props.width / 2) + (imageProps.width * zoom - imageProps.width) / 2
            } else if (
                temp.x <
                -imageProps.width + props.width / 2 - (imageProps.width * zoom - imageProps.width) / 2
            ) {
                // temp.x = (-(props.width / 2) - (imageProps.width / 2) - )
                temp.x = -imageProps.width + props.width / 2 - (imageProps.width * zoom - imageProps.width) / 2
            }
            if (temp.y > (imageProps.height * zoom - props.width) / 2) {
                temp.y = (imageProps.height * zoom - props.width) / 2
            } else if (temp.y < -(imageProps.height * zoom - props.width) / 2) {
                temp.y = -(imageProps.height * zoom - props.width) / 2
            }
            setCropPosition(temp)
        }
    }

    const handleClose = () => {
        setImageSrc(null)
    }

    const increaseZoom = () => {
        if (zoom < 3) {
            setZoom(zoom + 0.2)
        }
    }

    const decreaseZoom = () => {
        if (zoom > 1) {
            setZoom(zoom - 0.2)
        }
    }

    const handleImageSize = (size) => {
        const aspect = size.width / size.height
        let width, height
        if (aspect < 1) {
            height = props.width / aspect
            setImageClass("tall")
            setImageProps({ height, width: props.width })
        } else {
            width = props.width * aspect
            setImageProps({ width, height: props.width })
            setImageClass("wide")
            setCropPosition({ x: -width / 2, y: 0 })
        }
    }

    const showCroppedImage = useCallback(async () => {
        try {
            const croppedImage = await getCroppedImg(imageSrc, croppedAreaPixels, rotation)
            setCroppedImage(croppedImage)
            console.log("new image", croppedImage, imageSrc)
        } catch (e) {
            console.error(e)
        }
    }, [croppedAreaPixels, rotation])

    return (
        <>
            <div className={styles["profile-image"]}>
                {props.image ? (
                    <picture>
                        <img src={props.image} />
                        <input accept=".png, .jpg, .jpeg, gif" id="profile-image" type="file" onChange={onFileChange} />
                    </picture>
                ) : (
                    <div className={styles["profile-image-placeholder"]}>
                        <CommentAvatar />
                    </div>
                )}
                {props.edit && (
                    <div className={`${styles["profile-image-edit"]} ${props.image ? styles["placeholder"] : ""}`}>
                        <div className={styles["inner"]}>
                            <label for="profile-image">
                                <UploadImageIcon className={styles["add-icon"]} />
                            </label>

                            {props.image && <span></span>}
                            {props.image && <RemoveImageIcon />}
                        </div>
                    </div>
                )}
            </div>
            <div className={`${styles["modal"]} ${imageSrc && styles["active"]}`}>
                <div className={styles["buttons"]} onClick={handleClose}>
                    <div className={styles["close-button"]}>
                        <CloseIcon onClick={handleClose} />
                    </div>
                    <div className={styles["save-button"]}>
                        <button onClick={showCroppedImage}>تطبيق</button>
                    </div>
                </div>
                <Cropper
                    image={imageSrc}
                    crop={crop}
                    zoom={zoom}
                    aspect={1 / 1}
                    onCropChange={setCrop}
                    onCropComplete={onCropComplete}
                    onZoomChange={setZoom}
                    restrictPosition={false}
                    cropShape="round"
                    transform={`translate(${crop.x}px, ${crop.y}px) scale(${zoom})`}
                    cropSize={{
                        width: props.width > 500 ? 500 : props.width,
                        height: props.width > 500 ? 500 : props.width,
                    }}
                    onMediaLoaded={(mediaSize) => handleImageSize(mediaSize)}
                    classes={{
                        mediaClassName: imageClass == "tall" ? styles["tall"] : styles["wide"],
                        containerClassName: styles["container"],
                    }}
                />
                <div className={styles["zoom"]}>
                    <ZoomOutIcon onClick={decreaseZoom} />
                    <Slider
                        value={zoom}
                        min={1}
                        max={3}
                        step={0.1}
                        aria-labelledby="Zoom"
                        //classes={{ container: classes.slider }}
                        onChange={(e, zoom) => setZoom(zoom)}
                        className={styles["slider"]}
                    />
                    <ZoomInIcon onClick={increaseZoom} />
                </div>
            </div>
        </>
    )
}

function readFile(file) {
    return new Promise((resolve) => {
        const reader = new FileReader()
        reader.addEventListener("load", () => resolve(reader.result), false)
        reader.readAsDataURL(file)
    })
}
