import React, { Component } from 'react'
import styles from '../../scss/components/profile/profileNewsletters.module.scss'
import EditButton from '../buttons/editButton'
import NewsletterItem from '../newsletters/newsletterItem'

export default class ProfileNewsletters extends Component {

    constructor(props){
        super(props)
        this.handleEdit = this.handleEdit.bind(this)
        this.handleEditCancel = this.handleEditCancel.bind(this)
        this.state = {
            edit: false,
            checkedNewsletters: this.props.newsletters
        }
    }

    handleEdit(){
        this.setState({
            edit: true
        })
    }

    handleSave(){
        
    }

    handleEditCancel(){
        let temp_array = new Array(newsletters.length)
        temp_array.map((value, index) => {
            temp_array[index] = true
        })
        this.setState({
            edit: false,
            checkedNewsletters: temp_array
        })
    }

    handleCheckChange(id, state){
        console.log(id, state)
    }

    render() {
        const newsletters = this.props.newsletters
        return (
            <div className={styles['profile-newsletters']}>
                <div className={styles['title']}>النشرات الإخبارية الخاصة بك</div>
                {!this.state.edit ?  <EditButton className={styles['edit-button']} label="تعديل" icon handleClick={this.handleEdit} /> : null}
                <div className={styles['newsletters']}>
                    {newsletters.map((newsletter, index) => 
                        this.state.checkedNewsletters[index] ? <NewsletterItem handleCheckChange={this.handleCheckChange} className={styles['newsletter']} edit={this.state.edit} key={newsletter.id} newsletter={newsletter} /> : null
                    )}
                </div>
                {this.state.edit 
                ? <div className={styles['form-buttons']}>
                    <button className={styles['save']} type='submit'>حفظ</button>
                    <button className={styles['cancel']} onClick={this.handleEditCancel}>إلغاء</button>
                </div>
                : null}
            </div>
        )
    }
}
