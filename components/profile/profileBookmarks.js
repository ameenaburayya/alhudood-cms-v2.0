import React, { Component } from 'react'
import BookmarkIcon from '../icons/bookmarkIcon'
import PostBlock from '../blocks/postBlock'
import Button from '@material-ui/core/Button';
import styles from '../../scss/components/profile/profileBookmarks.module.scss'
import Snackbar from '@material-ui/core/Snackbar';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import ProfileBookmarkPost from './profileBookmarkPost';

export default class ProfileBookmarks extends Component {

    state = {
        posts: this.props.posts,
        newPosts: this.props.posts,
    }

    handleRemoveBookmark(id){
        const tempData = this.state.newPosts
        const newData = tempData.filter(post => post._id !== id)
        this.props.snackbarOpen('تم إزالة المقال من القائمة', 'normal', id, 'bookmark')
        this.setState({
            newPosts: newData,
            posts: tempData
        })
    }

    handleUndo(){
        this.setState({
            newPosts: this.state.posts
        })
    }

    componentDidMount(){
        this.setState({
            posts: this.props.posts,
            newPosts: this.props.posts
        })
    }

    

    render() {
            return (
                <>
                    <div className={styles['profile-bookmarks']}>
                        <div className={styles['heading']}>
                            <span>قائمة القراءة</span>
                        </div>
                        {this.state.newPosts.map((post, index) => 
                            <div className={styles['profile-bookmark']} key={post._id}>
                                <div className={styles['profile-bookmark-icon']} onClick={() => this.handleRemoveBookmark(post._id)}>
                                    <BookmarkIcon filled />
                                </div>
                                <ProfileBookmarkPost post={post} />
                            </div>
                        )}
                    </div>
                </>
            )
        
    }
}
