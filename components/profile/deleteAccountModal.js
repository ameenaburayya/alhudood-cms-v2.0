import React, {useState} from 'react'
import Modal from '@material-ui/core/Modal';

export default function DeleteAccountModal(props) {

    return (
        <Modal
            disableEnforceFocus
            disableAutoFocus
            open={props.modal}
            onClose={() => props.handleClose()}
            className={props.className}
        >
            {props.children}
        </Modal>
    )   
}
