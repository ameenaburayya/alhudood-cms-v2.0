import React, {useState} from 'react'
import CloseIcon from '../icons/closeIcon'
import DeleteAccountModal from './deleteAccountModal'
import styles from '../../scss/components/profile/profileDelete.module.scss'

export default function ProfileDelete() {

    const [modal, setModal] = useState(false)

    return (
        <div className={styles['delete-account']}>
            <p>لدى مسح حسابك في الحدود، سيتم تلقائياً شطب كل البيانات المرتبطة به، ولا يمكن عكس هذه العملية.</p>
            <div onClick={() => setModal(true)} className={styles['delete-button']}>
                <button>امسح الحساب</button>
            </div>

            <DeleteAccountModal className={styles['modal']} handleClose={() => setModal(false)} modal={modal}>
                <div className={styles['modal-inner']}>
                    <div className={styles['close']}>
                        <CloseIcon onClick={() => setModal(false)} />
                    </div>
                    <div className={styles['modal-content']}>
                        <p>هل أنت متأكد؟</p>
                        <div className={styles['buttons']}>
                            <div className={styles['yes']}>
                                <button>نعم</button>
                            </div>
                            <div className={styles['no']}>
                                <button onClick={() => setModal(false)}>لا</button>
                            </div>
                        </div>

                    </div>
                </div>
            </DeleteAccountModal>
        </div>
    )
}
