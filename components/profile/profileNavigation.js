import React, { Component } from 'react'
import styles from '../../scss/components/profile/profileNavigation.module.scss'
import { profileTabs } from '../../lib/constants'
import Snackbar from '@material-ui/core/Snackbar';
import CloseIcon from '@material-ui/icons/Close';
import snackbarStyles from '../../scss/components/snackbar.module.scss'
import IconButton from '@material-ui/core/IconButton';


export default class ProfileNavigation extends Component {


    constructor(props){
        super(props)
        profileTabs.map((tab, index) => {
            this[`tab_${index}`] = React.createRef()
        })    
        this.handleSnackbarClose = this.handleSnackbarClose.bind(this)
        this.parent = React.createRef()
        this.state = {
            snackbarOpen: false
        }
    }


    changeNavTab(index){
        if (!this.props.disabled){
            console.log('in')
            const tab_rect = this[`tab_${index}`].current.getBoundingClientRect()
            var parent_width = this.parent.current.getBoundingClientRect().width
            let scrollAmount = this.parent.current.scrollLeft
            this.parent.current.scrollLeft = tab_rect.right + scrollAmount - (parent_width / 2) - (tab_rect.width / 2)
        }
        else {
            this.props.snackbarOpen("الرجاء حفظ او الغاء التعديلات اولاً", 'error', new Date().getTime())
            // this.setState({
            //     snackbarOpen: true
            // })
        }
    }

    changeTab(index){
        this.props.changeTab(index)
        this.changeNavTab(index)
    }
    
    handleSnackbarClose(event, reason){
        if (reason === 'clickaway') {
            return;
        }
        this.setState({
            snackbarOpen: false
        })
    }


    render() {
        return (
            <>
                <div className={`${styles['tabs-navigation']} ${this.props.className || ''}`} ref={this.parent}>
                    <div className={styles['tabs-navigation-inner']}>
                        {profileTabs.map((type, index) =>
                            <div 
                                className={`${styles['tab-item']} ${this.props.activeTab == index ? styles['active'] : ""}`} 
                                onClick={() => this.changeTab(index)} 
                                key={type.slug}
                                ref={this[`tab_${index}`]}
                                >
                                
                                    <span>{type.title}</span>
                            </div>
                        )}
                    </div>
                </div>
                <Snackbar
                //key={this.state.snackbarId}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                autoHideDuration={3000}
                open={this.state.snackbarOpen}
                onClose={this.handleSnackbarClose}
                message={"الرجاء حفظ او الغاء التعديلات اولاً"}
                className={`${snackbarStyles['snackbar']} ${snackbarStyles['nav']}`}
                action={
                    <React.Fragment>
                        <IconButton
                            aria-label="close"
                            color="inherit"
                            //className={classes.close}
                            onClick={this.handleSnackbarClose}
                        >
                            <CloseIcon />
                        </IconButton>
                    </React.Fragment>
                }
                
            />
        </>
        )
    }
}
