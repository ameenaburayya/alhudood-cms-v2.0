import React from "react"
import styles from "../../scss/components/profile/profileAccountInfo.module.scss"
import EditButton from "../buttons/editButton"
import { checkUsername, checkEmail } from "../../requests/userApi"
import { FormControl, Input, InputLabel, Button } from "@material-ui/core"
import CircularProgress from "@material-ui/core/CircularProgress"
import InputAdornment from "@material-ui/core/InputAdornment"
import CheckIcon from "../icons/checkIcon"
import PasswordShowIcon from "../icons/passwordShowIcon"
import PasswordHideIcon from "../icons/passwordHideIcon"
import LineButton from "../buttons/lineButton"
import AlhudoodModal from "../alhudoodModal"
import ForgetPasswordForm from "../auth/forgetPasswordForm"
import SecondaryButton from "../buttons/secondaryButton"
import { editProfileInfo } from "../../requests/profileApi"

const validator = require("validator")

const e2a = (s) => s.replace(/\d/g, (d) => "٠١٢٣٤٥٦٧٨٩"[d])

export default class ProfileAccountInfo extends React.Component {
    constructor(props) {
        super(props)
        // this.validateAccountInfo = this.validateAccountInfo.bind(this)
        this.validateAccountInfo = this.validateAccountInfo.bind(this)
        this.showPassword = this.showPassword.bind(this)
        this.handleForgetPasswordClose = this.handleForgetPasswordClose.bind(this)
        this.handleForgetPasswordOpen = this.handleForgetPasswordOpen.bind(this)
        this.accountInfoSubmit = this.accountInfoSubmit.bind(this)
        this.state = {
            infoEdit: false,
            passwordEdit: false,
            forgetPassword: false,
            user: {
                firstName: "أمين",
                lastName: "",
                username: "امين_الاسطورة",
                email: "a.aburayya95@gmail.com",
                currentPassword: "",
                newPassword: "",
                newPasswordConfirm: "",
            },
            validation: {
                firstName: true,
                lastName: true,
                username: null,
                email: null,
                newPassword: true,
                newPasswordConfirm: true,
                currentPassword: true,
            },
            loading: {
                email: false,
                username: false,
            },
            hideCurrentPassword: true,
            hideNewPassword: true,
            formErrors: {
                firstName: {
                    notValid: "",
                    number: "",
                },
                lastName: {
                    notValid: "",
                    number: "",
                },
                username: {
                    empty: "",
                    alpha: "",
                    number: "",
                    available: "",
                },
                email: {
                    empty: "",
                    notValid: "",
                    available: "",
                },
                currentPassword: {
                    empty: "",
                },
                newPassword: {
                    empty: "",
                    notValid: "",
                    unique: "",
                },
                newPasswordConfirm: {
                    empty: "",
                    noMatch: "",
                },
            },
            accountInfoValid: true,
        }
    }

    handleEditButton(type) {
        if (type === "info") {
            this.setState({
                infoEdit: true,
            })
            this.props.handleInfoEdit("info", true)
        } else if (type === "password") {
            this.setState({
                passwordEdit: true,
            })
            this.props.handleInfoEdit("password", true)
        }
        this.props.checkSize()
    }

    handleEditCancel(type) {
        if (type === "info") {
            this.setState({
                infoEdit: false,
            })
            this.props.handleInfoEdit("info", false)
        } else if (type === "password") {
            this.setState({
                passwordEdit: false,
            })
            this.props.handleInfoEdit("password", false)
        }
        this.props.checkSize()
    }

    handleChange = (name) => (event) => {
        if (name == "username") {
            const newValue = e2a(event.target.value)
            event.target.value = newValue
        }
        const user = this.state.user
        user[name] = event.target.value
        this.setState({
            user,
        })
    }

    validateAccountInfoField(fieldName, user) {
        let fieldValidationErrors = this.state.formErrors
        let validation = this.state.validation
        switch (fieldName) {
            case "username":
                if (!user || typeof user.username !== "string" || user.username.trim().length === 0) {
                    fieldValidationErrors.username.number = null
                    fieldValidationErrors.username.alpha = null
                    fieldValidationErrors.username.available = null
                    fieldValidationErrors.username.empty = "يرجى ملء اسم المستخدم"
                    validation.username = false
                } else if (user.username.trim().length < 5 || user.username.trim().length > 16) {
                    if (!validator.isAlpha(user.username, ["ar"], { ignore: "_٠١٢٣٤٥٦٧٨٩" })) {
                        fieldValidationErrors.username.empty = null
                        fieldValidationErrors.username.available = null
                        fieldValidationErrors.username.number = "اسم المستخدم يجب أن يكون بين 5 و16 حرفاً"
                        fieldValidationErrors.username.alpha =
                            "اسم المستخدم يجب ان يتكون من الأحرف العربية، الأرقام أو الشَرطة (_) فقط"
                        validation.username = false
                    } else {
                        fieldValidationErrors.username.alpha = null
                        fieldValidationErrors.username.empty = null
                        fieldValidationErrors.username.available = null
                        fieldValidationErrors.username.number = "اسم المستخدم يجب أن يكون بين 5 و16 حرفاً"
                        validation.username = false
                    }
                } else if (!validator.isAlpha(user.username, ["ar"], { ignore: "_٠١٢٣٤٥٦٧٨٩" })) {
                    fieldValidationErrors.username.empty = null
                    fieldValidationErrors.username.number = null
                    fieldValidationErrors.username.available = null
                    fieldValidationErrors.username.alpha =
                        "اسم المستخدم غير صحيح: يرجى استخدام الأحرف العربية، الأرقام أو الشَرطة (_) فقط"
                    validation.username = false
                } else {
                    this.setState({ loading: { username: true } })
                    checkUsername({
                        username: user.username,
                    }).then((res) => {
                        if (res.status_code == 100) {
                            validation.username = true
                            fieldValidationErrors.username = {
                                empty: "",
                                number: "",
                                alpha: "",
                                available: "",
                            }
                        } else if (res.status_code == 510) {
                            ;(validation.username = false), (fieldValidationErrors.username.empty = null)
                            fieldValidationErrors.username.number = null
                            fieldValidationErrors.username.alpha = null
                            fieldValidationErrors.username.available = "سبقك عضوٌ آخر وأخذ اسم المستخدم"
                        } else {
                            ;(validation.username = false), (fieldValidationErrors.username.empty = null)
                            fieldValidationErrors.username.number = null
                            fieldValidationErrors.username.alpha = null
                            fieldValidationErrors.username.available =
                                "هناك خطأ لم نعرف سببه. يرجى المحاولة لاحقاً أو التواصل معنا على suppport@alhudood.net"
                        }
                        this.setState(
                            {
                                formErrors: fieldValidationErrors,
                                validation: validation,
                                loading: { username: false },
                            },
                            () => {
                                this.validateAccountInfo()
                            }
                        )
                        console.log(res)
                    })
                }
                break
            case "email":
                if (!user || typeof user.email !== "string" || user.email.trim().length === 0) {
                    fieldValidationErrors.email.notValid = ""
                    fieldValidationErrors.email.available = ""
                    fieldValidationErrors.email.empty = "يرجى ملء البريد الإلكتروني"
                    validation.email = false
                } else if (!validator.isEmail(user.email)) {
                    fieldValidationErrors.email.empty = ""
                    fieldValidationErrors.email.available = ""
                    fieldValidationErrors.email.notValid = "البريد الإلكتروني غير صحيح"
                    validation.email = false
                } else {
                    this.setState({ loading: { email: true } })
                    checkEmail({
                        email: user.email,
                    }).then((res) => {
                        if (res.status_code == 100) {
                            validation.email = true
                            fieldValidationErrors.email = {
                                empty: "",
                                notValid: "",
                                available: "",
                            }
                        } else if (res.status_code == 510) {
                            ;(validation.email = false), console.log("iam in")
                            fieldValidationErrors.email.empty = ""
                            fieldValidationErrors.email.notValid = ""
                            fieldValidationErrors.email.available = "البريد الإلكتروني المُدخل مستخدم من قبل."
                        } else {
                            ;(validation.email = false), (fieldValidationErrors.email.empty = "")
                            fieldValidationErrors.email.notValid = ""
                            fieldValidationErrors.email.available =
                                "هناك خطأ لم نعرف سببه. يرجى المحاولة لاحقاً أو التواصل معنا على suppport@alhudood.net"
                        }
                        this.setState(
                            {
                                formErrors: fieldValidationErrors,
                                validation,
                                loading: { email: false },
                            },
                            () => {
                                this.validateAccountInfo()
                            }
                        )
                    })
                }
                break
            case "firstName":
                if (user.firstName.trim().length > 0 && !validator.isAlpha(user.firstName, ["ar"], { ignore: " " })) {
                    fieldValidationErrors.firstName.number = ""
                    fieldValidationErrors.firstName.notValid = "الرجاء استخدام أحرف عربية."
                    if (user.firstName.trim().length > 20) {
                        fieldValidationErrors.firstName.number = "الاسم الأول لا يجب أن يزيد عن ٢٠ حرفاً"
                    }
                    validation.firstName = false
                } else if (user.firstName.trim().length > 20) {
                    fieldValidationErrors.firstName.notValid = ""
                    fieldValidationErrors.firstName.number = "الاسم الأول لا يجب أن يزيد عن ٢٠ حرفاً"
                    validation.firstName = false
                } else {
                    fieldValidationErrors.firstName.number = ""
                    fieldValidationErrors.firstName.notValid = ""
                    validation.firstName = true
                }
                break
            case "lastName":
                if (user.lastName.trim().length > 0 && !validator.isAlpha(user.lastName, ["ar"], { ignore: " " })) {
                    fieldValidationErrors.lastName.number = ""
                    fieldValidationErrors.lastName.notValid = "الرجاء استخدام أحرف عربية."
                    if (user.lastName.trim().length > 20) {
                        fieldValidationErrors.lastName.number = "الاسم الأول لا يجب أن يزيد عن ٢٠ حرفاً"
                    }
                    validation.lastName = false
                } else if (user.lastName.trim().length > 20) {
                    fieldValidationErrors.lastName.notValid = ""
                    fieldValidationErrors.lastName.number = "الاسم الأول لا يجب أن يزيد عن ٢٠ حرفاً"
                    validation.lastName = false
                } else {
                    fieldValidationErrors.lastName.number = ""
                    fieldValidationErrors.lastName.notValid = ""
                    validation.lastName = true
                }
                break
        }
        this.setState(
            {
                formErrors: fieldValidationErrors,
                validation,
            },
            () => {
                this.validateAccountInfo()
            }
        )
    }

    validatePassword(fieldName, user) {
        let fieldValidationErrors = this.state.formErrors
        let { currentPasswordValid, newPasswordConfirmValid, newPasswordValid } = this.state
        switch (fieldName) {
            case "currentPassword":
                if (!user || typeof user.currentPassword !== "string" || user.currentPassword.trim().length === 0) {
                    fieldValidationErrors.currentPassword.empty = "يرجى ملء كلمة السر"
                    currentPasswordValid = false
                } else {
                    currentPasswordValid = true
                    fieldValidationErrors.currentPassword = {
                        empty: "",
                    }
                }

                if (user.newPassword && user.newPassword == user.currentPassword) {
                    fieldValidationErrors.newPassword.unique = "لا يُسمح باستخدام كلمة السر القديمة"
                    newPasswordValid = false
                } else if (user.newPassword && user.newPassword != user.currentPassword) {
                    fieldValidationErrors.newPassword.unique = ""
                    newPasswordValid = true
                }
                break
            case "newPassword":
                if (!user || typeof user.newPassword !== "string" || user.newPassword.trim().length === 0) {
                    fieldValidationErrors.newPassword.notValid = ""
                    fieldValidationErrors.newPassword.unique = ""
                    fieldValidationErrors.newPassword.empty = "يرجى ملء كلمة السر"
                    newPasswordValid = false
                } else if (!validator.isStrongPassword(user.newPassword, { minSymbols: 0 })) {
                    fieldValidationErrors.newPassword.empty = ""
                    fieldValidationErrors.newPassword.unique = ""
                    fieldValidationErrors.newPassword.notValid =
                        "كلمة السر لا تلبي الشروط: ثمانية خانات باللغة الإنجليزية بحروف (كبيرة وصغيرة) وأرقام"
                    if (user.newPasswordConfirm && user.newPassword != user.newPasswordConfirm) {
                        fieldValidationErrors.newPasswordConfirm.noMatch = "كلمتا السر غير متطابقتين"
                        newPasswordConfirmValid = false
                    } else if (user.newPasswordConfirm && user.newPassword == user.newPasswordConfirm) {
                        fieldValidationErrors.newPasswordConfirm.noMatch = ""
                        newPasswordConfirmValid = true
                    }
                    newPasswordValid = false
                } else if (user.currentPassword && user.newPassword == user.currentPassword) {
                    fieldValidationErrors.newPassword.notValid = ""
                    fieldValidationErrors.newPassword.empty = ""
                    fieldValidationErrors.newPassword.unique = "لا يُسمح باستخدام كلمة السر القديمة"
                    newPasswordValid = false
                } else {
                    newPasswordValid = true
                    fieldValidationErrors.newPassword = {
                        empty: "",
                        notValid: "",
                        unique: "",
                    }
                }

                if (user.newPasswordConfirm && user.newPassword != user.newPasswordConfirm) {
                    fieldValidationErrors.newPasswordConfirm.noMatch = "كلمتا السر غير متطابقتين"
                    newPasswordConfirmValid = false
                } else if (user.newPasswordConfirm && user.newPassword == user.newPasswordConfirm) {
                    fieldValidationErrors.newPasswordConfirm.noMatch = ""
                    newPasswordConfirmValid = true
                }
                break
            case "newPasswordConfirm":
                if (!user || user.newPassword) {
                    if (user.newPasswordConfirm.trim().length === 0) {
                        fieldValidationErrors.newPasswordConfirm.noMatch = ""
                        fieldValidationErrors.newPasswordConfirm.empty = "يرجى تأكيد كلمة السر"
                        newPasswordConfirmValid = false
                    } else if (user.newPassword != user.newPasswordConfirm) {
                        fieldValidationErrors.newPasswordConfirm.noMatch = "كلمتا السر غير متطابقتين"
                        fieldValidationErrors.newPasswordConfirm.empty = ""
                        newPasswordConfirmValid = false
                    } else {
                        newPasswordConfirmValid = true
                        fieldValidationErrors.newPasswordConfirm = {
                            empty: "",
                            noMatch: "",
                        }
                    }
                }
            default:
                break
        }
        this.setState(
            {
                formErrors: fieldValidationErrors,
                currentPasswordValid,
                newPasswordValid,
                newPasswordConfirmValid,
            },
            () => {
                this.validatePasswordForm()
            }
        )
    }

    handleBlur = (name) => (event) => {
        this.validateAccountInfoField(name, this.state.user)
    }

    handlePasswordBlur = (name) => (event) => {
        this.validatePassword(name, this.state.user)
    }

    validateAccountInfo() {
        this.setState({
            accountInfoValid:
                this.state.emailValid &&
                this.state.usernameValid &&
                this.state.firstNameValid &&
                this.state.lastNameValid,
        })
    }
    validatePasswordForm() {
        console.log("password")
    }

    showPassword(type) {
        if (type == "current") {
            this.setState((prevState) => ({ hideCurrentPassword: !prevState.hideCurrentPassword }))
        } else if (type == "new") {
            this.setState((prevState) => ({ hideNewPassword: !prevState.hideNewPassword }))
        }
    }

    handleForgetPasswordOpen(e) {
        e.preventDefault()
        this.setState({
            forgetPassword: true,
        })
    }

    handleForgetPasswordClose() {
        console.log("ameen")
        this.setState({
            forgetPassword: false,
        })
    }

    formSubmit() {
        console.log("testttttttt")
    }

    accountInfoSubmit(e) {
        e.preventDefault()
        editProfileInfo("ameen", "aburayya", "admin1", "admin@alhudood.net", "").then((res) => {
            console.log("edited ", res)
        })
        this.props.snackbarOpen("تم حفظ المعلومات بنجاح", "success", new Date().getTime())
    }

    render() {
        return (
            <>
                <div className={styles["account-info"]}>
                    <div className={`${styles["info"]} ${this.state.infoEdit && styles["active"]}`}>
                        {!this.state.infoEdit ? (
                            <EditButton handleClick={() => this.handleEditButton("info")} icon label="تعديل" />
                        ) : null}
                        <div className={`${styles["info-inner"]} ${!this.state.infoEdit ? styles["disabled"] : ""}`}>
                            <form onSubmit={() => this.formSubmit}>
                                <FormControl className={styles["info-element"]} required fullWidth margin="normal">
                                    <label>الاسم الأول</label>
                                    <Input
                                        placeholder={!this.state.user.firstName && !this.state.infoEdit ? "فارغ" : null}
                                        name="first name"
                                        type="text"
                                        autoComplete="first name"
                                        disableUnderline
                                        onChange={this.handleChange("firstName")}
                                        className={`${styles["field"]} ${
                                            !this.state.validation.firstName ? styles["error"] : ""
                                        }`}
                                        onBlur={this.handleBlur("firstName")}
                                        disabled={!this.state.infoEdit}
                                        value={this.state.user.firstName}
                                    />
                                    {this.state.infoEdit && (
                                        <div className={styles["field-errors"]}>
                                            {Object.values(this.state.formErrors.firstName).map((errorType) => {
                                                if (errorType && errorType.length > 0) {
                                                    return <div className={styles["error"]}> - {errorType}</div>
                                                }
                                            })}
                                        </div>
                                    )}
                                </FormControl>
                                <FormControl className={styles["info-element"]} required fullWidth margin="normal">
                                    <label>الاسم الأخير</label>
                                    <Input
                                        placeholder={!this.state.user.lastName && !this.state.infoEdit ? "فارغ" : null}
                                        name="last name"
                                        type="text"
                                        autoComplete="last name"
                                        disableUnderline
                                        onChange={this.handleChange("lastName")}
                                        className={`${styles["field"]} ${
                                            !this.state.validation.lastName ? styles["error"] : ""
                                        }`}
                                        onBlur={this.handleBlur("lastName")}
                                        disabled={!this.state.infoEdit}
                                        value={this.state.user.lastName}
                                    />
                                    {this.state.infoEdit && (
                                        <div className={styles["field-errors"]}>
                                            {Object.values(this.state.formErrors.lastName).map((errorType) => {
                                                if (errorType && errorType.length > 0) {
                                                    return <div className={styles["error"]}> - {errorType}</div>
                                                }
                                            })}
                                        </div>
                                    )}
                                </FormControl>
                                <FormControl className={styles["info-element"]} required fullWidth margin="normal">
                                    <label>اسم المستخدم</label>
                                    <Input
                                        placeholder={!this.state.user.username && !this.state.infoEdit ? "فارغ" : null}
                                        name="username"
                                        type="text"
                                        autoComplete="username"
                                        disableUnderline
                                        onChange={this.handleChange("username")}
                                        className={`${styles["field"]} ${
                                            this.state.validation.username != null && !this.state.validation.username
                                                ? styles["error"]
                                                : ""
                                        }`}
                                        onBlur={this.handleBlur("username")}
                                        disabled={!this.state.infoEdit}
                                        value={this.state.user.username}
                                        endAdornment={
                                            this.state.loading.username ? (
                                                <CircularProgress className={styles["progress"]} />
                                            ) : this.state.validation.username ? (
                                                <CheckIcon />
                                            ) : null
                                        }
                                    />
                                    {this.state.infoEdit && (
                                        <div className={styles["field-errors"]}>
                                            {Object.values(this.state.formErrors.username).map((errorType) => {
                                                if (errorType && errorType.length > 0) {
                                                    return <div className={styles["error"]}> - {errorType}</div>
                                                }
                                            })}
                                        </div>
                                    )}
                                </FormControl>
                                <FormControl className={styles["info-element"]} required fullWidth margin="normal">
                                    <label>البريد الإلكتروني</label>
                                    <Input
                                        placeholder={!this.state.user.email && !this.state.infoEdit ? "فارغ" : null}
                                        name="email"
                                        type="email"
                                        autoComplete="email"
                                        disableUnderline
                                        onChange={this.handleChange("email")}
                                        className={`${styles["field"]} ${
                                            this.state.validation.email != null && !this.state.validation.email
                                                ? styles["error"]
                                                : ""
                                        }`}
                                        onBlur={this.handleBlur("email")}
                                        disabled={!this.state.infoEdit}
                                        value={this.state.user.email}
                                        endAdornment={
                                            this.state.loading.email ? (
                                                <CircularProgress className={styles["progress"]} />
                                            ) : this.state.validation.email ? (
                                                <CheckIcon />
                                            ) : null
                                        }
                                    />
                                    {this.state.infoEdit && (
                                        <div className={styles["field-errors"]}>
                                            {Object.values(this.state.formErrors.email).map((errorType) => {
                                                if (errorType && errorType.length > 0) {
                                                    return <div className={styles["error"]}>{errorType}</div>
                                                }
                                            })}
                                        </div>
                                    )}
                                </FormControl>
                                {this.state.infoEdit ? (
                                    <div className={styles["form-buttons"]}>
                                        <SecondaryButton onClick={this.accountInfoSubmit} label="حفظ" type="submit" />
                                        <button
                                            className={styles["cancel"]}
                                            onClick={() => this.handleEditCancel("info")}
                                        >
                                            إلغاء
                                        </button>
                                    </div>
                                ) : null}
                            </form>
                        </div>
                    </div>
                    <div
                        className={`${styles["info"]} ${styles["password"]} ${
                            this.state.passwordEdit && styles["active"]
                        }`}
                    >
                        {!this.state.passwordEdit ? (
                            <div className={styles["edit-button"]} onClick={() => this.handleEditButton("password")}>
                                <span>تغيير كلمة السر</span>
                            </div>
                        ) : null}
                        <div
                            className={`${styles["info-inner"]} ${!this.state.passwordEdit ? styles["disabled"] : ""}`}
                        >
                            {this.state.passwordEdit ? (
                                <form onSubmit={(e) => e.preventDefault()}>
                                    <FormControl className={`${styles["info-element"]} ${styles["half"]}`}>
                                        <label>كلمة السر</label>
                                        <Input
                                            placeholder="كلمة السر الحالية"
                                            name="password"
                                            type={this.state.hideCurrentPassword ? "password" : "text"}
                                            autoComplete="current-password"
                                            disableUnderline
                                            onChange={this.handleChange("currentPassword")}
                                            onBlur={this.handlePasswordBlur("currentPassword")}
                                            className={`${styles["field"]} ${
                                                !this.state.validation.currentPassword ? styles["error"] : ""
                                            }`}
                                            endAdornment={
                                                this.state.hideCurrentPassword ? (
                                                    <InputAdornment position="end">
                                                        <PasswordShowIcon
                                                            fontSize="default"
                                                            onClick={() => this.showPassword("current")}
                                                        />
                                                    </InputAdornment>
                                                ) : (
                                                    <InputAdornment position="end">
                                                        <PasswordHideIcon
                                                            fontSize="default"
                                                            onClick={() => this.showPassword("current")}
                                                        />
                                                    </InputAdornment>
                                                )
                                            }
                                        />
                                        <LineButton
                                            className={styles["forget-password"]}
                                            onClick={this.handleForgetPasswordOpen}
                                            label="هل نسيت كلمة السر؟"
                                        />
                                        {this.state.passwordEdit && (
                                            <div className={styles["field-errors"]}>
                                                {Object.values(this.state.formErrors.currentPassword).map(
                                                    (errorType) => {
                                                        if (errorType && errorType.length > 0) {
                                                            return <div className={styles["error"]}> - {errorType}</div>
                                                        }
                                                    }
                                                )}
                                            </div>
                                        )}
                                    </FormControl>
                                    <FormControl className={`${styles["info-element"]} ${styles["half"]}`}>
                                        <Input
                                            placeholder="كلمة السر الجديدة"
                                            name="password"
                                            type={this.state.hideNewPassword ? "password" : "text"}
                                            autoComplete="new-password"
                                            disableUnderline
                                            onChange={this.handleChange("newPassword")}
                                            onBlur={this.handlePasswordBlur("newPassword")}
                                            className={`${styles["field"]} ${styles["withIcon"]} ${
                                                !this.state.validation.newPassword ? styles["error"] : ""
                                            }`}
                                            endAdornment={
                                                this.state.hideNewPassword ? (
                                                    <InputAdornment position="end">
                                                        <PasswordShowIcon
                                                            fontSize="default"
                                                            onClick={() => this.showPassword("new")}
                                                        />
                                                    </InputAdornment>
                                                ) : (
                                                    <InputAdornment position="end">
                                                        <PasswordHideIcon
                                                            fontSize="default"
                                                            onClick={() => this.showPassword("new")}
                                                        />
                                                    </InputAdornment>
                                                )
                                            }
                                        />
                                        {this.state.passwordEdit && (
                                            <div className={styles["field-errors"]}>
                                                {Object.values(this.state.formErrors.newPassword).map((errorType) => {
                                                    if (errorType && errorType.length > 0) {
                                                        return <div className={styles["error"]}> - {errorType}</div>
                                                    }
                                                })}
                                            </div>
                                        )}
                                    </FormControl>
                                    <FormControl className={`${styles["info-element"]} ${styles["half"]}`}>
                                        <Input
                                            placeholder="تأكيد كلمة السر الجديدة"
                                            name="password"
                                            type={this.state.hideNewPassword ? "password" : "text"}
                                            autoComplete="new-password"
                                            disableUnderline
                                            onChange={this.handleChange("newPasswordConfirm")}
                                            onBlur={this.handlePasswordBlur("newPasswordConfirm")}
                                            className={`${styles["field"]} ${
                                                !this.state.validation.newPasswordConfirm ? styles["error"] : ""
                                            }`}
                                        />
                                        {this.state.passwordEdit && (
                                            <div className={styles["field-errors"]}>
                                                {Object.values(this.state.formErrors.newPasswordConfirm).map(
                                                    (errorType) => {
                                                        if (errorType && errorType.length > 0) {
                                                            return <div className={styles["error"]}> - {errorType}</div>
                                                        }
                                                    }
                                                )}
                                            </div>
                                        )}
                                    </FormControl>
                                    {this.state.passwordEdit ? (
                                        <div className={styles["form-buttons"]}>
                                            <SecondaryButton label="حفظ" type="submit" />
                                            <button
                                                className={styles["cancel"]}
                                                onClick={() => this.handleEditCancel("password")}
                                            >
                                                إلغاء
                                            </button>
                                        </div>
                                    ) : null}
                                </form>
                            ) : (
                                <div className={`${styles["info-element"]} ${styles["fake-password"]}`}>
                                    <label>كلمة السر</label>
                                    <div className={styles["field"]}>⬤⬤⬤⬤⬤⬤⬤⬤</div>
                                </div>
                            )}
                        </div>
                    </div>
                </div>

                <AlhudoodModal handleClose={this.handleForgetPasswordClose} modalOpen={this.state.forgetPassword}>
                    <ForgetPasswordForm modal profile />
                </AlhudoodModal>
            </>
        )
    }
}
