import React from 'react'
import ArrowButton from '../buttons/arrowButton'
import styles from '../../scss/components/profile/profileEmptySection.module.scss'

export default function ProfileEmptySection({arrowLabel, text, icon, section}) {
    return (
        <div className={`${styles['profile-empty']} ${section ? styles[`${section}`] : ''}`}>
            <div className={styles['icon']}>
                {icon}
            </div>
            <div className={styles['content']}>
                <div className={styles['label']} dangerouslySetInnerHTML={{__html: `${text}`}} />
                <ArrowButton href="/" label={arrowLabel} className={styles['button']} />
            </div>
        </div>
    )
}
