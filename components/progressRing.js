import React from 'react'
import PausedIcon from './icons/pauseIcon'
import PlayIcon from './icons/playIcon'
import CircularProgress from '@material-ui/core/CircularProgress';
import Box from '@material-ui/core/Box';


export default function ProgressRing(props) {
    const {autoplayTime, timerTime, handlePause, handlePlay, isPaused, handleClick} = props

    console.log('inside', timerTime)

    return (
        <Box position="relative" display="inline-flex">
            <CircularProgress variant="determinate" value={(1 - ((timerTime / autoplayTime) - 1)) * 100} />
            <Box
                top={0}
                left={0}
                bottom={0}
                right={0}
                position="absolute"
                display="flex"
                alignItems="center"
                justifyContent="center"
                onClick={handleClick}
            >
                {isPaused ? <PlayIcon /> : <PausedIcon />}
                
            </Box>
        </Box>
    )
}










