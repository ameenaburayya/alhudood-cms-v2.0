import Footer from '../../includes/footer/footer'
import Header from '../../includes/header/header'
import Head from 'next/head'
import React from 'react'
import {getImageUrl} from '@takeshape/routing'
import { contentTypes } from '../../lib/constants'
import styles from '../../scss/components/layouts/postLayout.module.scss'


export default function PostLayout({ children, post, pageTitle, empty }) {
    const postTitle = pageTitle ? pageTitle : post.title
    const tempTitle = empty ? 'شبكة الحدود' : postTitle
    const postDescription = post ? post.excerpt : null
    const postImage = post ? getImageUrl(post.featuredImage.path) : null
    const postId = post ? post.id : null
    const postCategory = post ? post.category.slug : null
    const postContentType = post ? post.contentType.slug : null
    
    return (
        <>
            <Head>
                    <title>{`${tempTitle}`}</title>
                    
                    <meta name="description" content={postDescription} />

                    <meta property="og:title" content={`${postTitle}`} />
                    <meta property="og:description" content={postDescription} />
                    <meta property="og:image" content={postImage} />
                    <meta property="og:url" content={`https://alhudood.net/${postCategory}/${postContentType}/${postId}`} />

                    <meta property="og:type" content="article" />
                    <meta property="article:publisher" content="https://facebook.com/AlHudoodNet/" />

                    
                    <meta name="twitter:title" content={postTitle} />
                    <meta name="twitter:description" content={postDescription} />
                    <meta name="twitter:image" content={postImage} />

                    
                    <meta name="keywords" content="Hong Kong’s government, Hong Kong’s primary, city’s legislative-council elections, Tiffany Yuen, eclectic array of prodemocracy activists, last day of February, Hong Kong, popular mandate of Eddie Chu, prodemocracy protests, law enforcement, numerous defendants, city’s democratic spirit, Hong Kong’s opposition, police report, bail proceedings, YouTube channel, cabin-crew union, next day, violation of the sweeping national-security law, new lawmakers, events of early March, political show trial, Hong Kong’s electoral system, alleged crime, democracy movement, cabin crew, prodemocracy movement, coming days, Wahsung Yau, unofficial primary election, Hong Kongers, British Airways, labor leaders, months of protests, time officials, media interviews, recent days, Chu’s friend Matthew Chapple, political hopefuls, first day of March, others, boisterous Hong Kong, pest control, rubber-stamp vote, former journalist, city's leader, Cardinal Joseph Zen, traffic flow, following year, jail time" itemid="#keywords" />
                    <meta name="news_keywords" content="Hong Kong’s government, Hong Kong’s primary, city’s legislative-council elections, Tiffany Yuen, eclectic array of prodemocracy activists, last day of February, Hong Kong, popular mandate of Eddie Chu, prodemocracy protests, law enforcement, numerous defendants, city’s democratic spirit, Hong Kong’s opposition, police report, bail proceedings, YouTube channel, cabin-crew union, next day, violation of the sweeping national-security law, new lawmakers, events of early March, political show trial, Hong Kong’s electoral system, alleged crime, democracy movement, cabin crew, prodemocracy movement, coming days, Wahsung Yau, unofficial primary election, Hong Kongers, British Airways, labor leaders, months of protests, time officials, media interviews, recent days, Chu’s friend Matthew Chapple, political hopefuls, first day of March, others, boisterous Hong Kong, pest control, rubber-stamp vote, former journalist, city's leader, Cardinal Joseph Zen, traffic flow, following year, jail time"></meta>


            </Head>
            <Header />
            <main className={styles['post']}>{children}</main>
            {post && post.contentType.slug != `${contentTypes.dictionary.slug}` ? <Footer /> : null }
        </>
    )
}
