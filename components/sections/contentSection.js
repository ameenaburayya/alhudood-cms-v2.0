import React from 'react'
import PostBlock from '../blocks/postBlock'
import MainHeading from '../headings/mainHeading'
import styles from '../../scss/components/sections/contentSection.module.scss'
import ArrowButton from '../buttons/arrowButton'
import PostsBlock from '../blocks/postsBlock'

export default function ContentSection(props) {
    return (
        <div className={`${styles[`${props.layout}`]} ${props.className ? props.className : ''} ${styles['content-blocks']}`}>
            {props.category && !props.noHeading && <div className={styles['category-name']}>
                <MainHeading withDash href={`category/${props.category.slug}`}>
                    {props.category.title}
                </MainHeading>
            </div>}
                <PostsBlock layout={props.layout} posts={props.posts} withTags={props.withTags ? true : false } />
                {/* {props.posts.map((post, index) => 
                    <PostBlock white={props.white} key={index}  withExcerpt layout={props.layout} post={post} />
                )} */}
            {props.category && <div className={styles['more']}>
                <ArrowButton small white={props.white} label="اقرأ المزيد" category={props.category} />
            </div>}
        </div>
    )
}
