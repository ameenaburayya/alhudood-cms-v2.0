import React, { Component } from 'react'
import { getImageUrl } from '@takeshape/routing'
import MainHeading from '../headings/mainHeading'
import styles from '../../scss/components/sections/visualsSection.module.scss'
import VisualsAuthorIcon from '../icons/visualsAuthorIcon'
import VisualsTitleIcon from '../icons/visualsTitleIcon'
import { contentTypes } from '../../lib/constants'
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/swiper-bundle.css';
import SwiperCore, { Mousewheel } from 'swiper';

SwiperCore.use([Mousewheel]);



export default class VisualsSection extends Component {


    constructor(){
        super()
        this.getImgSize = this.getImgSize.bind(this)
        this.getSize = this.getSize.bind(this)
        this.updateWidth = this.updateWidth.bind(this)
        this.state = {
            expanded: false,
            image_status: null
        }
    }


    getSize(image){
        return new Promise((resolve, reject) => {
            var newImg = new Image();
            console.log(newImg)
            newImg.onload = () => {
                var height = newImg.height;
                var width = newImg.width
                var aspect = width / height
                var info = {
                    width,
                    aspect
                }
                resolve(info)
            }
            newImg.onerror = reject
            newImg.src = getImageUrl(image); // this must be done AFTER setting onload
        })
    }

    async getImgSize(images) {
        console.log('hello', await this.getSize(images[0].asset.path))
        var heights = []
        for (var i = 0 ; i < images.length; i++){
            var temp = await this.getSize(images[i].asset.path)
            console.log(temp)
            if (temp.aspect > 1){
                this.setState({comicMode: "scroll"})
                break
            }
            else if(temp.aspect <= 1){
                heights[i] = (window.innerWidth >= 1024 ? window.innerWidth * 0.5 * 0.65 : window.innerWidth * 0.65 ) / temp.aspect
                console.log(heights)
            }
        }

        var target_height = Math.min(...heights)
        document.documentElement.style.setProperty("--comic-slider-height", (target_height + 'px')); 

    }


    updateWidth(){
        this.setState({windowWidth: window.innerWidth})
    }


    componentDidMount(){
        this.updateWidth();
        window.addEventListener('resize', this.updateWidth);
        this.props.post.contentType.slug == "comic" && this.props.post.comic.images.length > 1 && this.getImgSize(this.props.post.comic.images)
    }

    render(){
        return (
            <div className={this.props.className || ' '}>
                <div className={styles['inner']}>
                    <div className={styles['contentType-name']}>
                        <MainHeading>
                            {this.props.post.contentType.title}
                        </MainHeading>
                        <VisualsTitleIcon />
                    </div>
                    <div className={styles['content']}>
                        {(() => {
                            if(this.props.post.contentType.slug === contentTypes.caricature.slug){
                                return <img src={getImageUrl(this.props.post.featuredImage.path)} />
                            }
                            if(this.props.post.contentType.slug === contentTypes.comic.slug ){
                                if (this.props.post.comic.images.length > 1 ){
                                    if(this.state.comicMode == "scroll"){
                                        return(
                                            <div className={styles['images']}>
                                                {this.props.post.comic.images?.map((image, index) =>
                                                    <div key={index} className={styles['scroll-image']}>
                                                        <figure>
                                                            <img src={getImageUrl(image.asset.path)} />
                                                        </figure>
                                                    </div>
                                                )}
                                            </div>
                                            
                                        )
                                    }
                                    else{
                                        return(
                                            <Swiper
                                            spaceBetween={10}
                                            //initialSlide={initialSlide}
                                            slidesPerView='auto'
                                            mousewheel={true}
                                            loop={this.state.windowWidth >= 1024 ? true : false}
                                            className={styles['slider']}
                                            //onSlideChange={(swiper) => changeOnSwipe(swiper.activeIndex)}
                                            //onSwiper={(swiper) => setSwiper(swiper)}
                                            >
                                                {this.props.post.comic.images?.map((image, index) => 
                                                    <SwiperSlide className={styles['slide']} key={index}>
                                                        <img src={getImageUrl(image.asset.path)} />
                                                    </SwiperSlide>
                                                )}
                                            </Swiper>
                                        )
                                    }
                                }
                                else if (this.props.post.comic.images.length == 1 ){
                                    return(
                                        <div className={styles['wide-image']}>
                                            <figure>
                                                <img src={getImageUrl(this.props.post.comic.images[0].asset.path)} />
                                            </figure>
                                        </div>
                                    )
                                }
                            }
                            //else return null
                            
                        })()}
                    </div>
                    <div className={styles['author']}>
                        <span>{this.props.post.caricatureAuthor} <VisualsAuthorIcon /></span>
                    </div>
                </div>
            </div>
        )
        if (this.props.post.contentType.slug === contentTypes.caricature.slug){
            
        }
        if (this.props.post.contentType.slug === contentTypes.comic.slug){
            return (
                <div className={this.props.className || ' '}>
                    <div className={styles['inner']}>
                        <div className={styles['contentType-name']}>
                            <MainHeading>
                                {this.props.post.contentType.title}
                            </MainHeading>
                            <VisualsTitleIcon />
                        </div>
                        <div className={styles['content']}>
                            <img src={getImageUrl(this.props.post.featuredImage.path)} />
                        </div>
                        <div className={styles['author']}>
                            <span>{this.props.post.caricatureAuthor} <VisualsAuthorIcon /></span>
                        </div>
                    </div>
                </div>
            )
        }
        
        return null
        
    }
}
