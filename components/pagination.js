import React from 'react'
import styles from '../scss/components/pagination.module.scss'
import ArrowDownIcon from './icons/arrowDownIcon'

export default function Pagination({total, from, size, handleNext, handlePrev, className}) {

    const limit = total - size

    return (
        <div className={`${styles['pagination']} ${className ? className : ''}`}>
            {from !== 0 &&  <div onClick={(e) => handlePrev(e)} className={`${styles['item']} ${styles['prev']}`}>
                <span>السابق</span>
                <ArrowDownIcon />
            </div>}
            {from < limit && <div onClick={(e) => handleNext(e)} className={`${styles['item']} ${styles['next']}`}>
                <span>التالي</span>
                <ArrowDownIcon />
            </div>}
        </div>
    )
}
