import Link from 'next/link'
import React from 'react'
import { categories, contentTypes } from '../../lib/constants'
import styles from '../../scss/components/homePage/aaajSection.module.scss'
import AAAJLogo from '../logos/aaajLogo'
import ContentSection from '../sections/contentSection'

export default function AAAJSection(props) {

    const {windowWidth, posts, category} = props


    return (
        <section className={`${styles['aaaj-section']} ${props.className ? props.className : ''}`}>
            <div className={styles['header']}>
                <Link href={`/category/${categories.mediaMonitoring.slug}`}>
                    <a>
                        <div className={styles['logo']}>
                            <AAAJLogo />
                        </div>
                    </a>
                </Link>
                <div className={styles['description']}>
                    <p>كما أن وقام وبدأت، لم أدوات للمجهود بلا</p>
                </div>
            </div>
            <div className={styles['content']}>
                <div className={styles['side']}></div>
                <ContentSection className={styles['posts']} withTags noHeading white layout={windowWidth >= 1024 ? 'grid' : 'pyramid'} category={category}  posts={posts} />
                <div className={styles['side']}></div>
            </div>
        </section>
    )
}
