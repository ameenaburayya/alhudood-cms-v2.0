import React, { Component } from "react"
import FormLayout from "../layouts/formLayout"
import { FormControl, Input, InputLabel, Button } from "@material-ui/core"
import FormControlLabel from "@material-ui/core/FormControlLabel"
import InputAdornment from "@material-ui/core/InputAdornment"
import Checkbox from "@material-ui/core/Checkbox"
import PasswordShowIcon from "../icons/passwordShowIcon"
import PasswordHideIcon from "../icons/passwordHideIcon"
import formStyles from "../../scss/components/form/form.module.scss"
import Form from "../forms/form"
import PrimaryButton from "../buttons/primaryButton"
import GoogleButton from "../buttons/googleButton"
import FormDivider from "../forms/formDivider"
import FormHeading from "../forms/formHeading"
import CheckBox from "../icons/checkBox"
import CheckedBox from "../icons/checkedBox"
import axios from "axios"
import { login } from "../../requests/userApi"
import FormErrors from "../forms/formErrors"
import LineButton from "../buttons/lineButton"

axios.defaults.withCredentials = true

export default class LoginForm extends Component {
    constructor(props) {
        super(props)
        this.showPassword = this.showPassword.bind(this)
        this.handleForgetPassword = this.handleForgetPassword.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.handleRememberMe = this.handleRememberMe.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.validate = this.validate.bind(this)
        this.handleResendEmail = this.handleResendEmail.bind(this)
        this.state = {
            hidePassword: true,
            forgetPassword: false,
            rememberMe: true,
            errors_array: [],
            passwordValid: true,
            emailValid: true,
            user: {
                email: "",
                password: "",
                remember: true,
            },
            formErrors: {
                email: {
                    empty: "",
                },
                password: {
                    empty: "",
                },
                auth: {
                    verificationError: {
                        text: "",
                        buttonText: "إعادة إرسال رابط التفعيل",
                        modal: false,
                        onClick: this.handleResendEmail,
                        type: "emailNotVerified",
                    },
                    authError: "",
                    serverError: "",
                },
            },
        }
    }

    handleChange = (name) => (event) => {
        const user = this.state.user
        user[name] = event.target.value
        this.setState({
            user,
        })
    }

    async handleResendEmail() {
        console.log("email resent")
        //resendActivationEmail // fix this
    }

    async handleSubmit() {
        let fieldValidationErrors = {
            auth: {
                authError: "",
                verificationError: this.state.formErrors.auth.verificationError,
                serverError: "",
            },
        }
        fieldValidationErrors.auth.verificationError.text = ""
        let temp_isLoading = false
        this.setState({ isLoading: true })
        login({
            email: this.state.user.email,
            password: this.state.user.password,
            remember: this.state.user.remember,
        }).then((res) => {
            console.log("response ", res)
            if (res.status_code == 100) {
                this.props.loginSuccess()
                temp_isLoading = true
            } else if (res.status_code == 510 || res.status_code == 512) {
                fieldValidationErrors["auth"]["authError"] = "اسم المستخدم، عنوان البريد أو كلمة السر غير صحيحة"
            } else if (res.status_code == 511) {
                fieldValidationErrors["auth"]["verificationError"]["text"] = "الحساب غير مفعَّل"
            } else {
                fieldValidationErrors["auth"]["serverError"] = "حصل خطأ ما"
            }
            this.setState(
                {
                    formErrors: fieldValidationErrors,
                    isLoading: temp_isLoading,
                },
                () => {
                    this.convertObject()
                }
            )
        })
    }

    validate(e) {
        e.preventDefault()
        let fieldValidationErrors = this.state.formErrors
        let emailValid = this.state.emailValid
        let passwordValid = this.state.passwordValid
        let user = this.state.user

        if (!user || typeof user.email !== "string" || user.email.trim().length === 0) {
            fieldValidationErrors.email.empty = "يرجى ملء اسم المستخدم او البريد الإلكتروني "
            emailValid = false
        } else {
            emailValid = true
            fieldValidationErrors.email = {
                empty: "",
            }
        }

        if (!user || typeof user.password !== "string" || user.password.trim().length === 0) {
            fieldValidationErrors.password.empty = "يرجى ملء كلمة السر"
            passwordValid = false
        } else {
            passwordValid = true
            fieldValidationErrors.password = {
                empty: "",
            }
        }

        if (emailValid && passwordValid) {
            this.handleSubmit()
        }

        this.setState(
            {
                formErrors: fieldValidationErrors,
                passwordValid: passwordValid,
                emailValid: emailValid,
            },
            () => {
                this.convertObject()
            }
        )
    }

    convertObject() {
        let temp_array = []
        Object.values(this.state.formErrors).map((errorType, i) => {
            Object.values(errorType).map((error) => {
                if (error && error.length > 0) {
                    temp_array.push(error)
                } else if (typeof error === "object" && error?.text?.length > 0) {
                    temp_array.push(error)
                }
            })
        })
        this.setState({
            errors_array: temp_array,
        })
    }

    showPassword() {
        this.setState((prevState) => ({ hidePassword: !prevState.hidePassword }))
    }

    handleForgetPassword() {
        this.setState((prevState) => ({ forgetPassword: !prevState.forgetPassword }))
    }

    handleRememberMe(event) {
        this.setState({ ...this.state, rememberMe: event.target.checked })
    }

    render() {
        return (
            <>
                <FormLayout modal={this.props.modal}>
                    <FormHeading
                        className={`${formStyles["form-heading"]} ${this.props.modal ? formStyles["modal"] : ""}`}
                        label="تسجيل الدخول"
                    />
                    <FormErrors className={formStyles["errors"]} errors={this.state.errors_array} />
                    <Form
                        className={`${formStyles["form"]} ${this.props.modal ? formStyles["modal"] : ""}`}
                        onSubmit={this.validate}
                    >
                        <FormControl className={formStyles["form-control"]} required fullWidth margin="normal">
                            <Input
                                placeholder="اسم المستخدم او البريد الإلكتروني "
                                name="email"
                                type="text"
                                autoComplete="email"
                                disableUnderline
                                onChange={this.handleChange("email")}
                                className={`${formStyles["field"]} ${formStyles["email"]} ${
                                    !this.state.emailValid ? formStyles["error"] : ""
                                }`}
                            />
                        </FormControl>

                        <FormControl
                            className={`${formStyles["form-control"]} ${formStyles["password"]}`}
                            required
                            margin="normal"
                        >
                            <div className={formStyles["forget-password"]} onClick={this.props.handleForgetPassword}>
                                هل نسيت كلمة السر؟
                            </div>
                            <Input
                                placeholder="كلمة السر"
                                name="password"
                                type={this.state.hidePassword ? "password" : "input"}
                                autoComplete="current-password"
                                disableUnderline
                                onChange={this.handleChange("password")}
                                className={`${formStyles["field"]} ${formStyles["withIcon"]} ${
                                    formStyles["password"]
                                } ${!this.state.passwordValid ? formStyles["error"] : ""}`}
                                endAdornment={
                                    this.state.hidePassword ? (
                                        <InputAdornment className={formStyles["icon"]} position="end">
                                            <PasswordShowIcon fontSize="default" onClick={this.showPassword} />
                                        </InputAdornment>
                                    ) : (
                                        <InputAdornment className={formStyles["icon"]} position="end">
                                            <PasswordHideIcon fontSize="default" onClick={this.showPassword} />
                                        </InputAdornment>
                                    )
                                }
                            />
                        </FormControl>
                        <FormControlLabel
                            className={formStyles["checkbox-field"]}
                            control={
                                <Checkbox
                                    icon={<CheckBox />}
                                    checkedIcon={<CheckedBox />}
                                    className={formStyles["checkbox"]}
                                    checked={this.state.rememberMe}
                                    onChange={(e) => this.handleRememberMe(e)}
                                    name="rememberMe"
                                />
                            }
                            label={
                                <div className={formStyles["label-text"]}>
                                    <span>تذكرني</span>
                                </div>
                            }
                        />
                        <PrimaryButton
                            loading={this.state.isLoading}
                            className={formStyles["submit-button"]}
                            fullWidth
                            onClick={this.validate}
                            label="تسجيل الدخول"
                        />
                        <FormDivider modal={this.props.modal} />
                        <GoogleButton modal={this.props.modal} label="تسجيل دخول بواسطة جوجل" />
                        <div className={formStyles["change-form"]}>
                            <p>لست عضواً بالحدود؟</p>
                            <LineButton
                                className={formStyles["change-form-button"]}
                                label="سجل الان"
                                onClick={(e) => this.props.formChange(e)}
                            />
                        </div>
                    </Form>
                </FormLayout>
            </>
        )
    }
}
