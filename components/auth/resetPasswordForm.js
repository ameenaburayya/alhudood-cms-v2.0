import React, { Component } from 'react'
import FormLayout from '../layouts/formLayout'
import { FormControl, Input } from "@material-ui/core";
import InputAdornment from "@material-ui/core/InputAdornment";
import PasswordShowIcon from '../icons/passwordShowIcon';
import PasswordHideIcon from '../icons/passwordHideIcon';
import formStyles from '../../scss/components/form/form.module.scss'
import Form from '../forms/form'
import PrimaryButton from '../buttons/primaryButton'
import FormHeading from '../forms/formHeading';
import FormErrors from '../forms/formErrors';
import {forgetPasswordReset} from '../../requests/userApi'
const validator = require("validator");

import axios from 'axios'
import RegistrationSuccess from './registrationSuccess';

axios.defaults.withCredentials = true;


export default class ResetPasswordForm extends Component {


    constructor(){
        super()
        this.showPassword = this.showPassword.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.validateForm = this.validateForm.bind(this)
        this.convertObject = this.convertObject.bind(this)
        this.validateField = this.validateField.bind(this)
        this.state = {
            hidePassword: true,
            user: {
                email: "",
                passwordConfirm: "",
                password: "",
            },
            errors_array: [],
            passwordValid: true, 
            passwordConfirmValid: true,
            isLoading: false,
            formValid: false,
            submitSuccess: false,
            formErrors: {
                email: {
                    empty: '',
                    notValid: '',
                },
                password: {
                    empty: '',
                    notValid: ''
                },
                passwordConfirm: {
                    empty: '',
                    noMatch: ''
                },
            },
        }
    }

    handleBlur = name => event =>{
        this.validateField(name, this.state.user)
    }

    validateField(fieldName, user){
        let fieldValidationErrors = this.state.formErrors;
        let emailValid = this.state.emailValid;
        let passwordValid = this.state.passwordValid;
        let passwordConfirmValid = this.state.passwordConfirmValid
        switch(fieldName) {
            case 'email':
                if ( !user || typeof user.email !== "string" || user.email.trim().length === 0) {
                    fieldValidationErrors.email.notValid = ""
                    fieldValidationErrors.email.empty = "يرجى ملء البريد الإلكتروني"
                    emailValid = false
                } 
                else if ( !validator.isEmail(user.email) ) {
                    fieldValidationErrors.email.empty = ""
                    fieldValidationErrors.email.notValid = "البريد الإلكتروني غير صحيح"
                    emailValid = false
                }
                else {
                    this.setState({emailLoading: true})
                    emailValid = true
                    fieldValidationErrors.email = {
                        empty: '',
                        notValid: '',
                    }                
                }
                break;
            case 'password':
                if ( !user || typeof user.password !== "string" || user.password.trim().length === 0) {
                    fieldValidationErrors.password.notValid = ""
                    fieldValidationErrors.password.empty = "يرجى ملء كلمة السر"
                    passwordValid = false
                } 
                else if ( !validator.isStrongPassword(user.password, {minSymbols: 0})) {
                    fieldValidationErrors.password.empty = ""
                    fieldValidationErrors.password.notValid = "كلمة السر لا تلبي الشروط: ثمانية خانات باللغة الإنجليزية بحروف (كبيرة وصغيرة) وأرقام"
                    if (user.passwordConfirm && user.password != user.passwordConfirm){
                        fieldValidationErrors.passwordConfirm.noMatch = "كلمتا السر غير متطابقتين"
                        passwordConfirmValid = false
                    }
                    else if (user.passwordConfirm && user.password == user.passwordConfirm){
                        fieldValidationErrors.passwordConfirm.noMatch = ""
                        passwordConfirmValid = true
                    }
                    passwordValid = false
                }                
                else {
                    passwordValid = true
                    fieldValidationErrors.password = {
                        empty: '',
                        notValid: ''
                    }
                }

                if (user.passwordConfirm && user.password != user.passwordConfirm){
                    fieldValidationErrors.passwordConfirm.noMatch = "كلمتا السر غير متطابقتين"
                    passwordConfirmValid = false
                }
                else if (user.passwordConfirm && user.password == user.passwordConfirm){
                    fieldValidationErrors.passwordConfirm.noMatch = ""
                    passwordConfirmValid = true
                }
                break;
            case 'passwordConfirm':
                if (!user || user.password){
                    if (user.passwordConfirm.trim().length === 0){
                        fieldValidationErrors.passwordConfirm.noMatch = ""
                        fieldValidationErrors.passwordConfirm.empty = "يرجى تأكيد كلمة السر"
                        passwordConfirmValid = false
                    }
                    else if (user.password != user.passwordConfirm){
                        fieldValidationErrors.passwordConfirm.noMatch = "كلمتا السر غير متطابقتين"
                        fieldValidationErrors.passwordConfirm.empty = ""
                        passwordConfirmValid = false
                    }
                    else{
                        passwordConfirmValid = true
                        fieldValidationErrors.passwordConfirm = {
                            empty: '',
                            noMatch: ''
                        }
                    }
                }
            default: break;
        }
        this.setState({
            formErrors: fieldValidationErrors,
            emailValid,
            passwordValid,
            passwordConfirmValid
        }, () => {
            this.convertObject()
            this.validateForm()
        });
    }

    convertObject(){
        let temp_array = []
        Object.values(this.state.formErrors).map((errorType, i) => {
            Object.values(errorType).map((error) =>{
                if(error && error.length > 0){
                    temp_array.push(error)
                }
            })
        })
        console.log(temp_array)
        this.setState({
            errors_array: temp_array
        })
    }

    validateForm() {
        this.setState({formValid: this.state.passwordValid && this.state.passwordConfirmValid && this.state.emailValid && this.props.token});
    }

    showPassword(){
        this.setState(prevState => ({ hidePassword: !prevState.hidePassword }));
    }


    handleChange = name => event => {
        const user = this.state.user;
        user[name] = event.target.value;
        this.setState({
            user
        });
    };

    async handleSubmit() {
        let fieldValidationErrors = {auth: {}}
        let submitSuccess = this.state.submitSuccess
        this.setState({ isLoading: true });
        forgetPasswordReset({
            email: this.state.user.email,
            password: this.state.user.password,
            passwordConfirm: this.state.user.passwordConfirm,
            token: this.props.token

        }).then(res => {
            if (res.status_code == 100){
                submitSuccess = true
            }
            else{
                fieldValidationErrors['auth']['serverError'] = "هناك خطأ لم نعرف سببه. يرجى المحاولة لاحقاً أو التواصل معنا على suppport@alhudood.net"
            }
            console.log(res)
            this.setState({
                formErrors: fieldValidationErrors,
                isLoading: false,
                submitSuccess
            }, () => {
                this.convertObject()
            });
        })
    }

    fakeSubmit(e){
        console.log('testing')
        e.preventDefault();
    }



    render() {
        if (!this.state.submitSuccess){
            return (
                <FormLayout modal={this.props.modal}>
                    <FormHeading label="إعادة ضبط كلمة السر" />
                    <FormErrors className={formStyles['errors']} errors={this.state.errors_array} />
                    <Form
                        className={`${formStyles['form']} ${this.props.modal ? formStyles['modal'] : ''}`}
                        onSubmit={this.fakeSubmit}
                    >
                        <FormControl className={`${formStyles['form-control']}`} required  margin="normal">
                            <Input
                                placeholder="البريد الإلكتروني"
                                type="email"
                                name="email"
                                autoComplete="email"
                                disableUnderline
                                onChange={this.handleChange("email")}
                                className={`${formStyles['field']} ${formStyles['email']} ${!this.state.passwordConfirmValid ? formStyles['error'] : ''}`}
                                onBlur={this.handleBlur('email')}
                            />
                        </FormControl>
                        <FormControl className={`${formStyles['form-control']}`} required margin="normal">
                            <Input
                                placeholder="كلمة السر الجديدة"
                                name="password"
                                type={this.state.hidePassword ? "password" : "input"}
                                autoComplete="new-password"
                                disableUnderline
                                onChange={this.handleChange("password")}
                                onBlur={this.handleBlur('password')}
                                className={`${formStyles['field']} ${formStyles['withIcon']} ${formStyles['password']} ${!this.state.passwordValid ? formStyles['error'] : ''}`}
                                endAdornment={
                                    this.state.hidePassword ? (
                                        <InputAdornment position="end">
                                            <PasswordShowIcon
                                                fontSize="default"
                                                onClick={this.showPassword}
                                            />
                                        </InputAdornment>
                                    ) : (
                                        <InputAdornment position="end">
                                            <PasswordHideIcon
                                                fontSize="default"
                                                onClick={this.showPassword}
                                            />
                                        </InputAdornment>
                                    )
                                }
                            />
                        </FormControl>
                        <FormControl className={`${formStyles['form-control']}`} required  margin="normal">
                            <Input
                                placeholder="تأكيد كلمة السر الجديدة"
                                type={this.state.hidePassword ? "password" : "input"}
                                name="new-password"
                                autoComplete= {false}
                                disableUnderline
                                onChange={this.handleChange("passwordConfirm")}
                                className={`${formStyles['field']} ${formStyles['password']} ${!this.state.passwordConfirmValid ? formStyles['error'] : ''}`}
                                onBlur={this.handleBlur('passwordConfirm')}
                            />
                        </FormControl>
                        <PrimaryButton loading={this.state.isLoading} className={`${formStyles['submit-button']} ${!this.state.formValid ? formStyles['disabled']: ''}`} fullWidth onClick={this.state.formValid ? this.handleSubmit : e => { e.preventDefault(); } } label="تغيير كلمة السر" />
                        {/* <PrimaryButton className={`${formStyles['submit-button']} ${!this.state.formValid ? formStyles['disabled']: ''}`} fullWidth onClick={this.handleSubmit} label="تغيير كلمة السر" /> */}
                    </Form>
                </FormLayout>
            )
        }
        else{
            return <RegistrationSuccess type="password" />
        }
        
    }
}
