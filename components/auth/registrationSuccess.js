import React, { useEffect, useRef, useContext } from 'react'
import { Player } from '@lottiefiles/react-lottie-player';
import FormLayout from '../layouts/formLayout'
import styles from '../../scss/components/auth/registrationSuccess.module.scss'
import CommentAvatar from '../icons/commentAvatar'
import Link from 'next/link'
import WebsiteContext from '../../contexts/websiteContext'

export default function RegistrationSuccess(props) {

    const animation = useRef();
    const { auth } = useContext(WebsiteContext);
    useEffect(() => {
        doSomething()
    });
    const doSomething = () => {
        animation.current?.play()
        setTimeout(() => {
            animation.current.pause()
        }, 5100)
    }

    return (
        <FormLayout modal={props.modal}>
            <div className={styles['registration-success']}>
                {props.type == "account" && <div className={styles['avatar']}>
                    <CommentAvatar />
                </div>}
                <div className={styles['icon']}>
                    <Player
                        onEvent={event => {
                            if (event == 'load')  doSomething(); // check event type and do something
                        }}
                        ref={animation}
                        src="/Assets/animated_icons/check.json"
                        style={{ width: '240px' }}
                        loop
                        className={styles['icon']}
                    />
                </div>
                {props.type == 'account' && <p className={styles['text']}>صرت جزءاً من الحدود! <br />أهلاً أهلاً، أهلاً وسهلاً، نورتنا</p>}
                {props.type == "password" && <p className={styles['text']}>تم إعادة ضبط كلمة السر بنجاح</p>}
                
                <Link href={auth.loggedIn ? '/' : '/login'}>
                    <a>
                        <div className={styles['button']}>
                            {auth.loggedIn ? <span>تفضل للصفحة الرئيسية</span>
                            : <span>تسجيل الدخول</span>}
                        </div>

                    </a>
                </Link>
                
            </div>
        </FormLayout>
    )
}
