import React from 'react'
import AlhudoodModal from '../alhudoodModal'
import LoginForm from './loginForm'
import ForgetPasswordForm from './forgetPasswordForm'
import SignupForm from './signupForm'
import { clearAllBodyScrollLocks } from 'body-scroll-lock'

export default class UserModal extends React.Component {
    constructor(props){
        super(props)
        this.handleFormChange = this.handleFormChange.bind(this)
        this.handleForgetPassword = this.handleForgetPassword.bind(this)
        this.handleBack = this.handleBack.bind(this)
        this.handleOpen = this.handleOpen.bind(this)
        this.handleClose = this.handleClose.bind(this)
        this.modal = React.createRef()
        this.state = {
            currentForm: "login",
            modalOpen: false
        }
    }


    handleFormChange(e){
        e.preventDefault();
        if(this.state.currentForm == "signup"){
            this.setState({currentForm: "login"})
        }
        else if (this.state.currentForm == "login"){
            this.setState({currentForm: "signup"})
        }
        else if (this.state.currentForm == 'forgetPassword'){
            this.setState({currentForm: "signup"})
        }
    }

    handleForgetPassword(){
        this.setState({currentForm: "forgetPassword"})
    }

    handleBack(){
        this.setState({currentForm: "login"})
    }

    handleOpen(){
        this.setState({
            currentForm: "login",
            modalOpen: true
        })
    }
    handleClose(){
        this.setState({
            modalOpen: false
        })
        clearAllBodyScrollLocks()
    }

    componentWillUnmount(){
        clearAllBodyScrollLocks()
    }


    render(){
        return (
            <AlhudoodModal ref={this.modal} modalOpen={this.state.modalOpen} handleClose={this.handleClose} currentForm={this.state.currentForm}>
                {this.state.currentForm === "signup" ? <SignupForm modal formChange={this.handleFormChange} /> : null}
                {this.state.currentForm === "login" ? <LoginForm modal handleForgetPassword={this.handleForgetPassword} formChange={this.handleFormChange} /> : null}
                {this.state.currentForm === "forgetPassword" ? <ForgetPasswordForm formChange={this.handleFormChange} modal handleBack={this.handleBack} /> : null}
            </AlhudoodModal>
        )
    }
   
}
