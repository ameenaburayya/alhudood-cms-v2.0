import React, { Component } from 'react'
import FormLayout from '../layouts/formLayout'
import { FormControl, Input, InputLabel, Button } from "@material-ui/core";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import CircularProgress from '@material-ui/core/CircularProgress';
import InputAdornment from "@material-ui/core/InputAdornment";
import PasswordShowIcon from '../icons/passwordShowIcon';
import PasswordHideIcon from '../icons/passwordHideIcon';
import formStyles from '../../scss/components/form/form.module.scss'
import Form from '../forms/form'
import PrimaryButton from '../buttons/primaryButton'
import FormHeading from '../forms/formHeading';
import FormDivider from '../forms/formDivider';
import GoogleButton from '../buttons/googleButton';
import CheckBox from '../icons/checkBox';
import CheckedBox from '../icons/checkedBox';
import Checkbox from '@material-ui/core/Checkbox';
import FormErrors from '../forms/formErrors';
import axios from 'axios'
import {checkUsername, checkEmail, signup} from '../../requests/userApi'
import CheckIcon from '../icons/checkIcon';
import EmailActivation from './emailActivation'
import Link from 'next/link'
import LineButton from '../buttons/lineButton';
const validator = require("validator");


axios.defaults.withCredentials = true;



const e2a = s => s.replace(/\d/g, d => '٠١٢٣٤٥٦٧٨٩'[d])

export default class SignupForm extends Component {
    constructor(props){
        super(props)
        this.showPassword = this.showPassword.bind(this)
        this.handleAcceptTerms = this.handleAcceptTerms.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.validateForm = this.validateForm.bind(this)
        this.convertObject = this.convertObject.bind(this)
        this.validateSignUp = this.validateSignUp.bind(this)
        this.state = {
            hidePassword: true,
            user: {
                passwordConfirm: "",
                email: "",
                username: "",
                password: "",
                terms: false
            },
            errors_array: [],
            emailValid: null,
            usernameValid: null,
            passwordValid: true, 
            passwordConfirmValid: true,
            termsValid: null,
            usernameLoading: false,
            emailLoading: false,
            formLoading: false,
            signupSuccess: false,
            formErrors: {
                username: {
                    empty: '',
                    alpha: '',
                    number: '',
                    available: ''
                },
                email: {
                    empty: '',
                    notValid: '',
                    available: {
                        text: '',
                        buttonText: 'تسجيل الدخول',
                        modal: props.modal,
                        onClick: props.formChange,
                        type: 'emailUsed'
                    }
                },
                password: {
                    empty: '',
                    notValid: ''
                },
                passwordConfirm: {
                    empty: '',
                    noMatch: ''
                },
                terms: {
                    noCheck: ''
                },
                form: {
                    error: ''
                }
            },
        }
    }


    handleChange = name => event => {
        if (name == "username"){
            const newValue = e2a(event.target.value)
            event.target.value = newValue
        }
        const user = this.state.user;
        user[name] = event.target.value;
        this.setState({
            user
        });
    };

    handleBlur = name => event =>{
        this.validateSignUp(name, this.state.user)
    }


    validateSignUp(fieldName, user){
        let fieldValidationErrors = this.state.formErrors;
        let emailValid = this.state.emailValid;
        let passwordValid = this.state.passwordValid;
        let usernameValid = this.state.usernameValid
        let passwordConfirmValid = this.state.passwordConfirmValid
        switch(fieldName) {
            case 'username':
                if ( !user || typeof user.username !== "string" || user.username.trim().length === 0) {        
                    fieldValidationErrors.username.number = null
                    fieldValidationErrors.username.alpha = null
                    fieldValidationErrors.username.available = null
                    fieldValidationErrors.username.empty = "يرجى ملء اسم المستخدم"
                    usernameValid = false
                }
                else if (user.username.trim().length < 5 || user.username.trim().length > 16 ){
                    if (!validator.isAlpha(user.username, ['ar'] , {ignore:  "_٠١٢٣٤٥٦٧٨٩"})){
                        fieldValidationErrors.username.empty = null
                        fieldValidationErrors.username.available = null
                        fieldValidationErrors.username.number = "اسم المستخدم يجب أن يكون بين 5 و16 حرفاً"
                        fieldValidationErrors.username.alpha = "اسم المستخدم يجب ان يتكون من الأحرف العربية، الأرقام أو الشَرطة (_) فقط"
                        usernameValid = false
                    }  
                    else{
                        fieldValidationErrors.username.alpha = null
                        fieldValidationErrors.username.empty = null
                        fieldValidationErrors.username.available = null
                        fieldValidationErrors.username.number = "اسم المستخدم يجب أن يكون بين 5 و16 حرفاً"
                        usernameValid = false
                    }
                }
                else if (!validator.isAlpha(user.username, ['ar'] , {ignore:  "_٠١٢٣٤٥٦٧٨٩"})){
                    fieldValidationErrors.username.empty = null
                    fieldValidationErrors.username.number = null
                    fieldValidationErrors.username.available = null
                    fieldValidationErrors.username.alpha = "اسم المستخدم غير صحيح: يرجى استخدام الأحرف العربية، الأرقام أو الشَرطة (_) فقط"
                    usernameValid = false
                }
                else {
                    this.setState({usernameLoading: true})
                    checkUsername({
                        username: user.username,
                    }).then(res => {
                        if (res.status_code == 100){
                            usernameValid = true
                            fieldValidationErrors.username = {
                                empty: '',
                                number: '',
                                alpha: '',
                                available: ''
                            }
                        }
                        else if (res.status_code == 510){
                            usernameValid = false,
                            fieldValidationErrors.username.empty = null
                            fieldValidationErrors.username.number = null
                            fieldValidationErrors.username.alpha = null
                            fieldValidationErrors.username.available = "سبقك عضوٌ آخر وأخذ اسم المستخدم"
                        }
                        else {
                            usernameValid = false,
                            fieldValidationErrors.username.empty = null
                            fieldValidationErrors.username.number = null
                            fieldValidationErrors.username.alpha = null
                            fieldValidationErrors.username.available = "هناك خطأ لم نعرف سببه. يرجى المحاولة لاحقاً أو التواصل معنا على suppport@alhudood.net"
                        }
                        this.setState({
                            formErrors: fieldValidationErrors,
                            usernameValid,
                            usernameLoading: false
                        }, () => {
                            this.convertObject()
                            this.validateForm()
                        });
                        
                        console.log(res)
                    })
                    
                }
                break;
            case 'email':
                if ( !user || typeof user.email !== "string" || user.email.trim().length === 0) {
                    fieldValidationErrors.email.notValid = ""
                    fieldValidationErrors.email.available.text = ''
                    fieldValidationErrors.email.empty = "يرجى ملء البريد الإلكتروني"
                    emailValid = false
                } 
                else if ( !validator.isEmail(user.email) ) {
                    fieldValidationErrors.email.empty = ""
                    fieldValidationErrors.email.available.text = ''
                    fieldValidationErrors.email.notValid = "البريد الإلكتروني غير صحيح"
                    emailValid = false
                }
                else {
                    this.setState({emailLoading: true})
                    checkEmail({
                        email: user.email,
                    }).then(res => {
                        if (res.status_code == 100){
                            emailValid = true
                            fieldValidationErrors.email.empty = ''
                            fieldValidationErrors.email.notValid = ''
                            fieldValidationErrors.email.available.text = ''
                        }
                        else if (res.status_code == 510){
                            emailValid = false,
                            fieldValidationErrors.email.empty = ''
                            fieldValidationErrors.email.notValid = ''
                            fieldValidationErrors.email.available.text = `البريد الإلكتروني المُدخل مستخدم من قبل.`
                        }
                        else {
                            emailValid = false,
                            fieldValidationErrors.email.empty = ""
                            fieldValidationErrors.email.notValid = ''
                            fieldValidationErrors.email.available.text = "هناك خطأ لم نعرف سببه. يرجى المحاولة لاحقاً أو التواصل معنا على suppport@alhudood.net"
                        }
                        this.setState({
                            formErrors: fieldValidationErrors,
                            emailValid,
                            emailLoading: false
                        }, () => {
                            this.convertObject()
                            this.validateForm()
                        });
                    })
                    
                }
                break;
            case 'password':
                if ( !user || typeof user.password !== "string" || user.password.trim().length === 0) {
                    fieldValidationErrors.password.notValid = ""
                    fieldValidationErrors.password.empty = "يرجى ملء كلمة السر"
                    passwordValid = false
                } 
                else if ( !validator.isStrongPassword(user.password, {minSymbols: 0})) {
                    fieldValidationErrors.password.empty = ""
                    fieldValidationErrors.password.notValid = "كلمة السر لا تلبي الشروط: ثمانية خانات باللغة الإنجليزية بحروف (كبيرة وصغيرة) وأرقام"
                    if (user.passwordConfirm && user.password != user.passwordConfirm){
                        fieldValidationErrors.passwordConfirm.noMatch = "كلمتا السر غير متطابقتين"
                        passwordConfirmValid = false
                    }
                    else if (user.passwordConfirm && user.password == user.passwordConfirm){
                        fieldValidationErrors.passwordConfirm.noMatch = ""
                        passwordConfirmValid = true
                    }
                    passwordValid = false
                }                
                else {
                    passwordValid = true
                    fieldValidationErrors.password = {
                        empty: '',
                        notValid: ''
                    }
                }

                if (user.passwordConfirm && user.password != user.passwordConfirm){
                    fieldValidationErrors.passwordConfirm.noMatch = "كلمتا السر غير متطابقتين"
                    passwordConfirmValid = false
                }
                else if (user.passwordConfirm && user.password == user.passwordConfirm){
                    fieldValidationErrors.passwordConfirm.noMatch = ""
                    passwordConfirmValid = true
                }
                break;
            case 'passwordConfirm':
                if (!user || user.password){
                    if (user.passwordConfirm.trim().length === 0){
                        fieldValidationErrors.passwordConfirm.noMatch = ""
                        fieldValidationErrors.passwordConfirm.empty = "يرجى تأكيد كلمة السر"
                        passwordConfirmValid = false
                    }
                    else if (user.password != user.passwordConfirm){
                        fieldValidationErrors.passwordConfirm.noMatch = "كلمتا السر غير متطابقتين"
                        fieldValidationErrors.passwordConfirm.empty = ""
                        passwordConfirmValid = false
                    }
                    else{
                        passwordConfirmValid = true
                        fieldValidationErrors.passwordConfirm = {
                            empty: '',
                            noMatch: ''
                        }
                    }
                }
            default: break;
        }
        this.setState({
            formErrors: fieldValidationErrors,
            emailValid,
            usernameValid,
            passwordValid,
            passwordConfirmValid
        }, () => {
            this.convertObject()
            this.validateForm()
        });
    }

    convertObject(){
        let temp_array = []
        Object.values(this.state.formErrors).map((errorType, i) => {
            Object.values(errorType).map((error) =>{
                if(error && error.length > 0){
                    temp_array.push(error)
                }
                else if (typeof error === 'object' && error?.text?.length > 0) {
                    temp_array.push(error)
                }
            })
        })
        console.log('errors',temp_array)
        this.setState({
            errors_array: temp_array
        })
    }

    
    validateForm() {
        this.setState({formValid: this.state.emailValid && this.state.passwordValid && this.state.usernameValid && this.state.passwordConfirmValid && this.state.termsValid});
    }

    async handleSubmit(e) {
        e.preventDefault();
        let signupSuccess = this.state.signupSuccess
        let fieldValidationErrors = this.state.formErrors;
        if (this.state.formValid){
            this.setState({ formLoading: true });
            signup({
                username: this.state.user.username,
                email: this.state.user.email,
                password: this.state.user.password,
            }).then(res => {
                if (res.status == 100){
                    signupSuccess = true
                }
                else if (res.status == 510){
                    fieldValidationErrors.form.error = "هناك خطأ لم نعرف سببه. يرجى المحاولة لاحقاً أو التواصل معنا على suppport@alhudood.net"
                    signupSuccess = false
                }
                else{
                    fieldValidationErrors.form.error = "هناك خطأ لم نعرف سببه. يرجى المحاولة لاحقاً أو التواصل معنا على suppport@alhudood.net"
                    signupSuccess = false
                }
                this.setState({
                    formErrors: fieldValidationErrors,
                    signupSuccess: signupSuccess,
                    formLoading: false
                }, () => {
                    this.convertObject()
                });
            })
        }
    }


    showPassword(){
        this.setState(prevState => ({ hidePassword: !prevState.hidePassword }));
    }


    handleAcceptTerms(event){
        this.setState({termsValid: event.target.checked});
        let fieldValidationErrors = this.state.formErrors;
        if (!event.target.checked){
            fieldValidationErrors.terms.noCheck = "يرجى الموافقة على الشروط والأحكام"
            this.setState({formErrors: fieldValidationErrors}, () => {
                this.convertObject()
                this.validateForm()
            });
        }
        else {
            fieldValidationErrors.terms.noCheck = ""
            this.setState({formErrors: fieldValidationErrors},() => {
                this.convertObject()
                this.validateForm()
            });
        }
    }

    
    render() {
        if (!this.state.signupSuccess){
            return (
                <FormLayout modal={this.props.modal} currentForm="signup">
                    <FormHeading className={`${formStyles['form-heading']} ${this.props.modal ? formStyles['modal'] : ''}`} label="إنشاء حساب" />
                    <FormErrors className={formStyles['errors']} errors={this.state.errors_array} />
                    <Form
                        className={`${formStyles['form']} ${this.props.modal ? formStyles['modal'] : ''}`}
                        onSubmit={this.handleSubmit}
                    >
                        <FormControl className={formStyles['form-control']} required fullWidth margin="normal">
                            <Input
                                placeholder="اسم المستخدم"
                                name="username"
                                type="text"
                                autoComplete="username"
                                disableUnderline
                                onChange={this.handleChange('username')}
                                className={`${formStyles['field']} ${this.state.usernameValid != null && !this.state.usernameValid  ? formStyles['error'] : ''}`}
                                onBlur={this.handleBlur('username')}
                                endAdornment={
                                    this.state.usernameLoading ? <CircularProgress className={formStyles['progress']} /> : this.state.usernameValid ? <CheckIcon /> : null
                                }
                            />
                        </FormControl>
    
                        <FormControl className={formStyles['form-control']} required fullWidth margin="normal">
                            <Input
                                placeholder="البريد الإلكتروني"
                                name="email"
                                type="email"
                                autoComplete="email"
                                disableUnderline
                                onChange={this.handleChange("email")}
                                className={`${formStyles['field']} ${this.state.emailValid != null && !this.state.emailValid ? formStyles['error'] : ''}`}
                                onBlur={this.handleBlur('email')}
                                endAdornment={
                                    this.state.emailLoading ? <CircularProgress className={formStyles['progress']} /> : this.state.emailValid ? <CheckIcon /> : null
                                }
                            />
                        </FormControl>
    
                        <FormControl className={`${formStyles['form-control']} ${!this.props.modal ? formStyles['half'] : ''}`} required margin="normal">
                            <Input
                                placeholder="كلمة السر"
                                name="password"
                                type={this.state.hidePassword ? "password" : "text"}
                                autoComplete="new-password"
                                disableUnderline
                                onChange={this.handleChange("password")}
                                onBlur={this.handleBlur('password')}
                                className={`${formStyles['field']} ${formStyles['withIcon']} ${formStyles['password']} ${!this.state.passwordValid ? formStyles['error'] : ''}`}
                                endAdornment={
                                    this.state.hidePassword ? (
                                        <InputAdornment position="end">
                                            <PasswordShowIcon
                                                fontSize="default"
                                                onClick={this.showPassword}
                                            />
                                        </InputAdornment>
                                    ) : (
                                        <InputAdornment position="end">
                                            <PasswordHideIcon
                                                fontSize="default"
                                                onClick={this.showPassword}
                                            />
                                        </InputAdornment>
                                    )
                                }
                            />
                        </FormControl>
    
                        <FormControl className={`${formStyles['form-control']} ${!this.props.modal ? formStyles['half'] : ''}`} required  margin="normal">
                            <Input
                                placeholder="تأكيد كلمة السر"
                                type={this.state.hidePassword ? "password" : "input"}
                                name="password"
                                autoComplete="new-password"
                                disableUnderline
                                onChange={this.handleChange("passwordConfirm")}
                                className={`${formStyles['field']} ${formStyles['password']} ${!this.state.passwordConfirmValid ? formStyles['error'] : ''}`}
                                onBlur={this.handleBlur('passwordConfirm')}
                            />
                        </FormControl>
    
                        <FormControlLabel
                            className={`${formStyles['checkbox-field']} ${formStyles['small-label']} ${this.state.termsValid || this.state.termsValid === null ? '' : formStyles['error']}`}
                            control={<Checkbox icon={<CheckBox />} checkedIcon={<CheckedBox />} className={formStyles['checkbox']} checked={this.state.termsValid} onChange={(e) => this.handleAcceptTerms(e)} name="accept terms" />}
                            label={
                                <div className={formStyles['label-text']}>
                                    <span>إنشاء هذا الحساب يعني موافقتك على </span>
                                    <Link href="/privacy-policy">سياسة الخصوصية</Link>
                                    <span> في الحدود</span>
                                </div>
                            }
                        />
    
                        <PrimaryButton 
                            className={`${formStyles['submit-button']} ${!this.state.formValid ? formStyles['disabled']: ''}`} 
                            fullWidth 
                            onClick={this.state.formValid ? this.handleSubmit : e => { e.preventDefault()}} 
                            label="سجّل الآن" 
                            type='submit'
                        />
                        <FormDivider modal={this.props.modal} />
                        <GoogleButton modal={this.props.modal} label="إنشاء حساب بواسطة جوجل" />
    
                        <div className={formStyles['change-form']}>
                            <p>هل أنت جزء من الحدود؟</p>
                            <LineButton className={formStyles['change-form-button']} label="تفضل بالدخول" onClick={(e) => this.props.formChange(e)} />
                        </div>
                    </Form>
                </FormLayout>
            )
        }
        else{
            return <EmailActivation type="account" email={this.state.user.email} />
        }
        
    }
}


// export async function getServerSideProps({ res, req }) {
//     // // console.log(req.session)
//     // const parsedCookies = cookie.parse(req.headers.cookie);
//     // if (!parsedCookies['XSRF-TOKEN']){
//     //     const csrf = await fetch(`http://localhost:8082/sanctum/csrf-cookie`)
//     //     res.setHeader('set-cookie', csrf.headers.raw()['set-cookie']);
//     // }
//     // return { props: { } };
// }

