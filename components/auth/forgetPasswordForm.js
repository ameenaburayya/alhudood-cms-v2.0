import React, { Component } from 'react'
import FormLayout from '../layouts/formLayout'
import { FormControl, Input, InputLabel, Button } from "@material-ui/core";
import formStyles from '../../scss/components/form/form.module.scss'
import Form from '../forms/form'
import PrimaryButton from '../buttons/primaryButton'
import FormHeading from '../forms/formHeading';
import BackArrowIcon from '../icons/backArrowIcon';
import {forgetPassword} from '../../requests/userApi'
const validator = require("validator");

import axios from 'axios'
import FormErrors from '../forms/formErrors';
import EmailActivation from './emailActivation';
import LineButton from '../buttons/lineButton';

axios.defaults.withCredentials = true;


export default class ForgetPasswordForm extends Component {


    constructor(){
        super()
        this.handleChange = this.handleChange.bind(this)
        this.validate = this.validate.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.state = {
            email: '',
            formErrors: {
                email: {
                    empty: '',
                    notValid: ''
                }
            },
            emailValid: null,
            errors_array: [],
            submitSuccess: false,
            isLoading: false
        }
    }

    handleChange (e) {
        this.setState({
            email: e.target.value
        });
    };

    async handleSubmit() {
        let fieldValidationErrors = {auth: {}}
        let submitSuccess = this.state.submitSuccess
        this.setState({ isLoading: true });
        forgetPassword({
            email: this.state.email,
        }).then(res => {
            if (res.status_code == 100 || res.status_code == 510){
                submitSuccess = true
            }
            else{
                fieldValidationErrors['auth']['serverError'] = "هناك خطأ لم نعرف سببه. يرجى المحاولة لاحقاً أو التواصل معنا على suppport@alhudood.net"
            }
            this.setState({
                formErrors: fieldValidationErrors,
                isLoading: false,
                submitSuccess
            }, () => {
                this.convertObject()
            });
        })
    }


    validate(e){
        e.preventDefault();
        let fieldValidationErrors = this.state.formErrors;
        let emailValid = this.state.emailValid;
        let email = this.state.email
        
        if ( !email || typeof email !== "string" || email.trim().length === 0) {
            fieldValidationErrors.email.notValid = ""
            fieldValidationErrors.email.empty = "يرجى ملء البريد الإلكتروني"
            emailValid = false
        } 
        else if ( !validator.isEmail(email) ) {
            fieldValidationErrors.email.empty = ""
            fieldValidationErrors.email.notValid = "البريد الإلكتروني غير صحيح"
            emailValid = false
        }
        else {
            emailValid = true
            fieldValidationErrors.email = {
                empty: '',
                notValid: ''
            }
        }

        if (emailValid){
            this.handleSubmit()
        }
        
        this.setState({
            formErrors: fieldValidationErrors,
            emailValid: emailValid
        }, () => {
            this.convertObject()
        });
    }

    convertObject(){
        let temp_array = []
        Object.values(this.state.formErrors).map((errorType, i) => {
            Object.values(errorType).map((error) =>{
                if(error && error.length > 0){
                    temp_array.push(error)
                }
            })
        })
        this.setState({
            errors_array: temp_array
        })
    }

    render() {
        if (!this.state.submitSuccess){
            return (
                <FormLayout modal={this.props.modal} currentForm="forget-password">
                    {this.props.handleBack && <div className={formStyles['back-button']}>
                        <BackArrowIcon onClick={this.props.handleBack} />
                    </div>}
                    <FormHeading label="نسيت كلمة السر؟" />
                    <p className={formStyles['text']}>أدخل البريد الإلكتروني المستعمل لإنشاء الحساب وسنقوم بإرسال خطوات تغيير كلمة السر إليه</p>
                    <FormErrors className={formStyles['errors']} errors={this.state.errors_array} />
                    <Form
                        className={`${formStyles['form']} ${this.props.modal ? formStyles['modal'] : ''}`}
                        onSubmit={this.validate}
                    >
                        <FormControl className={formStyles['form-control']} required fullWidth margin="normal">
                            <Input
                                placeholder="البريد الإلكتروني "
                                name="email"
                                type="email"
                                autoComplete="email"
                                disableUnderline
                                onChange={(e) => this.handleChange(e)}
                                className={formStyles['field']}
                            />
                        </FormControl>
    
                        <PrimaryButton loading={this.state.isLoading} className={formStyles['submit-button']} fullWidth onClick={this.validate} label="ارسل" />
    
                        {!this.props.profile && <div className={`${formStyles['change-form']} ${formStyles['forget-password']}`}>
                            <p>لست عضواً بالحدود؟</p>
                            <LineButton className={formStyles['change-form-button']} label="سجل الان" onClick={(e) => this.props.formChange(e)} />
                        </div>}
                    </Form>
                </FormLayout>
            )
        }
        else{
            return <EmailActivation type="password" email={this.state.email} />
        }
        
    }
}
