import React from 'react'
import { Player, Controls } from '@lottiefiles/react-lottie-player';
import FormLayout from '../layouts/formLayout'
import { emailServices } from '../../lib/constants'
import styles from '../../scss/components/auth/emailActivation.module.scss'
import { resendActivationEmail, forgetPassword } from '../../requests/userApi'

export default class EmailActivation extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isStopped: false, 
            isPaused: false,
            emailService: null,
            numberOfClicks: 0,
            seconds: 59,
            timerActive: false
        };
        this.doSomething = this.doSomething.bind(this)
        this.resendEmail = this.resendEmail.bind(this)
        this.animation = React.createRef();
    }

    componentDidMount(){
        const domain = this.props.email.split('@').pop()
        const emailService = Object.values(emailServices).filter(function search(row) {
            if (row['emailExtension'] == domain){
                return row
            }
            else {
                return false
            }
        })
        this.setState({
            emailService: emailService[0]
        })
        this.doSomething()
    }

    startCounter(){
        this.setState({
            timerActive: true
        })
        this.time = setInterval(() => {
            const { seconds } = this.state
    
            if (seconds > 0) {
                this.setState(({ seconds }) => ({
                    seconds: seconds - 1
                }))
            }
            if (seconds === 0) {
                clearInterval(this.time)
                this.setState({
                    seconds: 59,
                    timerActive: false
                })
            } 
        }, 1000)
    } 

    resendEmail(){
        if (this.props.type == "account"){
            resendActivationEmail({
                email: this.props.email
            }).then(res => {
                if (res.status_code === 100){
                    this.setState((state) => ({
                        numberOfClicks: state.numberOfClicks + 1
                    }))
                    this.startCounter()
                }
                else {

                }
            })
        }
        else if (this.props.type == "password"){
            console.log('test')
        }
    }


    doSomething(){
        this.animation.current?.play()
        setTimeout(() => {
            this.animation.current.pause()
        }, 10000)
    }

    render(){
        return (
            <FormLayout modal={this.props.modal}>
                <div className={styles['email-activation']}>
                    <div className={styles['icon']}>
                        <Player
                            onEvent={event => {
                                if (event == 'load')  this.doSomething(); // check event type and do something
                            }}
                            ref={this.animation}
                            src="/Assets/animated_icons/email.json"
                            style={{ width: '240px' }}
                            
                        />
                    </div>
                    {this.props.type == "account" && <p className={styles['activation-text']}>تبقى أمامك خطوة أخيرة <br/> أرسلنا لك بريداً يحوي رابط تفعيل حسابك، يرجى ضغط  الرابط هناك لتصبح عضواً حقيقياً </p>}
                    {this.props.type == "password" && <p className={styles['activation-text']}>تم إرسال خطوات تغيير كلمة السر إلي البريد الإلكتروني الخاص بك</p>}
                    {this.props.email && <div className={styles['email']}><span>{this.props.email}</span></div>}

                    {this.state.emailService && 
                        <div className={styles['email-button']}>
                            <a target="_blank" href={`${this.state.emailService.link}`}>
                                <div>
                                    <span className={styles['service']}>{this.state.emailService.name}</span> <span className={styles['text']}>الذهاب الى</span>
                                </div>
                            </a>
                        </div>
                    }

                    {this.props.type == 'account' && 
                        <div className={styles['resend']}>
                            {this.state.timerActive ? 
                                <div className={styles['counter']}>
                                    <p>تستطيع ان تعيد إرساله خلال:</p>
                                    <div>00:{this.state.seconds}</div>
                                </div> 
                                : 
                                <div className={styles['resend-text']}>
                                    <p>لم يصلك الرابط؟</p>
                                    {this.state.numberOfClicks < 3 ? 
                                        <button onClick={this.resendEmail}>
                                            أرسل الرابط مرة أخرى
                                        </button>
                                        :
                                        <div className={styles['support']}>
                                            <span>الرجاء التواصل معنا على</span> <span className={styles['email-address']}><a href="mailto:support@alhudood.net">support@alhudood.net</a></span>
                                        </div>
                                    }
                                </div>
                            }
                        </div>
                    }
                    
                </div>
            </FormLayout>
        )
    }
}
