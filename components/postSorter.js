import React, { useState, useEffect } from 'react'
import DropDown from './dropDown';



export default function PostSorter(props) {

    const [options, setOptions] = useState(null)

    const defaultOptions = [
        {
            title: "الأحدث",
            slug: "date",
            id: 200
        },
        {
            title: "الأكثر قراءة",
            slug: "most_reading",
            id: 300
        }
    ]

    useEffect(() => {
        if (props.contentTypes){
            setOptions([...defaultOptions, ...props.contentTypes])
        }
        else{
            setOptions([...defaultOptions])
        }
    }, [props.contentTypes])



    return (
        <>
            <div className="sorting-dropdown" className={props.className ? props.className : ''}>
                <DropDown activeOption={props.activeOption} activeOption={props.activeOption} onClick={props.handleSorting} options={options} />
            </div>
        </>
    )
}
