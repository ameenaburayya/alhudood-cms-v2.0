import React, {useState} from 'react'
import MainHeading from './headings/mainHeading'
import styles from '../scss/components/aaajVotingCard.module.scss'
import PrimaryButton from './buttons/primaryButton'
import Sonboleh from './icons/sonboleh'


export default function AAAJVotingCard(props) {

    const [active, setActive] = useState(false)

    const handleCLick = () => {
        setActive(true)
    }

    return (
        <section className={`${styles['aaaj-section']} ${props.className ? props.className : ''}`}>
            <div className={styles['heading']}>
                <MainHeading withDash>
                    جَحُصَعْ    
                </MainHeading>
            </div>
            <div className={`${styles['aaaj-voting']} ${active ? styles['active'] : ' '}`}>
                <p>هل يستحق الخبر الفوز بجحصع؟</p>
                <div onClick={handleCLick} className={styles['card']}>
                    <div className={styles['details']}>
                        <div className={styles['category']}>
                            <div className={`${styles['sonboleh']} ${styles['left']}`}>
                                <Sonboleh />
                            </div>
                            <span>{props.post.category}</span>
                            <div className={`${styles['sonboleh']} ${styles['right']}`}>
                                <Sonboleh />
                            </div>
                        </div>
                        <div className={styles['source']}>{props.post.source}</div>
                    </div>
                    <div className={styles['image']}>
                        <figure>
                            <picture>
                                <img src={props.post.image} />
                            </picture>
                        </figure>
                    </div>
                    <div className={styles['title']}>
                        {props.post.title}
                    </div>
                    <div className={styles['buttons']}>
                        <div onClick={handleCLick} className={styles['button']}><span>نعم</span></div>
                        <div className={styles['button']}><span>لا</span></div>
                    </div>
                    <div className={styles['go-vote']}>
                        <p>كما أن وقام وبدأت، لم أدوات للمجهود بلا، كما أن وقام وبدأت، لم أدوات للمجهود بلا</p>
                        <PrimaryButton newTab label="صوت لجوائز الصحافة" link="https://aaaj.alhudood.net" />
                    </div>
                </div>
            </div>
        </section>
    )
}
