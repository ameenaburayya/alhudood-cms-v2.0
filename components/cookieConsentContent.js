import React from 'react'
import CloseIcon from './icons/closeIcon'
import styles from '../scss/components/cookieBar.module.scss'
import Link from 'next/link'
// import cookieImage from 'file!../public/Assets/UI_Elements/cookie.png'
import Image from 'next/image'

export default function CookieConsentContent(props) {
    return (
        <div className={styles['cookie-bar']}>
            <div className={styles['text']}>
                <span>نحن نستعمل</span>
                <Image
                    src="/Assets/UI_Elements/cookie.png"
                    alt="Picture of the author"
                    // layout="fill"
                    className={styles['cookie-image']}
                    width={18}
                    height={18}
                />
            </div>
            <div className={styles['buttons']}>
                <div className={styles['accept']}>
                    <button onClick={props.onAccept}>موافق</button>
                </div>
                <div className={styles['details']}>
                    <Link href="/privacy-policy">
                        <span>التفاصيل</span>
                    </Link>
                </div>
                <div className={styles['close']}>
                    <button onClick={props.onClose}>
                        <CloseIcon />
                    </button>
                </div>
            </div>
        </div>
    )
}
