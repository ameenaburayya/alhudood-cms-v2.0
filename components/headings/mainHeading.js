import React, { Component } from 'react'
import Link from 'next/link'


export default class MainHeading extends Component {
    render() {
        return (
            this.props.href  
            ? 
            <div className={`main-heading${this.props.withDash ? " with-dash": ""} ${this.props.className ? this.props.className : ''}`}>
                <Link href={this.props.href}>
                    <a>
                        <h2>{this.props.children}</h2>
                    </a>
                </Link>
            </div>
            :  <div className={`main-heading${this.props.withDash ? " with-dash": ""} ${this.props.className ? this.props.className : ''}`}>
                    <h2>{this.props.children}</h2>
            </div>
        )
    }
}
