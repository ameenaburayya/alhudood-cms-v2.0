import React, { Component } from 'react'
import styles from '../../scss/components/headings/pageTitle.module.scss'


export default class PageTitle extends Component {
    render() {
        return (
            <div className={`${styles['page-title']} ${this.props.withDash ? styles["with-dash"] : ""} ${this.props.className ? this.props.className : ''}`}>
                <h1>{this.props.children}</h1>
            </div> 
        )
    }
}
