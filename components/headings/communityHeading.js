import React, { Component } from 'react'

export default class CommunityHeading extends Component {
    render() {
        return (
            <h3 className="community-heading">{this.props.label}</h3>
        )
    }
}
