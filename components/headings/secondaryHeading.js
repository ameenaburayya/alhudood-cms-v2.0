import React, { Component } from 'react'

export default class SecHeading extends Component {
    render() {
        return (
            <div className={`sec-heading${this.props.withDash ? " with-dash" : ""} ${this.props.className}`}>{this.props.children}</div>
        )
    }
}
