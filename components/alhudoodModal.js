import React, {useState} from 'react'
import Modal from '@material-ui/core/Modal';
import styles from '../scss/components/alhudoodModal.module.scss'
import CloseIcon from './icons/closeIcon';
import { disableBodyScroll, enableBodyScroll, clearAllBodyScrollLocks } from 'body-scroll-lock';



var targetElement;
export default class AlhudoodModal extends React.Component {

    modalRef = React.createRef()
    

    componentDidUpdate(){
        if (this.props.modalOpen){
            setTimeout(() => {
                setTimeout(() => {
                    targetElement = this.modalRef.current;
                    disableBodyScroll(targetElement);
                }, 100);
            }, 10)
        }
        else {
            enableBodyScroll(targetElement);
        }
    }

    componentWillUnmount(){
        clearAllBodyScrollLocks()
    }


    render(){
        return (
            <Modal
                BackdropProps={{
                    className: styles.backdrop
                }}
                ref={this.modalRef}
                disableEnforceFocus
                disableAutoFocus
                open={this.props.modalOpen}
                onClose={() => this.props.handleClose()}
                className={`${styles['modal']} ${this.props.currentForm ? styles[`${this.props.currentForm}`] : ''}`}
            >
                
                <div className={styles['modal-content']}>
                    <div className={styles['close']}>
                        <CloseIcon onClick={() => this.props.handleClose()} />
                    </div>
                    {this.props.children}
                </div>
                
            </Modal>
        )
    }
   
}
