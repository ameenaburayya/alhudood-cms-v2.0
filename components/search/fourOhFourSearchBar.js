import React from "react"
import SearchBarIcon from "../icons/searchBarIcon"
import styles from "../../scss/components/search/customSearchBar.module.scss"
import SearchForm from "./searchForm"

export default function FourOhFourSearchBar() {
    return (
        <div className={styles["search"]}>
            <div className={styles["icon"]}>
                <SearchBarIcon />
            </div>
            <SearchForm />
        </div>
    )
}
