import React, { useRef, useState } from "react"
import styles from "../../scss/components/search/searchBar.module.scss"
import DropDown from "../dropDown"
import SearchBarIcon from "../icons/searchBarIcon"
import PostSorter from "../postSorter"

export default function SearchBar(props) {
    const options = [
        {
            title: "تيست",
        },
        {
            title: "تيست",
        },
        {
            title: "تيست",
        },
        {
            title: "تيست",
        },
        {
            title: "تيست",
        },
        {
            title: "تيست",
        },
        {
            title: "تيست",
        },
    ]

    const [term, setTerm] = useState("")
    const searchInput = useRef()

    const handleFieldChange = (term) => {
        setTerm(term)
        props.setTerm(term)
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        if (term.length == 0) {
            return
        }
        props.handleSearch()
        searchInput.current.blur()
    }

    return (
        <div className={styles["search-bar"]}>
            <div className={styles["icon"]}>
                <SearchBarIcon />
            </div>
            <div className={styles["search"]}>
                <form onSubmit={handleSubmit}>
                    <input
                        value={props.term}
                        ref={searchInput}
                        onChange={(e) => handleFieldChange(e.target.value)}
                        placeholder="ادخل كلمة مفتاحية..."
                    />
                </form>
                {props.desktop ? (
                    <div className={styles["sorter"]}>
                        <DropDown search options={options} />
                    </div>
                ) : null}
            </div>
        </div>
    )
}
