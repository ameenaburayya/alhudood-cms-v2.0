/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useRef, useState } from "react"
import { ClickAwayListener } from "@material-ui/core"
import SearchIcon from "../icons/searchIcon"
import styles from "../../scss/components/search/headerSearchBar.module.scss"
import SearchForm from "./searchForm"
import CloseIcon from "../icons/closeIcon"

export default function HeaderSearchBar() {
    const [open, setOpen] = useState(false)
    const form = useRef()
    const searchRef = useRef()

    const handleBarOpen = () => {
        if (open) {
            form.current.blur()
            form.current.resetTerm()
            setOpen(!open)
        } else if (!open) {
            form.current.focus()
            setOpen(!open)
        }
    }

    const handleClickAway = () => {
        setOpen(false)
    }

    return (
        <ClickAwayListener onClickAway={handleClickAway}>
            <div ref={searchRef} className={`${styles["search-bar"]} ${open ? styles["open"] : ""}`}>
                <div className={styles["icon"]} onClick={handleBarOpen}>
                    <CloseIcon className={styles["close"]} />
                    <SearchIcon className={styles["search"]} />
                </div>
                <div className={styles["form"]}>
                    <SearchForm ref={form} className={styles["field"]} header handleBar={handleBarOpen} />
                </div>
            </div>
        </ClickAwayListener>
    )
}
