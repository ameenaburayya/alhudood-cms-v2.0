import React, { forwardRef, useImperativeHandle, useRef, useState } from "react"
import { useRouter } from "next/router"

const SearchForm = forwardRef((props, ref) => {
    const [term, setTerm] = useState("")
    const [loading, setLoading] = useState(false)
    const searchInput = useRef()
    const router = useRouter()

    useImperativeHandle(ref, () => ({
        focus: () => {
            searchInput.current.focus()
        },
        blur: () => {
            searchInput.current.blur()
        },
        resetTerm: () => {
            setTerm("")
        },
    }))

    const handleSubmit = (e) => {
        e.preventDefault()
        if (term.length == 0) return
        if (!props.sideMenu && !props.header) setLoading(true)

        searchInput.current.blur()
        router.push({
            pathname: "/search",
            query: {
                s: term,
            },
        })
        setTerm("")
        if (props.modalClose) props.modalClose()
        if (props.header) props.handleBar()
    }

    const handleChange = (term) => {
        setTerm(term)
    }

    return (
        <form onSubmit={handleSubmit}>
            <input
                ref={searchInput}
                onChange={(e) => handleChange(e.target.value)}
                value={loading ? "" : term}
                placeholder={loading ? "جاري البحث..." : "ادخل كلمة مفتاحية ..."}
            />
        </form>
    )
})

export default SearchForm
