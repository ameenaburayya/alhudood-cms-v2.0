import React from 'react'
import SocialShare from '../social/socialShare';
import Comments from '../comments/comments';
import HashIcon from '../icons/hashIcon';
import Reacting from '../emojis/emojis';
import SecHeading from '../headings/secondaryHeading';
import SocialMobileShare from '../social/socialMobileShare';
import DalHeading from '../headings/dalHeading';
import PrimaryButton from '../buttons/primaryButton';
import LinkIcon from '../icons/linkIcon';
import SecondaryButton from '../buttons/secondaryButton';
import MainHeading from '../headings/mainHeading';
import {getImageUrl} from '@takeshape/routing'
import StreetSound from '../blocks/streetSound';
import { contentTypes } from '../../lib/constants';
import VotingButtons from '../aaaj/votingButtons'
import Image from 'next/image'

export default class PostBody extends React.Component{

    constructor(props) {
        super(props);
        this.content = React.createRef()
        this.shareButtons = React.createRef()
        this.wrapWords = this.wrapWords.bind(this)
        this.state = {
            blocks: null,
            sharing: false,

        }
    }

    wrapWords(str){
        console.log(str)
        var regex = /\S+/g;

        var result = str.replace(regex, function(a, b, c) {
            return "<span>" + a + "</span>";
        });
        return result
    }


    componentDidMount(){
        var result = []
        this.props.post.content.blocks.map((block, index) => {
            result[index] = block.type == "header-three" || block.type == "blockquote" ? this.wrapWords(block.text) : null;
        })
        this.setState({blocks: result})
    }


    render(){
        return (
            <div className="post_container">
                <section className="post-image">
                    <figure>
                        <picture>
                            <Image layout="fill" src={getImageUrl(this.props.post.featuredImage.path)}></Image>
                        </picture>
                    </figure>
                </section>
                <section ref={this.content} id="post-content" className="post-content">
                    {this.props.post.content.blocks.map((block, index) => 
                        block.type == "header-three"  ? <h3  dangerouslySetInnerHTML={{ __html: this.state.blocks ? this.state.blocks[index] : null }}></h3> 
                        : block.type == "blockquote" ? <p className={"quote"}  dangerouslySetInnerHTML={{ __html: this.state.blocks ? this.state.blocks[index] : null }}></p>
                        : <p className={block.type == "blockquote" ? "blockquote" : ''}><span>{block.text}</span></p>
                    )}
                </section>

                {this.props.post.streetSounds && 
                    <section className="post-street-sounds">
                        {this.props.post.streetSounds.map((sound) => 
                            <StreetSound sound={sound} />
                        )}
                    </section>
                }

                {this.props.post.contentType.slug == contentTypes.nominations.slug && 
                    <section>
                        <VotingButtons type="post" />
                    </section>
                }

                <section className="post-extra">
                    <div className="post-tags">
                        {this.props.post.tags?.length
                        ?  <> 
                                <div className="tags-icon">
                                    <HashIcon />
                                </div>
                                <div className="tags-list">
                                    <ul>
                                        {this.props.post.tags.map((tag) =>   
                                            <li key={tag.id}>{tag.name}</li>
                                        )}
                                    </ul>
                                </div>
                            </>
                        :   null
                        }
                        
                        
                    </div>
                    {this.props.post.contentType.slug != contentTypes.nominations.slug && <div className="post-emojis">
                        <SecHeading>
                        شعورك تجاه المقال؟  
                        </SecHeading>
                        <Reacting emojis={this.props.post.emojis} />
                    </div>}
                </section>

                {this.props.post && <SocialShare  post={this.props.post} target={this.content} withTransition />}

                {this.props.post.comment
                ? <>
                    <section className="post-comments">
                        <DalHeading label="تعليقات من المجتمع" />
                        <MainHeading label="تعليقات من المجتمع" withDash />
                        <Comments comments={this.props.post.comments} />
                        <SecondaryButton color="black" link="#" icon={<LinkIcon />} label="تابع النقاش على المجتمع" />
                    </section>
                </>
                : null
                }

                

            </div>
        );
    }
}
