
import { contentTypes } from '../../lib/constants';
import AAAJSource from '../aaaj/aaajSource';
import AAAJType from '../aaaj/aaajType';
import BookmarkButton from '../buttons/bookmarkButton'
import CommentsButton from '../buttons/commentsButton'
import SecondaryButton from '../buttons/secondaryButton';
import MainHeading from '../headings/mainHeading';
import SecHeading from '../headings/secondaryHeading';
import LaytahaLogo from '../logos/laytahaLogo'
import PostContentType from '../post/postContentType';


export default function PostHeader({post}) {
    const {title , correspondent, date, contentType, category, bookmark, id, aaaj} = post
    console.log(aaaj)
    return (
        <header className="post-header">

            {contentType.title && <PostContentType aaaj={aaaj} contentType={contentType} category={category} />}
            <h1 className="post-title">{title}</h1>
            {aaaj?.source && contentType.slug == contentTypes.nominations.slug && <AAAJSource source={aaaj.source} withCountry />}

            {correspondent.name.length && contentType.slug != contentTypes.nominations.slug ? <p className="post-correspondent"><span>{correspondent.name}،</span> {correspondent.description}</p> : null}
            
            <p className="post-date">{date}</p>
            {contentType.slug != contentTypes.nominations.slug && <div className="post-buttons">
                <ul>
                    <li className="bookmark">
                        <BookmarkButton id={id} bookmark={bookmark} />
                    </li>
                    <li className="comments">
                        <CommentsButton />
                    </li>
                </ul>
            </div>}
        </header>
    );
}
