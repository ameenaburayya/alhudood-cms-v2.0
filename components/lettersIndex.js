import React, { useContext, useEffect, useRef, useState } from 'react'
import BookIcon from './icons/bookIcon'
import {arabic_letters} from '../lib/letters'
import dictionaryContext from '../contexts/dictionaryContext'
import {getAllLetters} from '../requests/contentApi'
import styles from '../scss/components/lettersIndex.module.scss'
import CloseIcon from './icons/closeIcon'


var letterRefs = {}

export default function LettersIndex(props){

    // static contextType = DictionaryContext
    // const { filteredDictionaryPosts, filtered } = useContext(dictionaryContext);
    const {handleFilterDictionary, handleUnFilterDictionary} = useContext(dictionaryContext);

    const lettersInner = useRef()
    const lettersRef = useRef()
    arabic_letters.forEach((letter, index) => {
        letterRefs[`${letter.slug}_ref`] = useRef()
    });

    const [opened, setOpened] = useState(false)
    const [filtered, setFiltered] = useState(false)
    const [filteredLetter, setFilteredLetter] = useState(null)
    const [parentWidth, setParentWidth] = useState(0)
    const [letters, setLetters] = useState([])
    



    useEffect(() => {
        getLetters()
        setParentWidth(lettersRef.current.getBoundingClientRect().width)
    }, [])


    const getLetters = async () => {
        getAllLetters().then(
            res => {
                setLetters(res)
            }
        )
    }

    const handleIndexClick = () => {
        
        if (!opened && filtered){
            setTimeout(() => {
                const letter_position = letterRefs[`${filteredLetter}_ref`].current.offsetLeft
                lettersRef.current.scrollLeft = letter_position - ( parentWidth / 2 ) + 40
            }, 200)
            setOpened(true)
        }
        else if (!opened){
            setOpened(true)
        }
        else if (opened, filtered){

            setOpened(false)
        }
        else {
            setOpened(false)
        }
    }

    const handleLettersClick = () =>  {
        if(!opened){
            console.log('in in')
            setOpened(true)
        }
    }

    const handleLetterClick = (slug, index) =>{
        console.log(letterRefs[`${slug}_ref`])
        var letter_position
        // var parentWidth = lettersRef.current.getBoundingClientRect().width

        if(letterRefs[`${slug}_ref`].current.classList.contains(styles['dead'])){
            return
        }

        if(filtered && filteredLetter == slug){
            setFilteredLetter(null)
            setFiltered(false)
            handleUnFilterDictionary()
        }
        else if(filtered){
            //console.log(this[`${index}_ref`])
            letter_position = letterRefs[`${slug}_ref`].current.offsetLeft
            handleFilterDictionary(slug)
            setFilteredLetter(slug)
        }
        else if(!filtered && opened){
            handleFilterDictionary(slug)
            letter_position = letterRefs[`${slug}_ref`].current.offsetLeft
            setFiltered(true)
            setFilteredLetter(slug)
        }
        
        // setParentWidth(par)
        
        lettersRef.current.scrollLeft = letter_position - ( parentWidth / 2 ) + 40
    }


    const handleUnFilter = () => {
        handleUnFilterDictionary()
        setFiltered(false)
        setFilteredLetter(null)
    }

    return (
        <div 
            className={`
                ${styles['letters-index']} 
                ${opened ? styles['opened'] : ''} 
                ${filtered ? styles['filtered'] : ''} 
                ${props.className ? props.className : ''}
            `}
        >
            <div 
                onClick={handleIndexClick} 
                className={styles['icon']}
            >
                <BookIcon opened={styles['opened']} closed={styles['closed']} />
            </div> 
            <div className={styles['filtered-letter']}>
                <div className={styles['letter']} dangerouslySetInnerHTML={{__html: letterRefs[`${filteredLetter}_ref`]?.current.innerHTML }} />
                <div className={styles['close-icon']} onClick={() => handleUnFilter()}>
                    <CloseIcon />
                </div>
            </div>
            <div ref={lettersRef} className={styles['letters']}>
                <div 
                    ref={lettersInner} 
                    onClick={handleLettersClick} 
                    className={styles['letters-inner']}
                >
                    {letters?.map((letter, index) => 
                        <div 
                            key={index} 
                            ref={letterRefs[`${letter.slug}_ref`]} 
                            className={`${styles['letter']} ${filteredLetter == letter.slug ? styles['active'] : ''} ${letter._references.total ? styles['alive'] : styles['dead']}`} 
                            onClick={() => handleLetterClick(letter.slug, index)} 
                            dangerouslySetInnerHTML={{ __html: letter.indexSvg }} 
                        />
                    )}
                </div>
            </div>
        </div>
    )
}
