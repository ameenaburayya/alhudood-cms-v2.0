import React from 'react'
import CommentsButton from '../buttons/commentsButton'
import BookmarkButton from '../buttons/bookmarkButton'
import styles from '../../scss/components/post/postButtons.module.scss'

export default function PostButtons(props) {
    return (
        <div className={`${styles['post-buttons']} ${props.className ? props.className : ''}`}>
            <ul>
                <li className={styles['bookmark']}>
                    <BookmarkButton id={props.id} bookmark={props.bookmark} />
                </li>
                <li className={styles['comments']}>
                    <CommentsButton />
                </li>
            </ul>
        </div>
    )
}
