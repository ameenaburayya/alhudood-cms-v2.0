import React from "react"
import HashIconFilled from "../icons/hashIconFilled"
import HashIcon from "../icons/hashIcon"
import Link from "next/link"
import styles from "../../scss/components/post/postTags.module.scss"

export default function PostTags(props) {
    return (
        <>
            {props.postBlock ? (
                <div className={`${styles["block-tags"]} ${styles["post-tags"]}`}>
                    <HashIconFilled />
                    {props.tags.map((tag, i) => (
                        <div key={i} className={styles["tag"]}>
                            <Link as={`/tags/${tag.slug}`} href="/tags/[slug]">
                                <span>{tag.title}</span>
                            </Link>
                        </div>
                    ))}
                </div>
            ) : props.innerPost ? (
                <div className={`${styles["post-inner-tags"]} ${styles["post-tags"]}`}>
                    <HashIcon />
                    {props.tags.map((tag, i) => (
                        <div key={i} className="tag">
                            <Link as={`/tags/${tag.slug}`} href="/tags/[slug]">
                                <span>{tag.title}</span>
                            </Link>
                        </div>
                    ))}
                </div>
            ) : null}
        </>
    )
}
