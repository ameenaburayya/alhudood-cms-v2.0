import React from 'react'
import { contentTypes } from '../../lib/constants';
import AAAJType from '../aaaj/aaajType';
import SecHeading from '../headings/secondaryHeading';
import LaytahaLogo from '../logos/laytahaLogo';
import Link from 'next/link'

export default function PostContentType({contentType, category, aaaj}) {
    
    switch (contentType.slug) {
        case `${contentTypes.laytaha.slug}`: return <div className="post-type laytaha"><Link href={`/contentType/${contentType.slug}`}><a><span><LaytahaLogo /></span></a></Link></div>
        case `${contentTypes.nominations.slug}`: return <AAAJType type={aaaj.type} />
        default: return (
            <SecHeading 
                withDash 
                className={`post-type${contentType.slug == contentTypes.breaking.slug ? " breakingNews" : ""}`}
            >
                <Link href={`/contentType/${contentType.slug}`}><a>{contentType.title}</a></Link>
            </SecHeading>
        )
    }
}
