import React from 'react'
import styles from '../../scss/components/blocks/aaajEditrosChoicesBlock.module.scss'
import Link from 'next/link'

export default function AAAJEditrosChoicesBlock() {
    return (
        <div className={styles['aaaj-editor-block']}>
            <div className={styles['main']}>
                <div className={styles['image']}>
                    <Link href={`/`}>
                        <figure>
                            <picture>
                                <img src='/Assets/screen-shot-2020-11-12-at-16-46-08@3x.png' />
                            </picture>
                        </figure>
                    </Link>
                </div>
                <div className={styles['details']}>
                    <h2>السيسي يقر خطة لزيادة الدخل تتضمن تحويل أهرامات الجيزة لمدينة ترفيهية</h2>
                    <p>مجرمون، قتلة سفاحون يعملون في أجهزة الدولة السعودية ومؤسساتها، طاروا إلى اسطنبول لارتكاب مجزرة جماعية في شخص واحد.</p>
                </div>
            </div>
            <div className={styles['correction']}>
                <div className={styles['image']}>
                    <Link href={`/`}>
                        <figure>
                            <picture>
                                <img src='/Assets/screen-shot-2020-11-12-at-16-46-08@3x.png' />
                            </picture>
                        </figure>
                    </Link>
                </div>
                <div className={styles['details']}>
                    <p>خدمة التصحيح</p>
                    <h2>-٤ من ١٠.. خطاب كراهية ممتاز ولغة قوية، ولكن حاول قراءة التاريخ قبل استخدامه في مقالاتك</h2>
                </div>
            </div>
        </div>
    )
}
