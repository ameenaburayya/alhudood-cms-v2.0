import React from "react"
import PropsType from "prop-types"
import Link from "next/link"
import styles from "../../scss/components/blocks/dorarBlock.module.scss"

export default class DorarBlock extends React.Component {
    render() {
        const { dora } = this.props
        return (
            <div className={styles["dorarBlock"]}>
                <div className={styles["content"]}>{dora.content}</div>
                <div className={styles["post"]}>
                    <Link
                        href={`/${dora.post.category.slug}/${dora.post.contentType.slug}/${dora.post.id}`}
                    >
                        <a>
                            <span>{dora.post.title}</span>
                        </a>
                    </Link>
                </div>
                <div className={styles["date"]}>{dora.post.date}</div>
            </div>
        )
    }
}

DorarBlock.propTypes = {
    dora: PropsType.object,
}
