import React from 'react'
import PostBlock from './postBlock'
import styles from '../../scss/components/blocks/postsBlock.module.scss'

export default function PostsBlock({posts, layout, withTags, withDate, className, withBookmark, withExcerpt}) {
    return (
        <div className={`${styles['posts-block']} ${styles[`${layout}`]} ${className ? className : ''}`}>
            {posts.map((post, index) => (
                <PostBlock key={post._id} layout={layout} post={post} withTags={withTags} withExcerpt={withExcerpt} withBookmark={withBookmark} withDate={withDate} />
            ))}
        </div>
    )
}
