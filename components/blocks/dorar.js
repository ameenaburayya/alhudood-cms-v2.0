import React from "react"
import DoubleDalIcon from "../icons/doubleDalIcon"
import { Swiper, SwiperSlide } from "swiper/react"
import SwiperCore, { Pagination, Autoplay, EffectFade } from "swiper"
import { getDorar, gettingDorar } from "../../requests/contentApi"
import DorarBlock from "./dorarBlock"
import "swiper/swiper-bundle.css"
import styles from "../../scss/components/blocks/dorar.module.scss"

import { Player, Controls } from "@lottiefiles/react-lottie-player"

SwiperCore.use([Pagination, Autoplay, EffectFade])

export default class Dorar extends React.Component {
    _isMounted = false

    constructor(props) {
        super(props)
        this.animation = React.createRef()
        this.animation2 = React.createRef()
        this.doSomething = this.doSomething.bind(this)
        this.state = {
            dorar: null,
            animationData: null,
        }
    }

    async getDorar() {
        gettingDorar().then((res) => {
            console.log("dorar", res)
            if (this._isMounted) {
                this.setState({ dorar: res })
            }
        })
    }

    componentDidMount() {
        this._isMounted = true
        // import('./animation.json').then(
        //     this.setState({animationData: })
        // )

        this.getDorar()
    }

    componentWillUnmount() {
        this._isMounted = false
    }

    doSomething() {
        if (this._isMounted) {
            this.animation.current.play()
            this.animation2.current.play()
        }
        // setTimeout(() => {
        //     this.animation.current.pause()
        // }, 1900)
    }

    render() {
        return (
            <div className={`dorar ${styles["dorar"]} ${this.props.className ? this.props.className : ""}`}>
                <div className={styles["inner"]}>
                    <div className={styles["dal"]}>
                        <DoubleDalIcon />
                    </div>
                    <div className={styles["animated-icon"]}>
                        <Player
                            onEvent={(event) => {
                                if (event == "load") this.doSomething() // check event type and do something
                            }}
                            loop
                            ref={this.animation}
                            controls={true}
                            src="/Assets/animated_icons/stars.json"
                            style={{ height: "100%", width: "100%" }}
                        />
                        <Player
                            onEvent={(event) => {
                                if (event == "load") this.doSomething() // check event type and do something
                            }}
                            loop
                            ref={this.animation2}
                            controls={true}
                            src="/Assets/animated_icons/layer.json"
                            style={{ height: "100%", width: "100%" }}
                        />
                    </div>
                    <div className={styles["dorar-slider"]}>
                        <Swiper
                            spaceBetween={50}
                            pagination={{ clickable: true }}
                            effect={"fade"}
                            autoplay={{
                                delay: 5000,
                            }}
                            effect="fade"
                            //initialSlide={initialSlide}
                            slidesPerView={1}
                            loop={true}
                            //onSlideChange={(swiper) => changeOnSwipe(swiper.activeIndex)}
                            //onSwiper={(swiper) => setSwiper(swiper)}
                        >
                            {this.state.dorar?.map((dora, index) => (
                                <SwiperSlide key={index}>
                                    <DorarBlock dora={dora} />
                                </SwiperSlide>
                            ))}
                        </Swiper>
                    </div>
                </div>
            </div>
        )
    }
}
