import React, { Component } from "react"
import { getPolls, gettingPollData } from "../../requests/contentApi"
import PollQuestionIcon from "../icons/pollQuestionIcon"
import PollSkeleton from "../skeletons/pollSkeleton"
import styles from "../../scss/components/blocks/pollCard.module.scss"
import { pollVoting } from "../../requests/interactionApi"

const e2a = (s) => s.replace(/\d/g, (d) => "٠١٢٣٤٥٦٧٨٩"[d])

class PollCard extends Component {
    // Setting answers to state to reload the component with each vote

    _isMounted = false

    constructor(props) {
        super(props)
        this.getPoll = this.getPoll.bind(this)
        this.handleVote = this.handleVote.bind(this)
        this.handleResult = this.handleResult.bind(this)
        this.state = {
            poll: null,
            progressStart: false,
            voted: false,
            votedOption: null,
            allVotes: null,
        }
    }

    componentDidMount() {
        this._isMounted = true
        this.getPoll()
    }

    componentWillUnmount() {
        this._isMounted = false
    }

    handleVote(option) {
        if (this.state.voted) return

        const pollOptions = this.state.poll.options
        const newPollsOptions = pollOptions.map((item) => {
            if (item.id === option.id) item.votes++
            return item
        })
        this.setState({
            voted: true,
            votedOption: option,
            poll: { ...this.state.poll, options: newPollsOptions },
            allVotes: this.state.allVotes + 1,
        })
        pollVoting({
            pollId: this.state.poll.id,
            optionId: option.id,
        }).then((res) => {
            if (res.status == 100) {
            }
        })
        setTimeout(() => {
            this.setState({ progressStart: true })
        }, 2000)
    }

    async getPoll() {
        gettingPollData().then((res) => {
            var allVotes = 0
            res.options.map((option) => {
                allVotes += option.votes
            })
            if (this._isMounted) {
                this.setState({
                    poll: res,
                    allVotes: allVotes,
                    votedOption: res.voted,
                    voted: res.voted,
                    progressStart: res.voted,
                })
            }
        })
    }

    handleResult(item) {
        var value = Math.round((item.votes / this.state.allVotes) * 100)
        console.log("value", value)
        return value
    }

    render() {
        return (
            <>
                <div className={`${styles["poll-card"]} ${this.props.className ? this.props.className : ""}`}>
                    {(this.state.poll && (
                        <>
                            <div className={styles["poll-icon"]}>
                                <PollQuestionIcon />
                            </div>
                            <div className={styles["poll-question"]}>{this.state.poll.question}</div>
                            <div className={`${styles["poll-options"]} ${this.state.voted ? styles["voted"] : ""}`}>
                                {this.state.poll.options.map((option, index) => (
                                    <div
                                        key={index}
                                        onClick={() => this.handleVote(option, index)}
                                        className={`${styles["poll-option"]} ${
                                            this.state.votedOption == option.id ? styles["active"] : ""
                                        }`}
                                    >
                                        <div className={styles["option-content"]}>{option.text}</div>
                                        <div className={styles["option-result"]}>
                                            %{e2a(`${Math.round((option.votes / this.state.allVotes) * 100)}`)}
                                        </div>
                                        <progress
                                            id="file"
                                            value={
                                                this.state.progressStart
                                                    ? Math.round((option.votes / this.state.allVotes) * 100)
                                                    : 0
                                            }
                                            max="100"
                                        />
                                    </div>
                                ))}
                            </div>
                            {/* <Poll noStorage question={this.state.poll?.question} answers={this.state.poll?.options} vote='احتلال بلد نفطي' onVote={() => console.log('voted')} /> */}
                        </>
                    )) || <PollSkeleton className={styles["skeleton"]} />}
                </div>
            </>
        )
    }
}

export default PollCard
