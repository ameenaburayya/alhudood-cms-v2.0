import React from 'react'
import styles from '../../scss/components/blocks/postBlock.module.scss'
import Link from 'next/link'
import { getImageUrl } from '@takeshape/routing'
import {contentTypes} from '../../lib/constants'
import PostTags from '../post/postTags'
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';
import BookmarkButton from '../buttons/bookmarkButton'

export default function PostBlock(props) {
    const post = props.post

    return (
        <article className={`${styles['post-block']} ${props.white ? styles['white'] : ''}  ${props.mainPost ? styles['main-post'] : ''} ${styles[`${props.layout}`]} ${post.contentType.slug == contentTypes.breaking.slug ? styles['breaking'] : ' '}`}>
            <div className={styles['post-image']}>
                <figure>
                    <Link href={`/${post._id ? post._id : post.id}`}>
                        <a>
                            <picture>
                            <LazyLoadImage
                            // alt={image.alt}
                            // height={image.height}
                            visibleByDefault={props.layout == 'editors-choice' ? true : false}
                            // effect="blur"
                            wrapperClassName={styles['skeleton']}
                            src={getImageUrl(post.featuredImage.path)} // use normal <img> attributes as props
                            // width={image.width} 
                            />
                                {/* <img src={getImageUrl(post.featuredImage.path)} /> */}
                            </picture>
                        </a>
                    </Link>
                </figure>
            </div>
            <div className={styles['post-content']}>
                <div className={styles['contentType']}>
                    <Link href={`contentType/${post.contentType.slug}`}>
                        <a>
                            <span>{post.contentType.title}</span>
                        </a>
                    </Link>
                </div>
                {props.mainPost 
                ?   <h1 className={styles['title']}>
                        <Link href={`/${post._id}`}>
                            {post.title}
                        </Link>
                    </h1>
                :   <h2 className={styles['title']}>
                        <Link href={`/${post._id}`}>
                            {post.title}
                        </Link>
                    </h2>
                }

                
                {props.withExcerpt && post.excerpt ? 
                    <p className={styles['excerpt']}>
                        <Link href={`/${post._id}`}>{post.excerpt}</Link>
                    </p> 
                : null
                }
                <div className={styles['post-extra']}>
                    {props.withDate && post.date 
                        ? <div className={styles['date']}>{post.date}</div>
                        : null
                    }

                    {props.withTags && post.tags.length > 0
                        ? <PostTags postBlock tags={post.tags} />
                        : null
                    }

                    {props.withBookmark 
                        ? <div className={styles['post-buttons']}><BookmarkButton /></div>
                        : null
                    }

                </div>
                {/* {props.withTags && post.tags  ? <Tags></Tags> : null } */}
            </div>
            <div className={styles['post-extra']}>
                {props.withDate && post.date
                    ? <div className={styles['date']}>{post.date}</div>
                    : null
                }

                {props.withTags && post.tags.length > 0
                    ? <PostTags postBlock tags={post.tags} />
                    : null
                }

                {props.withBookmark 
                    ? <div className={styles['post-buttons']}><BookmarkButton /></div>
                    : null
                }

            </div>
        </article>
    )
}
