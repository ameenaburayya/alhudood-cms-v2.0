import React from 'react'
import {getImageUrl} from '@takeshape/routing'
import Link from 'next/link'


export default function DictionaryBlock({post, path}) {

    const id = post._id

    return (
        <article className={`alhudoodpedia__block dictionaryBlock`}>
            <Link as={`/${post.category.slug}/${post.contentType.slug}/${id}`} href="/[category]/[contentType]/[id]">
                <a>
                    <div className="block__inner">
                        <div className="block_inner_inner">
                            <div className="block__letter" dangerouslySetInnerHTML={{ __html: post.dictionary.letter.mainSvg }}>
                                {/* {post.dictionary.letter.mainSvg} */}
                            </div>
                            <div className="block__details">
                                <div className="block__icon">
                                    <figure>
                                        <picture>
                                            <img src={getImageUrl(post.dictionary.icon.path)} alt=""/>
                                        </picture>
                                    </figure>
                                </div>
                                <div className="block__title">
                                    <h2>{post.title}</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </Link>
        </article>
    )
}
