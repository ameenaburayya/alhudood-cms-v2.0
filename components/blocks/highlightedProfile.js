import React from 'react'
import Sonbolhe from '../icons/sonboleh'
import styles from '../../scss/components/highlightedProfile.module.scss'


export default function HighlightedProfile() {
    return (
        <div className={styles['highlight']}>
            <div className={styles['heading']}>
                من الحدودبيديا
            </div>
            <div className={styles['profile']}>
                <div className={`${styles['sonbeleh']}`}><Sonbolhe /></div>
                <div className={styles['content']}>
                    <div className={styles['image']}>
                        {/* <Link as={`${post.category.slug}/${post.contentType.slug}/${post.id}`} href="/[category]/[contentType]/[id]"> */}
                            <figure>
                                <picture>
                                    <img src="https://images.takeshape.io/606038f1-bbe7-4e2a-bf17-da2f9f8350b6/dev/198e35cc-62f8-4a40-99c0-7852ab99c6d8/%D8%B1%D8%AC%D8%A8-%D8%B7%D9%8A%D8%A8-%D8%A7%D8%B1%D8%AF%D9%88%D8%BA%D8%A7%D9%86-%D9%81%D9%8A-%D8%B4%D8%A8%D8%A7%D8%A8%D9%87-2.jpg" />
                                </picture>
                            </figure>
                        {/* </Link> */}
                    </div>
                    <div className={styles['title']}><span>رجب طيب اردوغان</span></div>
                    <div className={styles['button']}>
                        <a>اعرف المزيد</a>
                    </div>
                </div>
                <div className={`${styles['sonbeleh']} ${styles['invert']}`}><Sonbolhe /></div>
            </div>
        </div>
    )
}
