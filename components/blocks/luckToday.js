import React from "react"
import LuckyTempIcon from "../icons/luckyTempIcon"
import styles from "../../scss/components/blocks/luckyToday.module.scss"
import PrimaryButton from "../buttons/primaryButton"

export default function LuckyToday() {
    const handleClick = () => {
        console.log("your luck")
    }
    return (
        <div className={styles["lucky-today"]}>
            <div className="icon">
                <LuckyTempIcon />
            </div>
            <PrimaryButton link={"#"} label="حظك اليوم" />
        </div>
    )
}
