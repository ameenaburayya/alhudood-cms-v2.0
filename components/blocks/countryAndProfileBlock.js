import React from 'react'
import {getImageUrl} from '@takeshape/routing'
import Link from 'next/link'
import LinesEllipsis from 'react-lines-ellipsis'


export default function CountryAndProfileBlock({article}) {
    const id = article._id
    return (
        <article className={`alhudoodpedia__block countryProfileBlock`}>
            <div className="block__inner">
                <div className="block__title">
                    <Link as={`/${article.category.slug}/${article.contentType.slug}/${id}`} href="/[category]/[contentType]/[id]"> 
                        <h2>
                            {article.title}
                        </h2>
                    </Link>
                </div>
                <div className="block__excerpt">
                   {article.excerpt ? <LinesEllipsis
                        text={article.excerpt}
                        maxLine='4'
                        ellipsis='...'
                        trimRight
                    />
                    : null}
                </div>
                <div className="block__image">
                    <figure>
                        <Link as={`/${article.category.slug}/${article.contentType.slug}/${id}`} href="/[category]/[contentType]/[id]">
                            <picture>
                                <img src={getImageUrl(article.country ? article.country.countryMedia.flag.path : article.featuredImage.path)} alt=""/>
                            </picture>
                        </Link>
                    </figure>
                </div>
                <div className="block__button">
                    <Link as={`/${article.category.slug}/${article.contentType.slug}/${id}`} href="/[category]/[contentType]/[id]">
                        <span>اعرف المزيد</span>
                    </Link>
                </div>
            </div>
        </article>
    )
}
