import React from "react"
import { Player, Controls } from "@lottiefiles/react-lottie-player"
import styles from "../../scss/components/blocks/didYouKnow.module.scss"
import { gettingDidYouKnowData } from "../../requests/contentApi"

export default class DidYouKnow extends React.Component {
    _isMounted = false

    constructor(props) {
        super(props)
        this.state = { isStopped: false, isPaused: false, content: "" }
        this.doSomething = this.doSomething.bind(this)
        this.animation = React.createRef()
    }

    componentDidMount() {
        this._isMounted = true
        // this.getData()
        gettingDidYouKnowData().then((res) => {
            console.log("did you know ", res)
            if (this._isMounted) {
                this.setState({
                    content: res.content,
                })
            }
        })
    }
    componentWillUnmount() {
        this._isMounted = false
    }

    doSomething() {
        this.animation.current?.play()
        setTimeout(() => {
            this.animation.current.pause()
        }, 1900)
    }

    render() {
        return (
            <div className={`${styles["did-you-know"]} ${this.props.isVisible ? styles["active"] : " "}`}>
                <div className={styles["animated-icon"]}>
                    {this.props.isVisible ? this.doSomething() : null}
                    <Player
                        // onEvent={event => {
                        //     if (event == 'load')  this.doSomething(); // check event type and do something
                        // }}

                        ref={this.animation}
                        controls={true}
                        src="/Assets/animated_icons/eye.json"
                        style={{ height: "100%", width: "100%" }}
                    />
                </div>
                <div className={styles["titles"]}>
                    <div className={styles["title"]}>هل تعلم؟</div>
                    <div className={styles["hidden-title"]}>هل تعلم أن</div>
                </div>
                <div className={styles["content"]}>{this.state.content}</div>
            </div>
        )
    }
}
