import React from 'react'
import DoubleDalIcon from '../icons/doubleDalIcon'
import {getImageUrl} from '@takeshape/routing'


export default function StreetSound({sound}) {
    return (
        <div className="street-sound">
            <div className="details">
                <DoubleDalIcon />
                <div className="content">{sound.content}</div>
                <div className="name">{sound.name}</div>
            </div>
            <div className="image">
                <figure>
                    <picture>
                        <img src={getImageUrl(sound.image?.path)}></img>
                    </picture>
                </figure>
            </div>
        </div>
    )
}
