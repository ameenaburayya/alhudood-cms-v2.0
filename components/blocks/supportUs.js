import React from "react"
import PrimaryButton from "../buttons/primaryButton"
import SupporUsIcon from "../icons/supporUsIcon"
import styles from "../../scss/components/blocks/supportUs.module.scss"

export default function SupportUs() {
    return (
        <div className={styles["supportUs"]}>
            <div className={styles["icon"]}>
                <SupporUsIcon />
            </div>
            <p>لغزو احتار كل أسر, بـ هُزم النمسا الخاسر بعد, من مسرح ألمانيا البشريةً فعل. </p>
            <PrimaryButton newTab link="https://www.paypal.com/paypalme/alhudood/5" label="ادعم الحدود" />
        </div>
    )
}
