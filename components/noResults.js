import React from 'react'
import styles from '../scss/components/noResults.module.scss'
import NoResultsIcon from './icons/noResultsIcon'

export default function NoResults({text}) {
    return (
        <div className={styles['no-results']}>
            <div className={styles['icon']}>
                <NoResultsIcon />
            </div>
            <div className={styles['text']}>
                {text}
            </div>
        </div>
    )
}
