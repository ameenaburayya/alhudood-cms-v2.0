import React, {useState, useEffect} from 'react'
import {newsletters} from '../../lib/fakeData'
import NewsletterItem from './newsletterItem'
import styles from '../../scss/components/newsletters/newsletterList.module.scss'
import Drawer from '@material-ui/core/Drawer';
import NewsletterCounter from './newsletterCounter';


export default function NewslettersList() {

    const [edit, setEdit] = useState(true)
    const [drawerOpen, setDrawerOpen] = useState(false)
    const [newslettersCount, setNewslettersCount] = useState(0)
    

    const handleCheckChange = (id, state) => {
        if (state){
            setNewslettersCount(newslettersCount + 1)
            // document.getElementsByTagName('body')[0].style.overflow = "auto"
        }
        else if (!state){
            setNewslettersCount(newslettersCount - 1)
            // document.getElementsByTagName('body')[0].style.overflow = "auto"
        }
        console.log(id,state)
    }

    useEffect(() => {
        var styleSheet = document.createElement('style');
        styleSheet.type = 'text/css';
        styleSheet.id = 'styles_js';
        document.getElementsByTagName('head')[0].appendChild(styleSheet);
        styleSheet.appendChild(document.createTextNode('body {overflow: auto !important}'));
    })

    return (
        <>
        <div className={styles['newsletters-list']}>
            {newsletters.map(newsletter => 
                <NewsletterItem 
                    handleCheckChange={handleCheckChange} 
                    edit={true} 
                    key={newsletter.id} 
                    newsletter={newsletter}
                    className={styles['item']}
                    newslettersPage
                />
            )}
        </div>
        <div className="desktop-counter">
            {newslettersCount > 0 ? <NewsletterCounter newslettersCount={newslettersCount} /> : null }
        </div>
        <React.Fragment>
            <Drawer 
                className={styles['drawer']}
                anchor="bottom" 
                open={newslettersCount > 0}
                PaperProps={{
                    className: styles['inner']
                }}
            >
                <NewsletterCounter newslettersCount={newslettersCount} />
            </Drawer>
        </React.Fragment>
        </>
    )
}
