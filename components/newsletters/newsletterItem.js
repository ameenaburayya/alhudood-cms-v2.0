import React, { Component } from 'react'
import NewsletterCheckbox from '../icons/newsletterCheckbox'
import NewsletterCheckedbox from '../icons/newsletterCheckedbox'
import styles from '../../scss/components/newsletters/newsletterItem.module.scss'
import NewsletterComingSoon from '../svg/newsletterComingSoon'

export default class NewsletterItem extends Component {

    constructor(props){
        super(props)
        this.handleCheck = this.handleCheck.bind(this)
        this.state = {
            check: false
        }
    }


    handleCheck(){
        if(this.props.edit){
            this.props.handleCheckChange(this.props.newsletter.id, !this.state.check)
            this.setState({
                check: !this.state.check
            })
            //this.props.handleCheck(this.props.newsletter.id)
        }
    }

    render() {
        const newsletter = this.props.newsletter
        return (
            <div 
                className = {`${this.props.className || ''} ${styles['newsletter']} ${this.props.newslettersPage ? styles['newsletters-page'] : ''}`} 
                onClick = {this.props.newsletter.status != "soon" ? this.handleCheck : null} 
            >
                <div className={`${styles['inner']} ${this.props.newsletter.status == "soon" ? styles['coming-soon'] : ''} ${this.props.edit ? styles['active'] : ''}`}>
                    <div className={styles['icon']}>
                        {this.state.check && this.props.newsletter.status != "soon"
                        ? <NewsletterCheckedbox />
                        : <NewsletterCheckbox />
                        }
                    </div>
                    <div className={styles['details']}>
                        <div className={styles['title']}>{newsletter.title}</div>
                        {this.props.newsletter.status != "soon" && <div className={styles['description']}>{newsletter.description}</div>}
                    </div>
                    {this.props.newsletter.status == "soon" && 
                        <div className={styles['coming-soon-stamp']}>
                            <NewsletterComingSoon />
                        </div>
                    }
                </div>
            </div>
        )
    }
}
