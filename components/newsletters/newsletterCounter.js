import React from 'react'
import styles from '../../scss/components/newsletters/newsletterCounter.module.scss'
import NewsletterIcon from '../icons/newsletterIcon'


const e2a = s => s.replace(/\d/g, d => '٠١٢٣٤٥٦٧٨٩'[d])

export default function NewsletterCounter(props) {
    return (
        <div className={styles['counter']}>
            <div className={styles['title']}>
                قمت بتحديد <br /> من النشرات الإخبارية
            </div>
            <div className={styles['count']}>
                {props.newslettersCount ? e2a(`${props.newslettersCount}`) : null}
                <div className={styles['icon']}><NewsletterIcon /></div>
            </div>
        </div>
    )
}
