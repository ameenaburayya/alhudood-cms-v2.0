import React from 'react'
import NewsletterBar1 from '../svg/newsletterBar1'
import NewsletterBar2 from '../svg/newsletterBar2'
import NewsletterCheckbox from '../icons/newsletterCheckbox'
import NewsletterIcon from '../icons/newsletterIcon'
import NewsletterBarDesktop from '../svg/newsletterBarDesktop'



export default class NewsletterBox extends React.Component{

    constructor(props){
        super(props)
        this.handleTyping = this.handleTyping.bind(this)
        this.handleValidation = this.handleValidation.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.email = React.createRef()
        this.state = {
            emailError: false,
        }
    }


    handleValidation(value){
        const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if (!value){
            this.setState({emailError: true})
            return false
        }
        else if (!re.test(value)){
            this.setState({emailError: true})
            return false
        }
        return true
    }

    handleSubmit(){
        const value = this.email.current.value
        if(this.handleValidation(value)){
        }
    }

    handleTyping(){
        const value = this.email.current.value

        if (value){
            if (this.state.emailError) this.setState({emailError: false})
        }
    }

    render(){
        return (
            <div className="newsletterBox">
                <div className="bar">
                    {!this.props.desktop ? <NewsletterBar2 /> : null }
                    {this.props.desktop ? <NewsletterBarDesktop /> : null}
                </div>
                <div className="body">
                    <div className="details">
                        <div className="content">
                            <div className="title">الحدود كل صباح</div>
                            <div className="description">كما أن وقام وبدأت، لم أدوات للمجهود بلا، كما أن وقام وبدأت، لم أدوات للمجهود بلا</div>
                            <div className={`email-field${this.state.emailError ? " error" : " "}`}>
                                <input ref={this.email} onChange={this.handleTyping} type="email" placeholder="البريد الإلكتروني…" />
                            </div>
                        </div>
                        <div className={`checkbox}`}>
                            <NewsletterCheckbox  />
                        </div>
                        <div className="icon">
                            <NewsletterIcon />
                        </div>
                    </div>
                    <div className="submit">
                        <button onClick={this.handleSubmit}>اشتراك</button>
                    </div>
                </div>
                <div className="bar">{!this.props.desktop ? <NewsletterBar1 /> : null}</div>
            </div>
        )
    }
}
