import React from "react"
import styles from "../../scss/components/newsletters/footerNewsletter.module.scss"

export default class FooterNewsletter extends React.Component {
    state = {
        value: false,
    }

    handleEmail(value) {
        if (value) this.setState({ value: true })
        else this.setState({ value: false })
    }

    render() {
        return (
            <div className={`${styles.newsletter} ${this.props.className ? this.props.className : ""}`}>
                <form>
                    <div className={`${styles["email"]} ${this.state.value ? styles["inactive"] : ""}`}>
                        <label htmlFor="footer-email">البريد الإلكتروني</label>
                        <input id="footer-email" onChange={(e) => this.handleEmail(e.target.value)} type="email" />
                        <span className={styles["cursor"]} />
                    </div>

                    <button type="submit">اشترك</button>
                </form>
                <div className={styles["newsletter-sentence"]}>
                    أدخل بريدك الالكتروني هنا لتشترك بنشرتنا البريدية و تدخل السحب على سيارة لكزس مقدّمة من دائرة
                    الجمارك
                </div>
            </div>
        )
    }
}
