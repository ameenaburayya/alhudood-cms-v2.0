import React, { useEffect, useRef, useState } from "react"
// import styles from '../scss/components/pageCursor.module.scss'

export default function PageCursor() {
    const [mouseLocation, setMouseLocation] = useState()
    const cursor = useRef()

    useEffect(() => {
        document.querySelectorAll("a").forEach((item) => {
            item.addEventListener("mouseover", (event) => {
                cursor.current.classList.add(styles["cursor-custom-hover"])
                // console.log(item)
            })
            item.addEventListener("mouseout", (event) => {
                cursor.current.classList.remove(styles["cursor-custom-hover"])
                // console.log(item)
            })
        })
        // handleMouseMove()
        window.addEventListener("mousemove", handleMouseMove)

        return (_) => {
            window.removeEventListener("mousemove", handleMouseMove)
        }
    })

    const handleMouseMove = (e) => {
        if (window.innerWidth >= 1024) {
            // console.log(e)
            var x = e.clientX
            var y = e.clientY
            // console.log(cursor)
            cursor.current.style.transform = `translate3d(${x}px, ${y}px, 0px)`
            // setMouseLocation({x, y})
            // setTimeout(function() {
            //     $('.cursor-custom').css('transform','translate3d('+ x +'px,'+ y +'px, 0px)');
            // }, 70);

            // console.log(document.querySelectorAll('a'))

            // $('a, button').hover(function(){
            //     $('.cursor-custom').addClass('cursor-custom-hover');
            //   },
            //    function() {
            //        $('.cursor-custom').removeClass('cursor-custom-hover');
            //    });
        }
    }

    return (
        <div
            ref={cursor}
            // style={{transform: `translate3d(${mouseLocation?.x}px, ${mouseLocation?.y}px, 0px)`}}
            className={styles["cursor-custom"]}
        />
    )
}
