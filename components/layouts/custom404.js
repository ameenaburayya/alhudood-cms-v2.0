import React, { Component } from "react"
import Error404 from "../svg/error404"
import styles from "../../scss/components/layouts/custom404.module.scss"
import FourOhFourSearchBar from "../search/fourOhFourSearchBar"

export default class Custom404 extends Component {
    render() {
        if (this.props.error) {
            return (
                <div className={styles["custom404"]}>
                    <div className={styles["error-image"]}>
                        <img src={this.props.error.image} alt="404 illustration" />
                    </div>
                    <div className={styles["error-content"]}>
                        <div className={styles["error-svg"]}>
                            <Error404 />
                        </div>
                        <div className={styles["error-text"]}>{this.props.error.text}</div>
                        <div className={styles["error-search"]}>
                            <FourOhFourSearchBar />
                        </div>
                    </div>
                </div>
            )
        }
        return "loading"
    }
}
