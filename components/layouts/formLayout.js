import React from 'react'
import styles from '../../scss/components/layouts/formLayout.module.scss'
import SloganCircle from '../svg/sloganCircle'
import AlhudoodLogo from '../logos/alhudoodLogo'

export default class FormLayout extends React.Component {

    constructor(props){
        super(props)
        this.state = {
            width: 0
        }
    }


    componentDidMount(){
        this.updateWindowWidth();
        window.addEventListener('resize', this.updateWindowWidth);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowWidth);
    }

    updateWindowWidth() {
        const vh = window.innerHeight * 0.01;
        document.documentElement.style.setProperty('--innerHeight', `${vh}px`);
    }

    render(){
        return (
            <div className={`${styles['formLayout']} ${this.props.modal ? styles['modal'] : ''} ${this.props.currentForm ? styles[`${this.props.currentForm}`] : ''}`}>
                <div className={styles['content']}>
                    <div className={styles['inner']}>
                        {this.props.children}
                    </div>
                </div>
                <div className={styles['sideBox']}>
                    <div className={styles['inner']}>
                        <div className={styles['rotational']}>
                            <div className={styles['slogan']}><SloganCircle /></div>
                            <div className={styles['logo']}><AlhudoodLogo /></div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
    
}
