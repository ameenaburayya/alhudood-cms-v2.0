import Footer from '../../includes/footer/footer'
import Header from '../../includes/header/header'
import Head from 'next/head'
import React from 'react'
import { getImageUrl } from '@takeshape/routing'
import styles from '../../scss/components/layouts/categoryLayout.module.scss'

export default function CategoryLayout({ children, category, pageTitle }) {
	const categoryTitle = pageTitle ? pageTitle : category.title
	const categoryDescription = category.excerpt ? category.excerpt : ''
	// const categoryImage = getImageUrl(category.featuredImage.path)
	const categoryImage = 'getImageUrl(category.featuredImage.path)'
	const categoryId = category._id
	return (
		<React.Fragment>
			<Head>
				<title>{categoryTitle} - شبكة الحدود</title>

				<meta name='description' content={categoryDescription} />

				<meta
					property='article:publisher'
					content='https://facebook.com/AlHudoodNet/'
				/>

				<meta property='og:type' content='article' />
				<meta property='og:title' content={categoryTitle} />
				<meta property='og:description' content={categoryDescription} />
				<meta property='og:image' content={categoryImage} />
				<meta
					property='og:url'
					content={`https://alhudood.net/${categoryId}`}
				/>

				<meta name='twitter:card' content='summary_large_image' />
				<meta name='twitter:title' content={categoryTitle} />
				<meta
					name='twitter:description'
					content={categoryDescription}
				/>
				<meta name='twitter:image' content={categoryImage} />
			</Head>
			<main className={styles['category']}>{children}</main>
		</React.Fragment>
	)
}
