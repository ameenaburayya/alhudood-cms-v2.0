import React from "react"
import Head from "next/head"
import styles from "../../scss/components/layouts/pageLayout.module.scss"

export default function PageLayout({
    children,
    pageTitle,
    pageDescription,
    withPadding,
    noCSS,
    className,
    customTitle,
    pageImage,
    smallWidth,
}) {
    // const postImage = getImageUrl(post.featuredImage.path)
    const title = customTitle ? pageTitle : `${pageTitle} - شبكة الحدود`
    const description = pageDescription
        ? pageDescription
        : "الحدود منصة إبداع عربي تقدم محتوى ساخر. مسجلة كشركة مُسَاهمة مُجتمعيّة لا تهدف للربح"
    const image = pageImage || "https://alhudood.net/default-thumbnail.png"
    return (
        <>
            <Head>
                <title>{`${title}`}</title>

                <meta name="description" content={description} />

                <meta property="og:type" content="page" />
                <meta property="og:title" content={`${pageTitle}`} />
                <meta property="og:description" content={description} />
                <meta property="og:image" content={image} />

                <meta name="twitter:title" content={`${pageTitle}`} />
                <meta name="twitter:description" content={description} />
                <meta name="twitter:image" content={image} />
            </Head>
            <main
                className={`${!noCSS ? styles["page"] : ""} ${withPadding ? styles["padding-only"] : ""} ${
                    smallWidth ? styles["small-width"] : ""
                } ${className ? className : ""}`}
            >
                {children}
            </main>
        </>
    )
}
