import React from 'react'
import {getImageUrl} from '@takeshape/routing'
import DictionaryLetterIcon from '../icons/dictionaryLetterIcon'
import CircularProgress from '@material-ui/core/CircularProgress';
import ExpandIcon from '../icons/expandIcon';
import SocialShare from '../social/socialShare'
import DefinitionIcon from '../icons/definitionIcon'
import ArrowDownIcon from '../icons/arrowDownIcon'
import { isMobileOnly, isEdge, isOpera, browserName } from 'react-device-detect';
import styles from '../../scss/components/layouts/dictionary.module.scss'


export default class Dictionary extends React.Component {

    constructor(){
        super()
        this.map = React.createRef();
        this.info = React.createRef()
        this.updateWindowWidth = this.updateWindowWidth.bind(this)
        this.getImgSize = this.getImgSize.bind(this)
        this.state = {
            expanded: false,
            image_status: null
        }
    }

    

    componentDidMount(){
        const dimensions = this.updateWindowWidth();
        window.addEventListener('resize', this.updateWindowWidth);
        this.getImgSize(this.props.post?.dictionary.icon.path, dimensions)
        console.log(isMobileOnly, isEdge, browserName)
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowWidth);
    }



    getImgSize(imgSrc) {
        
        var newImg = new Image();
        newImg.onload = () => {
            var height = newImg.height;
            var width = newImg.width;
            var margin_amount;
            const map_status = height > width ? "tall" : "wide"
            if (map_status == "tall"){
                margin_amount = this.state.width * 0.4 / 1.6
            }
            else if (map_status == "wide"){
                margin_amount = this.state.width * 0.9 / 4
            }

            if(this.state.width > 767 && map_status == "wide"){
                    margin_amount = 550 * 0.9 / 4
            }
            if (this.state.width > 767){
                margin_amount = 400 / 3
            }
            this.setState({image_status: map_status, margin_amount: margin_amount})
            //alert ('The image size is '+width+'*'+height);
        }
    
        newImg.src = getImageUrl(imgSrc); // this must be done AFTER setting onload

    }



    updateWindowWidth() {
        const dimensions = {
            height: window.innerHeight,
            width: window.innerWidth
        }

        var margin_amount;
        if (this.state.image_status == "tall"){
            margin_amount = dimensions.width * 0.4 / 1.6
        }
        else if (this.state.image_status == "wide"){
            margin_amount = dimensions.width * 0.9 / 4
        }

        if(dimensions.width > 767 && this.state.image_status == "wide"){
                margin_amount = 550 * 0.9 / 4
        }

        if (dimensions.width > 767){
            margin_amount = 400 / 3
        }
        this.setState({ width: dimensions.width, height: dimensions.height, margin_amount: margin_amount});
        return dimensions
    }

    render(){
        const post = this.props.post

        if (!post){
            return <CircularProgress />
        }
        
        return (
            
            <div className={`${styles['dictionary']} ${isMobileOnly && isEdge || isOpera || browserName == "Samsung Browser" ? styles['edge'] : ''}`}>
                <div className={styles['dictionary-letter-section']}>
                    <div className={styles['dictionary-letter']} dangerouslySetInnerHTML={{ __html: post.dictionary.letter.mainSvg }} />
                    <div className={styles['dictionary-letter-icon']}>
                        <DictionaryLetterIcon />
                    </div>
                </div>
                <div className={styles['dictionary-content']}>
                    <div className={`${styles['dictionary-icon']} ${styles[`${this.state.image_status}`]}`}>
                        <figure>
                            <picture>
                                <img src={getImageUrl(post.dictionary.icon.path)} />
                            </picture>
                        </figure>
                    </div>
                    <div className={styles['dictionary-content-body']}>
                        <div className={styles['dictionary-title']}><h1>{post.title}</h1></div>
                        <div className={styles['dictionary-divider']}><DefinitionIcon /></div>
                        <div className={styles['dictionary-definition']}>
                            {post.dictionary.definition}
                        </div>
                        {post && <div className={styles['dictionary-share']}><SocialShare post={post} /></div>}
                        <div onClick={this.props.handleNext} className={styles['dictionary-next']}>
                            <ArrowDownIcon />
                        </div>
                    </div>
                    
                </div>
            </div>     
        )
    }
}
