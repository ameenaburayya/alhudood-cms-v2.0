import React from 'react'
import Link from 'next/link'
import AlhudoodpediaIcon from './icons/alhudoodpediaIcon'


export default function Breadcrumbs({category, contentType, title, className}) {
    return (
        <div className={`breadcrumb ${className || ''}`}>
            <div className="icon"><AlhudoodpediaIcon /></div>
                <div className="breadcrumb-item link">
                    {contentType 
                    ?  <>
                        <Link as={`/${category.slug}/${contentType.slug}`} href='/[category]/[contentType]'>
                            <span>{category.title}</span>
                        </Link>
                    </>
                    : <>
                        <Link as={`/${category.slug}`} href='/[category]'>
                            <span>{category.title}</span>
                        </Link>
                    </>
                    }
                </div>
                <div className="breadcrumb-item">
                    <span>{title}</span>
                </div> 
        </div>
    )
}
