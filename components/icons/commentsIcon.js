
import React from 'react';



export default class CommentsIcon extends React.Component {
    render() {
        return (
            <svg
            xmlns="http://www.w3.org/2000/svg"
            width="150"
            height="139.003"
            data-name="Group 4309"
            viewBox="0 0 150 139.003"
            >
            <g fill="#dfe4ea" data-name="Group 4308">
                <path
                d="M132.42 107.313L132.353 20H16v87.313h43.509l14.557 24.26 16.981-24.26zm-58.068 16.039l-12.309-20.516H20.478V24.478h107.4l.058 78.358H88.714z"
                data-name="Path 6376"
                transform="translate(-16 -20)"
                ></path>
                <path
                d="M103.655 35.828l20.7-.018v78.15H82.73l-12.1 22.191-12.1-22.191H46v4.478h9.864L70.627 145.5l14.763-27.063h43.446V30.916l-25.181-.016z"
                data-name="Path 6377"
                transform="translate(21.164 -6.497)"
                ></path>
                <path
                d="M0 0H53.731V4.478H0z"
                data-name="Rectangle 3583"
                transform="translate(26.866 24.627)"
                ></path>
                <path
                d="M0 0H42.537V4.478H0z"
                data-name="Rectangle 3584"
                transform="translate(26.866 44.776)"
                ></path>
            </g>
            </svg>
        )
    }
}