
import React from 'react';



export default class DictionaryLetterIcon extends React.Component {
    render() {
        return (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                x="0"
                y="0"
                enableBackground="new 0 0 30.3 11.5"
                version="1.1"
                viewBox="0 0 30.3 11.5"
                xmlSpace="preserve"
                >
                <g className="st0">
                    <path
                    d="M6.9 1.3c-2.4 1-3.7 2.1-3.7 4 0 1.7 1.3 2.7 3 3 .3 0 .7.4.7 1 0 1.3-1.2 1.9-2.1 1.9h-.2C2.5 11.1 0 9.3 0 6.3 0 2.9 3.2.8 6.4 0l.5 1.3zM25.8.3c2.1 0 4.6 1.8 4.6 4.9 0 3.4-3.2 5.5-6.3 6.3l-.4-1.3c2.3-1 3.7-2.1 3.7-4 0-1.6-1.3-2.6-3-2.9-.3 0-.6-.4-.6-1.1-.2-1.3 1-1.9 2-1.9z"
                    className="st1"
                    ></path>
                </g>
            </svg>
        )
    }
}