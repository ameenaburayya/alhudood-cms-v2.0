
import React from 'react';



export default class PlayIcon extends React.Component {
    render() {
        return (
            <svg
            xmlns="http://www.w3.org/2000/svg"
            width="20.079"
            height="23.163"
            viewBox="0 0 20.079 23.163"
            >
                <path
                    d="M28.8 46.763l20.079-11.581L28.8 23.6z"
                    data-name="Path 5613"
                    transform="translate(-28.8 -23.6)"
                ></path>
            </svg>
        )
    }
}