
import React from 'react';



export default class HashIcon extends React.Component {
    render() {
        return (
            <svg
            xmlns="http://www.w3.org/2000/svg"
            x="0"
            y="0"
            enableBackground="new 0 0 53.4 49.6"
            version="1.1"
            viewBox="0 0 53.4 49.6"
            xmlSpace="preserve"
            >
            <g>
                <path
                d="M18.9 31.6L18.4 35.1 14.9 35 15.4 31.5z"
                className="custom-color"
                fill="transparent"
                ></path>
                <path
                d="M18.2 36.5L16.8 48.2 13.3 48.2 14.7 36.4z"
                className="custom-color"
                fill="transparent"
                ></path>
                <path
                d="M20.4 19.1L19 30.2 15.5 30.1 16.9 19z"
                className="custom-color"
                fill="transparent"
                ></path>
                <path
                d="M21 14.2L20.6 17.7 17.1 17.6 17.5 14.1z"
                className="custom-color"
                fill="transparent"
                ></path>
                <path
                d="M22.6 1.4L21.2 12.8 17.7 12.7 19.1 1.4z"
                className="custom-color"
                fill="transparent"
                ></path>
                <path
                fill="#AFB7C4"
                d="M36.5 30.6l1.4-11.1-3.5-.1L33 30.5l3.5.1zm1.6-12.5l.4-3.5-3.5-.1-.4 3.5 3.5.1zm.6-4.9l1.5-11.8h-3.5l-1.5 11.7 3.5.1zm10.1 22.6l.5-3.5-11.5-.3-.4 3.5 11.4.3zm2.5-17.4l.5-3.5-11.9-.2-.4 3.5 11.8.2zM19 30.2l1.4-11.1-3.5-.1-1.4 11.1 3.5.1zm1.6-12.5l.4-3.5-3.5-.1-.4 3.5 3.5.1zm.6-4.9l1.4-11.4h-3.5l-1.4 11.3 3.5.1zm12.4 1.7l-11.2-.2-.4 3.5 11.2.2.4-3.5zM31 35.4l.4-3.5-11.2-.2-.4 3.5 11.2.2zm.6-4.9L33 19.4l-11.2-.2-1.4 11.1 11.2.2zm4.2 6.4l-3.5-.1-1.4 11.4h3.5l1.4-11.3zm.6-4.9l-3.5-.1-.4 3.5 3.5.1.4-3.5zM16.1 14.1l-11.4-.2-.5 3.5 11.5.2.4-3.5zM13.5 35l.4-3.5-11.7-.3-.5 3.5 11.8.3zm.6-4.9L15.5 19l-11.4-.2H2.5l.2-1.6.5-3.5.2-1.2h1.2l11.6.2 1.4-11.5.3-1.2h6.3L24 1.6l-1.4 11.3 11.2.2 1.5-11.9.1-1.2h6.3l-.2 1.6L40 13.2l11.7.2h1.6l-.2 1.6-.5 3.5-.2 1.2h-1.2l-11.9-.2-1.4 11.1 11.4.2h1.6l-.2 1.6-.5 3.6-.2 1.2h-1.2L37.2 37l-1.4 11.5-.1 1.2h-6.3l.2-1.6L31 36.8l-11.2-.2-1.5 11.9-.3 1.1h-6.3l.2-1.6 1.5-11.6-11.8-.2H0l.2-1.6.6-3.6.2-1.2h1.2l11.9.3zm4.1 6.4l-3.5-.1-1.5 11.8h3.5l1.5-11.7zM14.9 35l3.5.1.4-3.5-3.5-.1-.4 3.5z"
                ></path>
            </g>
            </svg>
        )
    }
}