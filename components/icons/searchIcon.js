import React from "react"

export default class SearchIcon extends React.Component {
    render() {
        return (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                x="0"
                y="0"
                enableBackground="new 0 0 87.8 87.9"
                version="1.1"
                viewBox="0 0 87.8 87.9"
                xmlSpace="preserve"
                onClick={this.props.onClick || null}
                className={this.props.className ? this.props.className : ""}
            >
                <path d="M73.1 68.1c13.8-16.4 12.8-40.7-2.4-55.9C54.5-4 28.3-4 12.1 12.1-4 28.3-4 54.5 12.1 70.7 27.3 85.9 51.7 86.9 68 73.1l14.8 14.8 5-5-14.7-14.8zm2.6-26.7c0 18.9-15.4 34.3-34.3 34.3S7.1 60.3 7.1 41.4 22.5 7.1 41.4 7.1s34.3 15.4 34.3 34.3z"></path>
            </svg>
        )
    }
}
