import React, { Component } from 'react'

export default class RemoveImageIcon extends Component {
    render() {
        return (
            <svg
            xmlns="http://www.w3.org/2000/svg"
            width="16.061"
            height="16.061"
            viewBox="0 0 16.061 16.061"
            >
            <g
                fill="none"
                stroke="#fff"
                strokeWidth="1.5"
                data-name="Close icon"
                transform="translate(.53 .53)"
            >
                <path d="M0 0L15 15" data-name="Line 57"></path>
                <path d="M15 0L0 15" data-name="Line 58"></path>
            </g>
            </svg>
        )
    }
}
