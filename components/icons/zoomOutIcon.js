import React from 'react'

export default function ZoomOutIcon(props) {
    return (
        <svg
        xmlns="http://www.w3.org/2000/svg"
        width="26.208"
        height="26.208"
        viewBox="0 0 26.208 26.208"
        onClick={props.onClick}
        >
        <g
            stroke="#000"
            strokeWidth="1"
            data-name="Group 4437"
            transform="translate(.5 .5)"
        >
            <path
            d="M9.369 10.618H6.246v1.249h9.994v-1.249H9.369z"
            data-name="Path 6612"
            ></path>
            <path
            d="M19.623 18.738a11.2 11.2 0 002.863-7.5A11.242 11.242 0 0011.243 0 11.242 11.242 0 000 11.243a11.243 11.243 0 0011.243 11.243 11.2 11.2 0 007.5-2.864L24.117 25l.883-.883zm-8.38 2.5a9.994 9.994 0 01-9.994-9.995 9.993 9.993 0 019.994-9.994 9.993 9.993 0 019.994 9.994 9.994 9.994 0 01-9.994 9.994z"
            data-name="Path 6613"
            ></path>
        </g>
        </svg>
    )
}
