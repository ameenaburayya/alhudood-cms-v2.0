
import React from 'react';

export default class LinkIcon extends React.Component {
    render() {
        return (
            <svg
            xmlns="http://www.w3.org/2000/svg"
            x="0"
            y="0"
            enableBackground="new 0 0 95.1 95.1"
            version="1.1"
            viewBox="0 0 95.1 95.1"
            xmlSpace="preserve"
            >
            <path
                d="M81.3 55.5v25.2c0 5.8-4.8 10.6-10.6 10.6H14.5c-5.9 0-10.6-4.8-10.6-10.6V24.4c0-5.8 4.8-10.6 10.6-10.6h25.2c1.2 0 1.9-1 1.9-1.9 0-.9-.6-1.9-1.9-1.9H14.5C6.5 10 0 16.5 0 24.4v56.2C0 88.6 6.5 95 14.5 95h56.2c8 0 14.4-6.5 14.4-14.4V55.5c0-2.5-3.8-2.5-3.8 0z"
                className="st0"
            ></path>
            <path
                d="M95.1 2.1c0-.6-.2-1.1-.6-1.5-.4-.4-.9-.6-1.4-.6H65.5c-1.2 0-1.9 1-1.9 1.9s.6 1.9 1.9 1.9h23.1L38.4 54c-.5.5-.7 1.1-.5 1.8.2.7.8 1.3 1.4 1.4.2 0 .3.1.5.1.5 0 .9-.2 1.3-.6L91.3 6.5v23.1c0 2.5 3.8 2.5 3.8 0V2.1z"
                className="st0"
            ></path>
            </svg>
        )
    }
}