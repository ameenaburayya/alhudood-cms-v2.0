
import React from 'react';



export default class HashIconFilled extends React.Component {
    render() {
        return (
            <svg
            xmlns="http://www.w3.org/2000/svg"
            x="0"
            y="0"
            enableBackground="new 0 0 173.9 152"
            version="1.1"
            viewBox="0 0 173.9 152"
            xmlSpace="preserve"
            >
                <path
                d="M173.9 53.6v-8h-45.2L144.2 3l.1-.2-7.6-2.8-16.6 45.6H75.9L91.4 3l.1-.2L83.9 0 67.4 45.6H15.1v8h49.4L48.1 98.4H0v8h45.2L29.7 149l-.1.2 7.5 2.7 16.6-45.6H98L82.5 149l-.1.2 7.5 2.7 16.6-45.6h52.3v-8h-49.4l16.3-44.8h48.2zm-56.7 0l-16.3 44.8H56.7L73 53.6h44.2z"
                className="st0"
                ></path>
            </svg>
        )
    }
}