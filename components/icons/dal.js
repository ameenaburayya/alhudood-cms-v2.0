import React, { Component } from 'react'

export default class Dal extends Component {
    render() {
        return (
            <div className={`${this.props.className? this.props.className : '' } dal`}>
                <svg
                xmlns="http://www.w3.org/2000/svg"
                x="0"
                y="0"
                fill={this.props.color ? this.props.color : null}
                enableBackground="new 0 0 354.8 634.4"
                version="1.1"
                viewBox="0 0 354.8 634.4"
                xmlSpace="preserve"
                >
                <path d="M354.8 225.7c0-91.4-46-165.9-136.8-221.1-5.2-3.1-9.5-4.6-12.9-4.6-1.8 0-6.1 0-9.5 8.9-33.4 73.6-61 132.2-82.5 174.5-4.6 14.7-.6 26.7 12.3 37.1 84.6 69.3 126 122.4 126 162.5 0 10.4-7.1 31.6-68.7 93.2-44.9 46-94 87.6-146.9 124.2C5.2 622.2.6 631.4 0 633.8c1.9.5 3.9.7 5.8.6 5.8 0 14.1-1.2 24.8-3.7 87.1-21.5 154.9-56.1 201.2-103 26.4-26.7 52.7-70.2 78.8-129.1 29.2-65.9 44.2-124.2 44.2-172.9z"></path>
                </svg>
            </div>
        )
    }
}
