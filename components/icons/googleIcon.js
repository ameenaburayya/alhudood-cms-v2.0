import React from 'react'

export default function GoogleIcon() {
    return (
        <svg
        xmlns="http://www.w3.org/2000/svg"
        width="16"
        height="16.323"
        viewBox="0 0 16 16.323"
        >
        <path
            d="M279.939 224.758a9.6 9.6 0 00-.141-1.658h-7.7v3.143h4.408a3.778 3.778 0 01-1.631 2.48v2.039h2.63a7.97 7.97 0 002.434-6.004z"
            data-name="Path 5733"
            transform="translate(-263.939 -216.409)"
        ></path>
        <path
            d="M36.193 330.9a7.813 7.813 0 005.41-1.97l-2.63-2.039a4.95 4.95 0 01-7.359-2.588H28.9v2.1a8.162 8.162 0 007.293 4.497z"
            data-name="Path 5734"
            transform="translate(-28.032 -314.574)"
        ></path>
        <path
            d="M3.529 155.227a4.888 4.888 0 010-3.125V150H.818a8.168 8.168 0 000 7.329z"
            data-name="Path 5735"
            transform="translate(.05 -145.501)"
        ></path>
        <path
            d="M36.193 3.212a4.435 4.435 0 013.131 1.224l2.33-2.33a7.844 7.844 0 00-5.461-2.124A8.16 8.16 0 0028.9 4.48l2.711 2.1a4.882 4.882 0 014.582-3.368z"
            data-name="Path 5736"
            transform="translate(-28.032 .018)"
        ></path>
        </svg>
    )
}
