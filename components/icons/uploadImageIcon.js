import React, { Component } from 'react'

export default class UploadImageIcon extends Component {
    render() {
        return (
            <svg
            xmlns="http://www.w3.org/2000/svg"
            width="17.746"
            height="15"
            viewBox="0 0 17.746 15"
            className={this.props.className}
          >
            <g data-name="Group 4341" transform="translate(-7.16 -26.29)">
              <path
                fill="#fff"
                d="M24.906 28.927h-4.585l-.661-2.637h-7.249l-.665 2.637H7.16V41.29h17.746zm-.828 11.535H7.989V29.755h4.4l.661-2.635h5.961l.661 2.635h4.4z"
                data-name="Path 6384"
              ></path>
              <g
                fill="none"
                stroke="#fff"
                strokeMiterlimit="10"
                strokeWidth="1.5"
                data-name="Group 4340"
                transform="translate(11.891 30.89)"
              >
                <path
                  d="M0 0L0 8.285"
                  data-name="Line 281"
                  transform="translate(4.143)"
                ></path>
                <path
                  d="M8.285 0L0 0"
                  data-name="Line 282"
                  transform="translate(0 4.143)"
                ></path>
              </g>
            </g>
          </svg>
        )
    }
}
