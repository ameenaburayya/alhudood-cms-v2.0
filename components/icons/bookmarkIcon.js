
export default function BookmarkIcon({filled}) {
    return (
        <>
            {filled
            ? <>
                 <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="14"
                    height="17.82"
                    viewBox="0 0 14 17.82"
                    >
                    <path
                        d="M11.43 8v17.82l7-5.09 7 5.09V8z"
                        data-name="Path 6375"
                        transform="translate(-11.43 -8)"
                    ></path>
                    </svg>
            </>
            : <>
                    <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="104"
                    height="133.214"
                    viewBox="0 0 104 133.214"
                    >
                    <path
                        fill="none"
                        stroke="#dfe4ea"
                        strokeWidth="4"
                        d="M2 2v127.287l50-36.356 50 36.356V2z"
                        data-name="Path 6375"
                    ></path>
                    </svg>
            </>
            }
        </>
    )
}