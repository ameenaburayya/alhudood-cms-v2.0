import React from 'react'

export default function PasswordShowIcon(props) {
    return (
        <svg
        xmlns="http://www.w3.org/2000/svg"
        width="30"
        height="13.531"
        viewBox="0 0 30 13.531"
        className={props.className || ''}
        onClick={props.onClick}
        >
        <g data-name="Group 4243" transform="translate(-7.1 -30.6)">
            <path
            d="M48.3 62.8h1.049v-3.5H48.3z"
            data-name="Path 6031"
            transform="translate(-26.794 -18.665)"
            ></path>
            <path
            d="M83.6 45.034l2.483 2.483c.245-.245.524-.49.769-.734L84.369 44.3c-.245.245-.489.49-.769.734z"
            data-name="Path 6032"
            transform="translate(-49.752 -8.91)"
            ></path>
            <path
            d="M68.829 58.736c.315-.14.664-.245.979-.385L68.479 55.1a8.144 8.144 0 01-.979.385z"
            data-name="Path 6033"
            transform="translate(-39.281 -15.934)"
            ></path>
            <path
            d="M9.583 44L7.1 46.448l.734.734 2.483-2.482a13.18 13.18 0 01-.734-.7z"
            data-name="Path 6034"
            transform="translate(0 -8.715)"
            ></path>
            <path
            d="M26.679 58.471l1.329-3.252c-.315-.14-.664-.245-.979-.42L25.7 58.052c.315.14.664.279.979.419z"
            data-name="Path 6035"
            transform="translate(-12.097 -15.738)"
            ></path>
            <path
            d="M37.983 31.124l-.909-.524a14.8 14.8 0 01-25.664.035l-.909.524a15.812 15.812 0 0027.483-.035z"
            data-name="Path 6036"
            transform="translate(-2.211)"
            ></path>
        </g>
        </svg>
    )
}
