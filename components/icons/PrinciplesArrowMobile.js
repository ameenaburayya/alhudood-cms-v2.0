
import React from 'react';



export default class PrinciplesArrowMobile extends React.Component {
    render() {
        return (
            <svg
            xmlns="http://www.w3.org/2000/svg"
            width="16"
            height="28.767"
            viewBox="0 0 16 28.767"
            className={this.props.className}
            >
                <g data-name="Group 4066">
                <path
                    fill="#afb7c4"
                    d="M30.851 42.719a.8.8 0 00.474-.224l11.831-11.182a.8.8 0 00-1.1-1.161L30.227 41.334a.8.8 0 00.624 1.385zm14.414-13.578a.8.8 0 00.437-1.385L31.325 14.178a.8.8 0 00-1.1 1.156L44.6 28.917a.8.8 0 00.665.224z"
                    data-name="Path 5697"
                    transform="translate(-29.966 -13.957)"
                ></path>
                </g>
            </svg>
        )
    }
}