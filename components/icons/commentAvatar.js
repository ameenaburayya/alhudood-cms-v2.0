
import React from 'react';



export default class CommentAvatar extends React.Component {
    render() {
        return (
            <svg
            xmlns="http://www.w3.org/2000/svg"
            x="0"
            y="0"
            enableBackground="new 0 0 197.5 254.8"
            version="1.1"
            viewBox="0 0 197.5 254.8"
            xmlSpace="preserve"
            >
            <path d="M197.2 21.2L183.1.5c-.1 0-.3-.1-.3-.2l-.3-.3H2.1C.9 0 0 .9 0 2.1v200.5c0 .6.2 1.1.6 1.5l19.8 20.4 2.3 2 20.9-20.8 11.1 11.1 5.1 5.1 3 3c.3.3.6.4.9.5v1.7l-37.3 23.8 1.1 3.9 39.2-25c.6-.4.9-1 .9-1.6v-5.1l17.5-17.6L108 225c.4.4 1 .6 1.5.5.6 0 1.1-.3 1.4-.7l15.7-19 11.1 11 4.3 4.2v6.8c0 .7.4 1.3.9 1.6l39.2 25 1.1-3.9-37.2-23.7V225c.4.3.9.5 1.4.5.5 0 1.1-.2 1.5-.6l1.6-1.8 3.2-3.4 13.4-14.4 12.5 19.3.4.3s.5.4 2.3.3c.3 0 .6-.3.7-.6.1-.1.2-.3.2-.5l5.2-15.2 5.1 15c.3.8 1.1 1.4 1.9 1.4h.4c1-.2 1.7-1.1 1.7-2.1v-201c0-.3-.1-.7-.3-1zm-17.9 195.2l-10-15.7c-.4-.5-.9-.9-1.6-1-.6-.1-1.3.2-1.7.7l-18.6 20-19.2-19.2c-.4-.4-1-.6-1.6-.6-.6 0-1.1.3-1.5.8l-15.9 19.1-23-19.4c-.8-.7-2-.7-2.8 0l-19.2 19.4L45 201.2c-.4-.3-.9-.5-1.4-.5-.6 0-1.1.2-1.5.6l-19.3 19.1-18.6-18.6V3.6h175.2v212.8zm14.1-5.6l-3-8.9c0-1.1-.9-2.1-2.1-2.1-1.1 0-2.1.9-2.1 2.1l-3 8.8.2-203.3 9.9 14.8v188.6z"></path>
            <path d="M129.2 91c8.4 0 15.3-6.8 15.3-15.3 0-8.4-6.8-15.3-15.3-15.3-8.4 0-15.3 6.8-15.3 15.3 0 8.4 6.9 15.3 15.3 15.3zM54.3 91c8.4 0 15.3-6.8 15.3-15.3 0-8.4-6.8-15.3-15.3-15.3-8.4 0-15.3 6.8-15.3 15.3C39 84.1 45.9 91 54.3 91z"></path>
            </svg>
        )
    }
}