import React from 'react'

export default function CheckIcon() {
    return (
        <svg
        xmlns="http://www.w3.org/2000/svg"
        x="0"
        y="0"
        enableBackground="new 0 0 19.4 11.4"
        version="1.1"
        viewBox="0 0 19.4 11.4"
        xmlSpace="preserve"
        >
        <path d="M19.4 1L18.7 0 5.6 9.7 1 3.4 0 4.1 5.4 11.4z"></path>
        </svg>
    )
}
