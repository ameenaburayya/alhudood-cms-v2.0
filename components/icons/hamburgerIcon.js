
import React from 'react';
import styles from '../../scss/components/icons/hamburgerIcon.module.scss';



export default class HamburgerIcon extends React.Component {
    render() {
        return (
            <button className={`${styles.hamburgerButton} ${this.props.className ? this.props.className : ''}`}  onClick={this.props.onClick}>
                <svg
                xmlns="http://www.w3.org/2000/svg"
                x="0"
                y="0"
                enableBackground="new 0 0 34.2 31.1"
                version="1.1"
                viewBox="0 0 34.2 31.1"
                xmlSpace="preserve"
                >
                <path
                    d="M-8.2 14.3H25.3V16.8H-8.2z"
                    transform="rotate(-63.455 8.6 15.6)"
                ></path>
                <path
                    d="M8.8 14.3H42.3V16.8H8.8z"
                    transform="rotate(-63.455 25.6 15.6)"
                ></path>
                </svg>
            </button>
        )
    }
}