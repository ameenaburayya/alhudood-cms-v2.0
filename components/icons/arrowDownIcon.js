
import React from 'react';



export default class ArrowDownIcon extends React.Component {
    render() {
        return (
            <svg
            xmlns="http://www.w3.org/2000/svg"
            width="25.034"
            height="12.078"
            viewBox="0 0 25.034 12.078"
            >
                <path
                d="M27.954 18.309a.559.559 0 00-.4.167l-10.3 10.272a1.8 1.8 0 01-2.524 0L4.441 18.476a.551.551 0 00-.4-.164.538.538 0 00-.393.164.551.551 0 000 .792L13.945 29.54a2.914 2.914 0 004.108 0l10.3-10.272a.563.563 0 00-.4-.959z"
                data-name="Path 5474"
                transform="translate(-3.483 -18.309)"
                ></path>
            </svg>
        )
    }
}