import React from 'react'

export default function LuckyTempIcon() {
    return (
        <svg
        xmlns="http://www.w3.org/2000/svg"
        width="150"
        height="114"
        viewBox="0 0 150 114"
        >
        <g data-name="Group 4118" transform="translate(-112.5 -3228)">
            <g data-name="Group 3628" transform="translate(8 -9.191)">
            <g data-name="Group 4111" transform="translate(115.5 3229.691)">
                <g data-name="Group 4110" transform="translate(7.5 7.5)">
                <path
                    d="M64.2 7.5a56.7 56.7 0 1056.7 56.7A56.762 56.762 0 0064.2 7.5zm0 112a55.3 55.3 0 1155.3-55.3 55.362 55.362 0 01-55.3 55.3z"
                    data-name="Path 5721"
                    transform="translate(-7.5 -7.5)"
                ></path>
                <path
                    d="M91.466 49A42.513 42.513 0 0149 91.466v1.4A43.917 43.917 0 0092.866 49z"
                    data-name="Path 5722"
                    transform="translate(9.098 9.098)"
                ></path>
                </g>
                <path
                d="M62.148 44.5c0-6.44-3.24-11.689-9.638-15.578a1.928 1.928 0 00-.907-.325c-.129 0-.432 0-.669.627-2.356 5.187-4.3 9.313-5.813 12.288a2.235 2.235 0 00.865 2.615c5.963 4.883 8.88 8.622 8.88 11.447 0 .734-.5 2.225-4.842 6.568a70.505 70.505 0 01-10.349 8.751c-2.159 1.546-2.483 2.193-2.526 2.366a1.306 1.306 0 00.411.043 8.558 8.558 0 001.751-.259c6.136-1.514 10.912-3.954 14.174-7.26a31.207 31.207 0 005.553-9.1 31.236 31.236 0 003.11-12.183z"
                transform="translate(14.858 13.437)"
                ></path>
            </g>
            </g>
            <path
            fill="none"
            stroke="#dfe4ea"
            strokeWidth="1"
            d="M0 0L150 0"
            data-name="Line 252"
            transform="translate(112.5 3341.5)"
            ></path>
        </g>
        </svg>
    )
}
