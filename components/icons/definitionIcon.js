
import React from 'react';

export default class DefinitionIcon extends React.Component {
    render() {
        return (
            <svg
            xmlns="http://www.w3.org/2000/svg"
            x="0"
            y="0"
            enableBackground="new 0 0 21.4 21.9"
            version="1.1"
            viewBox="0 0 21.4 21.9"
            xmlSpace="preserve"
            >
                <g className="st0">
                    <path d="M11 21.7L19.4 0h2L13 21.7h-2z" className="st1"></path>
                    <circle cx="1.9" cy="20" r="1.9" className="st1"></circle>
                </g>
            </svg>
        )
    }
}