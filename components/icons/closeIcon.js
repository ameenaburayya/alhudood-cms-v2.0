import React from "react"

export default function CloseIcon(props) {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            width="18.49"
            height="18.49"
            viewBox="0 0 18.49 18.49"
            onClick={props.onClick}
            className={props.className ? props.className : ""}
        >
            <g fill="none" stroke="#000" strokeWidth="2" data-name="Close icon" transform="translate(.707 .707)">
                <path d="M0 0L17.076 17.076" data-name="Line 57"></path>
                <path d="M17.076 0L0 17.076" data-name="Line 58"></path>
            </g>
        </svg>
    )
}
