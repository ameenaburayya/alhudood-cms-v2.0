import React from 'react'

export default function CheckedBox() {
    return (
        <svg
        xmlns="http://www.w3.org/2000/svg"
        x="0"
        y="0"
        enableBackground="new 0 0 26 20.1"
        version="1.1"
        viewBox="0 0 26 20.1"
        xmlSpace="preserve"
      >
        <g transform="translate(.248 -1.293)">
          <g>
            <path
              fill="#FFF"
              d="M0 -0.6H20V19.4H0z"
              transform="translate(-.248 2)"
            ></path>
            <path
              d="M20 19.4H0v-20h20v20zm-19-1h18V.4H1v18z"
              transform="translate(-.248 2)"
            ></path>
          </g>
          <g>
            <path d="M24.7 1.4l-12.4 13-6.2-5.9-1.7 1.8 8 7.6 14-14.8-1.7-1.7z"></path>
            <path
              fill="#FFF"
              d="M12.4 18.6l-8.7-8.3 2.4-2.5 6.2 5.9L24.7.7 27.1 3 12.4 18.6zm-7.3-8.4l7.3 6.9 13.3-14-1-1-12.4 13-6.2-5.9-1 1z"
            ></path>
          </g>
        </g>
      </svg>
    )
}
