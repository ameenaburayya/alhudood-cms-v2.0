import React from 'react'

export default function BackArrowIcon(props) {
    return (
        <svg
        onClick={props.onClick}
        xmlns="http://www.w3.org/2000/svg"
        x="0"
        y="0"
        enableBackground="new 0 0 19.5 16.2"
        version="1.1"
        viewBox="0 0 19.5 16.2"
        xmlSpace="preserve"
        >
        <path d="M18.5 7.2H2.4l5.7-5.7c.2-.2.3-.4.3-.6 0-.2-.1-.4-.2-.6-.3-.2-.5-.2-.7-.3-.2 0-.5.1-.6.3L.7 6.5c-.9.9-.9 2.3 0 3.2l6.2 6.2c.2.2.4.3.6.3.2 0 .4-.1.5-.2l.1-.1c.2-.2.3-.4.3-.6 0-.2-.1-.4-.2-.6L2.4 9.1h16.1c.5 0 1-.4 1-.9s-.5-1-1-1z"></path>
        </svg>
    )
}
