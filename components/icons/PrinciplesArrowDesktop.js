
import React from 'react';



export default class PrinciplesArrowDesktop extends React.Component {
    render() {
        return (
            <svg
            xmlns="http://www.w3.org/2000/svg"
            x="0"
            y="0"
            enableBackground="new 0 0 77.5 18.9"
            version="1.1"
            viewBox="0 0 77.5 18.9"
            xmlSpace="preserve"
            className={this.props.className}
            >
            <path d="M57.5 0L56.1 1.5 72.3 16.9 0 16.9 0 18.9 77.5 18.9z"></path>
            </svg>
        )
    }
}