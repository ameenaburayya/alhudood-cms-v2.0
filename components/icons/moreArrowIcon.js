import React from 'react'

export default function MoreArrowIcon() {
    return (
        <svg
        xmlns="http://www.w3.org/2000/svg"
        x="0"
        y="0"
        enableBackground="new 0 0 28.6 10.3"
        version="1.1"
        viewBox="0 0 28.6 10.3"
        xmlSpace="preserve"
        >
        <path d="M28.6 4.6L2.2 4.6 6 0.8 5.2 0 0 5.2 5.2 10.3 6 9.5 2.2 5.7 28.6 5.7z"></path>
        </svg>
    )
}
