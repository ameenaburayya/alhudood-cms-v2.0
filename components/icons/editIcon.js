import React from 'react'

export default function EditIcon() {
    return (
        <svg
        xmlns="http://www.w3.org/2000/svg"
        width="27.501"
        height="14.764"
        data-name="Group 4334"
        viewBox="0 0 27.501 14.764"
        >
        <path
            d="M-23.54 62.969L-13.407 73.1l.038.039L-12.3 77.7l-4.562-1.077-10.164-10.169zm9.8 13.283l-.616-2.565-9.185-9.187-1.952 1.952 9.181 9.183z"
            data-name="Path 6380"
            transform="translate(27.026 -62.969)"
        ></path>
        <path
            fill="none"
            stroke="#000"
            strokeWidth="1"
            d="M0 0L10 0"
            data-name="Line 279"
            transform="translate(17.501 14.264)"
        ></path>
        </svg>
    )
}
