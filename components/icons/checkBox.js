import React from 'react'

export default function CheckBox() {
    return (
        <svg
      xmlns="http://www.w3.org/2000/svg"
      x="0"
      y="0"
      enableBackground="new 0 0 26 20.1"
      version="1.1"
      viewBox="0 0 26 20.1"
      xmlSpace="preserve"
    >
      <g transform="translate(.248 -1.293)">
        <g>
          <path
            fill="#FFF"
            d="M0 -0.6H20V19.4H0z"
            transform="translate(-.248 2)"
          ></path>
          <path
            d="M20 19.4H0v-20h20v20zm-19-1h18V.4H1v18z"
            transform="translate(-.248 2)"
          ></path>
        </g>
      </g>
    </svg>
    )
}
