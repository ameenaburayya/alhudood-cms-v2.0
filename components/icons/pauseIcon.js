
import React from 'react';



export default class PauseIcon extends React.Component {
    render() {
        return (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width="9.744"
                height="22.74"
                viewBox="0 0 9.744 22.74"
            >
                <g data-name="Group 3947" transform="translate(-65.311 -21.081)">
                    <path
                    d="M0 0H1.697V22.74H0z"
                    data-name="Rectangle 3490"
                    transform="translate(65.311 21.081)"
                    ></path>
                    <path
                    d="M0 0H1.697V22.74H0z"
                    data-name="Rectangle 3491"
                    transform="translate(73.357 21.081)"
                    ></path>
                </g>
            </svg>
        )
    }
}