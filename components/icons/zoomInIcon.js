import React from 'react'

export default function ZoomInIcon(props) {
    return (
        <svg
        xmlns="http://www.w3.org/2000/svg"
        width="26.208"
        height="26.208"
        viewBox="0 0 26.208 26.208"
        onClick={props.onClick}
        >
        <g data-name="Group 4435" transform="translate(.5 .5)">
            <path
            stroke="#000"
            strokeWidth="1"
            d="M19.623 18.739L25 24.117l-.883.883-5.378-5.378a11.241 11.241 0 11.883-.883zm-8.38 2.5a9.994 9.994 0 10-9.994-9.994 9.994 9.994 0 009.994 9.992zm.625-10.618h4.372v1.249h-4.372v4.37h-1.25v-4.372H6.246v-1.25h4.372V6.246h1.249z"
            data-name="Path 6611"
            ></path>
        </g>
        </svg>
    )
}
