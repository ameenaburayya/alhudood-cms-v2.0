import React from 'react'

export default function PasswordHideIcon(props) {
    return (
        <svg
        xmlns="http://www.w3.org/2000/svg"
        width="30"
        height="21.504"
        viewBox="0 0 30 21.504"
        className={props.className || ''}
        onClick={props.onClick}
        >
        <g data-name="Group 4244" transform="translate(-7.1 -19.3)">
            <path
            d="M42.4 45.2a3.974 3.974 0 00-1.818.42A1.385 1.385 0 0139.6 48a1.27 1.27 0 01-.979-.42 4.476 4.476 0 00-.421 1.82 4.2 4.2 0 104.2-4.2z"
            data-name="Path 6037"
            transform="translate(-20.226 -16.844)"
            ></path>
            <path
            d="M49.749 19.3H48.7v3.5h.42a3.8 3.8 0 01.629.035z"
            data-name="Path 6038"
            transform="translate(-27.055)"
            ></path>
            <path
            d="M10.352 37.583L7.869 35.1c-.245.245-.524.49-.769.734l2.483 2.483c.244-.245.489-.49.769-.734z"
            data-name="Path 6039"
            transform="translate(0 -10.276)"
            ></path>
            <path
            d="M26.879 23.1c-.315.14-.664.245-.979.385l1.329 3.252a8.144 8.144 0 01.979-.385z"
            data-name="Path 6040"
            transform="translate(-12.227 -2.471)"
            ></path>
            <path
            d="M83.7 37.983l.734.734 2.483-2.483-.734-.734z"
            data-name="Path 6041"
            transform="translate(-49.817 -10.536)"
            ></path>
            <path
            d="M69.029 23.3L67.7 26.552c.315.14.664.245.979.42l1.329-3.252c-.315-.14-.665-.28-.979-.42z"
            data-name="Path 6042"
            transform="translate(-39.411 -2.601)"
            ></path>
            <path
            d="M38.122 42.131l.14-.28-.14-.28a15.812 15.812 0 00-27.483.035l-.14.28.14.28a15.846 15.846 0 0027.483-.035zm-26.434-.28a14.974 14.974 0 019.545-6.818 7.5 7.5 0 000 13.636 14.732 14.732 0 01-9.544-6.817zm6.224 0a6.469 6.469 0 116.469 6.469 6.476 6.476 0 01-6.468-6.468zm9.616 6.849a7.5 7.5 0 000-13.636 15.062 15.062 0 019.545 6.818 15.151 15.151 0 01-9.545 6.818z"
            data-name="Path 6043"
            transform="translate(-2.211 -9.3)"
            ></path>
        </g>
        </svg>
    )
}
