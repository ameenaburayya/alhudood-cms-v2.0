import React from 'react'
import LineButton from '../buttons/lineButton'
import styles from '../../scss/components/form/form.module.scss'

export default class FormError extends React.Component {

    constructor(props){
        super(props)
        this.getHeight = this.getHeight.bind(this)
        this.error = React.createRef()
        this.state = {
            loading: true,
            error: this.props.content
        }
    }

    componentDidMount() {
        this.getHeight()
        setTimeout(() => {
            if(this.state.loading){
                this.setState({ loading: false })
            }
        }, 1000);
    }

    componentWillUnmount(){
        const height = this.error.current.clientHeight
        this.props.removeHeight(height)
    }


    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            error: nextProps.content,
        };
    }


    getHeight(){
        const height = this.error.current.clientHeight
        this.props.sendHeight(height);
    }



    render(){
        let error = this.state.error
        if (typeof error === 'object'){
            if (error.type == 'emailUsed'){
                return (
                    <p ref={this.error}>
                        <span>- </span>
                        <span dangerouslySetInnerHTML={{__html: error.text}} />
                        <LineButton className={styles['error-button']} label={error.buttonText} link={error.modal ? null : '/login'} onClick={error.modal ? (e) => error.onClick(e): null} />
                    </p>
                )
            }
            else if (error.type == 'emailNotVerified'){
                return (
                    <p ref={this.error}>
                        <span>- </span>
                        <span dangerouslySetInnerHTML={{__html: error.text}} />
                        <LineButton className={styles['error-button']} label={error.buttonText} onClick={(e) => error.onClick(e)} />
                    </p>
                )
            }
            return(
                <p ref={this.error}><span>- </span><span dangerouslySetInnerHTML={{__html: this.state.error.text}} /></p>
            )
        }
        return (
            <p ref={this.error}><span>- </span><div dangerouslySetInnerHTML={{__html: this.state.error}} /></p>
        )
        if (!this.state.loading){
            
        }
        return ('')
    }
}
