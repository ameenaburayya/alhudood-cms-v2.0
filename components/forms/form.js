import React, { Component } from 'react'
import formStyles from '../../scss/components/form/form.module.scss'


export default class Form extends Component {


    constructor(){
        super()
        this.updateWindowWidth = this.updateWindowWidth.bind(this)
        this.state = {
            width: null,
        }
    }

    componentDidMount(){
        this.updateWindowWidth()
        window.addEventListener('resize', this.updateWindowWidth);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowWidth);
    }

    updateWindowWidth() {
        this.setState({ width: window.innerWidth });
    }

    render() {
        return (
            <form
                onSubmit={this.props.onSubmit}
                className={`${formStyles['form']} ${this.props.className || ''} ${this.state.width >= 768 ? formStyles['desktop'] : ''}`}
            >
                
                {this.props.children}
                
            </form>
        )
    }
}
