import React from 'react'
import styles from '../../scss/components/form/formHeading.module.scss'

export default function FormHeading(props) {
    return (
        <div className={`${styles['form-heading']} ${props.className ? props.className : ''}`}>
            <span>{props.label}</span>
        </div>
    )
}
