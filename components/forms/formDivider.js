import React from 'react'
import styles from '../../scss/components/form/formDivider.module.scss'

export default function FormDivider(props) {
    return (
        <div className={`${styles['font-divider']} ${props.modal ? styles['modal'] : ''}`}>
            <span>أو</span>
        </div>
    )
}
