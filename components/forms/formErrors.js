import React from 'react'
import ReactDOM from 'react-dom'
import FormError from './formError'

export default class FormErrors extends React.Component {

    constructor(props) {
        super(props)
        this.addHeight = this.addHeight.bind(this)
        this.removeHeight = this.removeHeight.bind(this)
        this.state = {
            totalHeight: 0,
            errors: this.props.errors,
            prevErrors: null
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            errors: nextProps.errors,
            prevErrors: prevState.errors
        };
    }

    addHeight(height){
        const temp_height = this.state.totalHeight + height
        this.setState({
            totalHeight: temp_height
        })
    }

    removeHeight(height){
        const temp_height = this.state.totalHeight - height
        this.setState({
            totalHeight: temp_height
        })
    }

    render(){
        return (
            <div style={{height: this.state.totalHeight}} className={this.props.className}>
                {this.props.errors.map((error, index) => {
                    if (error !== this.state.prevErrors[index]){
                        return (
                            <FormError removeHeight={this.removeHeight} sendHeight={this.addHeight} content={error} key={index} />
                        )
                    }
                    else return (
                        <FormError removeHeight={this.removeHeight} sendHeight={this.addHeight} content={error} key={index} />
                    )
                })}
            </div>
        )
    }
}
