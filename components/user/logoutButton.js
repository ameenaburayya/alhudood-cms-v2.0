import React from 'react'
import useUser from '../../data/useUser';
import { logout } from '../../requests/userApi';
import { useRouter } from 'next/router'
import styles from '../../scss/components/user/logoutButton.module.scss'


export default function LogoutButton({white}) {

    const { loading, loggedIn, user, mutate } = useUser();
    const router = useRouter()

    const onClick = async () => {
        await logout();
        mutate();
        router.reload()
    }
    return (
        <div onClick={onClick} className={`${styles['logout-button']} ${white ? styles['white'] : ''}`}>
            <span>تسجيل الخروج</span>
        </div>
    )
}
