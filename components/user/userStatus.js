import React, { useContext } from 'react'
import WebsiteContext from '../../contexts/websiteContext';
import {useRouter} from 'next/router'
import LoginButton from './loginButton';
import LogoutButton from './logoutButton';
import Account from './account';


export default function UserStatus({white, className}) {

    const { auth, loading } = useContext(WebsiteContext);
    const router = useRouter()
    return (
        <div className={className ? className : ''}>
            {auth.loggedIn 
                ? router.asPath == "/profile" ? <LogoutButton white={white} /> : <Account white={white} user={auth.user} />
                : <LoginButton white={white} />
            }
        </div>
    )
}
