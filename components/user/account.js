import React from 'react'
import styles from '../../scss/components/user/account.module.scss'
import CommentAvatar from '../icons/commentAvatar'
import Link from 'next/link'



export default function Account({user, white}) {
    return (
        <Link href="/profile">
            <div className={`${styles['account']} ${white ? styles['white'] : ''}`}>
                <div className={styles['image']}>
                    {user.image 
                    ? <img src='https://static.dw.com/image/53659583_303.jpg' alt="profile-image" />
                    : <CommentAvatar />
                    }
                </div>
                <div className={styles['text']}>
                    <span>حسابي</span>
                </div>
            </div>
        </Link>
    )
}
