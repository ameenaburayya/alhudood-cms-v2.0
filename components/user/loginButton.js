import React from 'react'
import Link from 'next/link'
import styles from '../../scss/components/user/loginButton.module.scss'

export default function LoginButton({white}) {
    return (
        <Link href="/login">
            <div className={`${styles['login-button']} ${white ? styles['white'] : ''}`}>
                <a>
                    <span>تسجيل دخول</span>
                </a>
            </div>
        </Link>
    )
}
