import {DOMAIN} from '../../lib/constants'
import TwitterIcon from '../icons/socialIcons/twitterIcon'



export default function TwitterShare({path}){

    return (
        <>
            <a target="_blank" href={"https://twitter.com/intent/tweet?url=https%3A//"+ process.env.MAIN_URL + path}><TwitterIcon /></a>
        </>
    )
}
