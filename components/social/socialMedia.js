import React, { Component } from 'react'

import FacebookIcon from '../icons/socialIcons/facebookIcon';
import TwitterIcon from '../icons/socialIcons/twitterIcon';
import InstagramIcon from '../icons/socialIcons/instagramIcon';
import MailIcon from '../icons/socialIcons/mailIcon';

export default class SocialMedia extends Component {
    render() {
        return (
            <ul className={`social-icons ${this.props.className ? this.props.className : ''}`}>
                <li><a rel="noreferrer" target="_blank" href="https://www.facebook.com/AlHudoodNet/"><FacebookIcon color={this.props.color} /></a></li>
                <li><a rel="noreferrer" target="_blank" href="https://twitter.com/alhudoodnet"><TwitterIcon color={this.props.color} /></a></li>
                <li><a rel="noreferrer" target="_blank" href="https://www.instagram.com/alhudoodnet/"><InstagramIcon color={this.props.color} /></a></li>
                <li><a rel="noreferrer" target="_blank" href="mailto: support@alhudood.net"><MailIcon color={this.props.color} /></a></li>
            </ul>
        )
    }
}
