import React, { useState, useRef } from 'react'
import ClipboardIcon from '../icons/socialIcons/clipboardIcon'
import Tooltip from '@material-ui/core/Tooltip';
import {DOMAIN, URL_SLUG} from '../../lib/constants'



export default function ClipboardShare({post}){
    const [open, setOpen] = React.useState(false);

    const handleTooltipClose = () => {
        setOpen(false);
    };

    const handleTooltipOpen = () => {
        setOpen(true);
        setTimeout(function(){ setOpen(false); }, 1500);
    };

    const handleCopy = () =>{
        navigator.clipboard.writeText(DOMAIN + `/${post.category?.slug}` + `/${post.contentType?.slug}` + `/${post.id}`)
    }
    return (
        <>
            <Tooltip 
                PopperProps={{
                    disablePortal: true,
                }}
                arrow={true}
                onClose={handleTooltipClose}
                open={open}
                placement="left"
                disableFocusListener
                disableHoverListener
                disableTouchListener
                title="تم النسخ" 
            >
                <a onClick={() => {handleCopy(); handleTooltipOpen()}}>
                    <ClipboardIcon onClick={handleTooltipOpen}  />
                </a>
            </Tooltip>
        </>
    )
}
