import React from 'react';
import PropTypes from 'prop-types';
import { isMobile, isChrome, isEdge, isEdgeChromium, isSafari, isOpera } from 'react-device-detect';
import SocialShare from './socialShare';
import ClipboardShare from './clipboardShare';
import ShareIcon from '../icons/socialIcons/shareIcon';


class SocialMobileShare extends React.Component{
    handleOnClick = () => {
        if (navigator.share) {
            navigator
            .share({
                title: `${this.props.postTitle}`,
                text: `${this.props.postTitle}`,
                url: document.location.href,
            })
            .then(() => {
            
            })
            .catch(error => {
            
            });
        }
    };

    renderShare = () => {
        if (isMobile) {
            if (navigator.share){
                if(isChrome || isEdge || isEdgeChromium || isSafari || isOpera){
                    return(
                        <div className={this.props.className}>
                            <button onClick={this.handleOnClick}>
                                <ShareIcon />
                            </button>
                        </div>
                    )
                }
                return <div className={this.props.className}><ClipboardShare post={this.props.post} /></div>
            }
            return <div className={this.props.className}><ClipboardShare post={this.props.post} /></div>
        }
        return <div className={this.props.className}><ClipboardShare post={this.props.post} /></div>
    }

    render () {
        return this.renderShare();
    }

};

SocialMobileShare.propTypes = {
    postTitle: PropTypes.string.isRequired,
};


export default SocialMobileShare;