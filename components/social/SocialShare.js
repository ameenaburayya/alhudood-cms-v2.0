import ClipboardShare from './clipboardShare'
import FacebookShare from './facebookShare';
import SocialMobileShare from './socialMobileShare';
import TwitterShare from './twitterShare';
import styles from '../../scss/components/social/socialShare.module.scss'
import React, { useEffect, useRef, useState } from 'react'
import { useRouter } from 'next/router';


export default function SocialShare(props){

    const router = useRouter()
    const path = router.asPath


    const {post, withTransition, target} = props

    const sharingRef = useRef()
    const [sharingPosition, setSharingPosition] = useState()
    const [test , set] = useState(null)
    const [active, setActive] = useState(false)

    useEffect(() => {
        const positionTop = sharingRef.current.getBoundingClientRect().top
        setSharingPosition(positionTop)
        // handlePageScroll()
        window.addEventListener('scroll', handlePageScroll)
        return () => window.removeEventListener('scroll', handlePageScroll)
    }, [])


    const handlePageScroll = () => {
        if (sharingRef) {
            const positionTop = sharingRef.current.getBoundingClientRect().top
            const contentPosition = target?.current.getBoundingClientRect().top
            if (contentPosition <= positionTop) setActive(true)
            else setActive(false)
        }
    }

    return(
        <div className={`${styles['social-share']} ${withTransition ? styles['transition'] : ''} ${active ? styles['active'] : ''}`}>
            <div ref={sharingRef} className={styles['desktop']}>
                <ul className={styles['social-icons']}>
                    <li><FacebookShare path={path} /></li>
                    <li><TwitterShare path={path} /></li>
                    <li><ClipboardShare path={path} /></li>
                </ul>
            </div>
            <div className={styles['mobile']}>
                <SocialMobileShare className={styles['mobile-share']} path={path} />
            </div>
        </div>
    )
}


