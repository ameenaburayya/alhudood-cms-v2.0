import { Router, useRouter } from 'next/router'
import {DOMAIN, URL_SLUG} from '../../lib/constants'
import FacebookIcon from '../icons/socialIcons/facebookIcon'



export default function FacebookShare({path}){

    console.log('env',process.env.MAIN_URL)
    return (
        <>
            <a target="_blank" href={"https://facebook.com/sharer/sharer.php?u=" + `${process.env.MAIN_URL}` + path}><FacebookIcon /></a>
        </>
    )
}
