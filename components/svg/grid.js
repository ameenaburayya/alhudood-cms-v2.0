import React from 'react'

export default function Grid() {
    return (
        <svg
        xmlns="http://www.w3.org/2000/svg"
        x="0"
        y="0"
        enableBackground="new 0 0 1386 462"
        version="1.1"
        viewBox="0 0 1386 462"
        xmlSpace="preserve"
        >
        <g transform="translate(-1350.091 1536.789)">
            <g transform="translate(636.09 -1598.334)">
            <g
                fill="none"
                stroke="#AFB7C4"
                strokeMiterlimit="10"
                strokeWidth="0.495"
            >
                <path d="M1176 61.5L1176 523.5"></path>
                <path d="M756 61.5L756 523.5"></path>
                <path d="M798 61.5L798 523.5"></path>
                <path d="M840 61.5L840 523.5"></path>
                <path d="M924 61.5L924 523.5"></path>
                <path d="M882 61.5L882 523.5"></path>
                <path d="M966 61.5L966 523.5"></path>
                <path d="M1050 61.5L1050 523.5"></path>
                <path d="M1008 61.5L1008 523.5"></path>
                <path d="M1134 61.5L1134 523.5"></path>
                <path d="M1092 61.5L1092 523.5"></path>
                <path d="M714 481.5L2100 481.5"></path>
                <path d="M714 439.5L2100 439.5"></path>
                <path d="M714 397.5L2100 397.5"></path>
                <path d="M714 313.5L2100 313.5"></path>
                <path d="M714 355.5L2100 355.5"></path>
                <path d="M714 271.5L2100 271.5"></path>
                <path d="M714 187.5L2100 187.5"></path>
                <path d="M714 229.5L2100 229.5"></path>
                <path d="M714 103.5L2100 103.5"></path>
                <path d="M714 145.5L2100 145.5"></path>
                <path d="M1638 61.5L1638 523.5"></path>
                <path d="M1218 61.5L1218 523.5"></path>
                <path d="M1260 61.5L1260 523.5"></path>
                <path d="M1302 61.5L1302 523.5"></path>
                <path d="M1386 61.5L1386 523.5"></path>
                <path d="M1344 61.5L1344 523.5"></path>
                <path d="M1428 61.5L1428 523.5"></path>
                <path d="M1512 61.5L1512 523.5"></path>
                <path d="M1470 61.5L1470 523.5"></path>
                <path d="M1596 61.5L1596 523.5"></path>
                <path d="M1554 61.5L1554 523.5"></path>
                <path d="M1680 61.5L1680 523.5"></path>
                <path d="M1722 61.5L1722 523.5"></path>
                <path d="M1764 61.5L1764 523.5"></path>
                <path d="M1848 61.5L1848 523.5"></path>
                <path d="M1806 61.5L1806 523.5"></path>
                <path d="M1890 61.5L1890 523.5"></path>
                <path d="M1974 61.5L1974 523.5"></path>
                <path d="M1932 61.5L1932 523.5"></path>
                <path d="M2058 61.5L2058 523.5"></path>
                <path d="M2016 61.5L2016 523.5"></path>
            </g>
            </g>
        </g>
        </svg>
    )
}
