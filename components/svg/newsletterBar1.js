
import React from 'react';

export default class NewsletterBar1 extends React.Component {
    render() {
        return (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                x="0"
                y="0"
                enableBackground="new 0 0 369.9 5.7"
                version="1.1"
                viewBox="0 0 369.9 5.7"
                xmlSpace="preserve"
                >
                <path d="M306.6 5.7L325.8 5.7 331.5 0 312.4 0z"></path>
                <path d="M0 5.7L19.2 5.7 24.9 0 5.7 0z"></path>
                <path d="M153.3 5.7L172.5 5.7 178.2 0 159.1 0z"></path>
                <path d="M76.7 5.7L95.8 5.7 101.6 0 82.4 0z"></path>
                <path d="M230 5.7L249.1 5.7 254.9 0 235.7 0z"></path>
                <path
                    fill="#DFE4EA"
                    d="M345 5.7L364.1 5.7 369.9 0 350.7 0z"
                    className="st0"
                ></path>
                <path
                    fill="#DFE4EA"
                    d="M268.3 5.7L287.5 5.7 293.2 0 274 0z"
                    className="st0"
                ></path>
                <path
                    fill="#DFE4EA"
                    d="M115 5.7L134.1 5.7 139.9 0 120.7 0z"
                    className="st0"
                ></path>
                <path
                    fill="#DFE4EA"
                    d="M38.3 5.7L57.5 5.7 63.2 0 44.1 0z"
                    className="st0"
                ></path>
                <path
                    fill="#DFE4EA"
                    d="M191.6 5.7L210.8 5.7 216.5 0 197.4 0z"
                    className="st0"
                ></path>
                </svg>
        )
    }
}