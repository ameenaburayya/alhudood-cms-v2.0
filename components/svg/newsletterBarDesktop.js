import React from 'react'

export default function NewsletterBarDesktop() {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            x="0"
            y="0"
            enableBackground="new 0 0 1096.8 4.7"
            version="1.1"
            viewBox="0 0 1096.8 4.7"
            xmlSpace="preserve"
            >
            <path d="M957.9 4.7L977.1 4.7 981.8 0 962.7 0z"></path>
            <path d="M804.6 4.7L823.8 4.7 828.5 0 809.4 0z"></path>
            <path d="M1034.6 4.7L1053.7 4.7 1058.5 0 1039.3 0z"></path>
            <path d="M728 4.7L747.1 4.7 751.9 0 732.7 0z"></path>
            <path d="M881.3 4.7L900.4 4.7 905.2 0 886 0z"></path>
            <path d="M919.6 4.7L938.8 4.7 943.5 0 924.3 0z"></path>
            <path d="M842.9 4.7L862.1 4.7 866.8 0 847.7 0z"></path>
            <path d="M996.3 4.7L1015.4 4.7 1020.2 0 1001 0z"></path>
            <path d="M1072.9 4.7L1092.1 4.7 1096.8 0 1077.7 0z"></path>
            <path d="M766.3 4.7L785.4 4.7 790.2 0 771 0z"></path>
            <path d="M498.3 4.7L517.5 4.7 522.2 0 503.1 0z"></path>
            <path d="M421.7 4.7L440.8 4.7 445.6 0 426.4 0z"></path>
            <path d="M575 4.7L594.1 4.7 598.9 0 579.7 0z"></path>
            <path d="M651.6 4.7L670.8 4.7 675.5 0 656.4 0z"></path>
            <path d="M536.6 4.7L555.8 4.7 560.5 0 541.4 0z"></path>
            <path d="M690 4.7L709.1 4.7 713.9 0 694.7 0z"></path>
            <path d="M613.3 4.7L632.5 4.7 637.2 0 618 0z"></path>
            <path d="M460 4.7L479.1 4.7 483.9 0 464.7 0z"></path>
            <path d="M383.3 4.7L402.5 4.7 407.2 0 388.1 0z"></path>
            <path d="M306.6 4.7L325.8 4.7 330.5 0 311.4 0z"></path>
            <path d="M153.3 4.7L172.5 4.7 177.2 0 158.1 0z"></path>
            <path d="M76.7 4.7L95.8 4.7 100.6 0 81.4 0z"></path>
            <path d="M0 4.7L19.2 4.7 23.9 0 4.7 0z"></path>
            <path d="M230 4.7L249.1 4.7 253.9 0 234.7 0z"></path>
            <path d="M115 4.7L134.1 4.7 138.9 0 119.7 0z"></path>
            <path d="M268.3 4.7L287.5 4.7 292.2 0 273 0z"></path>
            <path d="M191.6 4.7L210.8 4.7 215.5 0 196.4 0z"></path>
            <path d="M38.3 4.7L57.5 4.7 62.2 0 43.1 0z"></path>
            <path d="M345 4.7L345 4.7 364.1 4.7 364.2 4.7 368.9 0 349.7 0z"></path>
        </svg>
    )
}
