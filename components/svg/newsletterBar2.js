
import React from 'react';

export default class NewsletterBar2 extends React.Component {
    render() {
        return (
            <svg
            xmlns="http://www.w3.org/2000/svg"
            x="0"
            y="0"
            enableBackground="new 0 0 369.5 5.4"
            version="1.1"
            viewBox="0 0 369.5 5.4"
            xmlSpace="preserve"
            >
            <path
                fill="#DFE4EA"
                d="M254.5 0L235.4 0 230 5.4 249.1 5.4z"
                className="st0"
            ></path>
            <path
                fill="#DFE4EA"
                d="M177.9 0L158.7 0 153.3 5.4 172.5 5.4z"
                className="st0"
            ></path>
            <path
                fill="#DFE4EA"
                d="M331.2 0L312 0 306.6 5.4 325.8 5.4z"
                className="st0"
            ></path>
            <path
                fill="#DFE4EA"
                d="M101.2 0L82 0 76.7 5.4 95.8 5.4z"
                className="st0"
            ></path>
            <path
                fill="#DFE4EA"
                d="M24.5 0L5.4 0 0 5.4 19.2 5.4z"
                className="st0"
            ></path>
            <path d="M139.5 0L120.4 0 115 5.4 134.1 5.4z"></path>
            <path d="M292.8 0L273.7 0 268.3 5.4 287.5 5.4z"></path>
            <path d="M216.2 0L197 0 191.6 5.4 210.8 5.4z"></path>
            <path d="M62.9 0L43.7 0 38.3 5.4 57.5 5.4z"></path>
            <path d="M369.5 0L350.3 0 345 5.4 364.1 5.4z"></path>
            </svg>
        )
    }
}