import React from "react"
import PropsType from "prop-types"
import Ticker from "react-ticker"
import { isMobile } from "react-device-detect"
import styles from "../scss/components/tagsTicker.module.scss"
import HashIcon from "./icons/hashIcon"

export default function TagsTicker({ tags }) {
    return (
        <>
            <div className={styles["tags-ticker"]}>
                <div className={styles["hash-icon"]}>
                    <HashIcon />
                </div>
                {tags.length > 0 && tags && (
                    <>
                        <div className={styles["ticker"]}>
                            <Ticker
                                className={styles["ticker"]}
                                direction="toRight"
                                speed={3}
                            >
                                {() => (
                                    <>
                                        <div className={styles["tags-line"]}>
                                            {isMobile ? (
                                                <>
                                                    {tags.map((tag, index) => {
                                                        if (index % 2 === 0) {
                                                            return (
                                                                <span key={tag.id}>
                                                                    {tag.title}
                                                                </span>
                                                            )
                                                        }
                                                        return null
                                                    })}
                                                </>
                                            ) : (
                                                <>
                                                    {tags.map((tag) => {
                                                        return (
                                                            <span key={tag.id}>
                                                                {tag.title}
                                                            </span>
                                                        )
                                                    })}
                                                </>
                                            )}
                                        </div>
                                    </>
                                )}
                            </Ticker>
                        </div>
                        <div className={styles["ticker"]}>
                            <Ticker direction="toRight" speed={3}>
                                {() => (
                                    <>
                                        <div className={styles["tags-line"]}>
                                            {tags.map((tag, index) => {
                                                if (index % 2 !== 0) {
                                                    return (
                                                        <span key={tag.id}>
                                                            {tag.title}
                                                        </span>
                                                    )
                                                }
                                                return null
                                            })}
                                        </div>
                                    </>
                                )}
                            </Ticker>
                        </div>
                    </>
                )}
            </div>
        </>
    )
}

TagsTicker.propTypes = {
    tags: PropsType.array.isRequired,
}
