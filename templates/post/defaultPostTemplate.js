import PostHeader from "../../components/singlePost/postHeader"
import PostBody from "../../components/singlePost/postBody"
import postStyles from "../../scss/templates/post/defaultPostTemplate.module.scss"

export default function DefaultPostTemplate({ post }) {
    if (!post) {
        return "loading"
    }

    return (
        <div className={`${postStyles["post-inner"]} ${postStyles["default-post"]}`}>
            <PostHeader post={post} />
            <PostBody post={post} />
        </div>
    )
}
