import React, { useRef, useState, useEffect, createRef, Component } from 'react'
import Dal from '../../../components/icons/dal'
import PostButtons from '../../../components/post/postButtons';
import { getMediaMonitoringPosts , getPreviewPostById } from '../../../requests/contentApi'
import {getImageUrl} from '@takeshape/routing'
import styles from '../../../scss/templates/post/boxTemplate.module.scss'
import postStyles from '../../../scss/templates/post/defaultPostTemplate.module.scss'
import SocialShare from '../../../components/social/socialShare';

var post_header_height
var last_box_height 
var pin_position_limit
var window_height

export default class Files extends Component {

    constructor(props){
        super(props)
        this.parent = React.createRef()
        this.box = React.createRef()
        this.postHeader = React.createRef()
        this.elRefs = []
        this.handlePositionTop = this.handlePositionTop.bind(this)
        this.state = {
            position: null,
            pinClass: null,
        }
    }


    //const arrLength = post.files.length;
    

    handlePositionTop(){
        const element_position = this.box.current.getClientRects()[0].top
        var pin_position = post_header_height -  element_position + 65
        pin_position = pin_position > pin_position_limit ? pin_position_limit : pin_position;

        // elRefs.map((box, index) => {
        //     const box_position = box.current.getClientRects()[0].top - element_position.top
        //     var before_box_position = null
        //     var after_box_position = null
        //     if((index - 1) >= 0) {
        //         //console.log(index - 1)
        //         before_box_position = (elRefs[index - 1].current.getClientRects()[0]).top - element_position.top
        //     }
        //     if(index < (elRefs.length - 1)){
                
        //         after_box_position = (elRefs[index + 1].current.getClientRects()[0]).top - element_position.top
        //     }

        //     if(pin_position == after_box_position || pin_position == before_box_position ){
        //         setanything(true)
        //         elRefs[index].current.classList.remove('active')
        //     } else if (pin_position == box_position){
        //         elRefs[index].current.classList.add('active')
        //     }
            
        // })
        

        this.setState({
            position: pin_position
        })
    }

    componentDidMount() {
        window.addEventListener('scroll', this.handlePositionTop);

        post_header_height = this.postHeader.current.getClientRects()[0].height    
        last_box_height = this.elRefs.slice(-1)[0].getClientRects()[0].height
        pin_position_limit = this.box.current.getClientRects()[0].height - last_box_height


        window_height = window.innerHeight;

        document.documentElement.style.setProperty("--window-height", window_height); 
    }
    
    componentWillUnmount() {
        window.removeEventListener('scroll', this.handlePositionTop);
    }



    render(){
        const post = this.props.post
        return (
            <div ref={this.parent} className={`${postStyles['post-inner']} ${styles['box-template']} ${styles['files']}`}>
                <div className={styles['post-info']}>
                    <div className={styles['post-info-inner']}>
                        <div className={styles['post-type']}>{post.contentType.title}</div>
                        <PostButtons className={styles['post-buttons']} />
                    </div>
                </div>
                <div ref={this.postHeader} className={styles['post-header']}>
                    <div className={styles['post-image']}>
                        <figure>
                            <picture>
                                <img src={getImageUrl(post.featuredImage.path)} />
                            </picture>
                        </figure>
                    </div>
                    <div className={styles['post-title']}>
                        <h1>{post.title}</h1>
                    </div>
                </div>
                <div ref={this.box} className={styles['post-body']}>
                    <div className={styles['post-body-inner']}>
                        {post.files.map((file, index) =>{
                            return(
                                <div ref={(ref) => this.elRefs[index] = ref} key={index} className={styles['file']}>
                                    <div className={styles['file-info']}>
                                        <div className={styles['file-source']}>{file.source}</div>
                                        <div className={styles['file-time']}>
                                            <div className={styles['file-day']}>{file.day}</div>
                                            <div className={styles['file-date']}>{file.date}</div>
                                        </div>
                                    </div>
                                    <div className={styles['file-content']}>
                                        <div className={styles['file-title']}>{file.title}</div>
                                        <div className={styles['file-comment']}>
                                            <Dal  className={styles['dal']}/>
                                            <div className={styles['file-comment-inner']}>{file.comment}</div>
                                        </div>
                                    </div>
                                </div>
                            )
                        })}
                        <div className={styles['timeline']}>
                            <div style={{top: this.state.position}} className={`${styles['timeline-pin']} ${styles['stable']} ${this.state.pinClass ? styles['stable'] : styles['moving']}`}>
                                <span></span>
                            </div>
                        </div>
                    </div>
                </div>
                <SocialShare target={this.box} post={post} withTransition />
            </div>
        )
    }
}


