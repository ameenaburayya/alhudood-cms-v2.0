import React, { Component,findDOMNode  } from 'react'
import ReactDOM from 'react-dom'
import dynamic from 'next/dynamic'
import PostButtons from '../../../components/post/postButtons'
import $ from 'jquery'
import {getImageUrl} from '@takeshape/routing'


const ArrowLine = dynamic(
    () => import('../../../components/arrowLine'),
    { ssr: false }
)

import { annotate } from 'rough-notation';



// to do list: 
// you have to know multiline correction sentence online position (end, start, or whole line) "its imposibble to be on line_middle"
// we have studied the sentence that have arrow if the there is between lines comment overlapping with it (before the sentence) now we have to study if correction between lines has before it a circle and arrow overlapping with it



function WordCount(str) { 
    return str.split(" ").length;
}

export default class Correction extends Component {
    constructor(props){
        super(props)
        this.handleCorrection = this.handleCorrection.bind(this)
        //this.correctionIdentification = this.correctionIdentification.bind(this)
        this.state = {
            content: this.props.post.content
        }
    }

    handleParagraphSpans(paragraphs){
        // this function is to give the non correction text a span with "normal-text" class
        
        paragraphs.map( paragraph => {
            paragraph.childNodes.forEach( piece => {
                if (piece.nodeType == 3) {
                    $(piece).wrap('<span class="normal-text"></span>')
                }
            });
        });

        return paragraphs;
    }



    correctionIdentification(e){
        var attr_code = '';
        $(e.attributes).each(function(index , attribute){
            if(attribute.name == 'correction_after'){ 
                attr_code += 'A'; // A: correction after
            }
            if(attribute.name == 'correction_before'){
                attr_code += 'B'; // B: correction before
            }
            if(attribute.name == 'correction_strike'){
                attr_code += 'S'; // A: correction strike
            }
            if(attribute.name == 'correction_comment'){
                attr_code += 'C'; // A: correction comment
            }
        })
        return attr_code;
    }

    splitWords(sentence){
        // this function is to wrap each word in the sentence that has a comment with span
        var words = $(sentence).html().split(" ")
        $(sentence).empty();
        $(words).each(function(v, i) {
            $(sentence).append($("<span>").text(i + ' '));
        });
        return sentence;
    }



    getNumberOfLines(sentence){ // this function determine the sentence lines number (please note that this works only after splitting the sentence to words "span") 
        var word_height = sentence.childNodes[0].getBoundingClientRect().height
        var sentence_height = sentence.getBoundingClientRect().height
        var paragraph = $(sentence).parent()
        var paragraph_line_height = parseInt($(paragraph).css('line-height')); // getting the line-height css property for the paragraph

        var lines_number = ((paragraph_line_height - word_height) + sentence_height) / paragraph_line_height // I don't know what the hell is this equation, but if it works, don't break it

        return Math.round(lines_number);
    }

    filteringLines(sentence){  // this function is to store every line words in an array, then store the array in another array (lines_words)
        var number_of_lines = this.getNumberOfLines(sentence)
        //var paragraph_line_height = $(sentence).parent().height()
        var paragraph_line_height = parseInt($(sentence).parent().css('line-height'))
        var lines_words = []

        var i;
        for (i = 0 ; i < number_of_lines; i++){
            var line_words= []
            $(sentence.childNodes).each(function(){
                if ($(this).position().top == (i * paragraph_line_height)){
                    line_words.push(this)
                }
            })
            lines_words[i] = line_words
        }
        
        return lines_words;
    }


    highlight(correction, line_status, highlight_status){ 

        var choices = ['bracket', 'underline', 'box']

        if (highlight_status == 'above_small'){
            if (line_status == "multi_line"){
                const annotation = annotate(correction, { type: 'underline' , padding: [0, 5, 0 ,5], color: "#ef25258f", multiline: true, iterations: Math.floor(Math.random() * 3) + 1  , rtl: true});
                annotation.show();
            }
            else{
                var randomNumber = Math.floor(Math.random()*choices.length);
                const annotation = annotate(correction, { type: choices[randomNumber] , padding: [0, 5, 0 ,5], color: "#ef25258f", multiline: false, iterations: Math.floor(Math.random() *  2) + 1  , rtl: true, brackets: ['left', 'right']});
                annotation.show();
            }
        }
        else if (highlight_status == 'with_arrow'){
            if (line_status == "multi_line"){
                const annotation = annotate(correction, { type: 'underline' , padding: [0, 5, 0 ,5], color: "#ef25258f", multiline: true, iterations: Math.floor(Math.random() * 2) + 1  , rtl: true});
                annotation.show();
            }
            else{
                const annotation = annotate(correction, { type: 'circle' , padding: [0, 5, 0 ,5], color: "#ef25258f", multiline: false, iterations: 1  , rtl: true});
                annotation.show();
            }
        }
        else{
            const annotation = annotate(correction, { type: 'underline' , padding: [0, 5, 0 ,5], color: "#ef25258f", multiline: true, iterations: Math.floor(Math.random() * 2) + 1  , rtl: true});
            annotation.show();
        }
    }

    removeCircleHighlight(correction){
        
        const annotation = annotate(correction, { type: 'underline' , padding: [0, 5, 0 ,5], color: "#ef25258f", multiline: false, iterations: 1 , rtl: true});
        annotation.show();
    }

    strikeComment(correction, line_status){
        const annotation = annotate(correction, { type: 'strike-through' , padding: [5, 5, 5 ,5], color: "#ef25258f", multiline: line_status == "one_line" ? false : true , iterations: 1, rtl: true});
        annotation.show();
    }

    
    correctionWithInsert(element){
        $(element.attributes).each(function(index,attribute){
            if (attribute.name == "correction_before"){
                var correction_before_content = $(element).attr('correction_before')
                $( '<span class="text-addition hand-text">' + correction_before_content + '</span>' ).insertBefore( $(element) );
            }
            if (attribute.name == "correction_after"){
                var correction_after_content = $(element).attr('correction_after')
                $( '<span class="text-addition hand-text">' + correction_after_content + '</span>' ).insertAfter( $(element) );
            }
            
        })
    }

    getCorrectionPosition(sentence, lines_words){
        //var number_of_lines = this.getNumberOfLines(sentence)
        var sentence_number_of_lines = lines_words.length
        var sentence_css_position = $(sentence).css('position')
        $(sentence).css('position', 'unset')
        var sentence_parent_paragraph = $(sentence).parent()
        var sentence_parent_paragraph_line_height = parseInt($(sentence_parent_paragraph).css('line-height'))
        var sentence_parent_paragraph_number_of_lines = $(sentence).parent().height() / sentence_parent_paragraph_line_height
        var sentence_height = $(sentence).height();
        var error_margin = (sentence_parent_paragraph_line_height - sentence_height) / 2

        var summary = {
            correction: sentence,
            line_status: null,
            on_line_position: null,
            arrow_start: {
                top: null,
                right: null,
                bottom: null,
                left: null,
            },
            arrow_start_word: null,
            comment_position: null,
            first_line_or_last_line: false,
            multiline_target_line: 1
        }

        if (sentence_number_of_lines == 1){
            var sentence_position_top = $(sentence).position().top
            var sentence_position_bottom = $(sentence).position().top + $(sentence).height()
            var sentence_next_sibling_position_top = $(sentence).next().length ? $(sentence).next().position().top : null
            var sentence_prev_sibling_position_bottom = $(sentence).prev().length ? ($(sentence).prev().position().top + $(sentence).prev().height()) : null
            var random_line_start = [
                'right',
                'left'
            ];
            var random_number = Math.floor(Math.random()*random_line_start.length);
            var random_position = random_line_start[random_number]

            var sentence_line_number = ((sentence_position_top - error_margin) / sentence_parent_paragraph_line_height ) + 1


            if ((sentence_parent_paragraph_number_of_lines % 2) == 0 ){ // number of lines is even number
                summary.comment_position =  (sentence_line_number <=  (sentence_parent_paragraph_number_of_lines / 2)) ? "top" : "bottom"
                summary.first_line_or_last_line = sentence_line_number == 1 ? "first" : sentence_line_number == sentence_parent_paragraph_number_of_lines ? "bottom" : false
                summary.line_number = sentence_line_number
            }
            else { // number of lines is odd number
                var middle_line = Math.floor(sentence_parent_paragraph_number_of_lines / 2) + 1
                if (sentence_line_number == middle_line){
                    summary.comment_position =  "either"
                    summary.line_number = sentence_line_number
                }
                else {
                    summary.comment_position = (sentence_line_number <=  (sentence_parent_paragraph_number_of_lines / 2)) ? "top" : "bottom"
                    summary.first_line_or_last_line = sentence_line_number == 1 ? "first" : sentence_line_number == sentence_parent_paragraph_number_of_lines ? "bottom" : false
                    summary.line_number = sentence_line_number
                }
            }

            if (sentence_next_sibling_position_top != sentence_position_top && sentence_prev_sibling_position_bottom != sentence_position_bottom ){
                // only the sentence on the line 
                summary.line_status = 'one_line'
                summary.on_line_position = 'whole_line'

                if(random_position == "left"){
                    summary.arrow_start_word = lines_words[0][lines_words[0].length - 1]
                    summary.arrow_start =  {left: true} 
                }
                else if(random_position == "right"){
                    summary.arrow_start_word = lines_words[0][0]
                    summary.arrow_start =  {right: true} 
                }
            }
            else if (sentence_next_sibling_position_top != sentence_position_top){
                // the sentence on the ending of the line
                summary.line_status = 'one_line'
                summary.on_line_position = 'line_end'
                summary.arrow_start_word = lines_words[0][lines_words[0].length - 1]
                summary.arrow_start =  {left: true} 
            }
            else if (sentence_prev_sibling_position_bottom != sentence_position_bottom){
                // the sentence on the beginning of the line
                summary.line_status = 'one_line'
                summary.on_line_position = 'line_start'
                summary.arrow_start_word = lines_words[0][0]
                summary.arrow_start =  {right: true} 
            }
            else if (sentence_next_sibling_position_top == sentence_position_top && sentence_prev_sibling_position_bottom == sentence_position_bottom) {
                // the sentence in the middle of the line
                var sentence_next_sibling = $(sentence).next().text().length
                var sentence_width = $(sentence).width()
                var sentence_parent_width = $(sentence).parent().width()
                var sentence_left_offset = $(sentence).offset().left
                var sentence_parent_offset_left = $(sentence).parent().offset().left
                var sentence_right_offset = sentence_left_offset + sentence_width
                var sentence_parent_offset_right = sentence_parent_offset_left + sentence_parent_width
                var sentence_total_offset = sentence_parent_offset_right - sentence_right_offset
                
                var sentence_middle_point_offset = sentence_left_offset + (sentence_width/2) <= sentence_parent_offset_left + (sentence_parent_width/2)

                //console.log(sentence,sentence_left_offset, sentence_middle_point_offset , sentence_parent_width/2)

                summary.line_status = 'one_line'
                if (sentence_next_sibling <= 2){
                    summary.on_line_position = 'line_end'
                    summary.arrow_start_word = lines_words[0][lines_words[0].length - 1]
                    summary.arrow_start =  {left: true} 
                }
                else if(sentence_total_offset <= 10){
                    summary.on_line_position = 'line_start'
                    summary.arrow_start_word = lines_words[0][0]
                    summary.arrow_start =  {right: true} 
                }
                else{
                    summary.on_line_position = 'line_middle'
                    if(sentence_middle_point_offset){
                        summary.arrow_start_word = lines_words[0][lines_words[0].length - 1]
                        summary.arrow_start =  {left: true} 
                    }
                    else{
                        summary.arrow_start_word = lines_words[0][0]
                        summary.arrow_start =  {right: true} 
                    }
                }
                

            }
            
        }
        else{

            if (this.getWidthOfSentenceInLine(lines_words[0]) >= this.getWidthOfSentenceInLine(lines_words[sentence_number_of_lines - 1])){
                sentence = lines_words[0][0]
                sentence_height = $(sentence).height();
                error_margin = (sentence_parent_paragraph_line_height - sentence_height) / 2
                var sentence_position_top = $(sentence).position().top 
                var sentence_line_number =  ((sentence_position_top - error_margin) / sentence_parent_paragraph_line_height ) + 1
                summary.line_number = sentence_line_number
                summary.comment_position = sentence_line_number == (( sentence_parent_paragraph_number_of_lines / 2 ) + 1) ? "either" : sentence_line_number <=  (sentence_parent_paragraph_number_of_lines / 2) ? "top" : "bottom"
                summary.first_line_or_last_line = sentence_line_number == 1 ? "first" : sentence_line_number == sentence_parent_paragraph_number_of_lines ? "bottom" : false


            }
            else{
                sentence = lines_words[sentence_number_of_lines - 1][0]
                sentence_height = $(sentence).height();
                error_margin = (sentence_parent_paragraph_line_height - sentence_height) / 2
                var sentence_position_top = $(sentence).position().top 
                var sentence_line_number =  ((sentence_position_top - error_margin) / sentence_parent_paragraph_line_height ) + 1
                summary.line_number = sentence_line_number
                summary.comment_position = sentence_line_number == (( sentence_parent_paragraph_number_of_lines / 2 ) + 1) ? "either" : sentence_line_number <=  (sentence_parent_paragraph_number_of_lines / 2) ? "top" : "bottom"
                summary.first_line_or_last_line = sentence_line_number == 1 ? "first" : sentence_line_number == sentence_parent_paragraph_number_of_lines ? "bottom" : false

            }

            summary.line_status = 'multi_line'

            summary.arrow_start =  (this.getWidthOfSentenceInLine(lines_words[0]) >= this.getWidthOfSentenceInLine(lines_words[sentence_number_of_lines - 1]) ) ? {top: true, left: true} : {bottom: true, right: true}
            summary.multiline_target_line = (this.getWidthOfSentenceInLine(lines_words[0]) >= this.getWidthOfSentenceInLine(lines_words[sentence_number_of_lines - 1]) ) ? 1 : sentence_number_of_lines
            summary.arrow_start_word =  (this.getWidthOfSentenceInLine(lines_words[0]) >= this.getWidthOfSentenceInLine(lines_words[sentence_number_of_lines - 1]) ) ? lines_words[0][lines_words[0].length - 1] : lines_words[sentence_number_of_lines - 1][0]
            $(sentence).parent().css('position', sentence_css_position)
        }

        return summary;
    }

    correctionWithComment(correction, all_between_lines_comments, all_floating_comments , paragraph_width){
    
        var splittedSentence = this.splitWords(correction)
        var lines_words = this.filteringLines(splittedSentence) // see the function comments to know what this does
        var highlight_status
        var normal_comment_content = $(correction).attr('correction_comment')
    
        var correction_summary = this.getCorrectionPosition(correction, lines_words)

    
        $(correction).css('position', 'unset');

        $('<span class="comment between-lines normal-comment hand-text">'+ normal_comment_content +'</span>').appendTo(correction)
        var normal_comment = $('.normal-comment', correction)
        $(normal_comment).css('opacity', 0)

        var normal_comment_width = $(normal_comment).width()
        var line_error_margin = parseInt($(correction).parent().css('line-height')) - $(lines_words[0][0]).height()
        var correction_index = correction_summary.line_number - 1
    
    
        if (normal_comment_width > paragraph_width){
            $(normal_comment).remove()
            // we need to make this floating comment with arrow
            this.floatingComment(normal_comment, correction_summary, all_between_lines_comments)
            highlight_status = "with_arrow"
            //console.log(normal_comment)
        }
        else{
            var targetLineSentenceWidth = this.getWidthOfSentenceInLine(lines_words[correction_summary.multiline_target_line - 1]) 
            var parent_paragraph = $(correction).parent()
            
            var normal_comment_top_amount = line_error_margin + ((correction_summary.line_number - 1) * parseInt($(correction).parent().css('line-height')))
            var normal_comment_right_amount = $(correction).parent()[0].getBoundingClientRect().right - lines_words[correction_summary.multiline_target_line - 1][0].getBoundingClientRect().right + (targetLineSentenceWidth / 2)
            $(normal_comment).css('top', normal_comment_top_amount)
            $(normal_comment).css('right', normal_comment_right_amount)
    
            if (normal_comment[0].getBoundingClientRect().right >  parent_paragraph[0].getBoundingClientRect().right){
                normal_comment_right_amount += normal_comment[0].getBoundingClientRect().right - parent_paragraph[0].getBoundingClientRect().right 
                $(normal_comment).css('right', normal_comment_right_amount)
            }
            else if (normal_comment[0].getBoundingClientRect().left <  parent_paragraph[0].getBoundingClientRect().left){
                normal_comment_right_amount -= parent_paragraph[0].getBoundingClientRect().left - normal_comment[0].getBoundingClientRect().left 
                $(normal_comment).css('right', normal_comment_right_amount)
            }

            if ($(normal_comment).width() > targetLineSentenceWidth){
                highlight_status = "above_large"
            }
            else{
                highlight_status = "above_small"
            }
    
            if (all_between_lines_comments[correction_index] == undefined || all_between_lines_comments[correction_index].length == 0){
                $(normal_comment).css('opacity', 1)
    
                all_between_lines_comments[correction_index] = []
    
                all_between_lines_comments[correction_index].push({
                    comment: normal_comment, 
                    line_number: correction_summary.line_number,
                    width: $(normal_comment).width(),
                    coordinates: normal_comment[0].getBoundingClientRect()
                })
            }
            else{
                var comments_in_line_number = all_between_lines_comments[correction_index].length
                var before_comments_in_line_width;
                var i;
                for (i =0 ; i < comments_in_line_number; i++){
                    var temp_comment = all_between_lines_comments[correction_index][i]
                    all_comments_in_line_width += temp_comment.width
                }
    
                var all_comments_in_line_width = before_comments_in_line_width + normal_comment_width
    
                if (all_comments_in_line_width > $(parent_paragraph).width()){
                    $(normal_comment).remove()
                    // we need to make a function for what happens next, because we are using this more than one
                    this.floatingComment(normal_comment, correction_summary, all_between_lines_comments)
                    highlight_status = "with_arrow"
                }
                else{
                    var last_normal_comment_in_array_left = all_between_lines_comments[correction_index][comments_in_line_number -1].coordinates.left // this is the strike comment than comes before the strike comment that we are studying
                    if(last_normal_comment_in_array_left > normal_comment[0].getBoundingClientRect().right){ // this means that there is no overlap between the strike comments
                        $(normal_comment).css('opacity', 1)
                        all_between_lines_comments[correction_index].push({
                            comment: normal_comment, 
                            line_number: correction_summary.line_number,
                            width: $(normal_comment).width(),
                            coordinates: normal_comment[0].getBoundingClientRect()
                        })
                    }
                    else{
                        $(normal_comment).remove()
                        // we need to make a function for what happens next, because we are using this more than one
                        this.floatingComment(normal_comment, correction_summary, all_between_lines_comments)
                        highlight_status = "with_arrow"
                    }
                }
            }

        }

        //$(correction).css('position', 'relative');
        this.highlight(correction, correction_summary.line_status, highlight_status) 
        $(correction).css('position', 'unset');
    }
    
    getWidthOfSentenceInLine(words){ // this function can get the width of the words in a specific line
        var sentence_width = words.length > 1 ? (words[0].getBoundingClientRect().right - words[words.length - 1].getBoundingClientRect().left) : words[0].getBoundingClientRect().width
        return sentence_width;
    }


    floatingComment(comment, correction_summary, all_between_lines_comments ){

        $(comment).removeClass('between-lines').addClass('floating-comment')
        
        
        if(correction_summary.comment_position == "either"){
            var $top_comments_count = $(correction_summary.correction).parent().parent().children('.comments_top').children().length
            var $bottom_comments_count = $(correction_summary.correction).parent().parent().children('.comments_bottom').children().length
            if ($top_comments_count == $bottom_comments_count ){
                if(all_between_lines_comments[0] != undefined){
                    $(comment).appendTo($(correction_summary.correction).parent().parent().children(`.comments_bottom`))
                    correction_summary.comment_position = "bottom"
                }
                else{
                    $(comment).prependTo($(correction_summary.correction).parent().parent().children(`.comments_top`))
                    correction_summary.comment_position = "top"
                }
            }
            else if ($top_comments_count > $bottom_comments_count){
                $(comment).appendTo($(correction_summary.correction).parent().parent().children(`.comments_bottom`))
                correction_summary.comment_position = "bottom"
            }
            else{
                $(comment).prependTo($(correction_summary.correction).parent().parent().children(`.comments_top`))
                correction_summary.comment_position = "top"
            }
        }
        else{
            if (correction_summary.comment_position == "top"){
                $(comment).prependTo($(correction_summary.correction).parent().parent().children(`.comments_top`))
            }
            else {
                $(comment).appendTo($(correction_summary.correction).parent().parent().children(`.comments_bottom`))
            }
        }

        $(comment).css('opacity', 1)
        
        
        var div = document.createElement('div');

        ReactDOM.render(
            <ArrowLine remove_circle={() => this.removeCircleHighlight(correction_summary.correction)} lines_corrections={all_between_lines_comments} correctionSummary={correction_summary} toNode={comment} fromNode={correction_summary.correction} />,
            document.body.appendChild(div)
        );
        
    }

    correctionWithStrike(correction, all_between_lines_comments, all_floating_comments , paragraph_width){
        // we have to tell the backend to limit the characters numbers, also, if there is another strike comment on the same line and they overlap with each other, we have to put the comment outside the paragraph and with arrow
        // also, we have to make sure that the sentence with the "x"  is on one line only if not lets put a strike through line on the multi line sentence

        var splittedSentence = this.splitWords(correction)
        var lines_words = this.filteringLines(splittedSentence) // see the function comments to know what this does

        var strike_content = $(correction).attr('correction_strike')
        
        var correction_summary = this.getCorrectionPosition(correction, lines_words)

        var line_error_margin = parseInt($(correction).parent().css('line-height')) - $(lines_words[0][0]).height()

        this.strikeComment(correction, correction_summary.line_status) 
        $(correction).css('position', 'unset');
        
        var correction_index = correction_summary.line_number - 1

        $('<span class="comment between-lines strike-comment hand-text">'+ strike_content +'</span>').appendTo(correction)
        var strike_comment = $('.strike-comment', correction)
        $(strike_comment).css('opacity', 0)
        

        if ($(strike_comment).width() > paragraph_width){ // is the width of the between lines comment is bigger than the width of the paragraph we need to make it floating comment
            $(strike_comment).remove()
            // we need to make this floating comment with arrow
        }
        else{

            var strike_comment_width = $(strike_comment).width()
            var targetLineSentenceWidth = this.getWidthOfSentenceInLine(lines_words[correction_summary.multiline_target_line - 1]) 
            var parent_paragraph = $(correction).parent()

            var strike_comment_top_amount = line_error_margin + ((correction_summary.line_number - 1) * parseInt($(correction).parent().css('line-height')))
            var strike_comment_right_amount = $(correction).parent()[0].getBoundingClientRect().right - lines_words[correction_summary.multiline_target_line - 1][0].getBoundingClientRect().right + (targetLineSentenceWidth / 2)
            $(strike_comment).css('top', strike_comment_top_amount)
            $(strike_comment).css('right', strike_comment_right_amount)

            if (strike_comment[0].getBoundingClientRect().right >  parent_paragraph[0].getBoundingClientRect().right){
                strike_comment_right_amount += strike_comment[0].getBoundingClientRect().right - parent_paragraph[0].getBoundingClientRect().right 
                $(strike_comment).css('right', strike_comment_right_amount)
            }
            else if (strike_comment[0].getBoundingClientRect().left <  parent_paragraph[0].getBoundingClientRect().left){
                strike_comment_right_amount -= parent_paragraph[0].getBoundingClientRect().left - strike_comment[0].getBoundingClientRect().left 
                $(strike_comment).css('right', strike_comment_right_amount)
            }

            if (all_between_lines_comments[correction_index] == undefined || all_between_lines_comments[correction_index].length == 0){
                $(strike_comment).css('opacity', 1)

                all_between_lines_comments[correction_index] = []

                all_between_lines_comments[correction_index].push({
                    comment: strike_comment, 
                    line_number: correction_summary.line_number,
                    width: $(strike_comment).width(),
                    coordinates: strike_comment[0].getBoundingClientRect()
                })
            }
            else{
                var comments_in_line_number = all_between_lines_comments[correction_index].length
                var before_comments_in_line_width;
                var i;
                for (i =0 ; i < comments_in_line_number; i++){
                    var temp_comment = all_between_lines_comments[correction_index][i]
                    all_comments_in_line_width += temp_comment.width
                }

                var all_comments_in_line_width = before_comments_in_line_width + strike_comment_width

                if (all_comments_in_line_width > $(parent_paragraph).width()){
                    $(strike_comment).remove()
                    // we need to make a function for what happens next, because we are using this more than one
                }
                else{
                    var last_strike_comment_in_array_left = all_between_lines_comments[correction_index][comments_in_line_number -1].coordinates.left // this is the strike comment than comes before the strike comment that we are studying
                    if(last_strike_comment_in_array_left > strike_comment[0].getBoundingClientRect().right){ // this means that there is no overlap between the strike comments
                        $(strike_comment).css('opacity', 1)
                        all_between_lines_comments[correction_index].push({
                            comment: strike_comment, 
                            line_number: correction_summary.line_number,
                            width: $(strike_comment).width(),
                            coordinates: strike_comment[0].getBoundingClientRect()
                        })
                    }
                    else{
                        $(strike_comment).remove()
                        // we need to make a function for what happens next, because we are using this more than one
                    }
                }
            }
            
        }    
    }


    handleCorrection(all_paragraphs){

        // lets define the correction types, we have comments, and we have strike a word and put above something else, and we have inserting text into the text

        var paragraph_width = $(all_paragraphs[0]).width()
        this.handleParagraphSpans(all_paragraphs)

        all_paragraphs.map((paragraph) => {

            var paragraph_number_of_lines = $(paragraph).height() / parseInt($(paragraph).css('line-height'))
            var all_between_lines_comments = new Array(paragraph_number_of_lines) // this is an array to store the comments that going to be displayed between lines
            var all_floating_comments = new Array(paragraph_number_of_lines) // this is an array to store the comments that going to be floating
            var all_floating_comments = {
                top_comments: [],
                bottom_comments: []
            }

            $(paragraph).wrap('<div class="correction-parent"></div>')
            $(paragraph).parent().prepend('<div class="comments_top comments-box"></div>')
            $(paragraph).parent().append('<div class="comments_bottom comments-box"></div>')

            $(paragraph).children('.has-correction').each((index, correction) => { // we have to check all the paragraphs if they have any correction with inserting, because the inserting will change the coordinates of elements, so the calculations for comments will change
                const attr_code = this.correctionIdentification(correction)
                if (attr_code.includes('A') || attr_code.includes('B')) { 
                    this.correctionWithInsert(correction)
                }
            })

            setTimeout(function(){
                // we need to setTimeout because the elements form "correctionWithInsert" need's to settle down before measuring things

                $(paragraph).children('.has-correction').each((index, correction) => { // after we check for any correction with inserting, now we can make strike correction
                    const attr_code = this.correctionIdentification(correction)
                    if (attr_code.includes('S')){
                        this.correctionWithStrike(correction, all_between_lines_comments, all_floating_comments , paragraph_width)
                    }    
                })
            }
            .bind(this), 
            200)

            setTimeout(function(){
                // we need to setTimeout because the elements form "correctionWithInsert" need's to settle down before measuring things

                $(paragraph).children('.has-correction').each((index, correction) => { // now we do the correction with comments
                    const attr_code = this.correctionIdentification(correction)

                    if(attr_code.includes('C')){
                        this.correctionWithComment(correction, all_between_lines_comments, all_floating_comments , paragraph_width)
                    }
                })
            }
            .bind(this), 
            200)

            
            $(paragraph).children('.strike-only').each((index, correction) => { 
                // tell the backend to give class to strike only things
            })
        })
        
    }

    correctionReset(){
        $('body svg').remove()
    }

    componentDidMount(){
        
        var body = document.getElementsByTagName('body')
        $(body).css('overflow-x', 'hidden')
        var all_paragraphs = document.getElementsByClassName('correction')
        all_paragraphs = [...all_paragraphs];
        //window.addEventListener('scroll', this.handleCorrection(all_paragraphs));
        this.handleCorrection(all_paragraphs)

        //console.log(ReactDOM)
        //ReactDOM.render(<div>"test"</div>,)
    }

    componentWillUnmount() {
        var all_paragraphs = document.getElementsByClassName('correction')
        all_paragraphs = [...all_paragraphs];
        //window.removeEventListener('scroll', this.handleCorrection(all_paragraphs) );
        this.correctionReset()
    }

    render() {
        const post = this.props.post
        return (
            <div className="post-inner box-template correction_temp">
                <div className="post-info">
                    <div className="post-info-inner">
                        <div className="post-type">{post.contentType.title}</div>
                        <PostButtons />
                    </div>
                </div>
                <div ref={this.postHeader} className="post-header">
                    <div className="post-image">
                        <figure>
                            <picture>
                                <img src={getImageUrl(post.featuredImage.path)} />
                            </picture>
                        </figure>
                    </div>
                    <div className="post-title">
                        
                    </div>
                </div>
                <div ref={this.box} className="post-body">
                    
                    <div className="post-body-inner">
                        <div className="content" dangerouslySetInnerHTML={{ __html: this.props.post.content.blocks[0].text }}></div>
                        {/* <div className="content">
                            {this.state.content ? this.state.content.map((piece) => {
                                return(<p className="correction" dangerouslySetInnerHTML={{ __html: piece.innerHTML }}></p>)
                            }) : "hi"}
                        </div> */}
                    </div>
                </div>   
            </div>
        )
    }
}
