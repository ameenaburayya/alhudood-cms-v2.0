import React, {useState , useEffect ,useRef} from 'react'
import QuoteIcon from '../../../components/icons/quoteIcon';
import PostButtons from '../../../components/post/postButtons';
import ExpandIcon from '../../../components/icons/expandIcon'
import Dal from '../../../components/icons/dal';
import {getImageUrl} from '@takeshape/routing'
import styles from '../../../scss/templates/post/boxTemplate.module.scss'
import postStyles from '../../../scss/templates/post/defaultPostTemplate.module.scss'
import SocialShare from '../../../components/social/socialShare';


const wantedHeight = 121
export default class Fa9am extends React.Component {

    constructor(props) {
        super(props);
        this.handleBeforeExpand = this.handleBeforeExpand.bind(this)
        this.handleAfterExpand = this.handleAfterExpand.bind(this)
        this.before = React.createRef();
        this.after = React.createRef();
        this.afterTitle = React.createRef();
        this.beforeTitle = React.createRef();
        this.box = React.createRef()
        this.postHeader = React.createRef()
        this.state = {
            beforeHeight: 0,
            beforeExpand: false,
            afterHeight: 0,
            afterExpand: false,
        };
    }

    handleBeforeExpand(){
        this.setState({
            beforeExpand: !this.state.beforeExpand
        })
    }

    handleAfterExpand(){
        this.setState({
            afterExpand: !this.state.afterExpand
        })
    }

    componentDidMount() {
        const beforeHeight = this.before.current.clientHeight;
        const afterHeight = this.after.current.clientHeight;
        const titleHeight = Math.max(this.beforeTitle.current.clientHeight, this.afterTitle.current.clientHeight);
        this.setState({ beforeHeight, afterHeight });
        document.documentElement.style.setProperty('--excerptHeight', (wantedHeight + 'px'));
        document.documentElement.style.setProperty('--beforeHeight', (beforeHeight + 'px'));
        document.documentElement.style.setProperty('--afterHeight', (afterHeight + 'px'));
        document.documentElement.style.setProperty('--titleHeight', (titleHeight + 'px'));
    }


    render (){
        const post = this.props.post
        return (
            <div className={`${postStyles['post-inner']} ${styles['box-template']} ${styles['fe9am']}`}>
                <div className={styles['post-info']}>
                    <div className={styles['post-info-inner']}>
                        <div className={styles['post-type']}>{post.contentType.title}</div>
                        <PostButtons className={styles['post-buttons']} />
                    </div>
                </div>
                <div ref={this.postHeader} className={styles['post-header']}>
                    <div className={styles['post-image']}>
                        <figure>
                            <picture>
                                <img src={getImageUrl(post.featuredImage.path)} />
                            </picture>
                        </figure>
                    </div>
                    <div className={styles['post-title']}>
                        <div className={styles['fe9am-logo']}>
                            <img src={getImageUrl(post.fe9am.logo.path)} />
                        </div>
                        <div className={styles['fe9am-source']}>
                            <span>{post.fe9am.sourceName}</span>، <span>{post.fe9am.sourceCountry}</span>
                        </div>
                    </div>
                </div>
                <div ref={this.box} className={styles['post-body']}>
                    <div className={styles['post-body-inner']}>
                        <div className={styles['fe9am-content']}>
                            <div className={`${styles['fe9am-before']} ${styles['fe9am-content-item']}`}>
                                <div className={styles['fe9am-heading']}>في يوم</div>
                                <div className={styles['fe9am-date']}>{post.fe9am.before.date}</div>
                                <div className={styles['fe9am-pronoun']}>كتبت</div>
                                <div className={styles['fe9am-title']}>
                                    <div ref={this.beforeTitle}  className={styles['fe9am-title-inner']}>
                                        {post.fe9am.before.title}
                                    </div>
                                    <div className={styles['quote-icon']}><QuoteIcon /></div>
                                </div>
                                
                            </div>
                            <div className={`${styles['fe9am-after']} ${styles['fe9am-content-item']}`}>
                                <div className={styles['fe9am-heading']}>في يوم</div>
                                <div className={styles['fe9am-date']}>{post.fe9am.after.date}</div>
                                <div className={styles['fe9am-pronoun']}>كتبت</div>
                                <div  className={styles['fe9am-title']}>
                                    <div ref={this.afterTitle} className={styles['fe9am-title-inner']}>
                                        {post.fe9am.after.title}
                                    </div>
                                    <div className={styles['quote-icon']}><QuoteIcon /></div>
                                </div>
                            </div>
                            <div className={`${styles['fe9am-excerpt']} ${styles['fe9am-before']} ${this.state.beforeExpand ? styles['expanded'] : ''}`}>
                                    <div className={styles['excerpt-heading']}>وجاء في المقال</div>
                                    <div className={styles['inner-excerpt']}>
                                        <div ref={this.before} dangerouslySetInnerHTML={{ __html: post.fe9am.before.excerpt  }}></div>
                                    </div>
                                    {this.state.beforeHeight > wantedHeight  ? <span className={styles['expand']}><span onClick={this.handleBeforeExpand} className={styles['arrow']}></span></span> : null}
                            </div>
                            <div className={`${styles['fe9am-excerpt']}  ${styles['fe9am-after']} ${this.state.afterExpand ? styles['expanded'] : ''}`}>
                                <div className={styles['excerpt-heading']}>وجاء في المقال</div>
                                <div className={styles['inner-excerpt']}>
                                    <div ref={this.after} dangerouslySetInnerHTML={{ __html: post.fe9am.after.excerpt  }}></div>
                                </div>
                                {this.state.afterHeight > wantedHeight  ? <span className={styles['expand']}><span onClick={this.handleAfterExpand} className={styles['arrow']}></span></span> : null}
                            </div>
                        </div>
                        <div className={styles['fe9am-comment']}>
                            <span>فكتبت الحدود</span>
                            {post.fe9am.alhudoodComment}
                        </div>
                    </div>
                </div>   
                <SocialShare target={this.box} post={post} withTransition />
            </div>
            
        )
    }
}
