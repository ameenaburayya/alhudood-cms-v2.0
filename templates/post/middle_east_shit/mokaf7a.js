import React, { Component } from 'react'
import DangerIcon from '../../../components/icons/dangerIcon'
import EyeIcon from '../../../components/icons/eyeIcon'
import FingerprintIcon from '../../../components/icons/fingerprintIcon'
import GavelIcon from '../../../components/icons/gavelIcon'
import PostButtons from '../../../components/post/postButtons';
import {getImageUrl} from '@takeshape/routing'
import styles from '../../../scss/templates/post/boxTemplate.module.scss'
import postStyles from '../../../scss/templates/post/defaultPostTemplate.module.scss'
import SocialShare from '../../../components/social/socialShare'

export default class Mokaf7a extends Component {

    constructor(props) {
        super(props);
        this.box = React.createRef()
        this.postHeader = React.createRef()
    }

    render() {
        const post = this.props.post
        return (
            <div className={`${postStyles['post-inner']} ${styles['box-template']} ${styles['mokaf7a']}`}>
                <div className={styles['post-info']}>
                    <div className={styles['post-info-inner']}>
                        <div className={styles['post-type']}>{post.contentType.title}</div>
                        <PostButtons className={styles['post-buttons']} />
                    </div>
                </div>
                <div ref={this.postHeader} className={styles['post-header']}>
                    <div className={styles['post-image']}>
                        <figure>
                            <picture>
                                <img src={getImageUrl(post.featuredImage.path)} />
                            </picture>
                        </figure>
                    </div>
                    <div className={styles['post-title']}>
                        <div className={styles['criminal']}>
                            {post.mokaf7a.criminal}
                        </div>
                        <div className={styles['mokaf7a-title']}>
                            المجرم
                        </div>
                    </div>
                </div>
                <div ref={this.box} className={styles['post-body']}>
                    <div className={styles['post-body-inner']}>
                        <div className={`${styles['mokaf7a-section']} ${styles['crime']}`}>
                            <div className={styles['section-title']}>الجريمة</div>
                            <div className={styles['section-content']}>
                                {post.mokaf7a.crime}
                            </div>
                            <div className={styles['section-icon']}>
                                <EyeIcon />
                            </div>
                        </div>
                        <div className={`${styles['mokaf7a-section']} ${styles['report']}`}>
                            <div className={styles['section-title']}>محضر الضبط</div>
                            <div className={styles['section-content']}>
                                {post.mokaf7a.seizureReport}
                            </div>
                            <div className={styles['section-icon']}>
                                <FingerprintIcon />
                            </div>
                        </div>
                        <div className={`${styles['mokaf7a-section']} ${styles['report']}`}>
                            <div className={styles['section-title']}>درجة الخطورة</div>
                            <div className={styles['section-content']}>
                                {post.mokaf7a.severity}
                            </div>
                            <div className={styles['section-icon']}>
                                <DangerIcon />
                            </div>
                        </div>
                        <div className={`${styles['mokaf7a-section']} ${styles['report']}`}>
                            <div className={styles['section-title']}>منهجية الدولة في إعادة التأهيل</div>
                            <div className={styles['section-content']}>
                                {post.mokaf7a.rehabilitation}
                            </div>
                            <div className={styles['section-icon']}>
                                <GavelIcon />
                            </div>
                        </div>
                    </div>
                </div>
                <SocialShare target={this.box} post={post} withTransition />
            </div>
        )
    }
}
