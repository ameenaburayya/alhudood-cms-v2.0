import React, { Component } from 'react'
import DangerIcon from '../../../components/icons/dangerIcon'
import EyeIcon from '../../../components/icons/eyeIcon'
import FingerprintIcon from '../../../components/icons/fingerprintIcon'
import GavelIcon from '../../../components/icons/gavelIcon'
import PostButtons from '../../../components/post/postButtons';
import {getImageUrl} from '@takeshape/routing'
import styles from '../../../scss/templates/post/boxTemplate.module.scss'
import postStyles from '../../../scss/templates/post/defaultPostTemplate.module.scss'
import SocialShare from '../../../components/social/socialShare'

export default class Heros extends Component {

    constructor(props) {
        super(props);
        this.box = React.createRef()
        this.postHeader = React.createRef()
    }

    render() {
        const post = this.props.post
        return (
            <div className={`${postStyles['post-inner']} ${styles['box-template']} ${styles['heros']}`}>
                <div className={styles['post-info']}>
                    <div className={styles['post-info-inner']}>
                        <div className={styles['post-type']}>{post.contentType.title}</div>
                        <PostButtons className={styles['post-buttons']} />
                    </div>
                </div>
                <div ref={this.postHeader} className={styles['post-header']}>
                    <div className={styles['post-image']}>
                        <figure>
                            <picture>
                                <img src={getImageUrl(post.featuredImage.path)} />
                            </picture>
                        </figure>
                    </div>
                    <div className={styles['post-title']}>
                        <div className={styles['criminal']}>
                            <h1>{post.title}</h1>
                        </div>
                    </div>
                </div>
                <div ref={this.box} className={styles['post-body']}>
                    <div className={styles['post-body-inner']}>
                        <div className={styles['hero-content-section']}>
                            <div className={styles['hero-content']}>
                                {post.content.blocks.map((block) => 
                                    <p className={block.type == styles['blockquote'] ? styles['quote'] : ''}>{block.text}</p>
                                )}
                            </div>
                            <div className={styles['hero-dots']}>
                                <span className={styles['dot']}></span>
                                <span className={styles['dot']}></span>
                                <span className={styles['dot']}></span>
                            </div>
                            <div className={styles['hero-reason']}>
                                <div className={styles['title']}>عن</div>
                                <div className={styles['reason-content']}>{post.middleEastHero.heroReason} <br/> بتاريخ {post.middleEastHero.reasonDate}</div>
                            </div>
                        </div>
                        <div className={styles['hero-image']}>
                            <img src={getImageUrl(post.middleEastHero.heroImage.path)} />
                        </div>
                    </div>
                </div>
                <SocialShare target={this.box} post={post} withTransition />
            </div>
        )
    }
}
