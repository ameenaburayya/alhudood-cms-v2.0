import React from 'react'
//import dictionary from '../post/alhudoodpedia/dictionary';
import Profile from './alhudoodpedia/profilePostTemplate'
import Country from './alhudoodpedia/countryPostTemplate'
import DefaultPostTemplate from './defaultPostTemplate'
import dynamic from 'next/dynamic'
import Breadcrumbs from '../../components/breadcrumbs'
import LettersIndex from '../../components/lettersIndex'
import dictionaryContext from '../../contexts/dictionaryContext'
import { contentTypes } from '../../lib/constants'
import dictionaryStyles from '../../scss/templates/post/alhudoodpedia/dictionaryPostTemplate.module.scss'


// import dictionaryTemplate from './alhudoodpedia/dictionaryTemplate'

const DictionaryPostTemplate = dynamic(
    () => import('./alhudoodpedia/dictionaryPostTemplate'),
    { ssr: false }
)


// const window_height = window.innerHeight;

export default function AlhudoodpediaTemplate(props) {

    // static contextType = dictionaryContext



    switch (props.post.contentType.slug) {
        case `${contentTypes.dictionary.slug}`: return(
            <div className={`${dictionaryStyles['dictionary']}`}>
                <Breadcrumbs className={dictionaryStyles['breadcrumbs']} title={props.post.title} category={props.post.category} contentType={props.post.contentType} />
                <LettersIndex className={dictionaryStyles['letters-index']} />
                <DictionaryPostTemplate 
                    router={props.router} 
                    // post={initialized ?  dictionaryPosts[0] : props.post} 
                    post={props.post} 
                />
            </div>
        ) 
        case `${contentTypes.countries.slug}`: return( 
            <>
                <Breadcrumbs title={props.post.title} category={props.post.category} contentType={props.post.contentType}  />
                <Country post={props.post} />
            </>
        );
        case `${contentTypes.profiles.slug}`:  return (
            <>
                <Breadcrumbs title={props.post.title} category={props.post.category} contentType={props.post.contentType} />
                <Profile post={props.post} />
            </>
        );
        default: return <DefaultPostTemplate />;
    }
}
