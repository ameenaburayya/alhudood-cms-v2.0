import React, { useRef, useEffect, Component } from "react"

import Files from "./mediaMonitoring/files"
import Fe9am from "./mediaMonitoring/fa9am"
import Mokaf7a from "./middle_east_shit/mokaf7a"
import Heros from "./middle_east_shit/heros"
import Correction from "./mediaMonitoring/correction"
import DefaultPostTemplate from "./defaultPostTemplate"
import styles from "../../scss/templates/post/boxTemplate.module.scss"
import { contentTypes } from "../../lib/constants"

// const window_height = window.innerHeight;

export default class BoxTemplate extends Component {
    constructor(props) {
        super(props)
        this.handleBoxBottom = this.handleBoxBottom.bind(this)
        this.windowWidth = this.windowWidth.bind(this)
        this.child = React.createRef()
        this.state = {
            windowWidth: 0,
        }
    }

    componentDidMount() {
        {
            this.props.post.contentType.slug !== "correction" &&
                window.addEventListener("scroll", this.handleBoxBottom)
        }
        this.windowWidth()
        window.addEventListener("resize", this.windowWidth)
    }

    componentWillUnmount() {
        window.removeEventListener("scroll", this.handleBoxBottom)
        window.removeEventListener("resize", this.windowWidth)
    }

    handleBoxBottom() {
        if (this.state.windowWidth < 1024) {
            const box_position_bottom = this.child.current.box.current.getClientRects()[0]
                .bottom
            if (box_position_bottom < window.innerHeight) {
                this.child.current.postHeader.current.classList.add(
                    `${styles["done"]}`
                )
            } else {
                this.child.current.postHeader.current.classList.remove(
                    `${styles["done"]}`
                )
            }
        }
    }

    windowWidth() {
        this.setState({
            windowWidth: window.innerWidth,
        })
        if (window.innerWidth >= 1024) {
            this.child?.current?.postHeader?.current?.classList.remove(
                `${styles["done"]}`
            )
        }
    }

    render() {
        switch (this.props.post.contentType.slug) {
            case `${contentTypes.files.slug}`:
                return <Files ref={this.child} post={this.props.post} />
            case `${contentTypes.schizophrenia.slug}`:
                return <Fe9am ref={this.child} post={this.props.post} />
            case `${contentTypes.antiFreedom.slug}`:
                return <Mokaf7a ref={this.child} post={this.props.post} />
            case `${contentTypes.middleEastHeroes.slug}`:
                return <Heros ref={this.child} post={this.props.post} />
            case "correction":
                return <Correction post={this.props.post} />
            default:
                return <DefaultPostTemplate />
        }
    }
}
