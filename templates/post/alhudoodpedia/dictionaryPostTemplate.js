import React, {useState, useEffect, useContext, useRef} from 'react'
import Dictionary from '../../../components/layouts/dictionary'
import FlipPage from 'react-flip-page'
import {getIdsByContentType, getPreviewPostById} from '../../../requests/contentApi'
import { useRouter } from 'next/router'
import { Swiper, SwiperSlide } from 'swiper/react';
import SwiperCore, {EffectFade, Controller, Navigation} from 'swiper';
import 'swiper/swiper-bundle.css';
import ProgressRing from '../../../components/progressRing'
import { contentTypes } from '../../../lib/constants'
import dictionaryContext from '../../../contexts/dictionaryContext'
import styles from '../../../scss/templates/post/alhudoodpedia/dictionaryPostTemplate.module.scss'
import DictionaryLetterIcon from '../../../components/icons/dictionaryLetterIcon'
import ArrowDownIcon from '../../../components/icons/arrowDownIcon'
import { CircularProgress } from '@material-ui/core'
import SocialShare from '../../../components/social/socialShare'



function WordCount(str) { 
    return str.split(" ").length;
}

SwiperCore.use([EffectFade, Controller, Navigation]);

var temp, time
const autoplayTime = 2000

export default function DictionaryPostTemplate(props){

    const [activePage, setActivePage] = useState(startIndex)
    const [controlledSwiper, setControlledSwiper] = useState(null);
    const [windowWidth, setWidth] = useState(0)
    const [timerTime, setTimerTime] = useState(0)
    const [isPaused, setIsPaused] = useState(false)
    let flipPage = useRef(null);
    const mainSlider = useRef(null)
    const sideSlider = useRef(null)
    const { filteredDictionaryPosts, filtered } = useContext(dictionaryContext);
    let startIndex = filteredDictionaryPosts?.findIndex( el => el._id === props.post._id )

    const handleResize = () => {
        setWidth(window.innerWidth)
        // if (window.innerWidth >= 1024){
        //     handleTimerStart()
        // }
    }

    console.log(props.post)

    useEffect(() => {
        handleResize()
        window.addEventListener('resize', handleResize)
    
        return _ => {
        window.removeEventListener('resize', handleResize)
        }
    }, [])


    useEffect(() => {
        if (windowWidth < 1024){
            if (filtered){
                console.log('up')
                flipPage.gotoPage(0)
            }
            else if (filteredDictionaryPosts && !filtered && (activePage || activePage == 0)){ 
                console.log('down', activePage)
                // console.log('testing', flipPage)
                flipPage.gotoPage(activePage)
            }  
        }

    }, [filtered])

    const handleFlipChangeWhenFilter = () => {
        props.router.push({
            pathname: "/[category]/[contentType]/[id]",
            query: {
                category: props.router.query.category,
                contentType: props.router.query.contentType, 
                id: filteredDictionaryPosts[0]._id
            }
        }, { shallow: true })
    }

    const handleFlipChange = (pageIndex) => {
        setActivePage(pageIndex)
        props.router.push({
            pathname: "/[category]/[contentType]/[id]",
            query: {
                category: props.router.query.category,
                contentType: props.router.query.contentType, 
                id: filteredDictionaryPosts[pageIndex]._id
            }
        }, { shallow: true })
    }

    const handleNext = () => {
        flipPage.gotoNextPage();
    }


    const handleSliderNext = () => {

    }

    const handleSliderPrev = () => {

    }


    // const handleTimerStart = () => {
    //     // this.setState({isActive: true})
    //     temp = setInterval(() => {
    //         //console.log('siper', this.swiper.current.swiper)
    //         clearInterval(time)
    //         setTimerTime(0)
    //         // mainSlider.current?.swiper?.slideNext()
    //         // this.setState({isActive: false, interval: (this.state.postsData[active_index + 1] ? ((WordCount(this.state.postsData[active_index + 1].dictionary.definition) / 200) * 60 * 1000 ) : 8000)})
    //     }, (autoplayTime - timerTime + 200))
    
    // }

    // const handleTimerProgressStart = () => {
    //     time = setInterval(() => {
    //         //this.setState({timer: this.state.timer + 100})
    //         setTimerTime((prevTime) => (prevTime + 100))
    //         // this.setState({timer: this.state.timer + 100})
    //         //console.log(timer)
    //     }, 100)
    // }
        
        
    // const handlePause = () => {
    //     if(!isPaused){
    //         clearInterval(temp)
    //         clearInterval(time)
    //     }
    //     else{
    //         handleTimerStart()
    //     }
    //     // setIsPaused(!isPaused)
    // }
        

    const handleSlideChange = (activeSlide) => {
        console.log('slide change', activeSlide)
        // clearInterval(temp)
        clearInterval(time)
        setTimerTime(0)

        // temp = setInterval(() => {
        //     //console.log('siper', this.swiper.current.swiper)
        //     mainSlider.current?.swiper?.slideNext()
        //     // mainSlider.current?.swiper?.slideNext()
        //     // this.setState({isActive: false, interval: (this.state.postsData[active_index + 1] ? ((WordCount(this.state.postsData[active_index + 1].dictionary.definition) / 200) * 60 * 1000 ) : 8000)})
        // }, (autoplayTime))

        time = setTimeout(() => {
            //this.setState({timer: this.state.timer + 100})
            console.log(timerTime)
            setTimerTime(timerTime + 100)
            if (timerTime == autoplayTime){
                mainSlider.current?.swiper?.slideNext()
            }
            // this.setState({timer: this.state.timer + 100})
            //console.log(timer)
        }, 100) 
    }

    return (
        <>
            {filteredDictionaryPosts && 
                <div style={{display: windowWidth < 1024 ? 'block' : 'none'}}>
                    <FlipPage 
                    ref={(component) => { flipPage = component; }}
                    // ref={flipPage}
                    responsive 
                    startAt={startIndex}
                    noShadow={true}
                    className={styles['flipbox']} 
                    onPageChange={(pageIndex) => {
                        if (!filtered) handleFlipChange(pageIndex)
                        else handleFlipChangeWhenFilter(pageIndex)
                    }}
                    >
                        {filteredDictionaryPosts.map((post) => (
                            <Dictionary 
                                handleNext={handleNext} 
                                post={post}
                            />
                        ))}
                    </FlipPage>
                </div>
            || "loading"}
            <div style={{display: windowWidth < 1024 ? 'none' : 'flex'}} className={styles['dictionary-slider']}>
                <div className={styles['inner']}>
                    <div className={styles['main']}>
                        <div className={`${styles['arrow']} ${styles['prev']} prev`}>
                            <ArrowDownIcon />
                        </div>
                        {filteredDictionaryPosts && 
                            <Swiper
                            controller={{ control: controlledSwiper }}
                            spaceBetween={0}
                            effect={"fade"}
                            className={styles['slider']}
                            ref={mainSlider}
                            // onInit={() => this.handleTimerStart()}
                            onSlideChangeTransitionEnd={(swiper) => handleSlideChange(swiper.activeIndex)}
                            initialSlide={startIndex}
                            slidesPerView={1}
                            // allowTouchMove={false}
                            navigation={{
                                nextEl: '.next',
                                prevEl: '.prev'
                            }}
                            >
                                {filteredDictionaryPosts.map((post, index) => (
                                        <SwiperSlide className={styles['slide']} key={index}>
                                            <Dictionary handleSliderNext={handleSliderNext} handleSliderPrev={handleSliderPrev} handleNext={handleNext} key={index} post={post} />
                                        </SwiperSlide>
                                ))}
                                
                                {/* {this.state.isActive ? <ProgressRing isPaused={this.state.isPaused} interval={this.state.interval}  /> : null } */}

                                {/* <button onClick={() => this.handlePause()}>pause</button> */}
                            </Swiper>
                        || "loading"}
                        <div className={`${styles['arrow']} ${styles['next']} next`}>
                            <ArrowDownIcon />
                        </div>
                        <div className={styles['progress']}>
                            <ProgressRing autoplayTime={autoplayTime} timerTime={timerTime} isPaused={isPaused} />
                        </div>
                    </div>
                    <div className={styles['side']}>
                        <div className={styles['letter']}>
                            {filteredDictionaryPosts && 
                                <Swiper
                                    spaceBetween={0}
                                    onSwiper={setControlledSwiper}
                                    effect={"fade"}
                                    className={`${styles['slider']} ${styles['side-slider']}`}
                                    // ref={sideSlider}
                                    // onInit={() => this.handleTimerStart()}
                                    // onSlideChange={(swiper) => this.handleChange(swiper.activeIndex)}
                                    initialSlide={startIndex}
                                    slidesPerView={1}
                                    allowTouchMove={false}
                                >
                                    {filteredDictionaryPosts.map((post, index) => (
                                            <SwiperSlide className={styles['slide']} key={post._id}>
                                                <div className={styles['dictionary-letter']} dangerouslySetInnerHTML={{ __html: post.dictionary.letter.mainSvg }} />
                                            </SwiperSlide>
                                    ))}
                                </Swiper>
                            || null}
                        </div>
                        <div className={styles['dictionary-letter-icon']}>
                            <DictionaryLetterIcon />
                        </div>
                    </div>
                </div>
            </div>
            {props.post && <SocialShare post={props.post} />}
        </>
    )
}
// constructor(props){
//     super(props)
//     this.ids = this.getIds()
//     this.flipPage = React.createRef()
//     this.swiper = React.createRef()
//     this.handleChange = this.handleChange.bind(this)
//     this.getPreviousArticleWithId = this.getPreviousArticleWithId.bind(this)
//     this.updateWindowWidth = this.updateWindowWidth.bind(this)
//     this.handlePause = this.handlePause.bind(this)
//     this.handleNext = this.handleNext.bind(this)
//     this.getReadingTime = this.getReadingTime.bind(this)
//     this.handleTimerStart = this.handleTimerStart.bind(this)
//     this.getIds = this.getIds.bind(this)
//     this.state = {
//         isFetching: false,
//         postsData: null,
//         before_ids: null,
//         after_ids: null,
//         current_id: null,
//         current_id_index: null,
//         timer: 0,
//         isActive: false,
//         isPaused: false,
//         interval: 8000
//     }
// }


// handleChange(pageIndex){
//     const dictionary_id = this.state.ids[pageIndex]
//     //this.props.router.query._id = dictionary_id
//     // this.props.router.push({
//     //     pathname: "/[category]/[contentType]/[id]",
//     //     query: {
//     //         category: this.props.router.query.category,
//     //         contentType: this.props.router.query.contentType, 
//     //         id: dictionary_id._id
//     //     }
//     // }, { shallow: true })
//     //console.log(this.props.router, dictionary_id)
//     if(!this.state.postsData[pageIndex - 1] && pageIndex) {
//         this.getPreviousArticleWithId(this.state.ids[pageIndex - 1]._id , pageIndex - 1)
//     }  
//     else if (this.state.ids.length - 1 > pageIndex && !this.state.postsData[pageIndex + 1]){
//         this.getNextArticleWithId(this.state.ids[pageIndex + 1]._id , pageIndex + 1)
//     }
// }

// getReadingTime(words){
//     const reading_time = (WordCount(words) / 200) * 60 * 1000
//     return reading_time
// }


// handleTimerStart(){
//     this.setState({isActive: true})
//     temp = setInterval(() => {
//         //console.log('siper', this.swiper.current.swiper)
//         const active_index = this.swiper.current.swiper ? this.swiper.current.swiper.activeIndex : null
//         this.setState({isActive: false, interval: (this.state.postsData[active_index + 1] ? ((WordCount(this.state.postsData[active_index + 1].dictionary.definition) / 200) * 60 * 1000 ) : 8000)})
//         this.swiper.current?.swiper?.slideNext()
//         this.setState({isActive: true, timer: 0})
//     }, (8000 - this.state.timer))

//     time = setInterval(() => {
//         //this.setState({timer: this.state.timer + 100})
//         this.setState({timer: this.state.timer + 100})
//         //console.log(timer)
//     }, 100)
// }


// handlePause(){
//     this.setState({isPaused: !this.state.isPaused})
//     if(!this.state.isPaused){
//         clearInterval(temp)
//         clearInterval(time)
//     }
//     else{
//         this.handleTimerStart()
//     }
// }

// handleNext = () => {
//     this.flipPage.gotoNextPage();
// }


// async getPreviousArticleWithId(id, index){
//     getPreviewPostById(id).then(
//         res => {
//             temp_post_data[index] = res.post
//             this.setState({
//                 postsData: temp_post_data
//             })
//         }
//     )
// }

// async getNextArticleWithId(id, index){
//     getPreviewPostById(id).then(
//         res => {
//             temp_post_data[index] = res.post
//             this.setState({
//                 postsData: temp_post_data
//             })
//         }
//     )
// }


// async getIds() {
//     //this.state['isFetching'] = true
//     getIdsByContentType(contentTypes.dictionary.slug)
//         .then(
//             res => {
//                 target_id = this.filter_ids(res, this.state.current_id);
//                 index = this.getIndex(res, target_id[0]._id)
//                 before_ids = res.slice(0, (index))
//                 after_ids = res.slice(index + 1, res.length)
//                 temp_post_data[index] = this.props.post
            
//                 var before_dictionary_id = index ? res[index - 1]._id : null
//                 var after_dictionary_id = res[index + 1] ? res[index + 1]._id : null
//                 before_dictionary_id ? getPreviewPostById(before_dictionary_id).then(
//                     res => {
//                         temp_post_data[index - 1] = res.post
//                         this.setState({
//                             postsData: temp_post_data
//                         })
//                     }
//                 ) : null

//                 after_dictionary_id ? getPreviewPostById(after_dictionary_id).then(
//                     res => {
//                         temp_post_data[index + 1] = res.post
//                         this.setState({
//                             postsData: temp_post_data
//                         })
//                     }
//                 ) : null
                

//                 this.setState({
//                     before_ids: before_ids, 
//                     after_ids: after_ids, 
//                     current_id_index: index,
//                     ids: res,
//                     isFetching: false, 
                    
//                 })
//                 //console.log(this.state.postsData)
//                 //console.log('state',this.state.postsData[2])
                
                
//             }
//         )
//         .catch(
//             e => {
//                 this.setState({...this.state, isFetching: "error", postsData: null});
//             }
//         );
// }

// filter_ids(array, value) {
//     return array.filter(function(e) {
//         return e._id == value;
//     });
// }

// getIndex(array, target_id) {

//     var i = 0
//     var ii = array.length;
//     for(i; i < ii; i++) if(array[i]._id === target_id) break;
    
//     return i;
// }



// updateWindowWidth() {
//     this.setState({ width: window.innerWidth});
//     let vh = window.innerHeight * 0.01;
//     document.documentElement.style.setProperty('--dictionaryHeight', `${vh}px`);
// }


// componentDidMount(){
//     this.getIds()
//     var reading_time = (WordCount(this.props.post.dictionary.definition) / 200) * 60 * 1000
//     this.setState({current_id: this.props.post._id, interval: reading_time})
//     this.updateWindowWidth();
//     window.addEventListener('resize', this.updateWindowWidth);
//     //this.handleTimerStart()
// }

// componentWillUnmount() {
//     window.removeEventListener('resize', this.updateWindowWidth);
// }





