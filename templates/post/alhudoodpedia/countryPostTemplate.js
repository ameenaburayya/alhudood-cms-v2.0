import React , {componentDidMount} from 'react'
import {getImageUrl} from '@takeshape/routing'
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpandIcon from '../../../components/icons/expandIcon';
import styles from '../../../scss/templates/post/alhudoodpedia/country.module.scss'
import DefaultPostSkeleton from '../../../components/skeletons/defaultPostSkeleton';
import SocialShare from '../../../components/social/socialShare';



export default class Country extends React.Component {

    constructor(){
        super()
        this.map = React.createRef();
        this.info = React.createRef()
        this.target = React.createRef()
        this.handleChange = this.handleChange.bind(this)
        this.updateWindowWidth = this.updateWindowWidth.bind(this)
        this.state = {
            expanded: false,
            image_status: null,
            aspect: null,
        }
    }


    handleChange = panel => {
        return (event, isExpanded) => {
            this.setState({
                expanded: isExpanded ? panel : false
            });
        };
    }


    handleImageSize(){
        return new Promise(resolve => {
            const imgSrc = this.props.post.country.countryMedia.map.path
            var newImg = new Image();
            var image_status
            newImg.onload = () => {
                var height = newImg.height;
                var width = newImg.width;
                var aspect = height / width
                image_status = height > width ? "tall" : "wide"
                this.setState({image_status, aspect})
                document.documentElement.style.setProperty("--country-map-aspect", ((150 /aspect) + 'px')); 
                resolve(image_status)
            }
            newImg.src = getImageUrl(imgSrc); // this must be done AFTER setting onload
        });
    }


    async handleThings(){
        const image_status = await this.handleImageSize()
        this.updateWindowWidth()
    }

    updateWindowWidth() {
        const dimensions = {
            height: window.innerHeight,
            width: window.innerWidth
        }

        var margin_amount;
        if (this.state.image_status == "tall"){
            margin_amount = dimensions.width * 0.4 / 1.6
        }
        else if (this.state.image_status == "wide"){
            margin_amount = dimensions.width * 0.9 / 4
        }

        if (dimensions.width > 767){
            margin_amount = 400 / 3
        }

        if(dimensions.width > 767 && this.state.image_status == "wide"){
                margin_amount = 550 * 0.9 / 4
        }
        this.setState({ width: dimensions.width, height: dimensions.height, margin_amount: margin_amount});
        return dimensions
    }

    componentDidMount(){
        this.handleThings()
        window.addEventListener('resize', this.updateWindowWidth);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowWidth);
    }

    render(){
        const post = this.props.post
        return (
            <div className={`${styles['post-inner']} ${styles[`${post.contentType.slug}`]}`}>
                <div className={`${styles['country-info']} ${styles[`has-${this.state.image_status}`]}`}>
                    <div className={styles['country-map']}>
                        <figure>
                            <picture>
                                <img style={{marginBottom: this.state.width >= 1024 ? 0 : -this.state.margin_amount}} className={styles[`${this.state.image_status}`]} ref={this.map} src={getImageUrl(post.country.countryMedia.map.path)} />
                            </picture>
                        </figure>
                    </div>
                    <div style={{paddingTop: this.state.width >= 1024 ? 0 : this.state.margin_amount}} ref={this.info} className={styles['country-info-content']}>
                        {post.country.countryInfo.info.map((item, index) => (
                            <div key={index} className={styles['info-item']}>
                                <div className={styles['info-title']}>{item.infoTitle}</div>
                                <div className={styles['info-content']}>{item.infoContent}</div>
                            </div>
                        ))}
                    </div>
                </div>
                <div ref={this.target} className={styles['country-definition']}>
                    <div className={styles['country-name']}>{post.title}</div>
                    <div className={styles['country-definition-content']}>{post.country.countryBio}</div>
                </div>
                <div className={styles['country-about-section']}>
                    <div className={styles['about-title']}>عن {post.title}</div>
                    {this.state.width < 1024 
                    ? <div className={styles['about-accordion']}>
                        {post.country.aboutCountry.about.map((item, index) => (
                            <Accordion className={styles['accordion-item']} expanded={this.state.expanded === `panel${index}`} onChange={this.handleChange(`panel${index}`)}>
                                <AccordionSummary
                                    expandIcon={<ExpandIcon />}
                                    className={styles['accordion-title']}
                                >
                                    {item.aboutTitle}
                                </AccordionSummary>
                                <AccordionDetails className={styles['accordion-content']}>
                                    {item.aboutContent}
                                </AccordionDetails>
                            </Accordion>
                        ))}
                    </div>
                    : <div className={styles['about-sections']}>
                        {post.country.aboutCountry.about.map((item, index) => (
                            <div className={styles['info-item']}>
                                <div className={styles['title']}>{item.aboutTitle}</div>
                                <div className={styles['content']}>{item.aboutContent}</div>
                            </div>
                        ))}
                    </div>
                    }
                </div>
                <SocialShare target={this.target} post={post} withTransition />
            </div>
        )
        if(this.state.image_status){
            
        }
        else{
            return <DefaultPostSkeleton />
        }
    }
   
    
}
