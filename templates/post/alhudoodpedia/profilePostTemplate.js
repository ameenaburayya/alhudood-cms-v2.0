import React, { useRef } from 'react'
import {getImageUrl} from '@takeshape/routing'
import StarIcon from '../../../components/icons/startIcon'
import styles from '../../../scss/templates/post/alhudoodpedia/profile.module.scss'
import SocialShare from '../../../components/social/socialShare'


export default function Profile({post}) {

    const content = useRef()


    return (
        <div className={`${styles['profiles']}`}>
            <div className={styles["profile-info"]}>
                <div className={styles['profile-image']}>
                    <figure>
                        <picture>
                            <img src={getImageUrl(post.featuredImage.path)} />
                        </picture>
                    </figure>
                </div>
                <div className={styles['profile-info-content']}>
                    <div className={styles['info-item']}>
                        <div className={styles['info-title']}>الأسم</div>
                        <div className={styles['info-content']}>{post.profile.name}</div>
                    </div>
                    <div className={styles['info-item']}>
                        <div className={styles['info-title']}>ألقاب أخرى</div>
                        <div className={styles['info-content']}>{post.profile.nicknames.map((item) => (
                            <span className={styles['nickname']}>{item.nickname} <span className={styles['dash']}>-</span></span> 
                        ))}</div>
                    </div>
                    <div className={styles['info-item']}>
                        <div className={styles['info-title']}>مكان وتاريخ الولادة</div>
                        <div className={styles['info-content']}>
                            <div className="birth-content">{post.profile.birth.birthPlace} {post.profile.birth.birthDate}</div>
                            {post.profile.birth.notes ? <div className="">{post.profile.birth.notes}</div> : null}
                        </div>
                    </div>
                    <div className={styles['info-item']}>
                        <div className={styles['info-title']}>المهنة</div>
                        <div className={styles['info-content']}>{post.profile.occupations.map((item) => (
                            <span className={styles['occupation']}>{item.occupation} <span className={styles['dash']}>-</span></span> 
                        ))}</div>
                    </div>

                    {post.profile.info.map((item, index) => (
                        <div key={index} className={styles['info-item']}>
                            <div className={styles['info-title']}>{item.infoTitle}</div>
                            <div className={styles['info-content']}>{item.infoContent}</div>
                        </div>
                    ))}
                </div>
            </div>
            <div ref={content} className={styles['profile-bio']}>
                <div className={styles['content']}>{post.profile.bio}</div>
            </div>
            <div className={`${styles['profile-achievements-section']} ${styles['profile-section']}`}>
                <div className={styles['title']}>انجازات {post.title}</div>
                <div className={styles['achievements']}>
                    {post.profile.achievements.map((item) => (
                        <div className={styles['achievement']}><StarIcon /><span>{item.achievement}</span></div>
                    ))}
                </div>
            </div>
            <div className={`${styles['profile-quotes-section']} ${styles['profile-section']}`}>
                <div className={styles['title']}>اشهر اقوال {post.title}</div>
                <div className={styles['quotes']}>
                    {post.profile.quotes.map((item) => (
                        <div className={styles['quote']}>{item.quote}</div>
                    ))}
                </div>
            </div>
            <SocialShare target={content} post={post} withTransition />
        </div>
    )
}
