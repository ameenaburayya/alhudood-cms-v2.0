import React, { useState, useContext } from "react"
import { Swiper, SwiperSlide } from "swiper/react"
import { useRouter } from "next/router"
import Dictionary from "./alhudoodPedia/dictionary"
import "swiper/swiper-bundle.css"
import {
    alhudoodPediaContentTypes,
    categories,
    contentTypes,
} from "../../lib/constants"
import Countries from "./alhudoodPedia/countries"
import Profiles from "./alhudoodPedia/profiles"
import AlhudoodpediaIcon from "../../components/icons/alhudoodpediaIcon"
import LettersIndex from "../../components/lettersIndex"
import dictionaryContext from "../../contexts/dictionaryContext"
import styles from "../../scss/templates/category/alhudoodpediaTemplate.module.scss"

export default function AlhudoodpediaTemplate({ articles, path }) {
    // console.log(articles)

    const router = useRouter()
    const query = router.query
    var articlesByContentType = {}

    const { filteredDictionaryPosts, filtered } = useContext(dictionaryContext)

    alhudoodPediaContentTypes.map((type, index) => {
        function filterByContentType(item) {
            if (item.contentType.slug == type.slug) {
                return true
            }
            return false
        }
        articlesByContentType[type.slug] = articles.filter(filterByContentType)
    })

    const initialSlide = query.contentType
        ? query.contentType == contentTypes.dictionary.slug
            ? 0
            : query.contentType == contentTypes.countries.slug
            ? 1
            : query.contentType == contentTypes.profiles.slug
            ? 2
            : 0
        : 0

    const [activeSlide, changeActiveSlide] = useState(initialSlide)
    const [swiper, setSwiper] = useState(null)

    const changeOnClick = (index) => {
        changeActiveSlide(index)
        swiper.slideTo(index)
    }

    const changeOnSwipe = (index) => {
        changeActiveSlide(index)
        handleHrefChange(index)
    }

    const handleHrefChange = (index) => {
        router.push(
            {
                asPath: `/category/[slug]/?contentType=${alhudoodPediaContentTypes[index].slug}`,
                query: {
                    slug: router.query.slug,
                    contentType: alhudoodPediaContentTypes[index].slug,
                },
            },
            { shallow: true }
        )
    }

    return (
        <>
            <div className={styles["tabs-navigation"]}>
                <div className={styles["tabs-navigation-inner"]}>
                    <div className={styles["icon"]}>
                        <AlhudoodpediaIcon />
                    </div>
                    {alhudoodPediaContentTypes.map((type, index) => (
                        <div
                            className={`${styles["tab-item"]} ${
                                activeSlide == index ? styles["active"] : ""
                            }`}
                            onClick={() => changeOnClick(index)}
                            key={type.slug}
                        >
                            <span>{type.title}</span>
                        </div>
                    ))}
                </div>
            </div>
            <Swiper
                spaceBetween={50}
                effect={"Overflow"}
                initialSlide={initialSlide}
                slidesPerView={1}
                onSlideChange={(swiper) => changeOnSwipe(swiper.activeIndex)}
                onSwiper={(swiper) => setSwiper(swiper)}
                autoHeight={true}
                className={styles["slider"]}
                noSwipingClass={styles["letters-index"]}
            >
                <SwiperSlide>
                    <LettersIndex
                        posts={articlesByContentType.dictionary}
                        className={styles["letters-index"]}
                    />
                    <Dictionary
                        className={`${styles["alhudoodpedia-type"]} ${styles["dictionary-type"]}`}
                        path={path}
                        posts={filteredDictionaryPosts}
                        filtered={filtered}
                    />
                </SwiperSlide>
                <SwiperSlide>
                    <Countries
                        className={styles["alhudoodpedia-type"]}
                        posts={articlesByContentType.countries}
                    />
                </SwiperSlide>
                <SwiperSlide>
                    <Profiles
                        className={styles["alhudoodpedia-type"]}
                        posts={articlesByContentType.profiles}
                    />
                </SwiperSlide>
            </Swiper>
        </>
    )
}
