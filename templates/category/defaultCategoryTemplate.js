import React, { useState, useEffect } from "react"
import PropsType from "prop-types"
import { CircularProgress } from "@material-ui/core"
import TagsTicker from "../../components/tagsTicker"
import Sorter from "../../components/postSorter"
import { categories } from "../../lib/constants"
import PostsBlock from "../../components/blocks/postsBlock"
import MainHeading from "../../components/headings/mainHeading"
import Dal from "../../components/icons/dal"
import styles from "../../scss/templates/category/defaultCategoryTemplate.module.scss"
import {
    gettingArticlesByCategorySlug,
    gettingArticlesByContentTypeId,
    gettingContentTypeBySlug,
    gettingContentTypes,
    gettingEditorsChoiceArticles,
} from "../../requests/contentApi"
import NoResults from "../../components/noResults"
import PageTitle from "../../components/headings/pageTitle"
import Loading from "../../components/loading"

export default function DefaultCategoryTemplate({ categoryData, query }) {
    const { allTags, categoryInfo, categoryArticles } = categoryData

    const [contentTypes, setContentTypes] = useState(null)
    const [filteredArticles, setFilteredArticles] = useState(null)
    const [loadingPosts, setLoadingPosts] = useState(false)
    const [activeOption, setActiveOption] = useState(null)
    const [editorsChoiceArticles, setEditorsChoiceArticles] = useState(null)

    const getContentTypes = async (categoryId) => {
        const tempContentTypes = await gettingContentTypes(categoryId)
        setContentTypes(tempContentTypes)
    }

    const getEditorsChoice = async (categoryId) => {
        const data = await gettingEditorsChoiceArticles(categoryId)
        setEditorsChoiceArticles(data.editorsChoice)
    }
    const handleSorting = async (optionId) => {
        setLoadingPosts(true)
        if (optionId === 200 || optionId === 300) {
            const tempFilteredArticles = await gettingArticlesByCategorySlug("currentAffairs")
            setFilteredArticles(tempFilteredArticles.posts)
            setActiveOption(optionId)
        } else {
            const tempFilteredArticles = await gettingArticlesByContentTypeId(optionId)
            setFilteredArticles(tempFilteredArticles.posts)
            setActiveOption(optionId)
        }
        setLoadingPosts(false)

        // you want to call an api to get posts from that contentType
    }

    const handleUrlSorting = async (contentTypeSlug) => {
        const contentTypeInfo = await gettingContentTypeBySlug(contentTypeSlug)
        handleSorting(contentTypeInfo.id)
    }

    useEffect(() => {
        getContentTypes(categoryData.categoryInfo.id)
        setFilteredArticles(categoryArticles.posts)
        if (categoryData.categoryInfo.id === categories.currentAffairs.id) {
            getEditorsChoice(categoryData.categoryInfo.id)
        }
        if (query.contentType) {
            setLoadingPosts(true)
            handleUrlSorting(query.contentType)
        }
    }, [])

    return (
        <div className={styles["archive-page"]}>
            {allTags && <TagsTicker tags={allTags} />}

            {editorsChoiceArticles && editorsChoiceArticles.length > 0 && (
                <div className={styles["archive-editor-choices"]}>
                    <MainHeading className={styles["title"]} withDash>
                        من اختيار المحرر
                    </MainHeading>
                    <PostsBlock
                        className={styles["editor-choices-posts"]}
                        layout="editors-choice"
                        withExcerpt
                        posts={editorsChoiceArticles.slice(0, 3)}
                    />
                    <Dal className={styles["dal"]} />
                </div>
            )}

            <div className={styles["archive-posts"]}>
                <div className={styles["archive-posts-header"]}>
                    <PageTitle className={styles["category-title"]} withDash>
                        {categoryInfo.title}
                    </PageTitle>
                    <Sorter
                        className={styles["sorting-dropdown"]}
                        contentTypes={contentTypes}
                        handleSorting={handleSorting}
                        activeOption={activeOption}
                    />
                </div>
                <div className={styles["archive-posts-list"]}>
                    {filteredArticles?.length > 0 ? (
                        !loadingPosts ? (
                            <PostsBlock
                                withBookmark
                                className={styles["archive-posts-list"]}
                                layout="archive"
                                withTags
                                withDate
                                posts={filteredArticles}
                            />
                        ) : (
                            <Loading />
                        )
                    ) : (
                        <NoResults text="لم يعمل فريق التحرير على المحتوى   بعد" />
                    )}
                </div>
            </div>
            {/* {((allCategoryArticles.length - visibleBlogs) > 0) ? <SecondaryButton rounded color="black" onClick={handleMorePosts} label="المزيد" /> : null} */}
        </div>
    )
}

DefaultCategoryTemplate.propTypes = {
    categoryData: PropsType.object,
    query: PropsType.object,
}
