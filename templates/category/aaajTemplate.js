import React, { useEffect, useState } from "react"
import PropsType from "prop-types"
import Deck from "../../components/aaaj/votingDeck"
import VotingSlider from "../../components/aaaj/votingSlider"
import AAAJEditrosChoicesBlock from "../../components/blocks/aaajEditrosChoicesBlock"
import DidYouKnow from "../../components/blocks/didYouKnow"
import NewsletterBox from "../../components/newsletters/newsletterBox"
import ContentSection from "../../components/sections/contentSection"
import SupportUs from "../../components/blocks/supportUs"
import styles from "../../scss/templates/category/aaajTemplate.module.scss"
import AAAJLogo from "../../components/logos/aaajLogo"

export default function AAAJTemplate({ posts }) {
    const [windowWidth, setWindowWidth] = useState(0)

    const handleResize = () => {
        setWindowWidth(window.innerWidth)
    }

    useEffect(() => {
        handleResize()
        window.addEventListener("resize", handleResize)

        return (_) => {
            window.removeEventListener("resize", handleResize)
        }
    })

    if (!posts) {
        return "loading"
    }

    return (
        <div className={styles["aaaj"]}>
            <div className={styles["intro"]}>
                <AAAJLogo />
                <p>
                    حيث نقوم بالرصد اليومي لعشرات الصحف والمواقع الإخبارية والصحفية
                    العربية، بهدف ترشيح أسوأ المواد من تقارير أو أخبار، ومقالات،
                    اعتماداً على عدد من المعايير
                </p>
            </div>
            <div className={styles["editor"]}>
                <AAAJEditrosChoicesBlock article="null" correction="null" />
            </div>
            <div>
                <div>
                    <h3>هل يستحق هذا الخبر الفوز بجحصع؟</h3>
                    <p>
                        كما أن وقام وبدأت، لم أدوات للمجهود كما أن وقام وبدأت، لم
                        أدوات للمجهود
                    </p>
                </div>
                {windowWidth >= 1024 ? <VotingSlider /> : <Deck />}
            </div>
            {windowWidth >= 1024 ? null : (
                <div>
                    <DidYouKnow />
                </div>
            )}
            <div className={styles["three-posts"]}>
                <ContentSection
                    layout={windowWidth >= 1024 ? "default" : "square-list"}
                    posts={posts}
                />
            </div>
            <div>
                <NewsletterBox desktop={windowWidth >= 1024} />
            </div>
            <div>
                <SupportUs />
            </div>
        </div>
    )
}

AAAJTemplate.propsType = {
    posts: PropsType.object,
}
