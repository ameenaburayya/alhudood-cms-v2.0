import React from "react"
import PropsType from "prop-types"
import CountryAndProfileBlock from "../../../components/blocks/countryAndProfileBlock"

export default function Countries({ posts, className }) {
    return (
        <div className={`${className ? className : ""}`}>
            {posts.map((post) => (
                <CountryAndProfileBlock key={post._id} article={post} />
            ))}
        </div>
    )
}

Countries.propTypes = {
    posts: PropsType.array.isRequired,
    className: PropsType.object,
}
