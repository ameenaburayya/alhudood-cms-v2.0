import React from 'react'
import CountryAndProfileBlock from '../../../components/blocks/countryAndProfileBlock'



export default function Profiles(props) {
    return (
        <div className={`${props.className ? props.className : ''}`}>
            {props.posts.map((post) => 
                <CountryAndProfileBlock key={post._id} article={post} />
            )}
        </div>
    )
}
