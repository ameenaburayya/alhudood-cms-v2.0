import React from "react"
import DictionaryBlock from "../../../components/blocks/dictionaryBlock"

export default function dictionary(props) {
    return (
        <div className={`${props.className ? props.className : ""}`}>
            {props.posts?.map((post) => (
                <DictionaryBlock path={props.path} key={post._id} post={post} />
            ))}
        </div>
    )
}
