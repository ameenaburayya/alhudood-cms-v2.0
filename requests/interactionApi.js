import axios from "axios"
import { CMS_DOMAIN, CMS_URL, contentTypes, LOCAL_CMS_DOMAIN } from "../lib/constants"

axios.defaults.withCredentials = true

export const pollVoting = async ({ pollId, optionId }) => {
    try {
        const res = await axios.post(`${CMS_URL}/api/v1/poll/vote`, {
            id: pollId,
            vote: optionId,
        })
        return res.data
    } catch (error) {
        return error
    }
}

export const bookmarkArticle = async (id) => {
    try {
        const res = await axios.post(`${LOCAL_CMS_DOMAIN}/api/v1/bookmark?article_id=1}`)
        return res.data
    } catch (error) {
        return error
    }
}
