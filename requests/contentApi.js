import axios from "axios"
import { CMS_DOMAIN, CMS_URL, contentTypes } from "../lib/constants"

axios.defaults.withCredentials = true

const API_URL = `https://api.takeshape.io/project/606038f1-bbe7-4e2a-bf17-da2f9f8350b6/graphql`
const API_KEY = "7ea942a44cdd4c79b0fed4c7369ca9ee"

async function fetchAPI(query, { variables } = {}) {
    const res = await fetch(API_URL, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${API_KEY}`,
        },
        body: JSON.stringify({
            query,
            variables,
        }),
    })

    const json = await res.json()
    if (json.errors) {
        throw new Error("Failed to fetch API")
    }
    return json.data
}

export async function gettingPollData() {
    const res = await axios.get(`${CMS_URL}/api/v1/poll`)
    return res.data
}

export async function gettingDidYouKnowData() {
    const res = await axios.get(`${CMS_URL}/api/v1/didYouKnow`)
    return res.data
}

export async function gettingFansMail() {
    const res = await axios.get(`${CMS_URL}/api/v1/fanMail`)
    return res.data
}

export async function gettingDorar() {
    const res = await axios.get(`${CMS_URL}/api/v1/gems`)
    return res.data
}

export async function search(term, page, perPage, categoryId) {
    const res = await axios.get(
        `${CMS_URL}/api/v1/articles/search?page=${page}&per_page=${perPage}&category=${categoryId}&title=${term}`
    )
    return res.data
}

export async function gettingCategories() {
    const res = await axios.get(`${CMS_URL}/api/v1/categories`)
    return res.data
}

export async function gettingContentTypes(categoryId) {
    const res = await axios.get(`${CMS_URL}/api/v1/contentTypes?by=id&category=${categoryId}`)
    return res.data
}

export async function gettingCategoryBySlug(slug) {
    const res = await axios.get(`${CMS_URL}/api/v1/categories/?by=slug&category=${slug}`)
    return res.data
}

export async function gettingContentTypeBySlug(slug) {
    const res = await axios.get(`${CMS_URL}/api/v1/contentTypes/?by=slug&contentType=${slug}`)
    return res.data
}

export async function gettingArticlesByCategorySlug(categorySlug, perPage) {
    const res = await axios.get(
        `${CMS_URL}/api/v1/articles/?per_page=${perPage || 10}&page=1&category=${categorySlug}&by=slug`
    )
    return res.data
}

export async function gettingArticlesByContentTypeId(contentTypeId, perPage) {
    const res = await axios.get(
        `${CMS_URL}/api/v1/articles/?per_page=${perPage || 10}&page=1&contentType=${contentTypeId}&by=id`
    )
    return res.data
}

export async function gettingEditorsChoiceArticles(categoryId) {
    const res = await axios.get(`${CMS_URL}/api/v1/editorsChoice/${categoryId}`)
    return res.data
}

// export async function getAAAJPost(){
//     const res = await axios.get(`${CMS_DOMAIN}/api/v1/aaaj`)
//     return res.data
// }

// export async function getPostsByCategory(category){
//     const res = await axios.get(`${CMS_DOMAIN}/api/v1/articles/?per_page=3&page=1&category=${category}&by=slug`)
//     return res.data
// }

// export async function getAllPosts(){
//     const res = await axios.get(`${CMS_DOMAIN}/api/v1/articles`)
//     return res.data
// }

// export async function getPostByID(id){
//     const res = await axios.get(`${CMS_DOMAIN}/api/v1/article/${id}`)
//     return res.data
// }

export async function getPreviewPostById(id) {
    const data = await fetchAPI(
        `
            query PostBySlug($id: ID!) {
                post: getArticles(_id: $id) {
                    title,
                    _createdAt,
                    _id,
                    contentType{
                        title
                        slug
                    },
                    mokaf7a{
                        criminal
                        crime
                        seizureReport
                        severity
                        rehabilitation
                    }
                    fe9am{
                        sourceName,
                        sourceCountry,
                        alhudoodComment
                        logo{
                            path
                        }
                        before{
                            date
                            title
                            excerpt
                        }
                        after{
                            date
                            title
                            excerpt
                        }
                    },
                    aaaj{
                        type
                        source{
                            logo{
                                path
                            }
                            country
                            title
                        }
                    },
                    profile{
                        name
                        bio
                        nicknames{
                            nickname
                        }
                        occupations{
                            occupation
                        }
                        birth{
                            birthDate
                            birthPlace
                            notes
                        }
                        info{
                            infoTitle
                            infoContent
                        }
                        achievements{
                            achievement
                        }
                        quotes{
                            quote
                        }
                    }
                    middleEastHero{
                        heroImage{
                            path
                        }
                        heroReason
                        reasonDate
                    }
                    dictionary{
                        definition
                        icon{
                            path
                        }
                        letter{
                            letterArabic
                            slug
                            mainSvg
                            indexSvg
                        }
                    }
                    streetSounds{
                        name
                        image{
                            path
                        }
                        content
                    }
                    country{
                        countryBio
                        countryMedia{
                            map{
                                path
                            }
                        }
                        countryInfo{
                            info{
                                infoTitle
                                infoContent
                            }
                        }
                        aboutCountry{
                            about{
                                aboutTitle
                                aboutContent
                            }
                        }
                    }
                    content,
                    featuredImage{
                        path
                    }
                    category{
                        title
                        slug
                    },
                    correspondent{
                        name
                        jobDescription,
                        shortBio,
                        hasRecord
                        recordUrl
                        image{
                            path
                        }
                    },
                    tags{
                        title,
                        slug
                    },
                    emojis{
                        alreadyReacted,
                        cry,
                        fire,
                        laugh,
                        poop
                    },
                    bookmarked,
                    relatedArticles{
                        title
                        _id
                        featuredImage{
                            path
                        }
                        category{
                            slug
                        }
                        contentType{
                            slug
                        }
                        tags{
                            _id
                            title
                            slug
                        }
                    },
                    comments{
                        name,
                        comment,
                        image{
                            path
                        }
                    }
                    files{
                        title
                        source
                        day
                        date
                        comment
                    }
                }
            }
        `,
        {
            variables: {
                id,
            },
        }
    )
    return data
}

export async function getAllTags() {
    const data = await fetchAPI(
        `
            {
                allTags: getTagsList {
                    items {
                        _id
                        title
                        slug
                    }
                }
            }
        `
    )
    return data?.allTags?.items
}

export async function getAllPostsWithId() {
    const data = await fetchAPI(
        `
            {
                allPosts: getArticlesList {
                    items {
                        _id
                        category{
                            slug
                            title
                        },
                        contentType{
                            slug
                            title
                        }
                        title,
                        featuredImage{
                            path
                        }
                        tags{
                            title
                            slug
                        }
                        _createdAt
                    }
                }
            }
        `
    )
    return data?.allPosts?.items
}

export async function getAllCategories() {
    const data = await fetchAPI(
        `
            {
                allCategories: getCategoryList {
                    items {
                        _id
                        title
                        slug
                        featuredImage{
                            path
                        }
                    }
                }
            }
        `
    )
    return data?.allCategories?.items
}

export async function getAllContentTypes() {
    const data = await fetchAPI(
        `
            {
                allContentTypes: getContentTypeList {
                    items {
                        _id
                        title
                        slug
                        category{
                            slug
                        }
                    }
                }
            }
        `
    )
    return data?.allContentTypes?.items
}

export async function getCategoryBySlug(slug) {
    const data = await fetchAPI(
        `
            {
                category: getCategoryList(where: {slug: {match: "${slug}"}}) {
                    items {
                        title
                        _id
                        slug
                        featuredImage {
                            path
                        }
                        excerpt
                
                    }
                }
            }
        `,
        {
            variables: {
                slug,
            },
        }
    )
    return data?.category?.items[0]
}

export async function getTagData(slug) {
    const data = await fetchAPI(
        `
            {
                tag: getTagsList(where: {slug: {match: "${slug}"}}) {
                    items {
                        title
                        _id
                        slug
                    }
                }
            }
        `,
        {
            variables: {
                slug,
            },
        }
    )
    return data
}

export async function getContentTypeBySlug(slug) {
    const data = await fetchAPI(
        `
            {
                contentType: getContentTypeList(where: {slug: {match: "${slug}"}}) {
                    items {
                        title
                        _id
                        slug
                        category{
                            slug
                        }
                    }
                }
            }
        `,
        {
            variables: {
                slug,
            },
        }
    )
    return data?.contentType?.items[0]
}

export async function getEditorChoices() {
    const data = await fetchAPI(
        `
            {
                editorChoices: geteditorsChoices{
                    articles{
                        title
                        excerpt
                        featuredImage{
                            path
                        }
                        category{
                            slug
                        }
                        contentType{
                            slug
                        }
                        _id
                    }
                }
            }
        `
    )
    return data?.editorChoices?.articles
}

export async function getPolls() {
    const data = await fetchAPI(
        `
            {
                polls: getpollList{
                    items{
                        question
                        options{
                            option
                            votes
                        }
                    }
                }
            }
        `
    )
    return data?.polls?.items[0]
}

export async function getDorar() {
    const data = await fetchAPI(
        `
            {
                dorar: getdorarList {
                    items {
                        content
                        post {
                            title
                            _id
                            category {
                                slug
                            }
                            contentType {
                                slug
                            }
                        }
                        date
                    }
                }
            }
        `
    )
    return data?.dorar?.items.slice(0, 3)
}

export async function getCategoryContentTypes(category) {
    const data = await fetchAPI(
        `
            query{
                contentTypes: getContentTypeList(
                    where: {
                        category :{
                            slug :{
                                match : "${category}"
                            }
                        }
                    }
                ){
                    items {
                        _id
                        title
                        slug
                    }
                }
            }
        `,
        {
            variables: {
                category,
            },
        }
    )
    return data.contentTypes.items
}

export async function getArticlesByCategory(category) {
    const data = await fetchAPI(
        `
            query{
                articles: getArticlesList(
                    where: {
                        category :{
                            slug :{
                                match : "${category}"
                            }
                        }
                    }
                ){
                    items {
                        title
                        _createdAt
                        featuredImage {
                            path
                        }
                        comic{
                            images{
                                asset{
                                    path
                                }
                            }
                        }
                        aaaj{
                            type
                            source{
                                logo{
                                    path
                                }
                                country
                                title
                            }
                        }
                        excerpt
                        caricatureAuthor
                        dictionary{
                            icon{
                                path
                            }
                            letter{
                                letterArabic
                                slug
                            }
                        }
                        country {
                            countryBio
                            countryMedia {
                                flag {
                                    path
                                }
                                map{
                                    path
                                }
                            }
                            countryInfo {
                                info {
                                    infoTitle
                                    infoContent
                                }
                            }
                            aboutCountry {
                                about {
                                    aboutTitle
                                    aboutContent
                                }
                            }
                        }
                        
                        category {
                            title
                            slug
                        }
                        contentType {
                            slug
                            title
                        }
                        tags {
                            title
                            slug
                        }
                        _id
                    }
                }
            }
        `,
        {
            variables: {
                category,
            },
        }
    )
    return data.articles.items
}

export async function searchArticles(terms, size, from) {
    const data = await fetchAPI(
        `
            query{
                articles: getArticlesList(
                    terms: "${terms}" size: ${size} from: ${from}
                ){
                    total
                    items {
                        _id
                        title
                        featuredImage{
                            path
                        }
                        contentType{
                            slug
                            title
                        }
                        category{
                            title,
                            slug
                        }
                        _createdAt
                        tags{
                            title
                            slug
                        }
                    }
                }
            }
        `
    )
    return data.articles
}

export async function getArticlesByContentType(contentType) {
    const data = await fetchAPI(
        `
            query{
                articles: getArticlesList(
                    where: {
                        contentType :{
                            slug :{
                                match : "${contentType}"
                            }
                        }
                    }
                ){
                    items {
                        title
                        featuredImage {
                            path
                        }
                        excerpt
                        category {
                            title
                            slug
                        }
                        contentType {
                            slug
                            title
                        }
                        tags {
                            title
                            slug
                        }
                        _id
                    }
                }
            }
        `
    )
    return data.articles.items
}

export async function getArticlesByTag(tag, size, from) {
    const data = await fetchAPI(
        `
            query{
                articles: getArticlesList(
                    size: ${size}
                    from: ${from}
                    where: {
                        tags :{
                            slug :{
                                match : "${tag}"
                            }
                        }
                    }
                ){
                    total
                    items {
                        title
                        featuredImage {
                            path
                        }
                        excerpt
                        caricatureAuthor
                        dictionary{
                            icon{
                                path
                            }
                            letter{
                                letterArabic
                                slug
                            }
                        }
                        country {
                            countryBio
                            countryMedia {
                                flag {
                                    path
                                }
                                map{
                                    path
                                }
                            }
                            countryInfo {
                                info {
                                    infoTitle
                                    infoContent
                                }
                            }
                            aboutCountry {
                                about {
                                    aboutTitle
                                    aboutContent
                                }
                            }
                        }
                        category {
                            title
                            slug
                        }
                        contentType {
                            slug
                            title
                        }
                        tags {
                            title
                            slug
                        }
                        _id
                    }
                }
            }
        `
    )
    return data.articles
}

export async function getDictionaryPosts() {
    const data = await fetchAPI(
        `
            query{
                posts: getArticlesList(
                    where: {
                        contentType :{
                            slug :{
                                match : "${contentTypes.dictionary.slug}"
                            }
                        }
                    }
                ){
                    items {
                        title
                        _id
                        category{
                            slug
                        }
                        contentType{
                            slug
                        }
                        dictionary{
                            icon{
                                path
                            }
                            definition
                            letter{
                                indexSvg
                                mainSvg
                                slug
                            }
                        }
                    }
                }
            }
        `
    )
    return data.posts.items
}

export async function getAllLetters() {
    const data = await fetchAPI(
        `
            {
                letters: getletterList {
                    items {
                        slug
                        indexSvg
                        _references {
                            total
                        }
                    }
                }
            }
        `
    )
    return data.letters.items
}

export async function getIdsByContentType(contentType) {
    const data = await fetchAPI(
        `
            query{
                ids: getArticlesList(
                    where: {
                        contentType :{
                            slug :{
                                match : "${contentType}"
                            }
                        }
                    }
                ){
                    items {
                        _id
                    }
                }
            }
        `
    )
    return data.ids.items
}

export async function getAllPostsForHome(preview) {
    const data = await fetchAPI(
        `
        query AllPosts($onlyEnabled: Boolean) {
            allArticles: getArticlesList(sort: { field: "date", order: "desc" }, size: 20, onlyEnabled: $onlyEnabled) {
                items {
                    _id,
                    title,
                    featuredImage{
                        path
                    },
                    slug,
                    category{
                        title,
                        slug
                    },
                    contentType{
                        title,
                        slug
                    },
                    tags{
                        title,
                        slug,
                        _id
                    }
                    
                }
            }
        }
        
    `,
        {
            variables: {
                onlyEnabled: true,
                preview,
            },
        }
    )
    return data?.allArticles?.items
}
