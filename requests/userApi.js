import axios from "axios"
import { useRouter } from "next/router"
import { LOCAL_CMS_DOMAIN } from "../lib/constants"

axios.defaults.withCredentials = true

export const login = async ({ email, password, remember }) => {
    const temp = axios.get(`${LOCAL_CMS_DOMAIN}/sanctum/csrf-cookie`).then((res) => {
        const temp2 = axios
            .post(`${LOCAL_CMS_DOMAIN}/api/v1/login`, {
                email,
                password,
                remember,
            })
            .then((response) => {
                return response.data
            })
        return temp2
    })
    return temp
}

export const signup = async ({ username, email, password }) => {
    try {
        const res = await axios.post(`${LOCAL_CMS_DOMAIN}/api/v1/signup`, {
            username,
            email,
            password,
        })
        return res.data
    } catch (error) {
        return error
    }
}

export const forgetPasswordReset = async ({ email, password, passwordConfirm, token }) => {
    try {
        const res = await axios.post(`${LOCAL_CMS_DOMAIN}/api/v1/reset-password`, {
            email,
            password,
            password_confirmation: passwordConfirm,
            token,
        })
        return res.data
    } catch (error) {
        return error
    }
}

export const forgetPassword = async ({ email }) => {
    try {
        const res = await axios.post(`${LOCAL_CMS_DOMAIN}/api/v1/forgot-password`, {
            email,
        })
        return res.data
    } catch (error) {
        return error
    }
}

export const resendActivationEmail = async ({ email }) => {
    try {
        const res = await axios.post(`${LOCAL_CMS_DOMAIN}/api/email/verification-notification`, {
            email,
        })
        return res.data
    } catch (error) {
        return error
    }
}

export const checkEmail = async ({ email }) => {
    try {
        const res = await axios.post(`${LOCAL_CMS_DOMAIN}/api/v1/checkEmail`, {
            email,
        })
        return res.data
    } catch (error) {
        return error
    }
}

export const checkUsername = async ({ username }) => {
    try {
        const res = await axios.post(`${LOCAL_CMS_DOMAIN}/api/v1/checkUsername`, {
            username,
        })
        return res.data
    } catch (error) {
        return error
    }
}

export const getUser = async () => {
    try {
        let res = await axios.get(`${LOCAL_CMS_DOMAIN}/api/v1/islogged`)
        console.log("islogged", res)
        return res.data
    } catch (error) {
        console.log(error)
        throw error
    }
}

export const logout = async () => {
    try {
        let res = await axios.get(`${LOCAL_CMS_DOMAIN}/api/v1/logout`)
        console.log(res)
    } catch (error) {
        console.log(error)
    }
}
