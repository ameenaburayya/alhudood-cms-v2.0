/* eslint-disable import/prefer-default-export */
import axios from "axios"
import { CMS_DOMAIN, CMS_URL, contentTypes, LOCAL_CMS_DOMAIN } from "../lib/constants"

axios.defaults.withCredentials = true

export async function gettingProfile(infoType) {
    const res = await axios.get(`${LOCAL_CMS_DOMAIN}/api/v1/profile/${infoType}`)
    return res.data
}

export async function editProfileInfo(first_name, last_name, username, email, image) {
    const response = await axios
        .post(`${LOCAL_CMS_DOMAIN}/api/v1/profile/edit_user`, {
            first_name,
            last_name,
            username,
            email,
            image,
        })
        .then((res) => {
            console.log("edit profile", res)
            return res
        })
    return response.data
}
