import { createContext } from 'react';

const WebsiteContext = createContext();

export default WebsiteContext;