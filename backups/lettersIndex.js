import React from 'react'
import BookIcon from './icons/bookIcon'
import {arabic_letters} from '../lib/letters'
import DictionaryContext from '../contexts/dictionaryContext'
import {getAllLetters} from '../requests/contentApi'
import styles from '../scss/components/lettersIndex.module.scss'




export default class LettersIndex extends React.Component {

    static contextType = DictionaryContext

    constructor(props){
        super(props)
        this.handleIndexClick = this.handleIndexClick.bind(this)
        this.handleLettersClick = this.handleLettersClick.bind(this)
        this.getAllLetters = this.getAllLetters.bind(this)
        this.letters_inner = React.createRef()
        this.letters= React.createRef()
        arabic_letters.forEach((letter, index) => {
            this[`${letter.slug}_ref`] = React.createRef()
        });
        this.state = {
            opened: false,
            filtered: false,
            filteredLetter: null,
            parent_width: 0,
        }
    }

    handleIndexClick(){
        this.setState({opened: !this.state.opened})
        if (!this.state.opened && this.state.filtered){
            setTimeout(() => {
                const letter_position = this[`${this.state.filteredLetter}_ref`].current.offsetLeft
                this.letters.current.scrollLeft = letter_position - ( this.state.parent_width / 2 ) + 40
            }, 200)
        }
        
    }

    handleLettersClick(){
        if(!this.state.opened){
            this.setState({opened: !this.state.opened})
        }
    }

    handleLetterClick(slug, index){
        //console.log(slug)
        var letter_position
        var parent_width = this.letters.current.getBoundingClientRect().width

        const {handleFilterDictionary, handleUnFilterDictionary} = this.context;

        if(this.state.filtered && this.state.filteredLetter == slug){
            this.setState({filteredLetter: null, filtered: false})
            handleUnFilterDictionary()
        }
        else if(this.state.filtered){
            //console.log(this[`${index}_ref`])
            letter_position = this[`${slug}_ref`].current.offsetLeft
            handleFilterDictionary(slug)
            this.setState({filteredLetter: slug})
        }
        else if(!this.state.filtered && this.state.opened){

            handleFilterDictionary(slug)
            
            letter_position = this[`${slug}_ref`].current.offsetLeft
            this.setState({filtered: true, filteredLetter: slug})
        }   
        
        this.letters.current.scrollLeft = letter_position - ( parent_width / 2 ) + 40
    }

    async getAllLetters(){
        getAllLetters().then(
            res => {
                // console.log(res)
                this.setState({
                    letters: res
                })
            }
        )
    }


    componentDidMount(){
        this.getAllLetters()
        this.setState({parent_width: this.letters.current.getBoundingClientRect().width})
    }


    render(){
        //console.log('test context', filterdictionary, dictionaryPosts)
        return (
            <div className={`${styles['letters-index']} ${this.state.opened ? styles['opened'] : ''} ${this.state.filtered ? styles['filtered'] : ''} ${this.props.className ? this.props.className : ''}`}>
                <div onClick={this.handleIndexClick} className={styles['icon']}>
                    <BookIcon opened={styles['opened']} closed={styles['closed']} />
                </div>
                <div ref={this.letters} className={styles['letters']}>
                    <div ref={this.letters_inner} onClick={this.handleLettersClick} className={styles['letters-inner']}>
                        {this.state.letters?.map((letter, index) => 
                            <div 
                                key={index} 
                                ref={this[`${letter.slug}_ref`]} 
                                className={`${styles['letter']} ${this.state.filteredLetter == letter.slug ? styles['active'] : ''} ${letter._references.total ? styles['alive'] : styles['alive']}`} 
                                onClick={() => this.handleLetterClick(letter.slug, index)} 
                                dangerouslySetInnerHTML={{ __html: letter.indexSvg }} 
                            />
                        )}
                    </div>
                </div>
            </div>
        )
    }
}
