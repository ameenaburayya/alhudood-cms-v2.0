import React from 'react'
import Dictionary from '../../../components/layouts/dictionary'
import FlipPage from 'react-flip-page'
import {getIdsByContentType, getPreviewPostById} from '../../../lib/api'
import { useRouter } from 'next/router'
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/swiper-bundle.css';
import ProgressRing from '../../../components/progressRing'
import { contentTypes } from '../../../lib/constants'
import styles from '../../../scss/templates/post/alhudoodpedia/dictionaryTemplate.module.scss'



var target_id, index, before_ids, after_ids

var temp_post_data = []


function WordCount(str) { 
    return str.split(" ").length;
}

var temp, time


/// notes for me: change from the plugin autoplay to custom auto play (settimeinterval)

export default class dictionaryTemplate extends React.Component {

    constructor(props){
        super(props)
        this.ids = this.getIds()
        this.flipPage = React.createRef()
        this.swiper = React.createRef()
        this.handleChange = this.handleChange.bind(this)
        this.getPreviousArticleWithId = this.getPreviousArticleWithId.bind(this)
        this.updateWindowWidth = this.updateWindowWidth.bind(this)
        this.handlePause = this.handlePause.bind(this)
        this.handleNext = this.handleNext.bind(this)
        this.getReadingTime = this.getReadingTime.bind(this)
        this.handleTimerStart = this.handleTimerStart.bind(this)
        this.getIds = this.getIds.bind(this)
        this.state = {
            isFetching: false,
            postsData: null,
            before_ids: null,
            after_ids: null,
            current_id: null,
            current_id_index: null,
            timer: 0,
            isActive: false,
            isPaused: false,
            interval: 8000
        }
    }

    


    handleChange(pageIndex){
        const dictionary_id = this.state.ids[pageIndex]
        //this.props.router.query._id = dictionary_id
        // this.props.router.push({
        //     pathname: "/[category]/[contentType]/[id]",
        //     query: {
        //         category: this.props.router.query.category,
        //         contentType: this.props.router.query.contentType, 
        //         id: dictionary_id._id
        //     }
        // }, { shallow: true })
        //console.log(this.props.router, dictionary_id)
        if(!this.state.postsData[pageIndex - 1] && pageIndex) {
            this.getPreviousArticleWithId(this.state.ids[pageIndex - 1]._id , pageIndex - 1)
        }  
        else if (this.state.ids.length - 1 > pageIndex && !this.state.postsData[pageIndex + 1]){
            this.getNextArticleWithId(this.state.ids[pageIndex + 1]._id , pageIndex + 1)
        }
    }

    getReadingTime(words){
        const reading_time = (WordCount(words) / 200) * 60 * 1000
        return reading_time
    }


    handleTimerStart(){
        this.setState({isActive: true})
        temp = setInterval(() => {
            //console.log('siper', this.swiper.current.swiper)
            const active_index = this.swiper.current.swiper ? this.swiper.current.swiper.activeIndex : null
            this.setState({isActive: false, interval: (this.state.postsData[active_index + 1] ? ((WordCount(this.state.postsData[active_index + 1].dictionary.definition) / 200) * 60 * 1000 ) : 8000)})
            this.swiper.current?.swiper?.slideNext()
            this.setState({isActive: true, timer: 0})
        }, (8000 - this.state.timer))

        time = setInterval(() => {
            //this.setState({timer: this.state.timer + 100})
            this.setState({timer: this.state.timer + 100})
            //console.log(timer)
        }, 100)
    }


    handlePause(){
        this.setState({isPaused: !this.state.isPaused})
        if(!this.state.isPaused){
            clearInterval(temp)
            clearInterval(time)
        }
        else{
            this.handleTimerStart()
        }
    }

    handleNext = () => {
        this.flipPage.gotoNextPage();
    }
    

    async getPreviousArticleWithId(id, index){
        getPreviewPostById(id).then(
            res => {
                temp_post_data[index] = res.post
                this.setState({
                    postsData: temp_post_data
                })
            }
        )
    }

    async getNextArticleWithId(id, index){
        getPreviewPostById(id).then(
            res => {
                temp_post_data[index] = res.post
                this.setState({
                    postsData: temp_post_data
                })
            }
        )
    }


    async getIds() {
        //this.state['isFetching'] = true
        getIdsByContentType(contentTypes.dictionary.slug)
            .then(
                res => {
                    target_id = this.filter_ids(res, this.state.current_id);
                    index = this.getIndex(res, target_id[0]._id)
                    before_ids = res.slice(0, (index))
                    after_ids = res.slice(index + 1, res.length)
                    temp_post_data[index] = this.props.post
                
                    var before_dictionary_id = index ? res[index - 1]._id : null
                    var after_dictionary_id = res[index + 1] ? res[index + 1]._id : null
                    before_dictionary_id ? getPreviewPostById(before_dictionary_id).then(
                        res => {
                            temp_post_data[index - 1] = res.post
                            this.setState({
                                postsData: temp_post_data
                            })
                        }
                    ) : null

                    after_dictionary_id ? getPreviewPostById(after_dictionary_id).then(
                        res => {
                            temp_post_data[index + 1] = res.post
                            this.setState({
                                postsData: temp_post_data
                            })
                        }
                    ) : null
                    

                    this.setState({
                        before_ids: before_ids, 
                        after_ids: after_ids, 
                        current_id_index: index,
                        ids: res,
                        isFetching: false, 
                        
                    })
                    //console.log(this.state.postsData)
                    //console.log('state',this.state.postsData[2])
                    
                    
                }
            )
            .catch(
                e => {
                    this.setState({...this.state, isFetching: "error", postsData: null});
                }
            );
    }

    filter_ids(array, value) {
        return array.filter(function(e) {
            return e._id == value;
        });
    }

    getIndex(array, target_id) {

        var i = 0
        var ii = array.length;
        for(i; i < ii; i++) if(array[i]._id === target_id) break;
        
        return i;
    }
    


    updateWindowWidth() {
        this.setState({ width: window.innerWidth});
        let vh = window.innerHeight * 0.01;
        document.documentElement.style.setProperty('--dictionaryHeight', `${vh}px`);
    }


    componentDidMount(){
        this.getIds()
        var reading_time = (WordCount(this.props.post.dictionary.definition) / 200) * 60 * 1000
        this.setState({current_id: this.props.post._id, interval: reading_time})
        this.updateWindowWidth();
        window.addEventListener('resize', this.updateWindowWidth);
        //this.handleTimerStart()
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowWidth);
    }

    
    
    render(){
        console.log('mo3jm',this.props.post)
        return (
            <>
                {this.state.width < 1024 
                ?  <>
                        {this.state.postsData && 
                            <FlipPage 
                            ref={(component) => { this.flipPage = component; }}
                            responsive 
                            startAt={this.state.current_id_index}
                            noShadow={true}
                            className={styles['flipbox']} 
                            onPageChange={(pageIndex, direction) => this.handleChange(pageIndex)}
                            >
                                {this.state.before_ids && this.state.before_ids.map((item, index) => { return <Dictionary handleNext={this.handleNext} key={index} post={this.state.postsData[index] } />})}
                                <Dictionary handleNext={this.handleNext} post={this.state.postsData[this.state.current_id_index]}/>
                                {this.state.after_ids && this.state.after_ids.map((item, index) => { return <Dictionary handleNext={this.handleNext} key={index} post={this.state.postsData[index + 1 + this.state.current_id_index] } />})}
                            </FlipPage>
                        || "loading"}
                </>
                : <>
                    {this.state.postsData && 
                        <Swiper
                        spaceBetween={50}
                        effect={"Overflow"}
                        className={styles['slider']}
                        ref={this.swiper}
                        onInit={() => this.handleTimerStart()}
                        onSlideChange={(swiper) => this.handleChange(swiper.activeIndex)}
                        initialSlide={this.state.current_id_index}
                        slidesPerView={1}
                        >
                            {this.state.before_ids && this.state.before_ids.map((item, index) => { return <SwiperSlide key={index}><Dictionary handleNext={this.handleNext} key={index} post={this.state.postsData[index] } /></SwiperSlide> })}
                            <SwiperSlide><Dictionary handleNext={this.handleNext} post={this.state.postsData[this.state.current_id_index]}/></SwiperSlide>
                            {this.state.after_ids && this.state.after_ids.map((item, index) => { return <SwiperSlide key={index}><Dictionary handleNext={this.handleNext} key={index} post={this.state.postsData[index + 1 + this.state.current_id_index] } /></SwiperSlide>})}
                            {this.state.isActive ? <ProgressRing isPaused={this.state.isPaused} interval={this.state.interval}  /> : null }
                            <div className="test"></div>
                            
                            <button onClick={() => this.handlePause()}>pause</button>
                        </Swiper>
                    || "loading"}
                </>
                }
            </>
        )
    }
}



