import React, { Component } from 'react'
import { Swiper, SwiperSlide } from 'swiper/react';
import SwiperCore, {EffectFade} from 'swiper';
import 'swiper/swiper-bundle.css';
import styles from '../scss/pages/profile.module.scss'
import Header from '../includes/header/header'
import ProfileAccountInfo from '../components/profile/profileAccountInfo'
import ProfileNavigation from '../components/profile/profileNavigation';
import ProfileBookmarks from '../components/profile/profileBookmarks';
import ProfileCommunity from '../components/profile/profileCommunity'
import { getArticlesByCategory } from '../lib/api'
import { categories } from '../lib/constants'
import Snackbar from '@material-ui/core/Snackbar';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import snackbarStyles from '../scss/components/snackbar.module.scss'
import { profileTabs } from '../lib/constants'
import ProfileUser from '../components/profile/profileUser'
import {newsletters, profile, comments} from '../lib/fakeData'
import ProfileNewsletters from '../components/profile/profileNewsletters';
import ProfileEmptySection from '../components/profile/profileEmptySection';
import CommentsIcon from '../components/icons/commentsIcon';
import NewsletterIcon from '../components/icons/newsletterIcon'
import BookmarkIcon from '../components/icons/bookmarkIcon';
import ProfileImage from '../components/profile/profileImage';
import ProfileDelete from '../components/profile/profileDelete';
import PageLayout from '../components/layouts/pageLayout';


SwiperCore.use([EffectFade]);

export default class Profile extends Component {

    constructor(props){
        super(props)
        this.changeTab = this.changeTab.bind(this)
        this.setSwiper = this.setSwiper.bind(this)
        this.profileNavigation = React.createRef()
        this.handleSnackbarOpen = this.handleSnackbarOpen.bind(this)
        this.handleSnackbarClose = this.handleSnackbarClose.bind(this)
        this.handleUndo = this.handleUndo.bind(this)
        this.handleTabResize = this.handleTabResize.bind(this)
        this.handleInfoEdit = this.handleInfoEdit.bind(this)
        this.updateWindowWidth = this.updateWindowWidth.bind(this)
        this.handleImageSelect = this.handleImageSelect.bind(this)
        this.onCropChange = this.onCropChange.bind(this)
        this.profileBookmarks = React.createRef()
        profileTabs.forEach((tab, index) => {
            console.log('testing', index, tab)
            this[`slide_${index}`] = React.createRef()
        })
        this.state = {
            activeTab: 0,
            swiper: null,
            snackbarOpen: false,
            snackbarId: null,
            sliderHeight: null,
            windowWidth: null,
            infoEdit: false,
            passwordEdit: false,
            profileImage: null,
            crop: { x: 0, y: 0 },
            zoom: 1,
            aspect: 1/ 1,
            imageCrop: false
        }
    }

    componentDidMount(){
        this.handleTabResize(0)
        this.updateWindowWidth();
        window.addEventListener('resize', this.updateWindowWidth);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowWidth);
    }

    updateWindowWidth() {
        this.setState({
            windowWidth: window.innerWidth
        })
    }


    handleTabResize(index){
        // console.log('index', index)
        setTimeout(() => {
            this.state.swiper?.updateAutoHeight(10)
        }, 100)

    }

    onCropChange = (crop) => {
        console.log(crop)
        this.setState({ crop })
    }

    changeTab(index){
        if (!this.state.infoEdit && !this.state.passwordEdit){
            this.setState({
                activeTab: index
            })
            this.state.swiper?.slideToLoop(index);
        }
    }

    changeSlide(slider){
        console.log(slider)
        this.profileNavigation.current.changeNavTab(slider.realIndex)
        this.setState({
            activeTab: slider.realIndex
        })
        this.handleTabResize(slider.realIndex)
    }

    setSwiper(swiper){
        this.setState({
            swiper
        })
    }

    handleInfoEdit(type ,state){
        
        if(type == 'info'){
            this.setState({
                infoEdit: state
            })
            this.state.swiper.allowTouchMove = !state && !this.state.passwordEdit
        }
        else if (type == 'password'){
            this.setState({
                passwordEdit: state
            })
            this.state.swiper.allowTouchMove = !this.state.infoEdit && !state
        }
        
    }

    handleSnackbarOpen(id){
        this.setState({
            snackbarOpen: true,
            snackbarId: id
        })
    }

    handleSnackbarClose(event, reason){
        if (reason === 'clickaway') {
            return;
        }
        this.setState({
            snackbarOpen: false,
            snackbarId: null
        })
    }

    handleUndo(){
        this.profileBookmarks.current.handleUndo()
        this.setState({snackbarOpen: false})
    }

    handleImageSelect(e){
        console.log(e.target.files[0])
        this.setState({
            profileImage: e.target.files[0],
            imageCrop: true
        })
    }


    handleImageUpload(){
        const fd = new FormData()
        fd.append('image', this.state.profileImage, this.state.profileImage.name)
    }

    render() {

        // const profile = this.props.profile
        const {bookmarkPosts} = this.props
        return (
            <>
                <Header />
                <PageLayout pageTitle="الصفحة الشخصية">
                    <div className={styles['profile']}>
                        <div className={styles['profile-header']}>
                            <ProfileImage windowWidth={this.state.windowWidth >= 1024 ? 750 : this.state.windowWidth} width={this.state.windowWidth > 500 ? 500 : this.state.windowWidth } image={profile.userInfo.image} edit={this.state.infoEdit} />
                            {this.state.windowWidth < 1024 && <ProfileUser className={styles['profile-user']} />}
                            <ProfileNavigation className={styles['profile-tabs']} ref={this.profileNavigation} disabled={this.state.infoEdit || this.state.passwordEdit} activeTab={this.state.activeTab} changeTab={this.changeTab} />
                        </div>
                        <div className={styles['profile-sections']}>
                            {this.state.windowWidth >= 1024 && <ProfileUser className={styles['profile-user']} />}
                            {this.state.windowWidth && <Swiper
                                spaceBetween={0}
                                effect={this.state.windowWidth >= 1024 ? "fade" : "Overflow"}
                                slidesPerView={1}
                                onSlideChange={(swiper) => this.changeSlide(swiper)}
                                // onSlideChange={(swiper) => console.log(swiper)}
                                onSwiper={(swiper) => this.setSwiper(swiper)}
                                // autoHeight={true}
                                className={styles['profile-slider']}
                                allowTouchMove={this.state.windowWidth >= 1024 ? false : true}
                                autoHeight={true}
                                loop
                                >
                                <SwiperSlide ref={this[`slide_0`]} className={styles['profile-slider-content']}>
                                    <ProfileAccountInfo handleInfoEdit={this.handleInfoEdit} checkSize={() => this.handleTabResize(0)} />
                                </SwiperSlide >
                                <SwiperSlide ref={this[`slide_1`]} className={styles['profile-slider-content']}>
                                    العضوية
                                </SwiperSlide>
                                <SwiperSlide ref={this[`slide_2`]} className={styles['profile-slider-content']}>
                                    {comments && comments.length > 0 
                                    ? <ProfileCommunity comments={comments} />
                                    : <ProfileEmptySection type="community" icon={<CommentsIcon />} href="https://community.alhudood.net" text="نشاطك على <br /> مجتمع الحدود" arrowLabel="استكشف المجتمع" />
                                    }
                                    
                                </SwiperSlide>
                                <SwiperSlide ref={this[`slide_3`]} className={styles['profile-slider-content']}>
                                    {newsletters && newsletters.length > 0
                                    ? <ProfileNewsletters newsletters={newsletters} />
                                    : <ProfileEmptySection type="newsletters" icon={<NewsletterIcon />} href="/newsletters" text="لم تشترك بعد بأيٍّ من النشرات البريدية" arrowLabel="حسّن الإنبوكس" />
                                    }
                                    
                                </SwiperSlide>
                                <SwiperSlide ref={this[`slide_4`]} className={`${styles['profile-slider-content']} ${styles['bookmarks']}`}>
                                    {bookmarkPosts && bookmarkPosts.length > 0 
                                    ? <ProfileBookmarks ref={this.profileBookmarks} snackbarOpen={this.handleSnackbarOpen} posts={bookmarkPosts} />
                                    :  <ProfileEmptySection type="bookmark" icon={<BookmarkIcon />} href="/" text="ستجد مقالاتك المفضلة هنا" arrowLabel="الصفحة الرئيسية" />
                                    }
                                </SwiperSlide>
                                <SwiperSlide ref={this[`slide_5`]} className={`${styles['profile-slider-content']} ${styles['delete-account']}`}>
                                    <ProfileDelete />
                                </SwiperSlide>
                            </Swiper>}
                        </div>
                    </div>
                </PageLayout>
                
                <Snackbar
                    key={this.state.snackbarId}
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'center',
                    }}
                    autoHideDuration={5000}
                    open={this.state.snackbarOpen}
                    onClose={this.handleSnackbarClose}
                    //onExited={handleExited}
                    message={"تم إزالة المقال من القائمة"}
                    className={snackbarStyles['snackbar']}
                    action={
                        <React.Fragment>
                            <Button color="secondary" size="small" onClick={this.handleUndo}>
                                إعادة
                            </Button>
                            <IconButton
                                aria-label="close"
                                color="inherit"
                                //className={classes.close}
                                onClick={this.handleSnackbarClose}
                            >
                                <CloseIcon />
                            </IconButton>
                        </React.Fragment>
                    }
                    
                />
        </>
        )
    }
}


export async function getStaticProps({ preview = true }) {
    const currentAffairsArticles = await getArticlesByCategory(categories.currentAffairs.slug)
    return {
        props: {
            preview,
            bookmarkPosts: currentAffairsArticles
        },
    }
}



