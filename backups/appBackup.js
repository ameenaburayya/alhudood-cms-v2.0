import '../scss/main.scss'
import '../scss/singlePost.scss' 
//import '../scss/abstracts/_variables.scss'
import React from 'react';
import App, { Container } from 'next/app';
import dictionaryContext from '../contexts/dictionaryContext'
import {getdictionaryPosts} from '../lib/api'
import WebsiteContext from '../contexts/websiteContext'
import useUser from '../data/useUser';


class Alhudood extends App {
    static async getInitialProps({ Component, ctx }) {
        let pageProps = {};
        const dictionaryPosts = await getdictionaryPosts()
        if (Component.getInitialProps) {
            pageProps = await Component.getInitialProps(ctx);
        }
    
        return { pageProps, dictionaryPosts };
    }

    constructor(props){
        super(props)
        this.filterdictionary = this.filterdictionary.bind(this)
        this.unFilterdictionary = this.unFilterdictionary.bind(this)
        this.state = {
            history: [], // keep history items in state
            dictionaryPosts: null,
            initialized: false,
            filteredLetterSlug: null,
            isLoggedIn: null
        }
    }
    

    componentDidMount() {
        const { asPath } = this.props.router;
    // lets add initial route to `history`
        this.setState(prevState => ({ history: [...prevState.history, asPath], dictionaryPosts: this.props.dictionaryPosts }));
    }

    componentDidUpdate() {
        const { history } = this.state;
        const { asPath } = this.props.router;
        console.log('app updated')

        const { mutate, loggedIn } = useUser();

        console.log('isloggedin', loggedIn)
    
    // if current route (`asPath`) does not equal
    // the latest item in the history,
    // it is changed so lets save it
        if (history[history.length - 1] !== asPath) {
            this.setState(prevState => ({ history: [...prevState.history, asPath] }));
        }
    }

    filterdictionary(letterSlug){
        function filterByLetter(item) {
            if (item.dictionary.letter.slug == letterSlug) {
                return true
            }
            return false;
        }
        const filteredPosts = this.props.dictionaryPosts.filter(filterByLetter)
        this.setState({dictionaryPosts: filteredPosts, filtered: true, initialized: true, filteredLetterSlug: letterSlug})
        // console.log('filtering',filteredPosts)
    }

    unFilterdictionary(){
        this.setState({dictionaryPosts: this.props.dictionaryPosts, filtered: false, filteredLetterSlug: null})
        // console.log('unfilter',this.state.dictionaryPosts)
    }
    
    render() {
        const { Component, pageProps } = this.props;
        // console.log('state', this.state.dictionaryPosts)
        return (
            
            // <Container>
            //     <dictionaryContext.Provider value={{filterdictionary: this.filterdictionary, unFilterdictionary:this.unFilterdictionary,  dictionaryPosts: this.state.dictionaryPosts, filteredLetterSlug: this.state.filteredLetterSlug, initialized: this.state.initialized}}>
            //         <Component history={this.state.history} {...pageProps} />
            //     </dictionaryContext.Provider>
            // </Container>

            <Container>
                <WebsiteContext.Provider value={{isLoggedIn: this.state.isLoggedIn}}>
                    <Component history={this.state.history} {...pageProps} />
                </WebsiteContext.Provider>
            </Container>

        );
    }
}
    
export default Alhudood;


export async function getInitialProps({  preview = false }) {
    
    const data = await getdictionaryPosts()
    // console.log(data)
    return {
        props: {
            preview,
            dictionaryPosts: {
                ...(data?.post || [])
            }
        },
    }
}

export async function getServerSideProps({ res, req }) {
    console.log(req.session)
    const parsedCookies = cookie.parse(req.headers.cookie);
    if (!parsedCookies['XSRF-TOKEN']){
        const csrf = await fetch(`http://localhost:8082/sanctum/csrf-cookie`)
        res.setHeader('set-cookie', csrf.headers.raw()['set-cookie']);
    }
    return { props: { } };
}
