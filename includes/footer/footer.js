import AlhudoodLogo from "../../components/logos/alhudoodLogo"
import SocialMedia from "../../components/social/socialMedia"
import { FooterMenu } from "../../lib/menu"
import MenuList from "../../components/headerComponents/menuList"
import styles from "../../scss/includes/footer.module.scss"
import Dal from "../../components/icons/dal"
import Link from "next/link"
import FooterNewsletter from "../../components/newsletters/footerNewsletter"

const e2a = (s) => s.replace(/\d/g, (d) => "٠١٢٣٤٥٦٧٨٩"[d])

export default function Footer() {
    return (
        <footer className={styles["footer"]}>
            <nav className={styles["footer-inner"]}>
                <h2 className="hidden" id="site-index-label">
                    Site Index
                </h2>
                <div
                    className={`${styles["footer-about"]} ${styles["footer-section"]}`}
                >
                    <div className={styles["inner"]}>
                        <div className={styles["alhudood-logo"]}>
                            <Link href="/">
                                <a>
                                    <AlhudoodLogo fill="white" />
                                </a>
                            </Link>
                        </div>
                        <p>
                            خسائر اللازمة ومطالبة حدة بل. الآخر الحلفاء أن غزو, إجلاء
                            وتنامت عدد مع. لقهر معركة لبلجيكا، بـ انه, ربع الأثنان
                            المقيتة في, اقتصّت المحور حدة و. هذه ما طرفاً عالمية
                            استسلام, الصين وتنامت حين ٣٠, ونتج والحزب المذابح كل جوي.
                        </p>
                        <Dal className={styles["dal"]} />
                        <FooterNewsletter className={styles["newsletter"]} />
                    </div>
                </div>
                <div
                    className={`${styles["footer-menu"]} ${styles["footer-section"]}`}
                >
                    <h3>عن الحدود</h3>
                    <MenuList menu={FooterMenu} />
                </div>
                <div
                    className={`${styles["footer-social"]} ${styles["footer-section"]}`}
                >
                    <div className={styles["inner"]}>
                        <h3>تابع الحدود</h3>
                        <SocialMedia color={"white"} />
                    </div>
                </div>
            </nav>
            <div className={styles["footer-copyright"]}>
                <p className={styles["short-bio"]}>
                    الحدود منصة إبداع عربي تقدم محتوى ساخر. مسجلة كشركة مُسَاهمة
                    مُجتمعيّة لا تهدف للربح
                </p>
                <p className={styles["copyright"]}>
                    جميع الحقوق محفوظة لشبكة الحدود{" "}
                    <span className={styles["copyright-symbol"]}>©</span>{" "}
                    <span>{e2a(`${new Date().getFullYear()}`)}</span>
                </p>
            </div>
        </footer>
    )
}
