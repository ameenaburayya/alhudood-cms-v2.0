import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import Link from "next/link"
import AlhudoodLogo from "../../components/logos/alhudoodLogo"
import HamburgerIcon from "../../components/icons/hamburgerIcon"
import SearchIcon from "../../components/icons/searchIcon"
import NavMenu from "../../components/headerComponents/navMenu"
import UserStatus from "../../components/user/userStatus"
import styles from "../../scss/includes/header.module.scss"
import HeaderSearchBar from "../../components/search/headerSearchBar"

const lineStartWidth = 90
const lineFinishWidth = 78
const lineStartTop = 62
const lineFinishTop = 32
const dalStartWidth = 60
const dalFinishWidth = 77
const dalStartTop = 8
const dalFinishTop = -1

export default function Header(props) {
    const [y, setY] = useState(60)
    const [lineWidth, setLineWidth] = useState(lineStartWidth)
    const [lineTop, setLineTop] = useState(lineStartTop)
    const [dalWidth, setDalWidth] = useState(dalStartWidth)
    const [dalTop, setDalTop] = useState(dalStartTop)
    // const [target, setTarget] = useState(0)
    const [reverse, setReverse] = useState(false)
    const [windowWidth, setWindowWidth] = useState(0)
    // const [headerHeight, setHeaderHeight] = useState(headerStartHeight)

    const handlePageScroll = () => {
        const y = window.scrollY
        if (windowWidth < 1024) {
            if (y < 0) {
                setReverse(true)
            } else {
                setReverse(false)
            }

            const width = Math.max(lineStartWidth - 0.5 * y, lineFinishWidth)
            const top = Math.max(lineStartTop - 0.5 * y, lineFinishTop)

            var logoWidth = dalStartWidth
            var logoTop = dalStartTop

            if (width <= lineFinishWidth && top <= lineFinishTop) {
                // if(!target) setTarget(y)
                logoWidth = dalStartWidth + 0.08 * y
                logoTop = dalStartTop - 0.08 * y
            }

            setLineWidth(width < lineFinishWidth ? lineFinishWidth : width)
            setLineTop(top < lineFinishTop ? lineFinishTop : top)
            setDalWidth(logoWidth > dalFinishWidth ? dalFinishWidth : logoWidth)
            setDalTop(logoTop < dalFinishTop ? dalFinishTop : logoTop)
            setY(y)
        } else {
            setY(y)
        }
    }

    const handlePageWidth = () => {
        setWindowWidth(window.innerWidth)
    }

    useEffect(() => {
        if (!props.compact) {
            handlePageScroll()
            handlePageWidth()
            window.addEventListener("scroll", handlePageScroll)
            window.addEventListener("resize", handlePageWidth)

            return () => {
                window.removeEventListener("scroll", handlePageScroll)
                window.removeEventListener("resize", handlePageWidth)
            }
        } else {
            handlePageWidth()
            window.addEventListener("resize", handlePageWidth)

            return () => {
                window.removeEventListener("resize", handlePageWidth)
            }
        }
    })

    return (
        <nav
            className={`${styles["header"]} ${props.className ? props.className : ""} ${
                y > 20 ? styles["sticky"] : ""
            } ${props.compact ? styles["compact"] : ""} ${reverse ? styles["reverse"] : ""}`}
        >
            <div className={styles["container"]}>
                <div className={styles["site-logo"]}>
                    <Link href="/">
                        <a>
                            <AlhudoodLogo
                                style={{
                                    width: `${dalWidth}px`,
                                    top: `${dalTop}px`,
                                }}
                                dalClassName={styles["dal"]}
                                className={`${styles["logo"]} ${styles["fixed"]}`}
                            />
                            <AlhudoodLogo dalClassName={styles["dal"]} className={`${styles["logo"]}`} />
                            {/* <Dal /> */}
                        </a>
                    </Link>
                </div>
                <div className={styles["list"]}>
                    <div className={styles["search"]}>
                        {/* <SearchIcon /> */}
                        <HeaderSearchBar />
                    </div>
                    <div className={styles["nav-menu"]}>
                        <NavMenu sticky={y > 20} />
                    </div>
                    <HamburgerIcon className={styles["hidden-icon"]} />
                    <UserStatus className={styles["user-status"]} />
                </div>
                <div style={{ width: `${lineWidth}%`, top: `${lineTop}px` }} className={styles["line"]}></div>
                <div className={`${styles["hidden-line"]} ${styles["line"]}`}></div>
            </div>
        </nav>
    )
}

Header.propTypes = {
    className: PropTypes.string.isRequired,
}
